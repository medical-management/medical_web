export function formatNullObject(object) {
  let newObj = { ...object };
  const keys = Object.keys(object);
  keys.map((key) => {
    if (
      newObj[key] === undefined ||
      newObj[key] === "undefined" ||
      newObj[key] === null
    )
      newObj[key] = " ";
  });
  return newObj;
}

export const findRole = (name) => {
  const role = {
    employee: "Nhân viên",
    admin: "Quản trị viên",
  };

  return role[name] || "---";
};

export const findGender = (name) => {
  const role = {
    male: "Nam",
    admin: "Nữ",
  };

  return role[name] || "---";
};

export const normalizeString = (str) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  return str;
};

export const changeNameKey = (str) => {
  str = str.replace(`"recordType":`, "recordType:");
  str = str.replace(`"id":`, "id:");
  str = str.replace(`"lastname":`, "lastname:");
  str = str.replace(`"email":`, "email:");
  str = str.replace(`"firstname":`, "firstname:");
  str = str.replace(`"phone":`, "phone:");
  str = str.replace(`"address":`, "address:");
  str = str.replace(`"gender":`, "gender:");
  str = str.replace(`"city":`, "city:");
  str = str.replace(`"province":`, "province:");
  str = str.replace(`"nation":`, "nation:");
  str = str.replace(`"cardID":`, "cardID:");
  str = str.replace(`"status":`, "status:");
  str = str.replace(`"dob":`, "dob:");
  str = str.replace(`"role":`, "role:");
  str = str.replace(`"username":`, "username:");
  str = str.replace(`"password":`, "password:");
  str = str.replace(`"inactive":`, "inactive:");
  str = str.replace(`"note":`, "note:");

  str = str.replace(`"name":`, "name:");
  str = str.replace(`"shortName":`, "shortName:");
  str = str.replace(`"sku":`, "sku:");
  str = str.replace(`"upc":`, "upc:");
  str = str.replace(`"vendor":`, "vendor:");
  str = str.replace(`"category":`, "category:");
  str = str.replace(`"description":`, "description:");
  str = str.replace(`"startDate":`, "startDate:");
  str = str.replace(`"endDate":`, "endDate:");
  str = str.replace(`"seri":`, "seri:");
  str = str.replace(`"bin":`, "bin:");
  str = str.replace(`"salePrices":`, "salePrices:");
  str = str.replace(`"purchasePrices":`, "purchasePrices:");
  str = str.replace(`"taxPrices":`, "taxPrices:");
  str = str.replace(`"tax":`, "tax:");
  str = str.replace(`"close":`, "close:");
  str = str.replace(`"createdat":`, "createdat:");
  str = str.replace(`"updatedat":`, "updatedat:");
  str = str.replace(`"bloodpressure":`, "bloodpressure:");
  str = str.replace(`"heartbeat":`, "heartbeat:");
  str = str.replace(`"refill":`, "refill:");
  str = str.replace(`"height":`, "height:");
  str = str.replace(`"weight":`, "weight:");
  str = str.replace(`"button":null`, "");
  str = str.replace(`"button":"verify"`, "");
  str = str.replace(`"button":"draft"`, "");
  str = str.replace(`"button":"del"`, "");
  str = str.replace(`"index":0`, "");
  str = str.replace(`"index": 0`, "");

  return str;
};

export const changeStringToFloatPayload = (str) => {
  str = str.replace(`"startInt`, "");
  str = str.replace(`endInt"`, "");

  return str;
};

export const findButton = (value) => {
  if (value === "draft") return "Duyệt tạm";
  if (value === "verify") return "Duyệt";
  if (value === "del") return "Xóa";
};

export const truncateText = (string, limit) => {
  return string?.length > limit && limit > 0
    ? string?.slice(0, limit) + "..."
    : string;
};

export const detectGender = (value) => {
  return value
    ? value === "male"
      ? "Nam"
      : value === "female"
      ? "Nữ"
      : ""
    : "";
};
