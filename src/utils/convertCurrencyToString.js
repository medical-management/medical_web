import {
  ReadingConfig,
  parseNumberData,
  readNumber,
} from "read-vietnamese-number";

// Step 2
const config = new ReadingConfig();
config.unit = ["Đồng"];

export const convertCurrency = (string) => {
  try {
    // Step 3
    const number = parseNumberData(config, string);

    // Step 4
    return readNumber(config, number).toUpperCase();
  } catch (e) {
    return "";
  }
};
