/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import cookie from "js-cookie";

export const get = async (name) => {
    try {
        return await cookie.get(name);
    } catch (err) {
        throw err;
    }
};

export const set = async (name, value) => {
    try {
        await cookie.set(name, value, { expires: 1 / 2 });
        return true;
    } catch (err) {
        throw err;
    }
};

export const clear = () => {
    cookie.remove("accesstoken");
    cookie.remove("refreshtoken");
};
