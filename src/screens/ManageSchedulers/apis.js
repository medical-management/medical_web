/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_SCHEDULES = async (input) => {
  let payload = [];
  for (const key in input) {
    if (input[key] === "" || input[key] == null || input[key] === undefined)
      continue;
    else if (["take", "skip"].includes(key))
      payload.push(` ${key}: ${input[key]} `);
    else payload.push(` ${key}: "${input[key]}" `);
  }

  return await graphQLClient({
    queryString: ` query {
      find_all_medical_scheduled(req: {${payload}}) {
      data {
        inactive
        createdat
        updatedat
        recordType
        id
        status
        updatedby {
          id
          firstname
          lastname

        }
        patient {
          id
          firstname
          lastname
          dob
          phone
        }
        type
        category
        date
        note
        close
      }
      take
      skip
      total
      nextPage
    }
  }`,
  });
};

export const APIS_FIND_ONE_SCHEDULE = async (input) =>
  await graphQLClient({
    queryString: `query {
      find_one_medical_scheduled(id: ${input.id}) {
        inactive
        createdat
        updatedat
        recordType
        id
        status
        updatedby {
          id
          firstname
          lastname

        }
        patient {
          id
          firstname
          lastname
          dob
          phone
        }
        shift
        department
        type
        category
        date
        note
        close
      }
    }`,
  });

export const APIS_SAVE_SCHEDULE = async ({ input }) =>
  await graphQLClient({
    queryString: ` mutation {
      save_medical_scheduled(input: { 
            recordType: "medical_scheduled",
            ${input.id === "" ? "" : `id: ${input.id},`}
            status: "${input.status}",
            patient: ${input.patient},
            type: "${input.type}",
            shift: "${input.shift}",
            department: "${input.department || ""}",
            category: "${input.category}",
            date: "${input.date}",
            note: "${input.note}",
          }) {
              id
  
        }
    }`,
  });

export const APIS_SAVE_PATIENT = async ({ input }) =>
  await graphQLClient({
    queryString: ` 
        mutation {
          save_patient(input: { 
            recordType: "${input?.recordType || "patient"}",
            firstname: "${input?.firstname || ""}",
            inactive: false,
            lastname:  "${input?.lastname || ""}",
            gender:  "${input?.gender || ""}",
            phone:  "${input?.phone || ""}",
            dob:  "${input?.dob || ""}",
        }) {
          id
          dob
          firstname
          lastname
          inactive
        }
    }`,
  });

export const API_FIND_PERIOD_SCHEDULE = async (input) =>
  await graphQLClient({
    queryString: `query {
      group_medical_scheduled(req: { 
        startDate: "${input.startDate}", 
        endDate: "${input.endDate}", 
  }) {
        data {
          inactive
          id
          status
          type
          category
          date
          shift
          note
          close
          index
          department
          patient {
            id
            firstname
            lastname
            dob
            phone
          }
        }
        date
      }
    }`,
  });

export const APIS_DRAG_CARD = async ({ input }) =>
  await graphQLClient({
    queryString: ` mutation {
      save_medical_scheduled(input: { 
            recordType: "medical_scheduled",
            id: ${input.id},
            patient: ${input.patient},
            date: "${input.date}",
            shift: "${input.shift}",
            department: "${input.department}",
            note: "${input.note}",
            type: "${input.type}",
            category: "${input.category}",
          }) {
              id
              date
  
        }
    }`,
  });

export const APIS_DRAG_CARD_INSIDE = async ({ input }) => {
  let inputs = [];
  for (const item of input) {
    let dataConvert = [];
    for (const key in item) {
      dataConvert.push(` ${key}: ${item[key]} `);
    }
    inputs.push(`{${dataConvert.join()}}`);
  }

  return await graphQLClient({
    queryString: ` mutation {
      change_index_medical_scheduled(input:{
        shceduleds:[${inputs.join()}]
      }){
        id
        index
      }
    }`,
  });
};
