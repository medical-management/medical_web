/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import ConfirmModal from "../../../components/ConfirmModal";
import * as actions from "../actions";
import { APIS_SAVE_PATIENT } from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";
import { FormInput } from "../../../components/FormInputCustom";
import { useToasts } from "react-toast-notifications";

export default function QuickFormPatient(props) {
  return (
    <React.Fragment>
      <div>
        <SubmitValidationForm data={props} />
      </div>
    </React.Fragment>
  );
}

let SubmitValidationForm = (props) => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const dispatch = useDispatch();

  const [payload, setPayload] = useState(null);

  const { addToast } = useToasts();

  const [gender, setGender] = useState("male");

  const [visibleConfirm, setVisibleConfirm] = useState(false);

  const mutationAddPatient = useMutation("patient", APIS_SAVE_PATIENT);

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const submit = (values) => {
    setPayload({
      ...values,
      dob: moment(values?.dob).utc().format(),
      gender: gender,
    });
  };

  const handleSave = (status) => {
    mutationAddPatient.mutate({
      input: { ...payload },
    });
  };

  useEffect(() => {
    if (payload) setVisibleConfirm(true);
  }, [payload]);

  useEffect(() => {
    if (!visibleConfirm) setPayload(null);
  }, [visibleConfirm]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationAddPatient;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm nhanh thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        dispatch(actions.ADD_PATIENT_INFO_SUCCESS(data));
        setVisibleConfirm(false);
        props.data.onCancel();
      } else if (isError)
        addToast("Thêm nhanh thông tin thất bại, vui lòng thử lại", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationAddPatient.isLoading]);

  return (
    <>
      <form onSubmit={handleSubmit(submit)}>
        <Row>
          <Col sm={6}>
            <Controller
              {...register("lastname", { required: true })}
              control={control}
              render={({ field }) => (
                <FormInput
                  required
                  label={"Họ và Tên lót"}
                  error={errors?.lastname ? "Vui lòng nhập Họ và Tên lót" : ""}
                  {...field}
                />
              )}
            />
          </Col>
          <Col sm={6}>
            <Controller
              {...register("firstname", { required: true })}
              control={control}
              render={({ field }) => (
                <FormInput
                  required
                  label={"Tên"}
                  error={errors?.firstname ? "Vui lòng nhập Tên" : ""}
                  {...field}
                />
              )}
            />
          </Col>
          <Col sm={6}>
            <Controller
              {...register("dob", { required: true })}
              control={control}
              render={({ field }) => (
                <FormInput
                  label="Ngày sinh"
                  error={errors?.dob ? "Vui lòng nhập ngày sinh" : ""}
                  {...field}
                  required
                  type="date"
                />
              )}
            />
          </Col>
          <Col sm={6}>
            <div className="mt-10">
              <label htmlhtmlFor="">Giới tính</label>
              <div
                className="flex"
                style={{ marginTop: "-20px", marginLeft: "-10px" }}
              >
                <div className="form-check">
                  <FormInput
                    type="radio"
                    value="male"
                    checked={gender === "male"}
                    // {...field}
                    onChange={(e) =>
                      setGender(e?.target?.checked ? "male" : "female")
                    }
                  />

                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault1"
                  >
                    Nam
                  </label>
                </div>
                <div className="form-check" style={{ marginLeft: "50px" }}>
                  <FormInput
                    type="radio"
                    value="female"
                    onChange={(e) =>
                      setGender(!e?.target?.checked ? "male" : "female")
                    }
                    checked={gender === "female"}
                    // {...field}
                  />

                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault2"
                  >
                    Nữ
                  </label>
                </div>
              </div>
            </div>
          </Col>
          <Col sm={6}>
            <Controller
              {...register("phone", { required: true })}
              control={control}
              render={({ field }) => (
                <FormInput
                  label="Số điện thoại"
                  error={errors?.phone ? "Vui lòng nhập số điện thoại" : ""}
                  {...field}
                  required
                  prefixInput="(+84)"
                  type="autonumber"
                />
              )}
            />
          </Col>
        </Row>
        <Card.Footer
          style={{
            backgroundColor: "white",
          }}
        >
          <div
            style={{
              display: "flex",
              gap: "10px",
              justifyContent: "flex-end",
            }}
          >
            <Button
              className="margin-0 "
              disabled={mutationAddPatient.isLoading}
              variant="danger"
              onClick={props?.data?.onCancel}
            >
              Hủy
            </Button>

            {user?.decentralization[0]?.patients > 2 && (
              <Button
                type="submit"
                className="margin-0"
                disabled={mutationAddPatient.isLoading}
              >
                {FORM_CHANGE_INFO.processBtn}
              </Button>
            )}
          </div>
        </Card.Footer>
      </form>
      <ConfirmModal
        show={visibleConfirm}
        onChangeVisible={() => {
          setVisibleConfirm(!visibleConfirm);
        }}
        onConfirm={() => handleSave()}
        title="Xác nhận"
        content={`Bạn có chắc chắn thêm nhanh bệnh nhân này?`}
        loading={mutationAddPatient.isLoading}
      />
    </>
  );
};
