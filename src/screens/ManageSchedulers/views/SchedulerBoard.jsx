// import name from '../reducer'
import Parser from "html-react-parser";
import moment from "moment";
import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Row,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import Select from "react-select";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import Board from "react-trello";
import {
  categorySchedule,
  statusSchedule,
  typeSchedule,
} from "../../../constants/options";
import { truncateText } from "../../../utils/formatter";
import * as actions from "../actions";
import {
  APIS_DRAG_CARD,
  APIS_DRAG_CARD_INSIDE,
  API_FIND_PERIOD_SCHEDULE,
  APIS_FIND_ALL_SCHEDULES,
} from "../apis";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import CustomPagination from "../../../components/Pagination";
import CustomDrawer from "../../../components/CustomDrawer";

const SchedulerBoard = () => {
  const { user } = useSelector((state) => state["AUTHENTICATION"]);
  const {
    scheduleList,
    scheduleModeList,
    page,
    pageSize,
    totalPage,
    isNextPage,
  } = useSelector((state) => state["SCHEDULES"]);

  const history = useHistory();
  const dispatch = useDispatch();

  const [viewMode, setViewMode] = useState("list");

  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);
  const [show, setShow] = useState(false);

  const [queryList, setQueryList] = useState({
    take: pageSizes,
    skip: pages,
    startDate: moment(moment().startOf("week"))
      .utc()
      .add(1, "days")
      .add(7, "hours")
      .format(),
    endDate: moment(moment().endOf("week")).utc().add(1, "days").format(),
  });

  const [selectedStatus, setSelectedStatus] = useState("");

  const [selectedType, setSelectedType] = useState("");

  const [selectedCategory, setSelectedCategory] = useState("");

  // const [selectedShift, setSelectedShift] = useState("");
  const [board, setBoard] = useState(null);

  const [rangeWeek, setRangeWeek] = useState(0);

  const [query, setQuery] = useState({
    startDate: moment(moment().startOf("week"))
      .utc()
      .add(1, "days")
      .add(7, "hours")
      .format(),
    endDate: moment(moment().endOf("week")).add(1, "days").utc().format(),
  });

  const getWeek = (rangeWeek) => {
    let weeks = [];
    for (let i = 0; i <= 6; i++) {
      weeks.push(
        moment()
          .weekday(i)
          .startOf("day")
          .add(1 + rangeWeek * 7, "days")
          .add(7, "hours")
          .utc(0)
          .format()
      );
    }
    return weeks;
  };

  const mutationDragSchedule = useMutation("input", APIS_DRAG_CARD);
  const mutationDragCardInside = useMutation("input", APIS_DRAG_CARD_INSIDE);
  const queryClient = useQueryClient();

  const querySchedules = useQuery(
    "schedules",
    () => API_FIND_PERIOD_SCHEDULE(query),
    { enabled: viewMode === "grid" }
  );

  const querySchedulesList = useQuery(
    "schedulesList",
    () => APIS_FIND_ALL_SCHEDULES(queryList),
    { enabled: viewMode === "list" }
  );

  useEffect(() => {
    const { isLoading, status } = querySchedules;
    if (!isLoading && status === "success")
      dispatch(actions.GET_ALL_SCHEDULES_SUCCESS(querySchedules?.data));
  }, [querySchedules.data]);

  useEffect(() => {
    const { isLoading, status } = querySchedulesList;
    if (!isLoading && status === "success")
      dispatch(
        actions.GET_ALL_SCHEDULES_LIST_SUCCESS(querySchedulesList?.data)
      );
  }, [querySchedulesList.data]);

  useEffect(() => {
    if (scheduleList) {
      setBoard({
        lanes: getWeek(rangeWeek)?.map((day) => {
          const schedule = scheduleList?.find(
            (schedule) => schedule?.date === moment(day).format("DD/MM/YYYY")
          );
          return {
            id: day,
            title: moment(day).format("DD/MM/YYYY"),
            label:
              (schedule?.data.filter((item) => item.shift === "morning")
                .length || 0) +
              "/" +
              (schedule?.data.filter((item) => item.shift === "afternoon")
                .length || 0),
            cards:
              schedule?.data?.map((item) => {
                return {
                  id: item?.id?.toString(),
                  title: `${item?.patient?.lastname} ${item?.patient?.firstname}`,
                  draggable: item.status === "new",
                  onClick: () =>
                    history.push(`/app/dashboard/scheduler/${item.id}`),
                  item: item,
                };
              }) || [],
          };
        }),
      });
    }
  }, [scheduleList, rangeWeek]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationDragSchedule;

    if (!isLoading) {
      if (!isError && !!data) {
        // alert("Cập nhật thông tin phiếu KCB thành công!");
        queryClient.fetchQuery("schedules", querySchedules);
        // dispatch(actions.CHANGE_INFO_SCHEDULE_SUCCESS(data));
      }
      // else if (isError) alert("Cập nhật thông tin phiếu KCB thất bại!");
    }
  }, [mutationDragSchedule.isLoading]);

  // const [selectedStatus, setSelectedStatus] = useState("");

  // const [selectedType, setSelectedType] = useState("");

  // const [selectedCategory, setSelectedCategory] = useState("");

  // // const [selectedShift, setSelectedShift] = useState("");
  // const [board, setBoard] = useState(null);

  // const [rangeWeek, setRangeWeek] = useState(0);

  // const [query, setQuery] = useState({
  //     startDate: moment(moment().startOf("week"))
  //         .utc()
  //         .add(1, "days")
  //         .add(7, "hours")
  //         .format(),
  //     endDate: moment(moment().endOf("week")).add(1, "days").format(),
  // });

  // const getWeek = (rangeWeek) => {
  //     let weeks = [];
  //     for (let i = 0; i <= 6; i++) {
  //         weeks.push(
  //             moment()
  //                 .weekday(i)
  //                 .startOf("day")
  //                 .add(1 + rangeWeek * 7, "days")
  //                 .add(7, "hours")
  //                 .utc(0)
  //                 .format()
  //         );
  //     }
  //     return weeks;
  // };

  // const mutationDragSchedule = useMutation("input", APIS_DRAG_CARD);
  // const mutationDragCardInside = useMutation("input", APIS_DRAG_CARD_INSIDE);
  // const queryClient = useQueryClient();

  // const querySchedules = useQuery(
  //     "schedules",
  //     () => API_FIND_PERIOD_SCHEDULE(query),
  //     { enabled: viewMode === "grid" }
  // );

  // const querySchedulesList = useQuery(
  //     "schedulesList",
  //     () => APIS_FIND_ALL_SCHEDULES(queryList),
  //     { enabled: viewMode === "list" }
  // );

  useEffect(() => {
    const { isLoading, status } = querySchedules;
    if (!isLoading && status === "success")
      dispatch(actions.GET_ALL_SCHEDULES_SUCCESS(querySchedules?.data));
  }, [querySchedules.data]);

  useEffect(() => {
    const { isLoading, status } = querySchedulesList;
    if (!isLoading && status === "success")
      dispatch(
        actions.GET_ALL_SCHEDULES_LIST_SUCCESS(querySchedulesList?.data)
      );
  }, [querySchedulesList.data]);

  useEffect(() => {
    if (scheduleList) {
      setBoard({
        lanes: getWeek(rangeWeek)?.map((day) => {
          const schedule = scheduleList?.find(
            (schedule) => schedule?.date === moment(day).format("DD/MM/YYYY")
          );
          return {
            id: day,
            title: moment(day).format("DD/MM/YYYY"),
            label:
              (schedule?.data.filter((item) => item.shift === "morning")
                .length || 0) +
              "/" +
              (schedule?.data.filter((item) => item.shift === "afternoon")
                .length || 0),
            cards:
              schedule?.data?.map((item) => {
                return {
                  id: item?.id?.toString(),
                  title: `${item?.patient?.lastname} ${item?.patient?.firstname}`,
                  draggable: item.status === "new",
                  onClick: () =>
                    history.push(`/app/dashboard/scheduler/${item.id}`),
                  item: item,
                };
              }) || [],
          };
        }),
      });
    }
  }, [scheduleList, rangeWeek]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationDragSchedule;

    if (!isLoading) {
      if (!isError && !!data) {
        // alert("Cập nhật thông tin phiếu KCB thành công!");
        queryClient.fetchQuery("schedules", querySchedules);
        // dispatch(actions.CHANGE_INFO_SCHEDULE_SUCCESS(data));
      }
      // else if (isError) alert("Cập nhật thông tin phiếu KCB thất bại!");
    }
  }, [mutationDragSchedule.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationDragCardInside;

    if (!isLoading) {
      if (!isError && !!data) {
        // alert("Cập nhật lịch KCB thành công!");
        queryClient.fetchQuery("schedules", querySchedules);
      }
      // else if (isError) alert("Cập nhật lịch KCB thất bại!");
    }
  }, [mutationDragCardInside.isLoading]);

  function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
  }

  const columns = React.useMemo(
    () => [
      {
        Header: "Bệnh nhân",
        accessor: (row) => {
          return (
            <NavLink
              to={`/app/dashboard/scheduler/${row.id}`}
              target="_blank"
              style={{ textDecoration: "none", color: "#888" }}
            >
              <p className="cursor-pointer cell-p">
                {row?.patient?.lastname} {row?.patient?.firstname}
              </p>
            </NavLink>
          );
        },
      },
      {
        Header: "Ngày",
        accessor: "date",
      },
      {
        Header: "Trạng thái",
        accessor: (row) => (
          <p className="cell-p">
            {statusSchedule.find((item) => item?.value === row?.status)?.label}
          </p>
        ),
      },
      {
        Header: "Loại phiếu",
        accessor: (row) => (
          <p className="cell-p">
            {
              categorySchedule?.find((item) => item.value === row?.category)
                ?.label
            }
          </p>
        ),
      },

      {
        Header: "Ghi chú",
        accessor: (row) => (
          <div
            className="text-truncate mt-2"
            style={{ maxWidth: "230px", overflow: "auto" }}
          >
            {row?.note ? <div>{Parser(row?.note || "")}</div> : " "}
          </div>
        ),
      },
      // {
      //   Header: "Tên viết tắt",
      //   accessor: (row) => {
      //     return (
      //       <p
      //         className="cursor-pointer cell-p"
      //         onClick={() => history.push(`/app/dashboard/items/${row.id}`)}
      //       >
      //         {row?.shortName}
      //       </p>
      //     );
      //   },
      // },
      // {
      //   Header: "Phân loại",
      //   accessor: (row) => {
      //     return (
      //       <p
      //         className="cursor-pointer cell-p"
      //         onClick={() => history.push(`/app/dashboard/items/${row.id}`)}
      //       >
      //         {categoryOptions?.find((item) => item.value === row?.category)
      //           ?.label || " "}
      //       </p>
      //     );
      //   },
      // },
      // {
      //   Header: "SKU",
      //   accessor: "sku",
      // },
      // {
      //   Header: "UPC",
      //   accessor: "upc",
      // },
      // {
      //   Header: "Số SERI",
      //   accessor: "seri",
      // },

      // {
      //   Header: "Kho",
      //   accessor: "bin",
      // },

      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization[0]?.items > 2 && (
              <NavLink
                to={`/app/dashboard/scheduler/${row?.id}`}
                target={"_blank"}
                className="margin-0"
              >
                <Button className="feather icon-edit"></Button>
              </NavLink>
            )
          );
        },
      },
    ],
    [user?.decentralization]
  );
  useEffect(() => {
    setQueryList({ ...queryList, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQueryList({ ...queryList, skip: pages });
  }, [pages]);

  useEffect(() => {
    queryClient.prefetchQuery("schedules", querySchedules);
  }, [query]);

  useEffect(() => {
    setQuery({
      startDate: moment(moment().startOf("week"))
        .utc()
        .add(1 + rangeWeek * 7, "days")
        .add(7, "hours")
        .format(),
      endDate: moment(moment().endOf("week"))
        .add(1 + rangeWeek * 7, "days")
        .format(),
    });
  }, [rangeWeek]);

  useEffect(() => {
    queryClient.prefetchQuery("schedulesList", querySchedulesList);
  }, [queryList]);

  useEffect(() => {
    setQueryList({
      ...queryList,
      skip: 0,
      category: selectedCategory,
    });
  }, [selectedCategory]);

  useEffect(() => {
    setQueryList({
      ...queryList,
      skip: 0,
      type: selectedType,
    });
  }, [selectedType]);

  useEffect(() => {
    setQueryList({
      ...queryList,
      skip: 0,
      status: selectedStatus,
    });
  }, [selectedStatus]);

  const handleMoveCard = (
    cardId,
    sourceLaneId,
    targetLaneId,
    position,
    cardDetails
  ) => {
    const lane = board.lanes
      .find((item) => item.id === sourceLaneId)
      .cards.map((card, index) => ({
        id: parseInt(card?.id),
        index: index,
      }));

    if (sourceLaneId !== targetLaneId) {
      mutationDragSchedule.mutate({
        input: {
          id: cardId,
          date: moment(targetLaneId).utc().format(),
          shift: cardDetails?.item?.shift,
          note: cardDetails?.item?.note,
          department: cardDetails?.item?.department,
          category: cardDetails?.item?.category,
          type: cardDetails?.item?.type,
          patient: cardDetails?.item?.patient.id,
        },
      });
    } else if (
      sourceLaneId === targetLaneId &&
      position !== lane.find((item) => item.id === parseInt(cardId))?.index
    ) {
      let payload = array_move(
        lane,
        lane.find((item) => item.id === parseInt(cardId))?.index,
        position
      ).map((item, index) => ({ ...item, index: index }));

      mutationDragCardInside.mutate({ input: payload });
    }
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Card.Title as="h5" style={{ width: "100%", textAlign: "left" }}>
                Lịch khám chữa bệnh
              </Card.Title>
              {user?.decentralization?.length &&
                user?.decentralization[0]?.items > 1 && (
                  <Button
                    style={{
                      margin: 0,
                      whiteSpace: "nowrap",
                    }}
                    onClick={() => history.push("/app/dashboard/scheduler/add")}
                    className="btn btn-primary"
                  >
                    Thêm lịch khám bệnh
                  </Button>
                )}
              <Button
                style={{
                  margin: "0 0 0 10px",
                  whiteSpace: "nowrap",
                  height: "43px",
                }}
                onClick={() => {
                  if (viewMode === "grid")
                    queryClient.fetchQuery("schedules", querySchedules);
                  else if (viewMode === "list") {
                    setSelectedCategory("");
                    setSelectedStatus("");
                    setSelectedType("");
                  }
                }}
                className="feather icon-refresh-ccw btn btn-primary"
              ></Button>
              <span className="ml-2 mr-2">|</span>
              <OverlayTrigger
                placement="bottom"
                overlay={<Tooltip id={`tooltip-bottom`}>Xem dạng Bảng</Tooltip>}
              >
                <Button
                  className="feather icon-grid margin-0 mr-2"
                  style={{ height: 43 }}
                  onClick={() => setViewMode("grid")}
                ></Button>
              </OverlayTrigger>
              <OverlayTrigger
                placement="bottom"
                overlay={
                  <Tooltip id={`tooltip-bottom`}>Xem dạng danh sách</Tooltip>
                }
              >
                <Button
                  className="feather icon-list margin-0 mr-2"
                  onClick={() => setViewMode("list")}
                  style={{ height: 43 }}
                ></Button>
              </OverlayTrigger>
            </Card.Header>
            <Card.Body>
              {viewMode === "grid" && (
                <div>
                  <div className="flex" style={{ justifyContent: "end" }}>
                    <Button
                      className="feather icon-chevron-left"
                      style={{
                        color: "black",
                        backgroundColor: "transparent",
                        fontSize: "18px",
                        border: "none",
                        fontWeight: "bold",
                      }}
                      onClick={() => setRangeWeek(rangeWeek - 1)}
                    ></Button>
                    <p
                      className="margin-0"
                      style={{
                        color: "black",
                        fontSize: "16px",
                        fontWeight: "bold",
                        alignSelf: "center",
                        paddingBottom: "6px",
                        marginLeft: "-10px",
                      }}
                    >
                      {moment(query?.startDate).format("DD/MM/YYYY")} -{" "}
                      {moment(query?.endDate).format("DD/MM/YYYY")}
                    </p>
                    <Button
                      className="feather icon-chevron-right"
                      onClick={() => setRangeWeek(rangeWeek + 1)}
                      style={{
                        color: "black",
                        backgroundColor: "transparent",
                        fontSize: "18px",
                        border: "none",
                        marginRight: "0px",
                        fontWeight: "bold",
                      }}
                    ></Button>
                  </div>
                  <Board
                    style={{
                      // backgroundColor: "#fff",
                      height: "fit-content",
                      minHeight: "70vh",
                      maxHeight: "80vh",
                      overflow: "auto",
                    }}
                    handleDragEnd={(
                      cardId,
                      sourceLaneId,
                      targetLaneId,
                      position,
                      cardDetails
                    ) => {
                      handleMoveCard(
                        cardId,
                        sourceLaneId,
                        targetLaneId,
                        position,
                        cardDetails
                      );
                    }}
                    laneDraggable={false}
                    data={board || { lanes: [] }}
                    components={{ Card: CustomCard }}
                  ></Board>
                </div>
              )}
              {viewMode === "list" && (
                <div>
                  <Row className="mb-3">
                    <Col className="d-flex align-items-center">
                      Hiển thị
                      <select
                        className="form-control w-auto mx-2"
                        value={pageSizes}
                        onChange={(e) => {
                          setPageSizes(Number(e.target.value));
                        }}
                      >
                        {[5, 10, 20, 50, 100].map((pgsize) => (
                          <option key={pgsize} value={pgsize}>
                            {pgsize}
                          </option>
                        ))}
                      </select>
                      <p
                        className="feather icon-filter margin-0 cursor-pointer"
                        style={{
                          color: !show ? "black" : "#04a9f5",
                          fontWeight: "bold",
                          fontSize: 16,
                        }}
                        onClick={() => setShow(!show)}
                      ></p>
                    </Col>
                  </Row>
                  <Row>
                    <Col
                      sm={3}
                      className="self-center"
                      style={{
                        minWidth: "220px",
                        marginBottom: "10px",
                      }}
                    >
                      <label className="label-control items-center">
                        Từ ngày
                      </label>
                      <input
                        required
                        className="form-control"
                        type="date"
                        defaultValue={moment(moment().startOf("week"))
                          .utc()
                          .add(1, "days")
                          .add(7, "hours")
                          .format("YYYY-MM-DD")}
                        onChange={(e) =>
                          setQueryList({
                            ...queryList,
                            skip: 0,
                            startDate: moment(
                              moment(e.target.value).startOf("day")
                            )
                              .utc()
                              .add("7", "hours")
                              .format(),
                          })
                        }
                        style={{
                          paddingLeft: "10px",
                          flex: 12,
                          height: "38px",
                        }}
                      />
                    </Col>
                    <Col
                      sm={3}
                      className="self-center"
                      style={{
                        minWidth: "220px",
                        marginBottom: "10px",
                      }}
                    >
                      <label className="label-control items-center">
                        Đến ngày
                      </label>
                      <input
                        required
                        className="form-control"
                        type="date"
                        defaultValue={moment(moment().endOf("week"))
                          .add(1, "days")
                          .format("YYYY-MM-DD")}
                        onChange={(e) =>
                          setQueryList({
                            ...queryList,
                            skip: 0,
                            endDate: moment(moment(e.target.value).endOf("day"))
                              .utc()
                              .format(),
                          })
                        }
                        style={{
                          paddingLeft: "10px",
                          flex: 12,
                          height: "38px",
                        }}
                      />
                    </Col>
                  </Row>
                  <CustomTable
                    columns={columns}
                    data={scheduleModeList || []}
                  />
                  <CustomPagination
                    pageIndex={page}
                    totalPage={totalPage}
                    gotoPage={(e) => setPages(e)}
                    canPreviousPage={page > 0}
                    canNextPage={page < totalPage - 1}
                  ></CustomPagination>
                </div>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {show && (
        <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
          <div>
            <p
              style={{
                width: "100%",
                fontSize: 18,
                fontWeight: 700,
                borderBottom: "1px solid #ddd",
                paddingBottom: 10,
              }}
            >
              Lọc phiếu khám bệnh
            </p>
            <Row className="mb-4">
              <Col sm={12}>
                <div
                  className="self-center"
                  style={{
                    paddingTop: 10,
                    width: "100%",
                    zIndex: 100,
                  }}
                >
                  <label className="label-control items-center">
                    Loại phiếu
                  </label>
                  <Select
                    name="category"
                    style={{
                      width: "100%",
                    }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={categorySchedule}
                    value={categorySchedule?.find(
                      (item) => item.value === selectedCategory
                    )}
                    onChange={(e) => setSelectedCategory(e.value)}
                    isSearchable
                  />
                </div>
              </Col>
              <Col sm={12}>
                <div
                  className="self-center"
                  style={{
                    paddingTop: 10,
                    width: "100%",
                    zIndex: 100,
                  }}
                >
                  <label className="label-control items-center">
                    Trạng thái
                  </label>
                  <Select
                    style={{
                      width: "100%",
                    }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={statusSchedule}
                    value={statusSchedule?.find(
                      (item) => item.value === selectedStatus
                    )}
                    onChange={(e) => setSelectedStatus(e.value)}
                  />
                </div>
              </Col>
              <Col sm={12}>
                <div
                  className="self-center"
                  style={{
                    paddingTop: 10,
                    width: "100%",
                    zIndex: 100,
                  }}
                >
                  <label className="label-control items-center">Loại</label>
                  <Select
                    name="type"
                    style={{
                      width: "100%",
                    }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={typeSchedule}
                    value={typeSchedule?.find(
                      (item) => item.value === selectedType
                    )}
                    onChange={(e) => setSelectedType(e.value)}
                    isSearchable
                  />
                </div>
              </Col>
            </Row>
          </div>
        </CustomDrawer>
      )}
    </React.Fragment>
  );
};

const CustomCard = ({
  onClick,
  item,
  title,
  id,
  label,
  cardColor,
  onDelete,
}) => {
  return (
    <div
      style={{
        backgroundColor: "#fff",
        padding: "10px",
        marginBottom: "10px",
        // marginRight: 0,
        borderRadius: "5px",
        cursor: "pointer",
        width: "250px",
      }}
    >
      <header
        style={{
          paddingBottom: 6,
          marginBottom: 10,
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <a
          style={{
            fontSize: "0.92em",
            fontWeight: "bold",
            maxWidth: "130px",
            color: "black",
            textDecoration: "none",
          }}
          title={title}
          href={`/app/dashboard/scheduler/${item.id}`}
          target="_blank"
        >
          {truncateText(title, 16)}
        </a>
        <span
          style={{
            fontSize: "0.92em",
            fontWeight: "bold",
            cursor: "pointer",
          }}
        >
          {item?.shift ? (item?.shift === "morning" ? "SÁNG" : "CHIỀU") : " "}
        </span>
      </header>
      <div style={{ fontSize: "0.9em" }}>
        <div>
          Trạng thái:{" "}
          <b>{statusSchedule?.find((o) => o.value === item?.status).label}</b>
        </div>
        <div style={{ maxWidth: "240px" }} className="mt-2">
          Phiếu:{" "}
          <b>
            {categorySchedule?.find((o) => o.value === item?.category)?.label}
          </b>
        </div>
        {item?.department && (
          <div className="mt-2">
            Phòng số: <b> {item?.department}</b>
          </div>
        )}
        <div
          className="text-truncate mt-2"
          style={{ maxWidth: "230px", overflow: "auto" }}
        >
          <b>Ghi chú:</b>

          {item?.note ? <div>{Parser(item?.note || "")}</div> : " Không có"}
        </div>
      </div>
    </div>
  );
};

export default SchedulerBoard;
