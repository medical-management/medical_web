/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import _ from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import { useToasts } from "react-toast-notifications";
import EditorCkClassic from "../../../components/CK-Editor/CkClassicEditor";
import ConfirmModal from "../../../components/ConfirmModal";
import CustomModal from "../../../components/CustomModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import SpinnerBackDrop from "../../../components/SpinnerBackDrop/SpinnerBackDrop";
import {
  categorySchedule,
  shiftSchedule,
  statusSchedule,
  typeSchedule,
} from "../../../constants/options";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { APIS_FIND_FAST_PATIENT } from "../../ManagePatient/apis";
import * as actions from "../actions";
import { APIS_FIND_ONE_SCHEDULE, APIS_SAVE_SCHEDULE } from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";
import QuickFormPatient from "./QuickForm";
import Parser from "html-react-parser";

export default function ScheduleInfo(props) {
  return (
    <React.Fragment>
      <Breadcrumb />
      <div>
        <SubmitValidationForm data={props} />
      </div>
    </React.Fragment>
  );
}

let SubmitValidationForm = (props) => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const params = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  // const [description, setDescrition] = useState("");
  const [note, setNote] = useState("");
  const { item, listPatient, schedule, newPatient } = useSelector(
    (state) => state["SCHEDULES"]
  );
  const { patients } = useSelector((state) => state["PATIENTS"]);

  const { addToast } = useToasts();

  const [load, setLoad] = useState(true);

  const [payload, setPayload] = useState(null);

  const [select, setSelect] = useState("");

  const [selectedStatus, setSelectedStatus] = useState("new");

  const [selectedType, setSelectedType] = useState("not");

  const [selectedCategory, setSelectedCategory] = useState("normal");

  const [selectedShift, setSelectedShift] = useState("morning");

  const [listPatientFind, setListPatientFind] = useState([]);

  const [visibleConfirmCopy, setVisibleConfirmCopy] = useState(false);

  const [visibleConfirm, setVisibleConfirm] = useState(false);

  const [visibleConfirmCancel, setVisibleConfirmCancel] = useState(false);

  const [typeAction, setTypeAction] = useState("");

  const [openModalQuick, setOpenModalQuick] = useState(false);

  const queryFindOne = useQuery(
    ["find_one_item", params],
    () => params?.id !== "add" && APIS_FIND_ONE_SCHEDULE(params)
  );

  const queryPatient = useQuery("users", () =>
    APIS_FIND_FAST_PATIENT({ firstname: firstName, take: 10, skip: 0 })
  );

  const mutationChangeSchedule = useMutation("input", APIS_SAVE_SCHEDULE);
  const mutationAddSchedule = useMutation("input", APIS_SAVE_SCHEDULE);
  // const mutationCopy = useMutation("copy", APIS_SAVE_SCHEDULE);

  const [firstName, setFirstName] = useState("");

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [confirm, setConfirm] = useState(false);
  const queryClient = useQueryClient();
  const [selectedPatient, setSelectedPatient] = useState("");

  useEffect(() => {
    setLoad(true);
    queryClient.prefetchQuery("users", queryPatient);
  }, history.location);

  useEffect(() => {
    const { isLoading, isError, data, status, error } = queryFindOne;
    if (!isLoading) {
      if (error?.length || status === "error") {
        addToast("Lấy thông tin thất bại hoặc phiếu KCB không tồn tại!", {
          appearance: "error",
          autoDismiss: true,
        });
        history.push("/app/dashboard/scheduler");
      } else
        dispatch(
          actions.GET_INFO_SCHEDULE_SUCCESS(data?.find_one_medical_scheduled)
        );
    }
  }, [queryFindOne.isLoading]);

  useEffect(() => {
    if (queryPatient.status === "success") {
      setLoad(false);
      dispatch(actions.GET_ALL_PATIENT_INFO_SUCCESS(queryPatient?.data));
    }
  }, [queryPatient.data]);

  useEffect(() => {
    if (params.id !== "add") {
      queryClient.prefetchQuery(queryFindOne);
    } else {
      if (props?.data?.location.state) {
        Object.entries(props?.data?.location.state).forEach(([key, value]) => {
          if (key !== "id" || key !== "patient") setValue(key, value);
        });
        setSelectedType(props?.data?.location.state?.type);
        setSelectedCategory(props?.data?.location.state?.category);
        setSelectedShift(props?.data?.location.state?.shift);
        setSelectedStatus("new");
        setSelectedPatient("");
        setNote(props?.data?.location.state?.note);
      }
    }
  }, [params.id]);

  useEffect(() => {
    if (newPatient) setSelectedPatient(newPatient.value);
  }, [newPatient]);

  const submit = (values) => {
    if (!selectedPatient)
      addToast("Vui lòng chọn bệnh nhân!", {
        appearance: "warning",
        autoDismiss: true,
      });
    else
      setPayload({
        ...values,
        note: note || " ",
        shift: selectedShift,
        category: selectedCategory,
        type: selectedType,
        patient: selectedPatient,
        status: selectedStatus,
        date: moment(values?.date).utc().add(7, "hours").format(),
      });
  };

  const handleSave = (status) => {
    if (params.id !== "add") {
      mutationChangeSchedule.mutate({
        input: { ...payload, status: status || selectedStatus },
      });
    } else {
      mutationAddSchedule.mutate({
        input: {
          ...payload,
          status: "new",
          id: "",
        },
      });
    }
  };

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeSchedule;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin phiếu KCB thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        dispatch(actions.CHANGE_INFO_SCHEDULE_SUCCESS(data));
        setConfirm(false);
        setVisibleConfirm(false);
        setVisibleConfirmCancel(false);
      } else if (isError)
        addToast("Cập nhật thông tin phiếu KCB thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationChangeSchedule.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationAddSchedule;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm phiếu KCB thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push("/app/dashboard/scheduler");
      } else if (isError)
        addToast("Thêm phiếu KCB thất bại! Vui lòng thử lại.", {
          appearance: "success",
          autoDismiss: true,
        });
    }
  }, [mutationAddSchedule.isLoading]);

  useEffect(() => {
    if (listPatient?.length) {
      setListPatientFind(patients?.map((item) => item));
    }
  }, [listPatient]);

  useEffect(() => {
    if (params.id !== "add" && schedule) {
      Object.entries(schedule).forEach(([key, value]) => {
        setValue(key, value);
      });
      setSelectedType(schedule?.type);
      setSelectedCategory(schedule?.category);
      setSelectedShift(schedule?.shift);
      setSelectedStatus(schedule?.status);
      setNote(schedule?.note);
      setSelectedPatient(schedule?.patient?.id);
    }
  }, [schedule]);

  // const onChangeStatus = () => {
  //   const payload = {
  //     ...item,
  //     close: !item.close,
  //   };
  //   mutationChangeSchedule.mutate({
  //     input: {
  //       ...payload,
  //     },
  //   });
  // };

  // const loadOptions = (inputValue, callback) => {};

  useEffect(() => {
    queryClient.fetchQuery("users", queryPatient);
  }, [firstName]);

  const handleCopySchedule = () => {
    history.push({ pathname: "/app/dashboard/scheduler/add", state: schedule });
    setVisibleConfirmCopy(false);
  };

  useEffect(() => {
    if (typeAction === "normal") {
      if (payload) {
        setVisibleConfirm(true);
      }
    } else if (typeAction === "cancel") {
      if (payload) {
        setVisibleConfirmCancel(true);
      }
    }
  }, [typeAction, payload]);

  return (
    <>
      <form onSubmit={handleSubmit(submit)}>
        {queryPatient.isLoading && <SpinnerBackDrop></SpinnerBackDrop>}
        <Card>
          <Card.Header className="header-card" style={{ position: "relative" }}>
            <p
              className="feather icon-chevron-left icon"
              onClick={() => history.push("/app/dashboard/scheduler")}
            />
            <Card.Title as="h4" className="title">
              {params?.id === "add"
                ? "Thêm phiếu khám chữa bệnh"
                : "Sửa phiếu khám chữa bệnh"}
            </Card.Title>
          </Card.Header>
          <Card.Body>
            <SessionTitle>{`Thông tin chi tiết ${
              item?.id ? `- Mã phiếu: ${item?.id}` : ""
            }`}</SessionTitle>
            {/* {params.id !== "add" && <Field name="id" component={renderField} label={"Mã sản phẩm"} disabled />} */}
            <Row>
              <Col sm={6}>
                {schedule?.status === "cancel" ? (
                  <FormInput label={"Trạng thái"} disabled value="Hủy khám" />
                ) : (
                  <div
                    className="self-center"
                    style={{
                      paddingTop: 10,
                      width: "100%",
                      zIndex: 100,
                    }}
                  >
                    <label className="label-control items-center">
                      Trạng thái
                    </label>
                    <Select
                      style={{ width: "100%" }}
                      className="basic-single"
                      classNamePrefix="select"
                      options={statusSchedule}
                      value={statusSchedule?.find(
                        (item) => item.value === selectedStatus
                      )}
                      onChange={(e) => setSelectedStatus(e.value)}
                    />
                  </div>
                )}
              </Col>
              {params?.id !== "add" && (
                <Col sm={6}>
                  <Controller
                    {...register("updatedby")}
                    control={control}
                    render={({ field }) => (
                      <FormInput
                        label={"Người chỉnh sửa phiếu"}
                        disabled
                        {...field}
                      />
                    )}
                  />
                </Col>
              )}
              <Col
                sm={6}
                className="flex"
                style={{ alignItems: "flex-end", alignSelf: "center" }}
              >
                {schedule?.status === "cancel" ? (
                  <FormInput
                    label={"Bệnh nhân"}
                    disabled
                    full
                    value={
                      listPatient?.find(
                        (item) => item.value === selectedPatient || ""
                      )?.label
                    }
                  />
                ) : (
                  <div
                    className="self-center"
                    style={{
                      paddingTop: 10,
                      width: "100%",
                    }}
                  >
                    <label className="label-control items-center">
                      Bệnh nhân <span style={{ color: "red" }}>*</span>{" "}
                    </label>
                    <Select
                      name="patient"
                      style={{ width: "100%" }}
                      className="basic-single"
                      classNamePrefix="select"
                      options={queryPatient.isLoading ? [] : listPatient}
                      value={listPatient?.find(
                        (item) => item.value === selectedPatient || ""
                      )}
                      onChange={(e) => setSelectedPatient(e.value)}
                      onInputChange={_.debounce(function (e) {
                        setFirstName(e);
                      }, 800)}
                    />
                  </div>
                )}
                {params?.id === "add" && (
                  <Button
                    onClick={() => setOpenModalQuick(true)}
                    className="btn btn-primary margin-0 feather icon-user-plus ml-2"
                    style={{ height: "38px" }}
                  ></Button>
                )}
              </Col>
              {params?.id !== "add" && (
                <Col sm={6}>
                  <Controller
                    {...register("createdat")}
                    control={control}
                    render={({ field }) => (
                      <FormInput label={"Thời gian tạo"} disabled {...field} />
                    )}
                  />
                </Col>
              )}

              <Col sm={6}>
                {schedule?.status === "cancel" ? (
                  <FormInput
                    label={"Loại"}
                    disabled
                    value={
                      typeSchedule?.find((item) => item.value === selectedType)
                        ?.label
                    }
                  />
                ) : (
                  <div
                    className="self-center"
                    style={{
                      paddingTop: 10,
                      width: "100%",
                      zIndex: 100,
                    }}
                  >
                    <label className="label-control items-center">Loại</label>
                    <Select
                      name="type"
                      style={{ width: "100%" }}
                      className="basic-single"
                      classNamePrefix="select"
                      options={typeSchedule}
                      value={typeSchedule?.find(
                        (item) => item.value === selectedType
                      )}
                      onChange={(e) => setSelectedType(e.value)}
                      isSearchable
                    />
                  </div>
                )}
              </Col>
              <Col sm={6}>
                {schedule?.status === "cancel" ? (
                  <FormInput
                    label={"Loại phiếu"}
                    disabled
                    value={
                      categorySchedule?.find(
                        (item) => item.value === selectedCategory
                      )?.label
                    }
                  />
                ) : (
                  <div
                    className="self-center"
                    style={{
                      paddingTop: 10,
                      width: "100%",
                      zIndex: 100,
                    }}
                  >
                    <label className="label-control items-center">
                      Loại phiếu
                    </label>
                    <Select
                      name="category"
                      style={{ width: "100%" }}
                      className="basic-single"
                      classNamePrefix="select"
                      options={categorySchedule}
                      value={categorySchedule?.find(
                        (item) => item.value === selectedCategory
                      )}
                      onChange={(e) => setSelectedCategory(e.value)}
                      isSearchable
                    />
                  </div>
                )}
              </Col>
            </Row>
            <SessionTitle mt={30}>Thông tin khám bệnh</SessionTitle>
            <Row>
              <Col sm={4}>
                <Controller
                  {...register("date")}
                  control={control}
                  render={({ field }) => (
                    <FormInput
                      label={FORM_CHANGE_INFO.date}
                      type="date"
                      {...field}
                      disabled={schedule?.status === "cancel"}
                      // minVal={new Date().toISOString().split("T")[0]}
                      defaultValue={moment().format("YYYY-MM-DD")}
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <Controller
                  {...register("department")}
                  control={control}
                  render={({ field }) => (
                    <FormInput
                      label={FORM_CHANGE_INFO.department}
                      disabled={schedule?.status === "cancel"}
                      {...field}
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                {schedule?.status === "cancel" ? (
                  <FormInput
                    label={"Ca khám"}
                    disabled
                    value={
                      shiftSchedule?.find(
                        (item) => item.value === selectedShift
                      )?.label
                    }
                  />
                ) : (
                  <div
                    className="self-center"
                    style={{
                      paddingTop: 10,
                      width: "100%",
                      zIndex: 100,
                    }}
                  >
                    <label className="label-control items-center">
                      Ca khám
                    </label>
                    <Select
                      style={{ width: "100%" }}
                      className="basic-single"
                      classNamePrefix="select"
                      options={shiftSchedule}
                      // loadOptions={loadOptions}
                      value={shiftSchedule?.find(
                        (item) => item.value === selectedShift
                      )}
                      onChange={(e) => setSelectedShift(e.value)}
                      isSearchable
                    />
                  </div>
                )}
              </Col>
              <Col sm={12} style={{ zIndex: 0 }}>
                <label
                  className="label-control mt-2"
                  style={{ whiteSpace: "nowrap" }}
                >
                  Ghi chú
                </label>
                {schedule?.status === "cancel" ? (
                  <div
                    style={{
                      borderRadius: "4px",
                      border: "1px solid #ced4da",
                      minHeight: "50px",
                    }}
                  >
                    {Parser(note || " ")}
                  </div>
                ) : (
                  <EditorCkClassic
                    html={note || ""}
                    onChange={(value) => setNote(value)}
                  />
                )}
              </Col>
            </Row>
          </Card.Body>
          <Card.Footer>
            <div
              style={{
                display: "flex",
                gap: "10px",
                justifyContent: "flex-end",
              }}
            >
              {params?.id !== "add" &&
                user?.decentralization[0]?.schedules > 2 &&
                schedule?.status === "new" && (
                  <Button
                    type="submit"
                    className="margin-0 "
                    disabled={
                      mutationAddSchedule.isLoading ||
                      mutationChangeSchedule.isLoading
                    }
                    onClick={() => setTypeAction("cancel")}
                    variant="danger"
                  >
                    Hủy phiếu
                  </Button>
                )}
              {params?.id !== "add" &&
                user?.decentralization[0]?.schedules > 1 &&
                params?.id !== "add" && (
                  <Button
                    // type="submit"
                    onClick={() => setVisibleConfirmCopy(true)}
                    className="margin-0 btn btn-success "
                    disabled={
                      mutationAddSchedule.isLoading ||
                      mutationChangeSchedule.isLoading
                    }
                  >
                    Sao chép
                  </Button>
                )}
              {params?.id !== "add" &&
                user?.decentralization[0]?.schedules > 2 &&
                schedule?.status !== "cancel" && (
                  <Button
                    className="margin-0"
                    onClick={() =>
                      history.push({
                        pathname: "/app/dashboard/medicals/add",
                        state: schedule?.patient,
                      })
                    }
                    disabled={
                      mutationAddSchedule.isLoading ||
                      mutationChangeSchedule.isLoading
                    }
                  >
                    Khám bệnh
                  </Button>
                )}
              {((params?.id === "add" &&
                user?.decentralization[0]?.schedules > 1) ||
                (params?.id !== "add" &&
                  user?.decentralization[0]?.schedules > 2)) &&
                schedule?.status !== "cancel" && (
                  <Button
                    type="submit"
                    className="margin-0"
                    onClick={() => setTypeAction("normal")}
                    disabled={
                      mutationAddSchedule.isLoading ||
                      mutationChangeSchedule.isLoading
                    }
                  >
                    {FORM_CHANGE_INFO.processBtn}
                  </Button>
                )}
            </div>
          </Card.Footer>
        </Card>
      </form>
      <CustomModal
        onChangeVisible={() => setOpenModalQuick(!openModalQuick)}
        title="Thêm bệnh nhân"
        height="40vh"
        show={openModalQuick}
      >
        <QuickFormPatient
          onCancel={() => setOpenModalQuick(false)}
        ></QuickFormPatient>
      </CustomModal>
      <ConfirmModal
        show={visibleConfirmCopy}
        onChangeVisible={() => {
          setVisibleConfirmCopy(!visibleConfirmCopy);
        }}
        onConfirm={() => handleCopySchedule()}
        title="Xác nhận"
        content={`Bạn có chắc chắn muốn tạo phiếu KCB dựa trên thông tin phiếu KCB này?`}
        loading={mutationChangeSchedule.isLoading}
      />
      <ConfirmModal
        show={visibleConfirm}
        onChangeVisible={() => {
          setVisibleConfirm(!visibleConfirm);
        }}
        onConfirm={() => handleSave("")}
        title="Xác nhận"
        content={`Bạn có chắc chắn muốn ${
          params?.id === "add" ? "THÊM MỚI" : "CẬP NHẬT"
        } phiếu KCB này?`}
        loading={
          mutationChangeSchedule.isLoading || mutationAddSchedule.isLoading
        }
      />
      <ConfirmModal
        show={visibleConfirmCancel}
        onChangeVisible={() => {
          setVisibleConfirmCancel(!visibleConfirmCancel);
        }}
        onConfirm={() => handleSave("cancel")}
        title="Xác nhận"
        content={`Bạn có chắc chắn muốn HỦY phiếu KCB này?`}
        loading={mutationChangeSchedule.isLoading}
      />
    </>
  );
};
