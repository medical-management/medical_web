/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */

export const FORM_CHANGE_INFO = {
  title: "Thông tin sản phẩm",
  name: "Tên sản phẩm",
  shortName: "Tên rút gọn",
  department: "Số phòng",
  purchasePrices: "Giá nhập",
  date: "Ngày khám bệnh",

  processBtn: "Lưu",
  cancelBtn: "Hủy",
};
