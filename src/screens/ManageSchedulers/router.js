/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Manager User
 */
import { lazy } from "react";

export const ROUTER_MANAGE_SCHEDULE = "/app/dashboard/scheduler";

const routers = [
  {
    exact: true,
    path: ROUTER_MANAGE_SCHEDULE,
    component: lazy(() => import("./views/SchedulerBoard")),
  },
  {
    path: `${ROUTER_MANAGE_SCHEDULE}/:id`,
    component: lazy(() => import("./views/SchedulerInfo")),
  },
];
export default routers;
