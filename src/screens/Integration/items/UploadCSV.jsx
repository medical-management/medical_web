import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Row, Col, Form } from "react-bootstrap";
import BTable from "react-bootstrap/Table";
import {
  useTable,
  usePagination,
  useGlobalFilter,
  useRowSelect,
} from "react-table";
import { useDispatch, useSelector } from "react-redux";
import * as XLSX from "xlsx";
import { useMutation } from "react-query";
import moment from "moment";

import { GlobalFilter } from "../../../views/tables/react-table/GlobalFilter";
import * as actions from "../actions";
import { APIS_INTEGRATE_ITEM } from "../apis";
import { name } from "../reducer";
import { buttOption, processData } from "../utils";
import PreviewModal from "../PreviewModal";
import { useToasts } from "react-toast-notifications";

export default function UploadCSV() {
  const {
    listItems,
    listColumn,
    fileName,
    selectedRowsPreview,
    selectedOnePreview,
    convertFileItem,
  } = useSelector((state) => state[name]);

  const dispatch = useDispatch();
  const [columns, setColumns] = useState([]);
  const [visible, setVisible] = useState(false);
  const [dataModal, setDataModal] = useState({
    isVerify: false,
    isSave: false,
    isOpen: false,
  });
  const [actionSubmit, setActionSubmit] = useState("custom");

  const mutationImportItem = useMutation(
    "integrate_item",
    APIS_INTEGRATE_ITEM,
    { enabled: false }
  );

  const handleFileUpload = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = async (evt) => {
      /* Parse dataCSV */
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: "binary" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const dataCSV = XLSX.utils.sheet_to_csv(ws, { header: 1 });
      const { rows, columns } = await processData({
        dataString: dataCSV,
        convertFile: convertFileItem,
      });
      dispatch(actions.IMPORT_DATA({ rows, columns, fileName: file.name }));
    };
    file && reader.readAsBinaryString(file);
  };

  const handleImport = () => {
    if (!!selectedOnePreview) {
      mutationImportItem.mutate({
        data: [selectedOnePreview],
        isverify: dataModal.isVerify,
        fileName: "",
        inactive: false,
      });
      dispatch(actions.DELETE_ROW_TABLE_PREVIEW(selectedOnePreview.index));
    }
    if (selectedRowsPreview.length > 0) {
      const listRowVerify = selectedRowsPreview.filter(
        ({ button }) =>
          (actionSubmit === "custom" && button === buttOption.verify) ||
          actionSubmit === "verifyall"
      );
      const listRowSave = selectedRowsPreview.filter(
        ({ button }) =>
          (actionSubmit === "custom" && button === buttOption.save) ||
          actionSubmit === "saveall"
      );
      if (listRowVerify.length > 0)
        mutationImportItem.mutate({
          data: listRowVerify,
          isverify: true,
          fileName,
          inactive: false,
        });
      if (listRowSave.length > 0)
        mutationImportItem.mutate({
          data: listRowSave,
          isverify: false,
          fileName,
          inactive: false,
        });

      selectedRowsPreview.forEach((o) =>
        dispatch(actions.DELETE_ROW_TABLE_PREVIEW(o.index))
      );
    }
  };

  useEffect(() => {
    const listCols = [];
    listColumn.forEach((item) => {
      if (convertFileItem[item.name.toUpperCase()])
        listCols.push({
          Header: convertFileItem[item.name.toUpperCase()].label,
          accessor: convertFileItem[item.name].value,
          Cell: ({ row: { original }, column: { id } }) => (
            <Form.Control
              defaultValue={
                id !== "startDate" && id !== "endDate"
                  ? original[id]
                  : moment(original[id]).format("YYYY-MM-DD")
              }
              type={id !== "startDate" && id !== "endDate" ? "text" : "date"}
              placeholder=""
              className="mb-3"
              style={{ width: "250px" }}
              onBlur={(e) =>
                dispatch(
                  actions.HANDLE_INPUT_TABLE_REVIEW({
                    index: original.index,
                    id,
                    value: e.target.value,
                  })
                )
              }
            />
          ),
        });
    });
    setColumns(listCols);
  }, [listColumn, listItems]);
  const { addToast } = useToasts();

  useEffect(() => {
    const { isLoading, data } = mutationImportItem;
    if (!isLoading && !!data) {
      addToast("Nhập dữ liệu thành công!", {
        appearance: "success",
        autoDismiss: true,
      });
      setDataModal({ isOpen: false, isSave: false, isVerify: false });
    }
  }, [mutationImportItem.isLoading]);

  return (
    <>
      <input
        id="fileInput"
        type="file"
        accept=".csv,.xlsx,.xls"
        className="form-control"
        style={{ paddingTop: "8px", paddingBottom: "8px" }}
        onChange={handleFileUpload}
      />
      <Button
        variant="primary"
        className="text-capitalize white-space-nowrap ml-2"
        onClick={() => setVisible(true)}
        disabled={!!listItems && listItems.length === 0}
        style={{ marginBottom: 0 }}
      >
        Xem trước
      </Button>
      <PreviewModal
        show={visible}
        onChangeVisible={() => setVisible(false)}
        header="Danh sách nhân viên"
        body={<Table columns={columns} />}
        footer={
          <>
            <Button
              variant="primary"
              onClick={() => {
                setDataModal({
                  isOpen: true,
                  isVerify: false,
                  isSave: false,
                });
                setActionSubmit("saveall");
              }}
              className="flex"
              style={{ placeItems: "center", marginRight: "10px" }}
              disabled={
                !selectedRowsPreview || selectedRowsPreview.length === 0
              }
            >
              Lưu các nội dung đã chọn
            </Button>
            <Button
              variant="success"
              onClick={() => {
                setDataModal({
                  isOpen: true,
                  isVerify: true,
                  isSave: false,
                });
                setActionSubmit("verifyall");
              }}
              className="flex"
              style={{ placeItems: "center", marginRight: "10px" }}
              disabled={
                !selectedRowsPreview || selectedRowsPreview.length === 0
              }
            >
              Lưu và Duyệt các nội đã chọn
            </Button>
            <Button
              variant="secondary"
              onClick={() => {
                setVisible(false);
              }}
              className="flex"
              style={{ placeItems: "center" }}
            >
              Đóng
            </Button>
          </>
        }
      />

      <PreviewModal
        show={dataModal.isOpen}
        header="Thông báo"
        body="Bạn có chắc chắn lưu các thay đổi?"
        onChangeVisible={() =>
          setDataModal({
            isOpen: false,
            isVerify: false,
            isSave: false,
          })
        }
        size="sm"
        footer={
          <>
            <Button
              variant="primary"
              onClick={handleImport}
              disabled={!!listItems && listItems.length === 0}
              className="flex"
              style={{ placeItems: "center", marginRight: "10px" }}
            >
              Xác nhận
            </Button>
            <Button
              variant="secondary"
              onClick={() => {
                setDataModal({
                  isOpen: false,
                  isVerify: false,
                  isSave: false,
                });
              }}
              className="flex"
              style={{ placeItems: "center" }}
            >
              Đóng
            </Button>
          </>
        }
      />
    </>
  );
}

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <>
        <input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    );
  }
);

const Table = ({ columns }) => {
  const { listItems } = useSelector((state) => state[name]);
  const dispatch = useDispatch();

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,

    globalFilter,
    setGlobalFilter,
    selectedFlatRows,
    state: { selectedRowIds },
  } = useTable(
    {
      columns,
      data: listItems,
      manualPagination: true,
    },
    useGlobalFilter,
    usePagination,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: "selection",
          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),
          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          ),
        },
        ...columns,
        {
          id: "button",
          Header: () => "",
          Cell: ({ row: { original } }) => {
            return (
              <Button
                onClick={() =>
                  dispatch(actions.DELETE_ROW_TABLE_PREVIEW(original.index))
                }
                className="btn-icon btn-danger"
              >
                <i className="feather icon-trash-2 cell-p" />
              </Button>
            );
          },
        },
      ]);
    }
  );

  useEffect(() => {
    if (selectedFlatRows.length > 0) {
      dispatch(
        actions.SELECT_ROW_TABLE_REVIEW(selectedFlatRows.map((o) => o.original))
      );
    }
  }, [selectedRowIds]);
  return (
    <>
      <Row className="mb-3">
        <Col>
          <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
        </Col>
        <Col
          className="mt-3"
          xs={12}
          style={{ overflow: "scroll", height: "60vh" }}
        >
          <BTable striped bordered hover responsive {...getTableProps()}>
            <thead style={{ position: "sticky", top: 0 }}>
              {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column, index) => (
                    <th
                      className=""
                      {...column.getHeaderProps()}
                      style={{
                        minWidth: index > 0 && "150px",
                        textAlign: "center",
                        color: "#000",
                      }}
                    >
                      {column.render("Header")}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody striped {...getTableBodyProps()}>
              {page.map((row, i) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell, index) => (
                      <td
                        {...cell.getCellProps()}
                        style={{
                          overflowX:
                            index !== parseInt(row.cells.length - 1) &&
                            "hidden",
                          textOverflow: "ellipsis",
                          maxWidth: "300px",
                        }}
                      >
                        {cell.render("Cell")}
                      </td>
                    ))}
                  </tr>
                );
              })}
            </tbody>
          </BTable>
        </Col>
      </Row>
    </>
  );
};
