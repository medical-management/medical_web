import React, { useEffect, useState } from "react";
import { Row, Col, Card, Pagination, SplitButton, Dropdown, Button } from "react-bootstrap";
import BTable from "react-bootstrap/Table";
import { useTable, usePagination, useRowSelect } from "react-table";
import { useQuery, useQueryClient } from "react-query";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { useMutation } from "react-query";

import { GlobalFilter } from "../../../views/tables/react-table/GlobalFilter";
import { APIS_FIND_ALL_EMPLOYEE_INTEGRATION } from "../apis";
import { DEFAULT_VALUE } from "../../../config/constant";
import UploadCSV from "./UploadCSV";
import * as actions from "../actions";
import { APIS_INTEGRATE_EMPLOYEE } from "../apis";
import ConfirmModal from "../../../components/ConfirmModal";
import { name } from "../reducer";
import { useToasts } from "react-toast-notifications";

import { genderOption, buttOption } from "../utils";
import * as lang from "../Lang";
const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
        resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
        <>
            <input type="checkbox" ref={resolvedRef} {...rest} />
        </>
    );
});

function Table({ columns, onSubmit, disablebBtn }) {
    const { employees, paginationEmployee } = useSelector((state) => state[name]);

    const dispatch = useDispatch();
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        selectedFlatRows,
        state: { selectedRowIds },
    } = useTable(
        {
            columns,
            data: employees,
            manualPagination: true,
        },
        usePagination,
        useRowSelect,
        (hooks) => {
            hooks.visibleColumns.push((columns) => [
                {
                    id: "selection",
                    Header: ({ getToggleAllRowsSelectedProps }) => (
                        <div>
                            <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
                        </div>
                    ),
                    Cell: ({ row }) => (
                        <div>
                            <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                        </div>
                    ),
                },
                ...columns,
            ]);
        }
    );

    function handleSearch(e) {
        if (e)
            dispatch(
                actions.HANDLE_PAGINATION_TABLE({
                    ...paginationEmployee,
                    skip: 0,
                    firstname: e,
                })
            );
        else dispatch(actions.HANDLE_PAGINATION_TABLE({ ...paginationEmployee, skip: 0 }));
    }

    function handleChangeLimit(e) {
        const skip = e.target.value && Number(e.target.value) > 1 ? Number(e.target.value) - 1 : 0;
        dispatch(actions.HANDLE_PAGINATION_TABLE({ ...paginationEmployee, skip }));
    }

    function handleChangePage(skip) {
        dispatch(actions.HANDLE_PAGINATION_TABLE({ ...paginationEmployee, skip }));
    }

    useEffect(() => {
        if (selectedFlatRows.length > 0) {
            dispatch(actions.SELECT_ROW(selectedFlatRows.map((o) => o.original)));
        }
    }, [selectedRowIds]);

    return (
        <>
            <Row className="mb-3">
                <Col md={5} className="d-flex justify-content-start align-items-center">
                    <GlobalFilter setFilter={handleSearch} placeholder="Tìm kiếm theo họ, tên" />
                </Col>
                <Col className="d-flex justify-content-end align-items-center" md={7}>
                    <UploadCSV />
                </Col>
                <Col className="d-flex align-items-center mt-4" md={6}>
                    SL
                    <select
                        className="form-control w-auto mx-2"
                        value={paginationEmployee.take}
                        onChange={(e) =>
                            dispatch(
                                actions.HANDLE_PAGINATION_TABLE({
                                    ...paginationEmployee,
                                    skip: 0,
                                    take: Number(e.target.value),
                                })
                            )
                        }
                    >
                        {[5, 10, 20, 30, 40, 50].map((o) => (
                            <option key={o} value={o}>
                                {o}
                            </option>
                        ))}
                    </select>
                </Col>
                <Col className="d-flex  mt-4 justify-content-end">
                    <Button
                        variant="success"
                        onClick={() => onSubmit("verifyall")}
                        disabled={disablebBtn}
                        className="flex"
                        style={{ placeItems: "center", marginRight: "10px" }}
                    >
                        Duyệt tất cả
                    </Button>
                    <Button
                        variant="danger"
                        onClick={() => onSubmit("deleteall")}
                        disabled={disablebBtn}
                        className="flex"
                        style={{ placeItems: "center", marginRight: "10px" }}
                    >
                        Xóa tất cả
                    </Button>
                    <Button
                        variant="primary"
                        onClick={() => onSubmit("custom")}
                        disabled={disablebBtn}
                        className="flex"
                        style={{ placeItems: "center", marginRight: "10px" }}
                    >
                        Áp dụng tùy chỉnh
                    </Button>
                </Col>
                <Col xs={12} style={{ overflow: "scroll", height: "50vh" }}>
                    <BTable striped bordered hover responsive {...getTableProps()}>
                        <thead>
                            {headerGroups.map((headerGroup) => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map((column) => (
                                        <th {...column.getHeaderProps()} style={{ textAlign: "center" }}>
                                            <b style={{ color: "#000" }}>{column.render("Header")}</b>
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        <tbody {...getTableBodyProps()}>
                            {page.map((row) => {
                                prepareRow(row);
                                return (
                                    <tr {...row.getRowProps()}>
                                        {row.cells.map((cell) => (
                                            <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                        ))}
                                    </tr>
                                );
                            })}
                        </tbody>
                    </BTable>
                </Col>
            </Row>

            <Row className="justify-content-between mt-3">
                <Col sm={12} md={6}>
                    <span className="d-flex align-items-center">
                        Trang {paginationEmployee.skip + 1} of {paginationEmployee.totalPage} | Đến Trang:
                        <input
                            type="number"
                            className="form-control ml-2"
                            defaultValue={paginationEmployee.skip + 1}
                            onChange={handleChangeLimit}
                            style={{ width: "100px" }}
                        />
                    </span>
                </Col>
                <Col sm={12} md={6}>
                    <Pagination className="justify-content-end">
                        <Pagination.First
                            onClick={() => handleChangePage(0)}
                            disabled={!paginationEmployee.canPreviousPage}
                        />
                        <Pagination.Prev
                            onClick={() => handleChangePage(paginationEmployee.previousPage)}
                            disabled={!paginationEmployee.canPreviousPage}
                        />
                        <Pagination.Next
                            onClick={() => handleChangePage(paginationEmployee.nextPage)}
                            disabled={!paginationEmployee.canNextPage}
                        />
                        <Pagination.Last
                            onClick={() => handleChangePage(paginationEmployee.totalPage - 1)}
                            disabled={!paginationEmployee.canNextPage}
                        />
                    </Pagination>
                </Col>
            </Row>
        </>
    );
}

function App() {
    const { isEnabledEmployee, paginationEmployee, isChangeTBLEmployee, selectedRows, selectedOne } = useSelector(
        (state) => state[name]
    );
    const dispatch = useDispatch();
    const history = useHistory();

    const mutationImportEmployee = useMutation("integrate_employee", APIS_INTEGRATE_EMPLOYEE, { enabled: false });
    const queryClient = useQueryClient();
    const queryEmployee = useQuery(
        ["find_all_employee_integration", paginationEmployee],
        () => APIS_FIND_ALL_EMPLOYEE_INTEGRATION(paginationEmployee),
        { enabled: isEnabledEmployee }
    );

    const [dataModal, setDataModal] = useState({
        isVerify: false,
        isDel: false,
        isOpen: false,
    });

    const [actionSubmit, setActionSubmit] = useState("custom");

    const columns = React.useMemo(
        () => [
            {
                Header: "Thông tin chính",
                columns: [
                    {
                        Header: "Họ và Tên",
                        accessor: (row) => (
                            <span className="d-inline-block text-truncate">
                                {row?.lastname} {row?.firstname}
                            </span>
                        ),
                    },
                    {
                        Header: "Giới Tính",
                        accessor: (row) => <span>{genderOption[row.gender]}</span>,
                    },

                    {
                        Header: "Ngày Sinh ",
                        accessor: (row) => (
                            <span>{row.dob !== DEFAULT_VALUE ? moment(row.dob).format("DD/MM/YYYY") : ""}</span>
                        ),
                    },
                ],
            },
            {
                Header: "Liên Hệ",
                columns: [
                    {
                        Header: "Email",
                        accessor: "email",
                    },
                    {
                        Header: "Điện Thoại",
                        accessor: "phone",
                    },
                    {
                        Header: "Địa chỉ",
                        accessor: (row) => (
                            <span className="d-inline-block text-truncate" style={{ maxWidth: "150px" }}>
                                {row.address}
                            </span>
                        ),
                    },
                ],
            },
            {
                Header: "Thông tin khác",
                columns: [
                    {
                        Header: "Ghi Chú",
                        accessor: (row) => (
                            <span className="d-inline-block text-truncate" style={{ maxWidth: "150px" }}>
                                {row.note}
                            </span>
                        ),
                    },
                    {
                        Header: " ",
                        accessor: (row, index) => (
                            <SplitButton
                                title={lang.buttonLang[row.button]}
                                variant={row.button === buttOption.verify ? "success" : "danger"}
                                id={`dropdown-split-variants-`}
                                defaultValue={row.button}
                                onClick={async (e) => dispatch(actions.SELECT_ONE(row))}
                                className="mr-2 mb-2 text-capitalize"
                            >
                                <Dropdown.Item
                                    onClick={() =>
                                        dispatch(
                                            actions.HANDLE_DROP_BTN_TABLE({
                                                value: buttOption.verify,
                                                index: row.id,
                                            })
                                        )
                                    }
                                >
                                    {lang.buttonLang.verify}
                                </Dropdown.Item>
                                <Dropdown.Item
                                    onClick={() =>
                                        dispatch(
                                            actions.HANDLE_DROP_BTN_TABLE({
                                                value: buttOption.delete,
                                                index: row.id,
                                            })
                                        )
                                    }
                                >
                                    {lang.buttonLang.delete}
                                </Dropdown.Item>
                            </SplitButton>
                        ),
                    },
                ],
            },
        ],
        [selectedRows]
    );

    const prefetchQuery = async () =>
        await queryClient.prefetchQuery(["find_all_employee_integration", paginationEmployee], () =>
            APIS_FIND_ALL_EMPLOYEE_INTEGRATION(paginationEmployee)
        );

    const handleImport = () => {
        if (!!selectedOne) {
            mutationImportEmployee.mutate({
                data: [
                    {
                        ...selectedOne,
                        dob: moment(selectedOne.dob).utc().format(),
                        gender: selectedOne.gender === "Nam" ? "male" : "female",
                        username: selectedOne.email,
                        password: selectedOne.email,
                    },
                ],
                isverify: true,
                fileName: "",
                inactive: dataModal.isDel,
            });
        }
        if (selectedRows.length > 0) {
            const listRowVerify = selectedRows
                .filter(
                    ({ button }) =>
                        (actionSubmit === "custom" && button === buttOption.verify) || actionSubmit === "verifyall"
                )
                .map(({ button, index, ...employee }) => ({
                    ...employee,
                    dob: moment(employee.dob).utc().format(),
                    gender: employee.gender === "Nam" ? "male" : "female",
                    username: employee.email,
                    password: employee.email,
                }));
            const listRowDelete = selectedRows
                .filter(
                    ({ button }) =>
                        (actionSubmit === "custom" && button === buttOption.delete) || actionSubmit === "deleteall"
                )
                .map(({ button, index, ...employee }) => ({
                    ...employee,
                    dob: moment(employee.dob).utc().format(),
                    gender: employee.gender === "Nam" ? "male" : "female",
                    username: employee.email,
                    password: employee.email,
                }));
            if (listRowVerify.length > 0)
                mutationImportEmployee.mutate({
                    data: listRowVerify,
                    isverify: true,
                    fileName: " ",
                    inactive: false,
                });
            if (listRowDelete.length > 0)
                mutationImportEmployee.mutate({
                    data: listRowDelete,
                    isverify: true,
                    fileName: " ",
                    inactive: true,
                });
        }
    };

    useEffect(() => {
        prefetchQuery();
    }, [history.location.pathname]);

    const { addToast } = useToasts();

    useEffect(() => {
        const { isLoading, isError, isSuccess } = mutationImportEmployee;
        if (!isLoading && isSuccess) {
            addToast("Thao tác thành công!", {
                appearance: "success",
                autoDismiss: true,
            });
            setDataModal({
                isOpen: false,
                isVerify: false,
                isDel: false,
            });
        } else if (isError && !isLoading)
            addToast("Thao tác thất bại, vui lòng thử lại", {
                appearance: "error",
                autoDismiss: true,
            });
    }, [mutationImportEmployee.isLoading]);

    useEffect(() => {
        const { isLoading, data, isSuccess, isError, error } = queryEmployee;
        if (!isLoading && isSuccess) {
            // console.log({ data });
            dispatch(
                actions.FIND_ALL_DATA_SUCCESS({
                    data: data.find_all_employee_integration.data?.map((o) => ({
                        ...o,
                        button: buttOption.verify,
                    })),
                    pagination: {
                        take: data.find_all_employee_integration.take,
                        skip: data.find_all_employee_integration.skip,
                        total: data.find_all_employee_integration.total,
                    },
                })
            );
        } else if (!isLoading && isError) dispatch(actions.FIND_ALL_DATA_FAILED(error));
        prefetchQuery();
        setDataModal({
            isOpen: false,
            isVerify: false,
            isDel: false,
        });
    }, [queryEmployee.isLoading, queryEmployee.isSuccess]);

    useEffect(() => {
        if (isChangeTBLEmployee) prefetchQuery();
    }, [paginationEmployee, isChangeTBLEmployee]);

    useEffect(() => {
        if (selectedOne)
            setDataModal({
                isOpen: true,
                isVerify: selectedOne.button === buttOption.verify,
                isDel: selectedOne.button === buttOption.delete,
            });
    }, [selectedOne]);

    // console.log(history);
    return (
        <React.Fragment>
            <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            <h5>Quản lý danh sách nhân viên tích hợp</h5>
                        </Card.Header>
                        <Card.Body>
                            <Table
                                columns={columns}
                                onSubmit={(value) => {
                                    setDataModal({
                                        isOpen: true,
                                        isVerify: true,
                                        isDel: false,
                                    });
                                    setActionSubmit(value);
                                }}
                                disablebBtn={!selectedRows.length || mutationImportEmployee.isLoading}
                            />
                        </Card.Body>
                    </Card>
                </Col>
                <ConfirmModal
                    content={"Bạn có chắc chắn lưu các thay đổi?"}
                    show={dataModal.isOpen}
                    onChangeVisible={() => {
                        setDataModal({
                            isOpen: false,
                            isVerify: false,
                            isDel: false,
                        });
                        dispatch(actions.SELECT_ONE_SUCCESS());
                    }}
                    onConfirm={handleImport}
                    loading={mutationImportEmployee.isLoading}
                />
            </Row>
        </React.Fragment>
    );
}

export default App;
