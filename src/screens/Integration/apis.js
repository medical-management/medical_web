/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import graphQLClient from "../../services/graphql";
import moment from "moment";

export const APIS_FIND_ALL_EMPLOYEE_INTEGRATION = async (pagination) => {
    const request = [];
    for (const key in pagination) {
        if (pagination[key] && (key === "take" || key === "skip" || key === "firstname"))
            request.push(key === "firstname" ? ` ${key}: "${pagination[key]}"` : ` ${key}: ${pagination[key]}`);
    }
    return await graphQLClient({
        queryString: `
    query {
        find_all_employee_integration(req: { ${request.join()} }) {
            data {
                id
                firstname
                lastname
                gender
                address
                cardID
                email
                email
                phone
                role
                note
                dob
            }
            take
            skip
            nextPage
            total
        }
    }
    `,
    });
};

export const APIS_INTEGRATE_EMPLOYEE = async ({ data, isverify, fileName, inactive }) => {
    let input = [];
    for (const item of data) {
        let dataConvert = [];
        for (const key in item) {
            if (key === "button" || key === "index") continue;
            if (key === "gender") dataConvert.push(` ${key}: "${item[key] === "Nam" ? "male" : "female"}" `);
            else if (key === "dob" || key === "identityDate")
                dataConvert.push(` ${key}: "${moment(item[key]).utc().format()}" `);
            else dataConvert.push(` ${key}: "${item[key]}" `);
        }
        input.push(`{${dataConvert.join()}}`);
    }
    return await graphQLClient({
        queryString: ` 
      mutation {
        integrate_employee(integrate: {
          data: [${input.join()}],
          isverify: ${isverify},
          fileName: "${fileName}"
          inactive: ${inactive}
        }) {
        id
      }
    }`,
    });
};

//////////////////////////////ITEM

export const APIS_FIND_ALL_ITEM_INTEGRATION = async (pagination) => {
    const request = [];
    for (const key in pagination) {
        if (pagination[key] && (key === "take" || key === "skip" || key === "name"))
            request.push(key === "name" ? ` ${key}: "${pagination[key]}"` : ` ${key}: ${pagination[key]}`);
    }
    return await graphQLClient({
        queryString: `
    query {
        find_all_item_integration(req: { ${request.join()} }) {
            data {
              recordType
              id
              name
              shortName
              sku
              upc
              vendor
              category
              note
              description
              startDate
              endDate
              seri
              bin
              salePrices
              purchasePrices
              tax
              taxPrices
            }
            take
            skip
            nextPage
            total
        }
    }
    `,
    });
};

export const APIS_INTEGRATE_ITEM = async ({ data, isverify, fileName, inactive }) => {
    let input = [];
    for (const item of data) {
        let dataConvert = [];

        for (const key in item) {
            if (key === "button" || key === "index") continue;
            else if (key === "startDate" || key === "endDate")
                dataConvert.push(` ${key}: "${moment(item[key]).utc().format()}" `);
            else if (
                key === "id" ||
                key === "salePrices" ||
                key === "purchasePrices" ||
                key === "taxPrices" ||
                key === "close"
            )
                dataConvert.push(` ${key}: ${item[key]} `);
            else dataConvert.push(` ${key}: "${item[key]}" `);
        }
        input.push(`{${dataConvert.join()}}`);
    }

    return await graphQLClient({
        queryString: ` 
      mutation {
        integrate_item(integrate: {
          data: [${input}],
          isverify: ${isverify},
          fileName: "${fileName}"
          inactive: ${inactive}
        }) {
        id
      }
    }`,
    });
};

/////////////////////////PATIENT

export const APIS_FIND_ALL_PATIENT_INTEGRATION = async (pagination) => {
    const request = [];
    for (const key in pagination) {
        if (pagination[key] && (key === "take" || key === "skip" || key === "firstname"))
            request.push(key === "firstname" ? ` ${key}: "${pagination[key]}"` : ` ${key}: ${pagination[key]}`);
    }
    return await graphQLClient({
        queryString: `
    query {
        find_all_patient_integration(req: { ${request.join()} }) {
          data {
            id
            firstname,
            lastname,
            dob
            phone,
            email,
            gender,
            address,
            city,
            province,
            nation,
            cardID,
            identityDate
            identityNumber,
            identityLocation,
          }
          take
          skip
          total
          nextPage
        }
    }
    `,
    });
};

export const APIS_INTEGRATE_PATIENT = async ({ data, isverify, fileName, inactive }) => {
    let input = [];
    for (const item of data) {
        let dataConvert = [];

        for (const key in item) {
            if (key === "button" || key === "index") continue;
            else if (key === "gender")
                dataConvert.push(` ${key}: "${item[key].toUpperCase() === "Nam" ? "male" : "female"}" `);
            else if (key === "dob" || key === "indentityDate")
                dataConvert.push(` ${key}: "${moment(item[key]).utc().format() || moment(item[key]).utc().format()}" `);
            else if (key === "id" || key === "height" || key === "weight") dataConvert.push(` ${key}: ${item[key]} `);
            else dataConvert.push(` ${key}: "${item[key]}" `);
        }
        input.push(`{${dataConvert.join()}}`);
    }

    return await graphQLClient({
        queryString: ` 
      mutation {
        integrate_patient(integrate: {
          data: [${input}],
          isverify: ${isverify},
          fileName: "${fileName}"
          inactive: ${inactive}
        }) {
        id
      }
    }`,
    });
};
