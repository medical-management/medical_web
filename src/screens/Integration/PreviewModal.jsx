import { Modal } from "react-bootstrap";

const PreviewModal = ({ show, onChangeVisible, header, body, footer, size = "xl" }) => (
    <Modal centered show={show} onHide={onChangeVisible} size={size}>
        <Modal.Header closeButton>
            <Modal.Title as="h5">{header}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{body}</Modal.Body>
        {!!footer && <Modal.Footer>{footer}</Modal.Footer>}
    </Modal>
);

export default PreviewModal;
