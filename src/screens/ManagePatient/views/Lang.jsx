/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */

export const FORM_CHANGE_INFO = {
  title: "Lưu thông tin bệnh nhân",
  email: "Email",
  cardID: "CMND/CCCD",
  firstname: "Tên",
  lastname: "Họ và tên lót",
  phone: "Số điện thoại",
  gender: "Giới tính",
  address: "Địa chỉ",
  city: "Thành phố",
  province: "Tỉnh",
  nation: "Quốc tịch",
  dob: "Ngày sinh",
  processBtn: "Lưu",
  deleteBtn: "Xoá",
  cancelBtn: "Hủy",
  refill: "REFILL",
  heartbeat: "Nhịp tim",
  leftEye: "Mắt trái",
  rightEye: "Mắt phải",
  bmi: "Chỉ số BMI",
  bloodpressure: "Huyết áp (Tâm thu/Tâm trương)",
  note: "Chi tiết các bệnh lý khác",
  height: "Chiều cao",
  weight: "Cân nặng",
  ////khám lâm sàng
  respire: "Hô hấp",
  digestive: "Tiêu hóa",
  circulatorySystem: "Tuần hoàn",
  endocrineSystem: "Nội tiết",
  genitourinarySystem: "Sinh dục, tiết niệu",
  osteoarthritis: "Cơ xương khớp",
  nerveSystem: "Thần kinh",
  mental: "Tâm thần",
  surgery: "Ngoại khoa",
  obstetric: "Sản phụ khoa",
  upperTeeth: "Hàm trên",
  lowerTeeth: "Hàm dưới",
  dermatology: "Da liễu",
  //Cận lâm sàng
  redQty: "Số lượng hồng cầu",
  whiteQty: "Số lương bạch cầu",
  pateletQty: "Số lượng tiểu cầu",
  bloodSugar: "Đường trong máu",
  ure: "Urê",
  creatinin: "Creatinin",
  got: "ASAT(GOT)",
  gpt: "ALAT(GPT)",
  urineSugar: "Đường trong nước tiểu",
  protein: "Protein",
  image: "Chuẩn đoán hình ảnh",
  morphin: "Test Morphin/heroin",
  amphetamin: "Test Amphetamin",
  marijuana: "Test Marijuana (Cần sa)",
};
