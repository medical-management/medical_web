/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import _ from "lodash";
import moment from "moment";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import ConfirmModal from "../../../components/ConfirmModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import Address from "../../../constants/address.json";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { APIS_INTEGRATE_PATIENT } from "../../Integration/apis";
import * as actions from "../actions";
import {
    APIS_FIND_ONE_PATIENT,
    APIS_FIND_ONE_PATIENT_INTEGRATION,
    APIS_SAVE_PATIENT,
    APIS_FIND_PATIENT_CHECK,
} from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";
import { useToasts } from "react-toast-notifications";

export default function ChangeProfile() {
    const history = useHistory();
    const params = useParams();
    return (
        <React.Fragment>
            <Breadcrumb />
            <Card style={{ padding: 30 }}>
                <Card.Header className="header-card relative">
                    <p
                        className="feather icon-chevron-left icon"
                        onClick={() => history.goBack()}
                        style={{ marginLeft: "-30px" }}
                    />
                    <Card.Title as="h4" className="title">
                        {params?.id === "add" ? "Thêm bệnh nhân" : FORM_CHANGE_INFO.title}
                    </Card.Title>
                    {params?.id !== "add" && (
                        <NavLink to={`/app/dashboard/patients/${params?.id}/medical`}>
                            <Button className="margin-0 absolute" style={{ right: "0" }}>
                                Hồ sơ bệnh án
                            </Button>
                        </NavLink>
                    )}
                </Card.Header>
                <SubmitValidationForm />
            </Card>
        </React.Fragment>
    );
}

let SubmitValidationForm = (props) => {
    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    const watchInfo = watch(["firstname", "lastname", "dob", "phone"]);

    const check = (value) => {
        return Boolean(value);
    };

    const params = useParams();

    const history = useHistory();

    const dispatch = useDispatch();
    const { addToast } = useToasts();

    const [patientQuery, setPatientQuery] = useState(null);

    const { patient, isLoading } = useSelector((state) => state["PATIENTS"]);
    const [disableField, setDisableField] = useState(false);

    const queryPatient = useQuery(
        ["user", params],
        () => params.id !== "add" && APIS_FIND_ONE_PATIENT(params)
        // {enabled: params?.id !== 'add' && !!!employee}
    );

    // const [firstname, setFirstname] = useState(null);
    // const [lastname, setLastname] = useState(null);
    // const [phone, setPhone] = useState(null);
    // const [dob, setDob] = useState(null);

    const queryCheck = useQuery(["check", patientQuery], () => APIS_FIND_PATIENT_CHECK(patientQuery), {
        enabled: watchInfo.every(check) && params?.id === "add",
    });

    const queryPatientIntegration = useQuery(
        ["patientIntegration", params],
        () => APIS_FIND_ONE_PATIENT_INTEGRATION(params),
        {
            enabled: params.id !== "add" && history?.location?.search === "?integration=true",
        }
    );
    const queryClient = useQueryClient();
    const mutationChangeProfile = useMutation("save", APIS_SAVE_PATIENT);
    const mutationIntegration = useMutation("inte", APIS_INTEGRATE_PATIENT);
    const mutationAddProfile = useMutation("add", APIS_SAVE_PATIENT);

    const { user } = useSelector((state) => state["AUTHENTICATION"]);
    useEffect(() => {
        if (user && user?.decentralization?.length && user?.decentralization[0].patients < 2) {
            setDisableField(true);
        }
    }, [user]);
    const cityList = _.sortedUniq(Address.address.map((item) => item.city)).map((item) => {
        return {
            value: item,
            label: item,
        };
    });

    const [selected, setSelected] = useState(null);
    const [isVisible, setIsVisible] = useState(false);
    const [visibleConfirm, setVisibleConfirm] = useState(false);

    const typeAction = params?.id === "add" ? "add" : "edit";

    const [selectedCity, setSelectedCity] = useState("");
    const [selectedProvince, setSelectedProvince] = useState("");
    const [selectedWard, setSelectedWard] = useState("");

    const [listProvince, setListProvince] = useState([]);
    const [listWard, setListWard] = useState([]);

    const [gender, setGender] = useState("male");

    const [payload, setPayload] = useState(null);

    const [note, setNote] = useState(null);

    useEffect(() => {
        if (
            user &&
            (user?.decentralization == null ||
                (user?.decentralization?.length && user?.decentralization[0]?.patients === 0))
        ) {
            alert("Bạn không có quyền thao tác");
            history.push("/profile");
        }
    }, [user]);

    useEffect(() => {
        if (params.id !== "add") {
            if (history?.location?.search === "?integration=true") queryClient.prefetchQuery(queryPatientIntegration);
            else queryClient.prefetchQuery(queryPatient);
        }
        if (params.id === "add") {
            dispatch(actions.GET_PATIENT_INFO_SUCCESS(null));
            setNote(null);
        }
    }, [params.id]);

    useEffect(() => {
        const { isSuccess, isLoading, data } = queryPatient;
        if (isSuccess && !isLoading && data !== false) {
            dispatch(actions.GET_PATIENT_INFO_SUCCESS(queryPatient?.data?.find_one_patient));
            // setNote(queryPatient?.data?.find_one_patient?.note);
            setGender(queryPatient?.data?.find_one_patient?.gender);
        } else {
            setNote(null);
        }
    }, [queryPatient.isLoading]);

    useEffect(() => {
        if (queryPatientIntegration?.isSuccess && !queryPatientIntegration.isLoading && params.id !== "add") {
            dispatch(actions.GET_PATIENT_INFO_SUCCESS(queryPatientIntegration?.data?.find_one_patient_integration));
        }
    }, [queryPatientIntegration.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationChangeProfile;

        if (!isLoading) {
            if (!isError && !!data) {
                setVisibleConfirm(false);
                addToast("Cập nhật thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
            } else if (isError)
                addToast("Cập nhật thông tin thất bại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
        }
    }, [mutationChangeProfile.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError, status } = mutationIntegration;

        if (!isLoading) {
            if (!isError && !!data) {
                setVisibleConfirm(false);
                addToast("Cập nhật thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
            } else if (isError || status === "error")
                addToast("Cập nhật thông tin thất bại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
        }
    }, [mutationIntegration.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError, status } = mutationAddProfile;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Thêm thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });

                history.push("/app/dashboard/patients");
                setVisibleConfirm(false);
            } else if (status === "error")
                addToast("Thêm thông tin thất bại! Vui lòng thử lại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
        }
    }, [mutationAddProfile.isLoading]);

    useEffect(() => {
        if (params.id !== "add" && patient) {
            setSelectedCity(patient?.city);
            setSelectedProvince(patient?.province);

            Object.entries(patient).forEach(([key, value]) => {
                setValue(key, value);
            });
            setGender(patient?.gender);

            // setNote(patient?.note);
        }
    }, [patient]);

    useEffect(() => {
        if (watchInfo.every(check)) {
            let firstname = watchInfo[0];
            let lastname = watchInfo[1];
            let dob = watchInfo[2];
            let phone = watchInfo[3];
            queryClient.prefetchQuery(queryCheck);
        }
    }, [watchInfo]);

    useLayoutEffect(() => {
        const { isLoading, data, isError } = queryCheck;
        if (!isLoading) {
            if (!isError) {
                console.log(data);
            }
        }
    }, [queryCheck.isLoading]);

    const submit = (values) => {
        if (history?.location?.search !== "?integration=true") {
            if (params.id !== "add") {
                setPayload({
                    input: {
                        ...values,
                        recordType: "patient",
                        dob: moment(values?.dob).utc().format(),
                        identityDate: values?.identityDate ? moment(values?.identityDate).utc().format() : "",
                        province: selectedProvince,
                        city: selectedCity,
                        gender: gender,
                        // ward: selectedWard,
                        // note: note,
                        // height: parseFloat(values?.height?.toString().replace(/\./g, "")),
                        // weight: parseFloat(values?.weight?.toString().replace(/\./g, "")),
                    },
                });
            } else {
                setPayload({
                    input: {
                        ...values,
                        recordType: "patient",
                        id: "",
                        dob: moment(values?.dob).utc().format(),
                        identityDate: values?.identityDate ? moment(values?.identityDate).utc().format() : "",
                        province: selectedProvince,
                        city: selectedCity,
                        // ward: selectedWard,
                        gender: gender,
                        // note: note,
                        // height: parseFloat(values?.height?.toString().replace(/\./g, "")),
                        // weight: parseFloat(values?.weight?.toString().replace(/\./g, "")),
                    },
                });
            }
        } else
            setPayload({
                data: [
                    {
                        ...values,
                        recordType: "patient",
                        dob: moment(values?.dob).utc().format(),
                        identityDate: values?.identityDate ? moment(values?.identityDate).utc().format() : "",
                        province: selectedProvince,
                        city: selectedCity,
                        ward: selectedWard,
                        gender: gender,
                        // note: note,
                        // height: parseFloat(values?.height?.toString().replace(/\./g, "")),
                        // weight: parseFloat(values?.weight?.toString().replace(/\./g, "")),
                    },
                ],
                isverify: false,
                inactive: false,
                fileName: `patient-${moment().valueOf()}`,
            });
    };

    useEffect(() => {
        if (payload !== null) setVisibleConfirm(true);
    }, [payload]);

    useEffect(() => {
        if (!visibleConfirm) setPayload(null);
    }, [visibleConfirm]);

    const handleChooseCity = (value) => {
        setSelectedCity(value);
        setSelectedWard(null);
        setListWard([]);
        setListProvince([]);
        setSelectedProvince(null);
        if (value !== null) {
            setSelectedCity(value);
            setListProvince(
                _.uniqBy(
                    Address.address.map((item) => {
                        if (item.city === value) {
                            return {
                                label: item.province,
                                value: item.province,
                            };
                        }
                    }),
                    function (e) {
                        if (e) return e.label;
                    }
                ).filter((n) => n)
            );
        } else setSelectedCity("");
    };

    const handleChooseProvince = (value) => {
        setSelectedWard(null);
        setListWard([]);
        if (value !== null) {
            setSelectedProvince(value);
            setListWard(
                _.uniqBy(
                    Address.address.map((item) => {
                        if (item.province === value && item.city === selectedCity) {
                            return {
                                label: item.ward,
                                value: item.ward,
                            };
                        }
                    }),
                    function (e) {
                        if (e) return e.label;
                    }
                ).filter((n) => n)
            );
        } else setSelectedProvince("");
    };

    useEffect(() => {
        handleChooseCity(selectedCity);
    }, [selectedCity]);

    useEffect(() => {
        handleChooseProvince(selectedProvince);
    }, [selectedProvince]);

    useEffect(() => {
        setSelectedWard(patient?.ward);
    }, [selectedWard]);

    useEffect(() => {
        if (selected !== null) {
            setIsVisible(true);
        }
    }, [selected]);

    const onSavePatient = () => {
        if (history?.location?.search !== "?integration=true") {
            if (typeAction === "add") {
                mutationAddProfile.mutate(payload);
            } else if (typeAction === "edit") {
                mutationChangeProfile.mutate(payload);
            }
        } else if (history?.location?.search === "?integration=true") {
            mutationIntegration.mutate(payload);
        }
    };

    return (
        //  <form onSubmit={handleSubmit(submit)}>
        <form onSubmit={handleSubmit(submit)}>
            <SessionTitle>Thông tin chính</SessionTitle>
            <Row>
                <Col sm={6}>
                    <Controller
                        {...register("lastname", { required: true })}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                required
                                disabled={disableField}
                                onBlurInput={(e) => setPatientQuery({ ...patientQuery, lastname: e.target.value })}
                                label={FORM_CHANGE_INFO.lastname}
                                {...field}
                                error={errors?.lastname ? "Vui lòng nhập Họ" : ""}
                            />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("firstname", { required: true })}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                required
                                disabled={disableField}
                                onBlurInput={(e) =>
                                    setPatientQuery({
                                        ...patientQuery,
                                        firstname: e.target.value,
                                    })
                                }
                                label={FORM_CHANGE_INFO.firstname}
                                error={errors?.firstname ? "Vui lòng nhập Tên" : ""}
                                {...field}
                            />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("phone", { required: true })}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                label={FORM_CHANGE_INFO.phone}
                                {...field}
                                disabled={disableField}
                                onBlurInput={(e) => setPatientQuery({ ...patientQuery, phone: e.target.value })}
                                required
                                error={errors?.phone ? "Vui lòng nhập Số điện thoại" : ""}
                                prefix={"(+84)"}
                            />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("dob", { required: true })}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                label={FORM_CHANGE_INFO.dob}
                                {...field}
                                required
                                disabled={disableField}
                                onBlurInput={(e) =>
                                    setPatientQuery({
                                        ...patientQuery,
                                        dob: moment(e.target.value).utc().format(),
                                    })
                                }
                                error={errors?.dob ? "Vui lòng nhập Ngày sinh" : ""}
                                type="date"
                            />
                        )}
                    />
                </Col>

                <Col sm={6}>
                    <div className="mt-10">
                        <label htmlhtmlFor="">Giới tính</label>
                        <div className="flex" style={{ marginLeft: "-10px" }}>
                            <div className="form-check">
                                <FormInput
                                    type="radio"
                                    value="male"
                                    checked={gender === "male"}
                                    disabled={disableField}
                                    // {...field}
                                    onChange={(e) => setGender(e?.target?.checked ? "male" : "female")}
                                />

                                <label className="form-check-label" htmlFor="flexRadioDefault1">
                                    Nam
                                </label>
                            </div>
                            <div className="form-check" style={{ marginLeft: "50px" }}>
                                <FormInput
                                    type="radio"
                                    value="female"
                                    disabled={disableField}
                                    onChange={(e) => setGender(!e?.target?.checked ? "male" : "female")}
                                    checked={gender === "female"}
                                    // {...field}
                                />

                                <label className="form-check-label" htmlFor="flexRadioDefault2">
                                    Nữ
                                </label>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("cardID")}
                        control={control}
                        render={({ field }) => (
                            <FormInput label={FORM_CHANGE_INFO.cardID} disabled={disableField} {...field} />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("identityLocation")}
                        control={control}
                        render={({ field }) => <FormInput disabled={disableField} label={"Nơi cấp"} {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("identityDate")}
                        control={control}
                        render={({ field }) => (
                            <FormInput label={"Ngày cấp"} disabled={disableField} {...field} type="date" />
                        )}
                    />
                </Col>
            </Row>
            <SessionTitle mt="30">Thông tin liên lạc</SessionTitle>
            <Row>
                <Col sm={12}>
                    <Controller
                        {...register("email")}
                        control={control}
                        render={({ field }) => (
                            <FormInput disabled={disableField} label={FORM_CHANGE_INFO.email} {...field} />
                        )}
                    />
                </Col>

                <Col>
                    <Row className="">
                        <Col sm={6} className="self-center">
                            <label className="label-control items-center">Tỉnh / Thành phố</label>

                            {disableField ? (
                                <FormInput value={cityList.find((o) => o.value === selectedCity)?.label} disabled />
                            ) : (
                                <Select
                                    className="basic-single"
                                    placeholder="Chọn Tỉnh/Thành phố"
                                    classNamePrefix="select"
                                    options={cityList}
                                    value={cityList.find((o) => o.value === selectedCity)}
                                    onChange={(e) => handleChooseCity(e.value)}
                                />
                            )}
                        </Col>

                        <Col sm={6} className="self-center">
                            <label className="label-control items-center">Quận / Huyện / Thành phố</label>
                            {disableField ? (
                                <FormInput
                                    value={listProvince.find((o) => o.value === selectedProvince)?.label || ""}
                                    disabled
                                />
                            ) : (
                                <Select
                                    className="basic-single"
                                    classNamePrefix="select"
                                    options={listProvince}
                                    placeholder="Chọn Quận/Huyện/Thành phố"
                                    value={listProvince.find((o) => o.value === selectedProvince) || null}
                                    onChange={(e) => handleChooseProvince(e.value)}
                                />
                            )}
                        </Col>
                        <Col sm={12}>
                            <Controller
                                {...register("address")}
                                control={control}
                                render={({ field }) => <FormInput label={FORM_CHANGE_INFO.address} {...field} />}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>

            <div className="mt-20">
                {((params?.id === "add" && user?.decentralization[0]?.items > 1) ||
                    (params?.id !== "add" && user?.decentralization[0]?.items > 2)) && (
                    <Button
                        type="submit"
                        className="mr-0 float-right"
                        disabled={queryPatient.isLoading || queryPatientIntegration.isLoading}
                    >
                        {FORM_CHANGE_INFO.processBtn}
                    </Button>
                )}
                {params?.id !== "add" && user?.decentralization[0]?.items === 4 && (
                    <Button
                        className="mr-2 float-right btn btn-danger"
                        disabled={queryPatient.isLoading || queryPatientIntegration.isLoading}
                        onClick={() => {
                            setSelected(patient);
                        }}
                    >
                        Xóa
                    </Button>
                )}
            </div>
            <ConfirmModal
                show={visibleConfirm}
                onChangeVisible={() => {
                    setVisibleConfirm(!visibleConfirm);
                }}
                onConfirm={() => onSavePatient()}
                title="Xác nhận"
                content={`Bạn có chắc chắn muốn ${typeAction === "add" ? "thêm" : "sửa"} thông tin bệnh nhân này?`}
                loading={
                    mutationAddProfile.isLoading || mutationChangeProfile.isLoading || mutationIntegration.isLoading
                }
            />
        </form>
    );
};
