import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Row,
  InputGroup,
  Dropdown,
  SplitButton,
} from "react-bootstrap";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Select from "react-select";
import CustomPagination from "../../../components/Pagination";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import * as actions from "../actions";
import { APIS_FIND_ALL_PATIENT, APIS_SAVE_PATIENT } from "../apis";
import { useToasts } from "react-toast-notifications";
import CustomDrawer from "../../../components/CustomDrawer";

function UserList() {
  const queryClient = useQueryClient();
  const { addToast } = useToasts();

  const history = useHistory();

  const dispatch = useDispatch();

  const { patients, page, pageSize, totalPage, isNextPage, isLoading } =
    useSelector((state) => state["PATIENTS"]);
  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const mutationChangeProfile = useMutation("input", APIS_SAVE_PATIENT);
  const mutationDeleteProfile = useMutation("input", APIS_SAVE_PATIENT);

  const [isVisible, setIsVisible] = useState(false);

  const [selected, setSelected] = useState(null);

  const [show, setShow] = useState(false);

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });

        // history.push("/app/admin/patients");
      } else if (isError && !!data)
        addToast("Cập nhật thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationChangeProfile.isLoading]);

  const [userList, setUserList] = useState([]);

  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);

  const [query, setQuery] = useState({
    take: pageSizes,
    skip: pages,
    inactive: false,
  });

  const queryPatients = useQuery("patients", () =>
    APIS_FIND_ALL_PATIENT(query)
  );

  useEffect(() => {
    queryClient.fetchQuery("patients", queryPatients);
  }, [query]);

  useEffect(() => {
    if (!queryPatients.isLoading && !queryPatients.isError)
      dispatch(actions.GET_ALL_PATIENT_INFO_SUCCESS(queryPatients?.data));
  }, [queryPatients.data]);

  useEffect(() => {
    setUserList(patients);
  }, [patients]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      take: pageSizes,
      skip: 0,
      firstname: e?.target[1]?.value || "",
    });
  };

  useEffect(() => {
    setQuery({ ...query, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQuery({ ...query, skip: pages });
  }, [pages]);

  useEffect(() => {
    setSelected(null);
  }, []);

  useEffect(() => {
    if (selected !== null) {
      setIsVisible(true);
    }
  }, [selected]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationDeleteProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        setIsVisible(false);
        addToast("Xóa thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
      }
    }
  }, [mutationDeleteProfile.isLoading]);

  const columns = React.useMemo(
    () => [
      {
        Header: "Thông tin cơ bản",
        columns: [
          {
            Header: "Họ và Tên",
            accessor: (row) => {
              return (
                <p
                  style={{ cursor: "pointer" }}
                  className="cell-p"
                  onClick={() =>
                    history.push(`/app/dashboard/patients/${row.id}`)
                  }
                >
                  {row?.lastname} {row?.firstname}
                </p>
              );
            },
          },
          {
            Header: "Số điện thoại",
            accessor: "phone",
          },
          {
            Header: "Email",
            accessor: "email",
          },

          {
            Header: "Ngày sinh",
            accessor: "dob",
          },
        ],
      },
      {
        Header: "Thông tin hành chính",
        columns: [
          {
            Header: "CMND/CCCD",
            accessor: (row) => {
              return (
                <p className="cell-p">
                  {row?.cardID === "undefined" || !row?.cardID
                    ? "---"
                    : row.cardID}
                </p>
              );
            },
          },
          {
            Header: "Tỉnh/Thành phố",
            accessor: "city",
          },
          {
            Header: "Quận/Huyện/TP(Tỉnh)",
            accessor: "province",
          },
        ],
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization?.length &&
            user?.decentralization[0]?.patients > 1 && (
              <div className="flex" style={{ justifyContent: "center" }}>
                <SplitButton
                  title="Chi tiết"
                  id={`dropdown-split-variants-`}
                  onClick={() =>
                    history.push(`/app/dashboard/patients/${row.id}`)
                  }
                  className="mr-2 mb-2 text-capitalize"
                >
                  <Dropdown.Item
                    onClick={() =>
                      history.push(`/app/dashboard/patients/${row.id}`)
                    }
                  >
                    Sửa
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() =>
                      history.push({
                        pathname: `/app/dashboard/medicals/add`,
                        state: row,
                      })
                    }
                  >
                    Thêm bệnh án
                  </Dropdown.Item>
                </SplitButton>
              </div>
            )
          );
        },
      },
    ],
    [user?.decentralization]
  );

  const handleAdvanceSearch = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      skip: 0,
      lastname: e.target[0].value,
      firstname: e.target[1].value,
      email: e.target[2].value,
      phone: e.target[3].value,
      // refill: e.target[5].value,
      // dob: moment(e.target[4].value).utc().format(),
      // gender: gender,
    });
    setShow(false);
  };

  // const onChangeGender = (e) => {
  //   setQuery({
  //     ...query,
  //     skip: 0,
  //     gender: e,
  //   });
  // };

  const onChangeDob = (e) => {
    setQuery({
      ...query,
      skip: 0,
      dob: e,
    });
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                gap: 20,
              }}
            >
              <Card.Title
                as="h5"
                style={{ whiteSpace: "nowrap", textAlign: "left" }}
              >
                Danh sách bệnh nhân
              </Card.Title>
              <form
                style={{ width: "100%" }}
                onSubmit={(e) => {
                  e.preventDefault();
                  setQuery({
                    ...query,
                    take: pageSizes,
                    skip: 0,
                    firstname: e?.target[0]?.value || "",
                  });
                }}
              >
                <InputGroup>
                  <input
                    placeholder={`Tìm bệnh nhân bằng Tên`}
                    className="form-control"
                    style={{ flex: 1 }}
                  />
                  <InputGroup.Append>
                    <Button
                      type="submit"
                      className="feather icon-search"
                    ></Button>
                  </InputGroup.Append>
                </InputGroup>
              </form>
              {user?.decentralization[0]?.patients > 1 && (
                <Button
                  style={{ margin: 0, whiteSpace: "nowrap" }}
                  onClick={() => history.push("/app/dashboard/patients/add")}
                >
                  Thêm bệnh nhân
                </Button>
              )}
            </Card.Header>
            <Card.Body>
              <form onSubmit={handleSubmit}>
                <Row className="mb-3">
                  <Col className="d-flex align-items-center">
                    Hiển thị
                    <select
                      className="form-control w-auto mx-2"
                      value={pageSizes}
                      onChange={(e) => {
                        setPageSizes(Number(e.target.value));
                      }}
                    >
                      {[5, 10, 20, 50, 100].map((pgsize) => (
                        <option key={pgsize} value={pgsize}>
                          {pgsize}
                        </option>
                      ))}
                    </select>
                    <p
                      className="feather icon-filter margin-0 cursor-pointer"
                      style={{
                        color: !show ? "black" : "#04a9f5",
                        fontWeight: "bold",
                        fontSize: 16,
                      }}
                      onClick={() => setShow(!show)}
                    ></p>
                  </Col>
                </Row>
                <Button className="hidden" type="submit"></Button>
              </form>
              <div className="responsive-section">
                <CustomTable
                  columns={columns}
                  data={patients || []}
                  pageSize={pageSize}
                />
              </div>
              <CustomPagination
                pageIndex={page}
                totalPage={totalPage}
                gotoPage={(e) => setPages(e)}
                canPreviousPage={page > 0}
                canNextPage={page < totalPage - 1}
              ></CustomPagination>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {show && (
        <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
          <div>
            <p
              style={{
                width: "100%",
                fontSize: 18,
                fontWeight: 700,
                borderBottom: "1px solid #ddd",
                paddingBottom: 10,
              }}
            >
              Lọc bệnh nhân
            </p>
            <form onSubmit={handleAdvanceSearch}>
              <div
                className="flex"
                style={{ gap: "10px", flexDirection: "column" }}
              >
                <div className="self-center" style={{ flex: 1, width: "100%" }}>
                  <label className="label-control items-center">Họ</label>
                  <input
                    placeholder={`Nhập Họ`}
                    className="form-control"
                    style={{ height: "38px" }}
                    defaultValue={query?.lastname}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    flex: 1,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">Tên</label>
                  <input
                    placeholder={`Nhập Tên`}
                    className="form-control"
                    style={{ height: "38px" }}
                    defaultValue={query?.firstname}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    flex: 1,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">Email</label>
                  <input
                    placeholder={`Nhập Email`}
                    className="form-control"
                    style={{ height: "38px" }}
                  />
                </div>
                <div className="self-center" style={{ width: "100%" }}>
                  <label className="label-control items-center">
                    Số điện thoại
                  </label>
                  <input
                    placeholder={`Nhập số điện thoại`}
                    className="form-control"
                    style={{ height: "38px" }}
                    defaultValue={query?.phone}
                  />
                </div>
              </div>
              <div className="d-flex" style={{ gap: "5px", marginTop: "10px" }}>
                <div
                  className="self-center"
                  style={{
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Ngày sinh
                  </label>
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={[
                      { label: "Tăng dần", value: "ASC" },
                      { label: "Giảm dần", value: "DESC" },
                    ]}
                    value={[
                      { label: "Tăng dần", value: "ASC" },
                      { label: "Giảm dần", value: "DESC" },
                    ].find((item) => item.value === query?.dob)}
                    onChange={(e) => onChangeDob(e.value)}
                  />
                </div>
                {/* <div
                    className="self-center"
                    style={{
                      paddingRight: "10px",
                      width: "120px",
                    }}
                  >
                    <label className="label-control items-center">
                      Giới tính
                    </label>
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      options={[
                        { label: "Nam", value: "male" },
                        { label: "Nữ", value: "female" },
                      ]}
                      onChange={(e) => onChangeGender(e.value)}
                    />
                  </div> */}
                {/* <div
                    className="self-center"
                    style={{ paddingRight: "10px", }}
                  >
                    <label className="label-control items-center">Refill</label>
                    <input
                      placeholder={`Nhập refill`}
                      className="form-control"
                      style={{ paddingLeft: "10px", flex: 1, height: "38px" }}
                    />
                  </div> */}
              </div>

              <Button
                type="submit"
                style={{
                  float: "right",
                  margin: 0,
                  marginRight: "0px",
                  marginTop: 10,
                }}
              >
                Áp dụng
              </Button>
            </form>
          </div>
        </CustomDrawer>
      )}
    </React.Fragment>
  );
}

export default UserList;
