/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_PATIENT = async (input) => {
  return await graphQLClient({
    queryString: `
    query {
      find_all_patient(req: { 
      take: ${input.take}, 
      skip: ${input.skip}, 
      ${input.firstname ? `firstname: "${input.firstname}"` : ""} 
      ${input.lastname ? `lastname: "${input.lastname}"` : ""} 
      ${input.dob ? `dob: "${input.dob}"` : ""} 
      ${input.gender ? `gender: "${input.gender}"` : ""} 
      ${input.email ? `email: "${input.email}"` : ""} 
      ${input.phone ? `phone: "${input.phone}"` : ""} 
    }) {
        data {
          inactive
          createdat
          updatedat
          recordType
          id
          firstname
          lastname
          dob
          phone
          email
          gender
          address
          city
          province
          nation
          cardID
        }
        take
        skip
        total
        nextPage
      }
    }
  `,
  });
};

export const APIS_FIND_FAST_PATIENT = async (input) => {
  return await graphQLClient({
    queryString: `
    query {
      find_fast_patient(req: { 
      take: ${input.take}, 
      skip: ${input.skip}, 
      ${input.firstname ? `firstname: "${input.firstname}"` : ""} 
      ${input.lastname ? `lastname: "${input.lastname}"` : ""} 
      ${input.dob ? `dob: "${input.dob}"` : ""} 
      ${input.gender ? `gender: "${input.gender}"` : ""} 
      ${input.email ? `email: "${input.email}"` : ""} 
      ${input.phone ? `phone: "${input.phone}"` : ""} 
    }) {
          inactive
          createdat
          updatedat
          recordType
          id
          firstname
          lastname
          dob
          phone
          email
          gender
          address
          city
          province
          nation
          cardID
          identityDate
          identityLocation
      }
    }
  `,
  });
};

export const APIS_FIND_ONE_PATIENT = async (input) => {
  return await graphQLClient({
    queryString: `
      query {
        find_one_patient(id: "${input.id}") {
          inactive
          createdat
          updatedat
          recordType
          id
          firstname
          lastname
          dob
          phone
          email
          gender
          identityLocation
          identityDate
          address
          city
          province
          nation
          cardID
          }
      }
    `,
  });
};
export const APIS_FIND_PATIENT_CHECK = async ({
  lastname,
  firstname,
  dob,
  phone,
}) => {
  return await graphQLClient({
    queryString: `
      query {
        find_distin_patient(req: { 
        take: 10, 
        skip: 0, 
        ${firstname ? `firstname: "${firstname}"` : ""} 
        ${lastname ? `lastname: "${lastname}"` : ""} 
        ${dob ? `dob: "${dob}"` : ""} 
        ${phone ? `phone: "${phone}"` : ""} 
      }) {
          data{
            id
            firstname
            lastname
            dob
            phone
          }
        }
      }
    `,
  });
};

export const APIS_FIND_ONE_PATIENT_INTEGRATION = async (input) =>
  await graphQLClient({
    queryString: `
  query {
    find_one_patient_integration(id: "${input.id}") {
      inactive
      recordType
      id
      firstname
      lastname
      dob
      phone
      email
      gender
      address
      city
      province
      nation
      cardID
      }
  }
`,
  });

export const APIS_CHANGE_INFO = async ({ input }) =>
  await graphQLClient({
    queryString: `mutation {
      update_user_info(input: { 
          recordType: "${input.recordType || ""}",
          id:  "${input.id}",
          firstname: "${input.firstname || ""}",
          lastname:  "${input.lastname || ""}",
          dob:  "${input.dob || ""}",
          email:  "${input.email || ""}",
          phone:  "${input.phone || ""}",
          gender:  "${input.gender || ""}",
          address:  "${input.address || ""}",
          city:  "${input.city || ""}",
          province:  "${input.province || ""}",
          nation:  "${input.nation || ""}",
          cardID:  "${input.cardID || ""}",
          note:  "${input.note || ""}"}) {
          id
      }
  }`,
  });

export const APIS_SAVE_PATIENT = async ({ input }) => {
  let payload = [];
  for (const key in input) {
    if (key === "createdat" || key === "updatedat" || input[key] === "")
      continue;
    else if (key === "id") {
      if (input[key]) payload.push(` id: ${input[key]} `);
    } else if (["height", "weight", "inactive", "isverify"].includes(key))
      payload.push(` ${key}: ${input[key]} `);
    else if (key.includes("detail") || key.includes("insurance")) {
      if (input[key] === undefined) payload.push(` ${key}: false `);
      else payload.push(` ${key}: ${input[key]} `);
    } else if (input[key] !== undefined)
      payload.push(` ${key}: "${input[key]}" `);
  }

  return await graphQLClient({
    queryString: ` 
        mutation {
      save_patient(input:{ ${payload.join()}}) {
            id
            inactive
        }
    }`,
  });
};

export const APIS_RESET_PASSWORD = async ({ input }) =>
  await graphQLClient({
    queryString: ` 
        mutation {
          reset_password_employee(input: { 
            recordType: "${input.recordType || ""}",
            id: "${input.id}",
            username: "${input.username || ""}",
            password: "${input.password || ""}",
            email:  "${input.email || ""}",
            phone:  "${input.phone || ""}",
            gender:  "${input.gender || ""}",
          }) {
            id
        }
    }`,
  });

export const APIS_SAVE_QUESTION = async ({ input }) =>
  await graphQLClient({
    queryString: `mutation {
    save_security_questions(input: { 
        question: "${input.question || ""}",
        answer:  "${input.answer || ""}"
        }) {
        id
    }
}`,
  });

export const APIS_DELETE_EMPLOYEE = async ({ input }) =>
  await graphQLClient({
    queryString: ` 
        mutation {
      save_employee(input: { 
            recordType: "${input.recordType || ""}",
            ${input.id === "" ? "" : `id: "${input.id}",`}
            firstname: "${input.firstname || ""}",
            inactive: ${input.inactive || false},
            lastname:  "${input.lastname || ""}",
            username: "${input.username || ""}",
            password: "${input.password || ""}",
            email:  "${input.email || ""}",
            phone:  "${input.phone || ""}",
            gender:  "${input.gender || ""}",
            dob:  "${input.dob || ""}",
 }) {
            id
            inactive
        }
    }`,
  });
