/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Manager User
 */
import { lazy } from "react";

export const ROUTER_MANAGE_PATIENT = "/app/dashboard/patients";

const routers = [
  {
    exact: true,
    path: ROUTER_MANAGE_PATIENT,
    component: lazy(() => import("./views/PatientList")),
  },
  {
    path: `${ROUTER_MANAGE_PATIENT}/:id/medical/:id`,
    component: lazy(() => import("../ManageMedical/views/MedicalProfile")),
  },
  {
    path: `${ROUTER_MANAGE_PATIENT}/:id/medical`,
    component: lazy(() => import("../ManageMedical/views/OneMedicalList")),
  },
  {
    path: `${ROUTER_MANAGE_PATIENT}/:id`,
    component: lazy(() => import("./views/PatientProfile")),
  },
];
export default routers;
