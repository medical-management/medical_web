/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import { gql } from "graphql-request";
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_MEDICAL = async ({
  take,
  skip,
  name,
  firstname,
  lastname,
  category,
}) =>
  await graphQLClient({
    queryString: ` query {
      find_all_medical(req: { 
            take: ${take}, 
            skip: ${skip}, 
            ${name ? `name: "${name}"` : ""}  
            ${firstname ? `firstname: "${firstname}"` : ""}  
            ${category ? `category: "${category}"` : ""}  
            ${lastname ? `lastname: "${lastname}"` : ""}  
          
      }) {
      data {
        inactive
        createdat
        updatedat
        updatedby{
          id
          firstname
          lastname
        }
        patient {
          id
          firstname
          lastname
          cardID
        }
        results {
          createdat
          updatedat
          reqXQuang
          resultXQuang
          resultWBC
          resultRBC
          resultHP
          resultHCT
          resultMCV
          resultMCH
          resultMCHC
          resultPLT
          resultMPV
          resultRDW
          resultNeutro
          resultLym
          resultMono
          resultLEU
          resultNIT
          resultURO
          resultPRO
          resultUrePH
          resultLBD
          resultSG
          resultCentonic
          resultBIL
          resultGLU
          resultChyme
          resultPorphyrin
          resultProteinBenceJones
          resultGlucoza
          resultHbA1c
          resultUre
          resultCritine
          resultSGOT
          resultSGPT
          resultGGT
          resultHbsAg
          resultHbeAg
          resultAnitHbs
          resultAnitHcv
          resultCholestrol
          resultTriglyceric
          resultASO
          resultAFP
          resultAcidUric
          resultAnotherHP
          resultHIV
          resultTC
          resultTS
        }
        recordType
        id
        name
        status
        category
        firstname
        lastname
        dob
        phone
        email
        gender
        address
        city
        province
        nation
        cardID
        identityDate
        identityNumber
        identityLocation
        temperature
        bloodpressure1
        bloodpressure2
        heartbeat
        height
        weight
        note
        checkFamilyHistory
        reasonFamilyHistory
        checkPersonalHistory
        reasonPersonalHistory
        checkPregnancyHistory
        reasonPregnancyHistory
        checkSpecialDiseaseHistory
        reasonSpecialDiseaseHistory
        bmi
        leftEye
        rigthtEye
        respire
        digestive
        circulatorySystem
        endocrineSystem
        genitourinarySystem
        osteoarthritis
        nerveSystem
        mental
        surgery
        obstetric
        upperTeeth
        lowerTeeth
        dermatology
        detail1
        detail2
        detail3
        detail4
        detail5
        detail6
        detail7
        detail8
        detail9
        detail10
        detail11
        detail12
        detail13
        detail14
        detail15
        detail16
        detail17
        detail18
        detail19
        detail20
        detail21
        insurance1
        insurance2
        insurance3
        insurance4
        insurance5
        insurance6
        insurance7
        insurance8
        insurance9
        insurance10
        insurance11
        insurance12
        insurance13
        insurance14
        insurance15
        insurance16
        reason
        result
        reExaminate
        dateExaminate
        transaction {
          id
        }
      }
      take
      skip
      total
      nextPage
    }
  }`,
  });

export const APIS_FIND_ALL_PATIENT_MEDICAL = async ({ patient, take, skip }) =>
  await graphQLClient({
    queryString: ` query {
      find_all_medical(req: { 
            take: ${take}, 
            skip: ${skip}, 
            patient: ${patient}
          
      }) {
      data {
        inactive
        createdat
        updatedat
        updatedby{
          id
        }
        patient {
          id
          firstname
          lastname
          dob
        }
        id
        name
        status
        category
        firstname
        lastname
        dob
        phone
        email
        gender
        address
        city
        province
        nation
        cardID
        identityDate
        identityNumber
        identityLocation
        temperature
        bloodpressure1
        bloodpressure2
        heartbeat
        height
        weight
        result
        reExaminate
        dateExaminate
      },
      take
      skip
      total
      nextPage
    }
  }`,
  });

export const APIS_FIND_ONE_MEDICAL_PROFILE = async (input) =>
  await graphQLClient({
    queryString: `query {
      find_one_medical(id: ${input.id}) {
        inactive
        createdat
        updatedat
        updatedby{
          id
          firstname
          lastname
        }
        patient {
          id
          firstname
          lastname
          cardID
        }
        results {
          type
          createdat
          updatedat
          reqXQuang
          resultXQuang
          resultWBC
          resultRBC
          resultHP
          resultHCT
          resultMCV
          resultMCH
          resultMCHC
          resultPLT
          resultMPV
          resultRDW
          resultNeutro
          resultLym
          resultMono
          resultLEU
          resultNIT
          resultURO
          resultPRO
          resultUrePH
          resultLBD
          resultSG
          resultCentonic
          resultBIL
          resultGLU
          resultChyme
          resultPorphyrin
          resultProteinBenceJones
          resultGlucoza
          resultHbA1c
          resultUre
          resultCritine
          resultSGOT
          resultSGPT
          resultGGT
          resultHbsAg
          resultHbeAg
          resultAnitHbs
          resultAnitHcv
          resultCholestrol
          resultTriglyceric
          resultASO
          resultAFP
          resultAcidUric
          resultAnotherHP
          resultHIV
          resultTC
          resultTS
        }
        recordType
        id
        name
        status
        category
        firstname
        lastname
        dob
        phone
        email
        gender
        address
        city
        province
        nation
        cardID
        identityDate
        identityNumber
        identityLocation
        temperature
        bloodpressure1
        bloodpressure2
        heartbeat
        height
        weight
        note
        checkFamilyHistory
        reasonFamilyHistory
        checkPersonalHistory
        reasonPersonalHistory
        checkPregnancyHistory
        reasonPregnancyHistory
        checkSpecialDiseaseHistory
        reasonSpecialDiseaseHistory
        bmi
        leftEye
        rigthtEye
        respire
        digestive
        circulatorySystem
        endocrineSystem
        genitourinarySystem
        osteoarthritis
        nerveSystem
        mental
        surgery
        obstetric
        upperTeeth
        lowerTeeth
        dermatology
        detail1
        detail2
        detail3
        detail4
        detail5
        detail6
        detail7
        detail8
        detail9
        detail10
        detail11
        detail12
        detail13
        detail14
        detail15
        detail16
        detail17
        detail18
        detail19
        detail20
        detail21
        insurance1
        insurance2
        insurance3
        insurance4
        insurance5
        insurance6
        insurance7
        insurance8
        insurance9
        insurance10
        insurance11
        insurance12
        insurance13
        insurance14
        insurance15
        insurance16
        reason
        result
        reExaminate
        dateExaminate
        transaction {
          id
        }
        }
    }`,
  });

export const APIS_FIND_ONE_ITEM_INTEGRATION = async (input) =>
  await graphQLClient({
    queryString: `query {
      find_one_item_integration(id: ${input.id}) {
        inactive
        recordType
        id
        name
        shortName
        sku
        upc
        vendor
        category
        note
        description
        startDate
        endDate
        seri
        bin
        salePrices
        purchasePrices
        tax
        taxPrices
        close
        }
    }`,
  });

export const APIS_SAVE_MEDICAL = async (input) => {
  let payload = [];
  for (const key in input) {
    if (
      [
        "startDate",
        "endDate",
        "tax",
        "name",
        "createdat",
        "updatedat",
        "updatedby",
        "results",
        "transaction",
        "inactive",
      ]?.includes(key) ||
      input[key] === ""
    )
      continue;
    else if (key === "id") {
      if (input[key]) payload.push(` id: ${input[key]} `);
    } else if (["height", "weight", "isverify", "patient"].includes(key)) {
      if (input[key] !== undefined) payload.push(` ${key}: ${input[key]} `);
      else payload.push("");
    } else if (
      key.includes("detail") ||
      key.includes("insurance") ||
      [
        "checkFamilyHistory",
        "checkSpecialDiseaseHistory",
        "checkPregnancyHistory",
        "checkPersonalHistory",
        "reExaminate",
      ].includes(key)
    ) {
      if (input[key] === undefined) payload.push(` ${key}: false `);
      else payload.push(` ${key}: ${input[key]} `);
    } else if (input[key] !== undefined && input[key] !== null)
      payload.push(` ${key}: "${input[key]}" `);
  }

  return await graphQLClient({
    queryString: ` 
        mutation {
          save_medical(input:{ ${payload.join()}}) {
            id
        }
    }`,
  });
};

export const APIS_FIND_ALL_PATIENT_LOG = async ({
  recordId,
  take,
  skip,
  type,
}) =>
  await graphQLClient({
    queryString: ` query {
      find_all_logs(req: { 
            take: ${take}, 
            skip: ${skip}, 
            recordId: "${recordId}",
            type: "${type}"
      }) {
      data {
        name
        createdat
        type
      }
      take
      skip
      total
      nextPage
    }
  }`,
  });

export const APIS_FIND_ONE_PATIENT = async (input) => {
  return await graphQLClient({
    queryString: `
        query {
          find_one_patient(id: "${input.id}") {
            inactive
            id
            firstname
            lastname
            dob
            phone
            email
            gender
            address
            city
            province
            nation
            cardID
            medical{
              id
              reason
              result
              reExaminate
              dateExaminate
            }
            }
        }
      `,
  });
};

export const APIS_SAVE_MEDICAL_RESULT = async ({ input }) => {
  let payload = [];
  for (const key in input) {
    if (
      [
        "startDate",
        "endDate",
        "tax",
        "name",
        "createdat",
        "updatedat",
        "updatedby",
        "inactive",
      ]?.includes(key) ||
      input[key] === "" ||
      input[key] === undefined
    )
      continue;
    else if (key === "id") {
      if (input[key]) payload.push(` id: ${input[key]} `);
    } else if (
      ["height", "weight", "inactive", "isverify", "patient"].includes(key)
    ) {
      if (input[key] !== undefined) payload.push(` ${key}: ${input[key]} `);
      else payload.push("");
    } else if (
      key.includes("detail") ||
      (key.includes("result") &&
        ![
          "resultProteinBenceJones",
          "resultPorphyrin",
          "resultChyme",
          "resultXQuang",
        ].includes(key)) ||
      key.includes("insurance") ||
      [
        "checkFamilyHistory",
        "checkSpecialDiseaseHistory",
        "checkPregnancyHistory",
        "checkPersonalHistory",
        "reExaminate",
        "medicalId",
      ].includes(key)
    ) {
      if (input[key] === undefined) payload.push(` ${key}: false `);
      else payload.push(` ${key}: ${input[key]} `);
    } else if (input[key] !== undefined && input[key] !== null)
      payload.push(` ${key}: "${input[key]}" `);
  }

  return await graphQLClient({
    queryString: ` 
      mutation {
        save_medical_result(input:{ ${payload.join()}}) {
          inactive
          createdat
          updatedat
          id
          type
          reqXQuang
          resultXQuang
          resultWBC
          resultRBC
          resultHP
          resultHCT
          resultMCV
          resultMCH
          resultMCHC
          resultPLT
          resultMPV
          resultRDW
          resultNeutro
          resultLym
          resultMono
          resultLEU
          resultNIT
          resultURO
          resultPRO
          resultUrePH
          resultLBD
          resultSG
          resultCentonic
          resultBIL
          resultGLU
          resultChyme
          resultPorphyrin
          resultProteinBenceJones
          resultGlucoza
          resultHbA1c
          resultUre
          resultCritine
          resultSGOT
          resultSGPT
          resultGGT
          resultHbsAg
          resultHbeAg
          resultAnitHbs
          resultAnitHcv
          resultCholestrol
          resultTriglyceric
          resultASO
          resultAFP
          resultAcidUric
          resultAnotherHP
          resultHIV
          resultTC
          resultTS
          isverify
      }
  }`,
  });
};
