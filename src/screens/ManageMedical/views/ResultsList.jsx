/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect, useState } from "react";
import { Col, Collapse, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";

export const ResultList = (props) => {
  return (
    <React.Fragment>
      <div>
        <SubmitValidationForm data={props} />
      </div>
    </React.Fragment>
  );
};

const hematologyForm = [
  { type: "WBC", name: "resultWBC", normalValue: "4 -10", unit: "10^9/l" },
  { type: "RBC", name: "resultRBC", normalValue: "3.8 - 5.6", unit: "10^12/l" },
  { type: "Hp", name: "resultHP", normalValue: "12 - 18", unit: "g/l	" },
  { type: "Hct", name: "resultHCT", normalValue: "35 -52", unit: "%" },
  { type: "MCV", name: "resultMCV", normalValue: "80 - 97", unit: "Fl" },
  { type: "MCH", name: "resultMCH", normalValue: "26 - 32", unit: "g/dl" },
  {
    type: "MCHC",
    name: "resultMCHC",
    normalValue: "130 - 400",
    unit: "10^9/l	",
  },
  { type: "PLT", name: "resultPLT", normalValue: "11 - 15.7", unit: "%	" },
  { type: "MPV", name: "resultMPV", normalValue: "6.30 - 12", unit: "FI" },
  { type: "RDW", name: "resultRDW", normalValue: "11 - 15.7", unit: "%" },
  {
    type: "% Neutro",
    name: "resultNeutro",
    normalValue: "40 -74",
    unit: "%",
  },
  { type: "% Lym", name: "resultLym", normalValue: "19 - 48", unit: "%" },
  { type: "% Mono", name: "resultMono", normalValue: "3 - 9", unit: "%" },
];

const urineForm = [
  { type: "Bạch cầu (LEU)", name: "resultLEU", normalValue: "< 10/μL" },
  { type: "Nitrit (NIT)", name: "resultNIT", normalValue: "Âm Tính" },
  {
    type: "Urobilinogen (URO)",
    name: "resultURO",
    normalValue: "< 16,9μmol/L",
  },
  { type: "Protein (PRO)", name: "resultPRO", normalValue: "<0.1 g/L" },
  { type: "pH (PH)", name: "resultUrePH", normalValue: "4,8 - 7,4" },
  { type: "Hồng cầu (BLD)", name: "resultBLD", normalValue: "< 0,5/μL" },
  { type: "Tỉ trọng (SG)", name: "resultSG", normalValue: "1,015 - 1,025" },
  { type: "Thể cetonic", name: "resultcetonic", normalValue: "< 5 mmol/L" },
  { type: "Bilirubin (BIL)", name: "resultBIL", normalValue: "< 3,4 μmol/L" },
  { type: "Glucose (GLU)", name: "resultGLU", normalValue: "< 0,84 mol/L" },
  { type: "Dưỡng chấp", name: "resultChyme", normalValue: "" },
  { type: "Porphyrin", name: "resultPorphyrin", normalValue: "" },
  {
    type: "Protein Bence-Jones",
    name: "resultProteinBencejones",
    normalValue: "",
  },
];

const biochemicalForm = [
  {
    type: "Glucoza",
    name: "resultGlucoza",
    section: 1,
    normalValue: "80 - 110",
    unit: " mg/dl",
  },
  {
    type: "HbA1c",
    name: "resultHbA1c",
    section: 1,
    normalValue: "4,7 - 6,4",
    unit: " %",
  },
  {
    type: "URÊ",
    name: "resultUre",
    section: 2,
    normalValue: "20 - 40 ",
    unit: "mg/dl",
  },
  {
    type: "Creatine",
    name: "resultCritine",
    section: 2,
    normalValue: "1 - 2 ",
    unit: "mg/dl",
  },
  {
    type: "SGOT",
    name: "resultSGOT",
    section: 3,
    normalValue: "12 - 28",
    unit: " U/L",
  },
  {
    type: "SGPT",
    name: "resultSGPT",
    section: 3,
    normalValue: "12 - 35 ",
    unit: "U/L",
  },
  {
    type: "GGT",
    name: "resultGGT",
    section: 3,
    normalValue: "7 - 50 ",
    unit: "U/L",
  },

  { type: "HbsAg", name: "resultHbsAg", section: 4, normalValue: "Âm tính" },
  { type: "HbeAg", name: "resultHbeAg", section: 4, normalValue: "Âm tính" },
  {
    type: "Anit Hbs",
    name: "resultAnitHbs",
    section: 4,
    normalValue: "Âm tính",
  },
  {
    type: "Anti HCV",
    name: "resultAnitHCV",
    section: 4,
    normalValue: "Âm tính",
  },
  {
    type: "Cholesterol",
    name: "resultCholesterol",
    section: 5,
    normalValue: "130 - 180 ",
    unit: "mg/dl",
  },
  {
    type: "Triglyceric",
    name: "resultTriglyceric",
    section: 5,
    normalValue: "< 175",
    unit: " mg/dl",
  },
  { type: "ASO", name: "resultASO", section: 6, normalValue: "Âm tính" },
  { type: "AFP", name: "resultAFP", section: 7, normalValue: "Âm tính" },
  {
    type: "Acid Uric",
    name: "resultAcidUric",
    section: 7,
    normalValue: "120 - 420",
    unit: " μmol/L",
  },
  { type: "HP", name: "resultAnotherHP", section: 7, normalValue: "Âm tính" },
  {
    type: "Troponin",
    name: "resultTroponin",
    section: 7,
    normalValue: "Âm tính",
  },
  { type: "HIV", name: "resultHIV", section: 7, normalValue: "Âm tính" },
  { type: "TS", name: "resultTS", section: 7, normalValue: "2 - 5 phút" },
  { type: "TC", name: "resultTC", section: 7, normalValue: "8 - 15 phút" },
];

const listResultType = [
  {
    value: "xray",
    label: "X-Quang",
  },
  {
    value: "hematology",
    label: "Xét nghiệm huyết học",
  },
  {
    value: "urine",
    label: "Xét nghiệm nước tiểu",
  },
  {
    value: "biochemical",
    label: "Xét nghiệm sinh hóa",
  },
  {
    value: "ECG",
    label: "Điện tâm đồ",
  },
];

let SubmitValidationForm = (props) => {
  const {
    register,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const [collapse, setCollapse] = useState(false);

  useEffect(() => {
    if (props?.data?.data) {
      // setSelectedResult(props?.data?.data?.type);
      Object.entries(props?.data?.data).forEach(([key, value]) => {
        if (value && key) setValue(key, value);
      });
    }
  }, [props]);

  return (
    <form>
      <SessionTitle
        collapse
        active={collapse}
        onClick={() => setCollapse(!collapse)}
        mt={0}
      >
        {
          listResultType.find((item) => item.value === props?.data?.data?.type)
            ?.label
        }
      </SessionTitle>
      <Collapse in={collapse}>
        <div>
          {props?.data?.data?.type === "xray" && (
            <div>
              <Row>
                <Col sm={12}>
                  <Controller
                    {...register("reqXQuang", { required: true })}
                    control={control}
                    render={({ field }) => (
                      <FormInput
                        required
                        label={"Yêu cầu"}
                        error={
                          errors?.resultXQuang ? "Vui lòng nhập Yêu cầu" : ""
                        }
                        {...field}
                      />
                    )}
                  />
                </Col>
                <Col sm={12}>
                  <Controller
                    {...register("resultXQuang", { required: true })}
                    control={control}
                    render={({ field }) => (
                      <FormInput
                        required
                        label={"Kết quả"}
                        error={
                          errors?.resultXQuang ? "Vui lòng nhập kết quả" : ""
                        }
                        {...field}
                      />
                    )}
                  />
                </Col>
              </Row>
            </div>
          )}

          {props?.data?.data?.type === "hematology" && (
            <div>
              <Row style={{ alignItems: "center" }}>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Loại XN
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={6}>
                  Kết quả
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Giá trị <br /> bình thường
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Đơn vị
                </Col>
              </Row>
              {hematologyForm.map((item, key) => {
                return (
                  <Row style={{ alignItems: "center" }}>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                        fontWeight: "bold",
                      }}
                    >
                      {item.type}
                    </Col>
                    <Col
                      sm={6}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        alignItems: "center",
                      }}
                    >
                      <Controller
                        {...register(item.name)}
                        control={control}
                        render={({ field }) => <FormInput {...field} />}
                      />
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                      }}
                    >
                      {item?.normalValue}
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                      }}
                    >
                      {item?.unit}
                    </Col>
                  </Row>
                );
              })}
            </div>
          )}

          {props?.data?.data?.type === "urine" && (
            <div>
              <Row style={{ alignItems: "center" }}>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Loại XN
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={8}>
                  Kết quả
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Giá trị <br /> bình thường
                </Col>
              </Row>
              {urineForm.map((item, key) => {
                return (
                  <Row style={{ alignItems: "center" }}>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                        fontWeight: "bold",
                      }}
                    >
                      {item.type}
                    </Col>
                    <Col
                      sm={8}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        alignItems: "center",
                      }}
                    >
                      <Controller
                        {...register(item.name)}
                        control={control}
                        render={({ field }) => <FormInput {...field} />}
                      />
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                      }}
                    >
                      {item?.normalValue}
                    </Col>
                  </Row>
                );
              })}
            </div>
          )}

          {props?.data?.data?.type === "biochemical" && (
            <div>
              <Row style={{ alignItems: "center" }}>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Loại XN
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={6}>
                  Kết quả
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Giá trị <br /> bình thường
                </Col>
                <Col style={{ textAlign: "center", fontWeight: "bold" }} sm={2}>
                  Đơn vị
                </Col>
              </Row>
              {biochemicalForm.map((item, key) => {
                return (
                  <Row style={{ alignItems: "center" }}>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                        fontWeight: "bold",
                      }}
                    >
                      {item.type}
                    </Col>
                    <Col
                      sm={6}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        alignItems: "center",
                      }}
                    >
                      <Controller
                        {...register(item.name)}
                        control={control}
                        render={({ field }) => <FormInput {...field} />}
                      />
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        borderRight: "1px solid #cecece",
                        borderBottom: "1px solid #cecece",
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                      }}
                    >
                      {item?.normalValue}
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        height: "60px",
                        display: "flex",
                        alignItems: "center",
                        borderBottom: "1px solid #cecece",
                        placeContent: "center",
                      }}
                    >
                      {item?.unit}
                    </Col>
                  </Row>
                );
              })}
            </div>
          )}
        </div>
      </Collapse>
    </form>
  );
};
