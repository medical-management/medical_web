/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */

export const FORM_CHANGE_INFO = {
  title: "Thông tin bệnh án",
  name: "Tên bệnh án",
  shortName: "Tên rút gọn",
  sku: "SKU",
  upc: "UPC",
  description: "Mô tả",
  vendor: "Nhà cung cấp",
  category: "Phân loại",
  bin: "Kho",
  seri: "Số SERIAL",
  purchasePrices: "Giá nhập",
  salePrices: "Giá bán",
  tax: "Loại thuế",
  taxPrices: "Giá thuế",
  startDate: "Ngày sản xuất",
  endDate: "Ngày hết hạn",
  processBtn: "Lưu",
  cancelBtn: "Hủy",
};
