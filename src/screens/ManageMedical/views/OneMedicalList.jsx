import React, { useEffect, useState } from "react";
import { Button, Card, Col, InputGroup, Row } from "react-bootstrap";
import { useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams, NavLink } from "react-router-dom";
// import moment from "moment";
import * as actions from "../actions";
import { APIS_FIND_ALL_MEDICAL, APIS_FIND_ALL_PATIENT_LOG, APIS_FIND_ONE_PATIENT } from "../apis";
import Avatar from "../../../components/Avatar";
import moment from "moment";
import _ from "lodash";
import { Timeline, TimelineItem } from "vertical-timeline-component-for-react";
import Parser from "html-react-parser";
import { name } from "../reducer";
import { ROUTER_MANAGE_PATIENT } from "../../ManagePatient/router";
import { ROUTER_MANAGE_MEDICALS } from "../router";

const color = ["#e86971", "#61b8ff", "#76bb7f", "#00695f", "#ffb74d"];

function OneMedicalList() {
    const dispatch = useDispatch();
    const queryClient = useQueryClient();
    const params = useParams();
    const [pageSizes, setPageSizes] = useState(20);
    const [pages, setPages] = useState(0);
    const { user } = useSelector((state) => state["AUTHENTICATION"]);
    const { patientLogs, patient, medicals } = useSelector((state) => state[name]);
    const history = useHistory();

    const [query, setQuery] = useState({
        take: pageSizes,
        skip: pages,
        patient: params?.id,
    });

    const [queryLog, setQueryLog] = useState({
        take: 10,
        skip: pages,
        recordId: params?.id,
        type: "patient",
    });

    const queryMedicals = useQuery("medicals", () => APIS_FIND_ALL_MEDICAL(query));

    const queryFindLogs = useQuery("logs", () => APIS_FIND_ALL_PATIENT_LOG(queryLog), {
        enabled: !!queryLog?.recordId,
    });

    useEffect(() => {
        if (!queryMedicals.isLoading && !queryMedicals.isError)
            dispatch(actions.GET_ALL_MEDICALS_SUCCESS(queryMedicals?.data));
    }, [queryMedicals.data]);

    useEffect(() => {
        if (!queryFindLogs.isLoading && !queryFindLogs.isError)
            dispatch(actions.GET_PATIENT_LOG_SUCCESS(queryFindLogs?.data));
    }, [queryFindLogs.data]);

    const queryPatient = useQuery(["user", params], () => params.id !== "add" && APIS_FIND_ONE_PATIENT(params));

    useEffect(() => {
        setQuery({ ...query, pages: pages });
    }, [pages]);

    useEffect(() => {
        queryClient.prefetchQuery("medicals", queryMedicals);
    }, [query]);

    useEffect(() => {
        const { isSuccess, isLoading, data } = queryPatient;
        if (isSuccess && !isLoading && data !== false) {
            dispatch(actions.GET_PATIENT_INFO_SUCCESS(queryPatient?.data?.find_one_patient));
        }
    }, [queryPatient.isLoading]);
    console.log({ medicals });

    return (
        <React.Fragment>
            <Row>
                <Col sm={4}>
                    <Card>
                        <Card.Body className="p-0">
                            <div
                                className="flex"
                                style={{ padding: 20 }}
                                onClick={() => history.push(`${ROUTER_MANAGE_PATIENT}/${patient?.id}`)}
                            >
                                <Avatar src={patient?.firstname || "Test"}></Avatar>

                                <div style={{ marginLeft: 20 }}>
                                    <h5>
                                        {patient?.lastname} {patient?.firstname}
                                    </h5>
                                    <span>{patient?.dob ? moment(patient?.dob).format("DD/MM/YYYY") : ""}</span>
                                </div>
                            </div>
                            <div
                                style={{
                                    paddingRight: 15,
                                    height: "75vh",
                                    overflowY: "auto",
                                    backgroundColor: "white",
                                }}
                            >
                                <ul className="task-list">
                                    {patientLogs?.map((item, index) => (
                                        <li>
                                            <i className="task-icon bg-c-green" />
                                            <h6>
                                                {item?.name}
                                                <p className="text-muted">
                                                    {moment(item?.createdat).format("DD/MM/YYYY-HH:mm")}
                                                </p>
                                            </h6>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
                <Col
                    sm={8}
                    style={{
                        height: "85vh",
                        overflowY: "auto",
                        backgroundColor: "white",
                        boxShadow: "0 1px 20px 0 rgb(69 90 100 / 8%)",
                    }}
                >
                    <Timeline lineColor={"#ddd"}>
                        {medicals?.map((item, index) => (
                            <TimelineItem
                                key={index}
                                dateText={moment(item?.updatedat).format("DD/MM/YYYY")}
                                dateInnerStyle={{
                                    background: color[(index % color?.length) + 1],
                                    color: "#000",
                                }}
                            >
                                <div style={{ position: "relative" }}>
                                    <NavLink to={`${ROUTER_MANAGE_MEDICALS}/${item?.id}`} bak className="h4">
                                        Bệnh án: {item?.name}
                                    </NavLink>
                                    {item?.result && <div>Kết quả:</div>}
                                    <div className="text-truncate mt-2" style={{ maxWidth: "300px", overflow: "auto" }}>
                                        {item?.result ? <div>{Parser(item?.result || "")}</div> : " "}
                                    </div>

                                    {item?.reason && " Chẩn đoán:"}
                                    <div className="text-truncate mt-2" style={{ maxWidth: "300px", overflow: "auto" }}>
                                        {item?.reason ? <div>{Parser(item?.reason || "")}</div> : " "}
                                    </div>
                                    {item?.reExaminate && (
                                        <p
                                            style={{
                                                position: "absolute",
                                                right: 0,
                                                top: "-5px",
                                                borderRadius: "10px 0 0 10px",
                                                backgroundColor: "#ff3535",
                                                padding: "3px 10px",
                                                color: "white",
                                                fontWeight: "600",
                                            }}
                                        >
                                            Tái khám: {moment(item?.dateExaminate).format("DD/MM/YYYY")}
                                        </p>
                                    )}
                                </div>
                            </TimelineItem>
                        ))}
                    </Timeline>
                    {/* <div style={{ textAlign: "center" }}>
            <Button className="margin-0" onClick={() => setPages(pages + 1)}>
              Xem thêm
            </Button>
          </div> */}
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default OneMedicalList;
