import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Row,
  InputGroup,
  SplitButton,
  Dropdown,
} from "react-bootstrap";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Select from "react-select";
import CustomPagination from "../../../components/Pagination";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import * as actions from "../actions";
import { APIS_FIND_ALL_MEDICAL } from "../apis";
import { useToasts } from "react-toast-notifications";
import moment from "moment";
import { categorySchedule } from "../../../constants/options";
import Parser from "html-react-parser";
import CustomDrawer from "../../../components/CustomDrawer";

function AllMedicalList() {
  const queryClient = useQueryClient();
  const { addToast } = useToasts();

  const history = useHistory();

  const dispatch = useDispatch();

  const { medicals, page, pageSize, totalPage } = useSelector(
    (state) => state["MEDICALS"]
  );
  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [isVisible, setIsVisible] = useState(false);

  const [selected, setSelected] = useState(null);

  const [show, setShow] = useState(false);

  const [userList, setUserList] = useState([]);

  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);

  const [query, setQuery] = useState({
    take: pageSizes,
    skip: pages,
    inactive: false,
  });

  const queryMedicals = useQuery("medicals", () =>
    APIS_FIND_ALL_MEDICAL(query)
  );

  useEffect(() => {
    queryClient.prefetchQuery("medicals", queryMedicals);
  }, [query]);

  useEffect(() => {
    if (!queryMedicals.isLoading && !queryMedicals.isError)
      dispatch(actions.GET_ALL_MEDICALS_SUCCESS(queryMedicals?.data));
  }, [queryMedicals.data]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      take: pageSizes,
      skip: 0,
      firstname: e?.target[1]?.value || "",
    });
  };

  useEffect(() => {
    setQuery({ ...query, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQuery({ ...query, skip: pages });
  }, [pages]);

  useEffect(() => {
    setSelected(null);
  }, []);

  useEffect(() => {
    if (selected !== null) {
      setIsVisible(true);
    }
  }, [selected]);

  const columns = React.useMemo(
    () => [
      {
        Header: "Tên bệnh án",
        accessor: (row) => {
          return (
            <p
              className="cell-p"
              style={{ fontWeight: "bold", cursor: "pointer" }}
              onClick={() => history.push(`/app/dashboard/medicals/${row?.id}`)}
            >
              {row?.name}
            </p>
          );
        },
      },
      {
        Header: "Bệnh nhân",
        accessor: (row) => {
          return (
            <a
              style={{
                cursor: "pointer",
                textDecoration: "none",
                color: "#333",
                fontWeight: "bold",
              }}
              className="cell-p"
              href={`/app/dashboard/patients/${row?.patient?.id}/medical`}
              target="_blank"
            >
              {row?.patient?.lastname} {row?.patient?.firstname}
            </a>
          );
        },
      },
      {
        Header: "Loại",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {
                categorySchedule?.find((item) => item.value === row?.category)
                  ?.label
              }
            </p>
          );
        },
      },
      // {
      //   Header: "Quốc tịch",
      //   accessor: "nation",
      // },
      // {
      //   Header: "Tỉnh/Thành phố",
      //   accessor: "city",
      // },
      // {
      //   Header: "Quận/Huyện/TP(Tỉnh)",
      //   accessor: "province",
      // },
      {
        Header: "Ngày tạo",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {moment(row?.createdat).format("DD/MM/YYYY")}
            </p>
          );
        },
      },
      {
        Header: "Chẩn đoán",
        accessor: (row) => (
          <div
            className="text-truncate mt-2"
            style={{ maxWidth: "230px", overflow: "auto" }}
          >
            {row?.reason ? <div>{Parser(row?.reason || "")}</div> : " "}
          </div>
        ),
      },
      {
        Header: "Kết quả",
        accessor: (row) => (
          <div
            className="text-truncate mt-2"
            style={{ maxWidth: "230px", overflow: "auto" }}
          >
            {row?.reason ? <div>{Parser(row?.result || "")}</div> : " "}
          </div>
        ),
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization?.length &&
            user?.decentralization[0]?.patients > 1 && (
              <SplitButton
                title={"Sửa"}
                onClick={() =>
                  history.push(`/app/dashboard/medicals/${row.id}`)
                }
                className="mr-2 mb-2 text-capitalize"
              >
                <Dropdown.Item
                  onClick={() =>
                    history.push(
                      `/app/dashboard/patients/${row.patient.id}/medical`
                    )
                  }
                >
                  Xem
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() =>
                    history.push(`/app/dashboard/medicals/${row.id}`)
                  }
                >
                  Sửa
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() =>
                    history.push({
                      pathname: `/app/dashboard/medicals/add`,
                      state: row,
                    })
                  }
                >
                  Tạo bản sao
                </Dropdown.Item>
              </SplitButton>
            )
          );
        },
      },
    ],
    [user?.decentralization]
  );

  const handleAdvanceSearch = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      skip: 0,
      lastname: e.target[0].value,
      firstname: e.target[1].value,
    });
    setShow(false);
  };

  // const onChangeGender = (e) => {
  //   setQuery({
  //     ...query,
  //     skip: 0,
  //     gender: e,
  //   });
  // };

  const onChangeDob = (e) => {
    setQuery({
      ...query,
      skip: 0,
      dob: e,
    });
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                gap: 20,
              }}
            >
              <Card.Title
                as="h5"
                style={{ whiteSpace: "nowrap", textAlign: "left" }}
              >
                Danh sách bệnh án
              </Card.Title>
              <form
                style={{ width: "100%" }}
                onSubmit={(e) => {
                  e.preventDefault();
                  setQuery({
                    ...query,
                    take: pageSizes,
                    skip: 0,
                    name: e?.target[0]?.value || "",
                  });
                }}
              >
                <InputGroup>
                  <input
                    placeholder={`Tìm kiếm bệnh án`}
                    className="form-control"
                    style={{ flex: 1 }}
                  />
                  <InputGroup.Append>
                    <Button
                      type="submit"
                      className="feather icon-search"
                    ></Button>
                  </InputGroup.Append>
                </InputGroup>
              </form>
              {user?.decentralization[0]?.patients > 1 && (
                <Button
                  style={{ margin: 0, whiteSpace: "nowrap" }}
                  onClick={() => history.push("/app/dashboard/medicals/add")}
                >
                  Thêm bệnh án
                </Button>
              )}
            </Card.Header>
            <Card.Body>
              <form onSubmit={handleSubmit}>
                <Row className="mb-3">
                  <Col className="d-flex align-items-center">
                    Hiển thị
                    <select
                      className="form-control w-auto mx-2"
                      value={pageSizes}
                      onChange={(e) => {
                        setPageSizes(Number(e.target.value));
                      }}
                    >
                      {[5, 10, 20, 50, 100].map((pgsize) => (
                        <option key={pgsize} value={pgsize}>
                          {pgsize}
                        </option>
                      ))}
                    </select>
                    <p
                      className="feather icon-filter margin-0 cursor-pointer"
                      style={{
                        color: !show ? "black" : "#04a9f5",
                        fontWeight: "bold",
                        fontSize: 16,
                      }}
                      onClick={() => setShow(!show)}
                    ></p>
                  </Col>
                </Row>
                <Button className="hidden" type="submit"></Button>
              </form>

              <div className="responsive-section">
                <CustomTable
                  columns={columns}
                  data={medicals || []}
                  pageSize={pageSize}
                />
              </div>
              <CustomPagination
                pageIndex={page}
                totalPage={totalPage}
                gotoPage={(e) => setPages(e)}
                canPreviousPage={page > 0}
                canNextPage={page < totalPage - 1}
              ></CustomPagination>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {show && (
        <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
          <div>
            <p
              style={{
                width: "100%",
                fontSize: 18,
                fontWeight: 700,
                borderBottom: "1px solid #ddd",
                paddingBottom: 10,
              }}
            >
              Lọc bệnh án
            </p>
            <form onSubmit={handleAdvanceSearch}>
              <div
                className="flex"
                style={{ flexDirection: "column", gap: 10 }}
              >
                <div className="self-center" style={{ flex: 1, width: "100%" }}>
                  <label className="label-control items-center">Họ</label>
                  <input
                    placeholder={`Nhập Họ và Tên lót bệnh nhân`}
                    className="form-control"
                    style={{ height: "38px" }}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    flex: 1,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">Tên</label>
                  <input
                    placeholder={`Nhập Tên bệnh nhân`}
                    className="form-control"
                    style={{ height: "38px" }}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    paddingBottom: 10,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Trạng thái
                  </label>
                  <Select
                    style={{ width: "100%" }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={categorySchedule}
                    value={
                      categorySchedule?.find(
                        (item) => item.value === query?.category
                      ) ||
                      categorySchedule?.find((item) => item.label === "Tất cả")
                    }
                    onChange={(e) =>
                      setQuery({
                        ...query,
                        skip: 0,
                        category: e.value,
                      })
                    }
                  />
                </div>
              </div>

              <Button
                type="submit"
                style={{ float: "right", margin: 0, marginRight: "0px" }}
              >
                Áp dụng
              </Button>
            </form>
          </div>
        </CustomDrawer>
      )}
    </React.Fragment>
  );
}

export default AllMedicalList;
