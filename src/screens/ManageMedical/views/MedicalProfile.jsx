/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import _ from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Collapse, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import { useToasts } from "react-toast-notifications";
import EditorCkClassic from "../../../components/CK-Editor/CkClassicEditor";
import ConfirmModal from "../../../components/ConfirmModal";
import CustomModal from "../../../components/CustomModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import Address from "../../../constants/address.json";
import {
  clinicalSections,
  detailDiases,
  insuranceSections,
  patientSection,
  vitalSections,
} from "../../../constants/medical-form-items";
import { categorySchedule } from "../../../constants/options";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { APIS_CREATE_SALE_ORDER_FROM_MEDICAL } from "../../ManageOrders/apis";
import OrderInformation from "../../ManageOrders/views/OrderInfo";
import {
  APIS_FIND_FAST_PATIENT,
  APIS_FIND_ONE_PATIENT,
} from "../../ManagePatient/apis";
import QuickFormPatient from "../../ManageSchedulers/views/QuickForm";
import * as actions from "../actions";
import { APIS_FIND_ONE_MEDICAL_PROFILE, APIS_SAVE_MEDICAL } from "../apis";
import { ROUTER_MANAGE_MEDICALS } from "../router";
import { FORM_CHANGE_INFO } from "./Lang";
import QuickForm from "./QuickForm";
import { ResultList } from "./ResultsList";

export default function ChangeProfile(props) {
  return (
    <React.Fragment>
      <Breadcrumb />
      <Card style={{ padding: 30 }}>
        <SubmitValidationForm
          data={props}
          copy={props?.location?.state || null}
        />
      </Card>
    </React.Fragment>
  );
}

const SubmitValidationForm = ({ data, copy }) => {
  const params = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  const watchBMI = watch(["height", "weight"]);
  const [isEnable, setIsEnable] = useState(false);

  const { medicalProfile, listPatients, patient, patientOptions } = useSelector(
    (state) => state["MEDICALS"]
  );
  const typeAction = params?.id === "add" ? "add" : "edit";

  const queryFindOneMedical = useQuery(
    ["medical", params],
    () => APIS_FIND_ONE_MEDICAL_PROFILE(params),
    {
      enabled: params?.id !== "add",
    }
  );
  const queryOnePatient = useQuery(
    ["one_patient", { id: data?.location.state?.id }],
    () => APIS_FIND_ONE_PATIENT({ id: data?.location.state?.id }),
    { enabled: isEnable }
  );

  const { user } = useSelector((state) => state["AUTHENTICATION"]);
  const queryPatient = useQuery("users", () =>
    APIS_FIND_FAST_PATIENT({ firstname: firstName, take: 10, skip: 0 })
  );
  const mutationSaveMedical = useMutation("medical", APIS_SAVE_MEDICAL);
  const mutationSaveSaleOrder = useMutation(
    "saleOrder",
    APIS_CREATE_SALE_ORDER_FROM_MEDICAL
  );
  const mutationAddMedical = useMutation("add_medical", APIS_SAVE_MEDICAL);

  const [selectedPatient, setSelectedPatient] = useState(null);
  const [reason, setReason] = useState(null);
  const [result, setResult] = useState(null);
  const [firstName, setFirstName] = useState("");
  const [payload, setPayload] = useState(null);
  const [note, setNote] = useState("");
  const [gender, setGender] = useState("male");
  const [showResults, setShowResults] = useState(false);

  const [visibleConfirmSave, setVisibleConfirmSave] = useState(false);
  const [showModalResult, setShowModalResult] = useState(false);
  const [checkFamilyHistory, setCheckFamilyHistory] = useState(false);
  const [reasonFamilyHistory, setReasonFamilyHistory] = useState("");
  const [checkSpecialDiseaseHistory, setCheckSpecialDiseaseHistory] =
    useState(false);
  const [reasonSpecialDiseaseHistory, setReasonSpecialDiseaseHistory] =
    useState("");
  const [checkPregnancyHistory, setCheckPregnancyHistory] = useState(false);
  const [reasonPregnancyHistory, setReasonPregnancyHistory] = useState("");
  const [checkPersonalHistory, setCheckPersonalHistory] = useState(false);
  const [reasonPersonalHistory, setReasonPersonalHistory] = useState("");
  const [checkReExaminate, setCheckReExaminate] = useState(false);
  const [openModalQuick, setOpenModalQuick] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState("normal");
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedProvince, setSelectedProvince] = useState("");
  const [listProvince, setListProvince] = useState([]);
  const [medicalResult, setMedicalResult] = useState(null);

  ////collapse

  const [collapseInfo, setCollapseInfo] = useState(true);
  const [collapseLive, setCollapseLive] = useState(true);
  const [collapseClinical, setCollapseClinical] = useState(true);
  const [collapseHistory, setCollapseHistory] = useState(true);
  const [collapseDetail, setCollapseDetail] = useState(true);
  const [collapseWarranty, setCollapseWarranty] = useState(true);
  const [collapseSum, setCollapseSum] = useState(true);
  const [collapseOrder, setCollapseOrder] = useState(true);
  const [dataCollapse, setDataCollapse] = useState({
    collapseInfo,
    collapseLive,
    collapseClinical,
    collapseHistory,
    collapseDetail,
    collapseWarranty,
    collapseSum,
    collapseOrder,
  });
  const queryClient = useQueryClient();

  useEffect(() => {
    if (params.id !== "add") {
      queryClient.prefetchQuery(queryFindOneMedical);
    }
    if (params.id === "add") {
      dispatch(actions.GET_MEDICAL_PROFILE_SUCCESS(null));
      setNote(null);
    }
  }, [params.id]);

  useEffect(() => {
    const { isSuccess, isLoading, data } = queryFindOneMedical;
    if (isSuccess && !isLoading && data !== false) {
      dispatch(
        actions.GET_MEDICAL_PROFILE_SUCCESS(
          queryFindOneMedical?.data?.find_one_medical
        )
      );
      // setNote(queryFindOneMedical?.data?.find_one_patient?.note);
      setGender(queryFindOneMedical?.data?.find_one_medical?.gender);
    } else {
      setNote(null);
    }
  }, [queryFindOneMedical.isLoading]);

  const cityList = _.sortedUniq(Address?.address?.map((item) => item.city)).map(
    (item) => {
      return {
        value: item,
        label: item,
      };
    }
  );

  const handleChooseCity = (value) => {
    setSelectedCity(value);
    setListProvince([]);
    setSelectedProvince(null);
    if (value !== null) {
      setSelectedCity(value);
      setListProvince(
        _.uniqBy(
          Address?.address?.map((item) => {
            if (item.city === value) {
              return {
                label: item.province,
                value: item.province,
              };
            }
          }),
          function (e) {
            if (e) return e.label;
          }
        ).filter((n) => n)
      );
    } else setSelectedCity("");
  };

  const handleChooseProvince = (value) => {
    if (value !== null) {
      setSelectedProvince(value);
    } else setSelectedProvince("");
  };

  const onSaveMedical = () => {
    if (history?.location?.search !== "?integration=true") {
      if (typeAction === "add") {
        mutationAddMedical.mutate(payload);
      } else if (typeAction === "edit") {
        mutationSaveMedical.mutate(payload);
      }
    }
  };

  useEffect(() => {
    if (payload !== null && !showModalResult) setVisibleConfirmSave(true);
  }, [payload]);

  useEffect(() => {
    if (!visibleConfirmSave) setPayload(null);
  }, [visibleConfirmSave]);

  useEffect(() => {
    const { status, isLoading, data, isError } = queryPatient;
    if (status === "success" && !isLoading && data)
      dispatch(actions.GET_ALL_PATIENT_INFO_SUCCESS(queryPatient?.data));
  }, [queryPatient.isLoading]);

  useEffect(() => {
    const h = parseInt(watchBMI[0]) || 0;
    const w = parseInt(watchBMI[1]) || 0;
    setValue("bmi", parseFloat(w / ((h / 100) * (h / 100))).toFixed(2));
  }, [watchBMI]);

  useEffect(() => {
    if (data?.location.state && data?.location.state?.id) setIsEnable(true);
  }, [data]);

  useEffect(() => {
    const { isLoading, isError, data, status, error } = queryOnePatient;
    if (!isLoading) {
      if (error?.length || status === "error") {
        dispatch(actions.GET_PATIENT_INFO_SUCCESS(null));
      } else dispatch(actions.GET_PATIENT_INFO_SUCCESS(data?.find_one_patient));
    }
  }, [queryOnePatient.isLoading]);

  useEffect(() => {
    if (patient && patient?.id && params?.id !== "add") {
      setSelectedPatient(patient?.id);
    }
  }, [patient]);

  useEffect(() => {
    if (selectedPatient && listPatients?.length)
      Object.entries(
        listPatients?.find((item) => item?.id === selectedPatient)
      ).forEach(([key, value]) => {
        if (value && key) setValue(key, value);
      });
  }, [selectedPatient, listPatients]);

  useEffect(() => {
    if (params.id !== "add" && medicalProfile) {
      Object.entries(medicalProfile).forEach(([key, value]) => {
        if (typeof value !== "object" && typeof value !== "undefined")
          setValue(key, value);
      });
      if (medicalProfile?.patient)
        Object.entries(medicalProfile?.patient).forEach(([key, value]) => {
          setValue(key, value);
        });

      setSelectedPatient(medicalProfile?.patient?.id);
      setCheckFamilyHistory(medicalProfile?.checkFamilyHistory);
      setCheckPersonalHistory(medicalProfile?.checkPersonalHistory);
      setCheckPregnancyHistory(medicalProfile?.checkPregnancyHistory);
      setCheckSpecialDiseaseHistory(medicalProfile?.checkSpecialDiseaseHistory);

      setReasonFamilyHistory(medicalProfile?.reasonFamilyHistory);
      setReasonPersonalHistory(medicalProfile?.reasonPersonalHistory);
      setReasonPregnancyHistory(medicalProfile?.reasonPregnancyHistory);
      setReasonSpecialDiseaseHistory(
        medicalProfile?.reasonSpecialDiseaseHistory
      );

      setCheckReExaminate(medicalProfile?.reExaminate);

      setResult(medicalProfile?.result);
      setReason(medicalProfile?.reason);
    }
  }, [medicalProfile]);

  useEffect(() => {
    if (copy && params?.id === "add") {
      Object.entries(copy).forEach(([key, value]) => {
        if (
          typeof value !== "object" &&
          typeof value !== "undefined" &&
          !["firstname", "lastname", "dob"].includes(key)
        )
          setValue(key, value);
      });

      setSelectedPatient(null);
      setCheckFamilyHistory(copy?.checkFamilyHistory);
      setCheckPersonalHistory(copy?.checkPersonalHistory);
      setCheckPregnancyHistory(copy?.checkPregnancyHistory);
      setCheckSpecialDiseaseHistory(copy?.checkSpecialDiseaseHistory);

      setReasonFamilyHistory(copy?.reasonFamilyHistory);
      setReasonPersonalHistory(copy?.reasonPersonalHistory);
      setReasonPregnancyHistory(copy?.reasonPregnancyHistory);
      setReasonSpecialDiseaseHistory(copy?.reasonSpecialDiseaseHistory);

      setCheckReExaminate(copy?.reExaminate);

      setResult(copy?.result);
      setReason(copy?.reason);
    }
  }, [copy]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationSaveMedical;

    if (!isLoading) {
      if (!isError && !!data) {
        setVisibleConfirmSave(false);
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
      } else if (isError)
        addToast("Cập nhật thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationSaveMedical.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationAddMedical;

    if (!isLoading) {
      if (!isError && !!data) {
        setVisibleConfirmSave(false);
        addToast("Thêm thông tin bệnh án thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        mutationSaveSaleOrder.mutate({
          patient: selectedPatient,
          medical: mutationAddMedical?.data?.save_medical?.id,
          status: "pending",
          id: "",
          type: "salesOrder",
        });
      } else if (isError)
        addToast("Thêm thông tin bệnh án thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationAddMedical.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationSaveSaleOrder;

    if (!isLoading) {
      if (!isError && !!data) {
        setVisibleConfirmSave(false);
        addToast("Thêm thông tin hóa đơn thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push(ROUTER_MANAGE_MEDICALS);
      } else if (isError)
        addToast("Thêm thông tin hóa đơn thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationSaveSaleOrder.isLoading]);

  useEffect(() => {
    if (medicalResult && medicalResult?.length > 0 && !showModalResult) {
      setShowModalResult(true);
    }
  }, [medicalResult]);

  useEffect(() => {
    if (!showModalResult) {
      setMedicalResult(null);
    }
  }, [showModalResult]);

  const handleDeleteMedical = () => {
    mutationAddMedical.mutate({
      ...medicalProfile,
      patient: selectedPatient || medicalProfile?.patient?.id,
      recordType: "medical",
      dob: moment(medicalProfile?.dob).utc().format(),
      category: selectedCategory,
      identityDate: medicalProfile?.identityDate
        ? moment(medicalProfile?.identityDate).utc().format()
        : "",
      reasonSpecialDiseaseHistory,
      reasonPregnancyHistory,
      reasonPersonalHistory,
      checkFamilyHistory,
      checkSpecialDiseaseHistory,
      checkPregnancyHistory,
      checkPersonalHistory,
      reasonFamilyHistory,
      reExaminate: checkReExaminate,
      dateExaminate: moment(medicalProfile?.dateExaminate).utc().format(),
      reason,
      result,
      bmi: medicalProfile?.bmi,
      height: medicalProfile?.height ? parseFloat(medicalProfile?.height) : "",
      weight: medicalProfile?.weight ? parseFloat(medicalProfile?.weight) : "",
      inactive: true,
    });
  };

  const submit = (values) => {
    if (selectedPatient)
      if (typeAction === "add")
        setPayload({
          ...values,
          patient: selectedPatient,
          recordType: "medical",
          id: "",
          dob: moment(values?.dob).utc().format(),
          category: selectedCategory,
          identityDate: values?.identityDate
            ? moment(values?.identityDate).utc().format()
            : "",
          checkFamilyHistory,
          checkSpecialDiseaseHistory,
          checkPregnancyHistory,
          checkPersonalHistory,
          reasonFamilyHistory,
          reasonSpecialDiseaseHistory,
          reasonPregnancyHistory,
          reasonPersonalHistory,
          reExaminate: checkReExaminate,
          dateExaminate: moment(values?.dateExaminate).utc().format(),
          reason,
          result,
          bmi: values?.bmi,
          height: values?.height ? parseFloat(values?.height) : "",
          weight: values?.weight ? parseFloat(values?.weight) : "",
        });
      else
        setPayload({
          ...values,
          patient: selectedPatient || medicalProfile?.patient?.id,
          recordType: "medical",
          dob: moment(values?.dob).utc().format(),
          category: selectedCategory,
          identityDate: values?.identityDate
            ? moment(values?.identityDate).utc().format()
            : "",
          reasonSpecialDiseaseHistory,
          reasonPregnancyHistory,
          reasonPersonalHistory,
          checkFamilyHistory,
          checkSpecialDiseaseHistory,
          checkPregnancyHistory,
          checkPersonalHistory,
          reasonFamilyHistory,
          reExaminate: checkReExaminate,
          dateExaminate: moment(values?.dateExaminate).utc().format(),
          reason,
          result,
          bmi: values?.bmi,
          height: values?.height ? parseFloat(values?.height) : "",
          weight: values?.weight ? parseFloat(values?.weight) : "",
        });
    else {
      addToast("Vui lòng chọn bệnh nhân!", {
        appearance: "error",
        autoDismiss: true,
      });
    }
  };

  const HandleBtns = () => (
    <div style={{ display: "flex", gap: 10, justifyContent: "flex-end" }}>
      {((params?.id === "add" && user?.decentralization[0]?.medical > 1) ||
        (params?.id !== "add" && user?.decentralization[0]?.medical > 2)) && (
        <Button
          type="submit"
          className="margin-0 float-right"
          disabled={mutationSaveMedical.isLoading}
        >
          {FORM_CHANGE_INFO.processBtn}
        </Button>
      )}
      {params?.id !== "add" && (
        <Button
          className="margin-0 float-right"
          disabled={mutationSaveMedical.isLoading}
          onClick={() =>
            history.push({
              pathname: `/app/dashboard/medicals/add`,
              state: medicalProfile,
            })
          }
        >
          Tạo bản sao
        </Button>
      )}
      {params?.id !== "add" && (
        <Button
          className="btn btn-info margin-0"
          onClick={() => setShowModalResult(true)}
        >
          Thêm kết quả
        </Button>
      )}
      {params?.id !== "add" && (
        <Button
          className="btn btn-info margin-0"
          onClick={() => setShowResults(true)}
        >
          Xem kết quả
        </Button>
      )}
    </div>
  );

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card.Header
        className="header-card"
        style={{
          position: "relative",
          marginLeft: "-30px",
          justifyContent: "space-between",
        }}
      >
        <div className="flex" style={{ gap: 10 }}>
          <p
            className="feather icon-chevron-left icon"
            onClick={() => history.goBack()}
          />
          <Card.Title as="h4" className="title">
            {params?.id === "add" ? "Thêm bệnh án" : FORM_CHANGE_INFO.title}
          </Card.Title>
        </div>
      </Card.Header>
      <HandleBtns />
      <SessionTitle
        collapse
        active={collapseInfo}
        onClick={() => setCollapseInfo(!collapseInfo)}
      >{`Thông tin bệnh nhân`}</SessionTitle>
      <Collapse in={collapseInfo}>
        <Row>
          {params?.id !== "add" && (
            <Col sm={6}>
              <label className="label-control items-center">Bác sĩ</label>

              <input
                className="form-control"
                style={{ paddingLeft: 10 }}
                value={medicalProfile?.updatedby}
                disabled
              />
            </Col>
          )}
          {params?.id !== "add" && (
            <Col sm={6}>
              <label className="label-control items-center">
                Ngày cập nhật
              </label>
              <input
                className="form-control"
                style={{ paddingLeft: 10 }}
                value={medicalProfile?.updatedat}
                disabled
              />
            </Col>
          )}
          <Col
            sm={6}
            className="flex"
            style={{ alignItems: "flex-end", alignSelf: "center" }}
          >
            <div
              className="self-center"
              style={{
                paddingTop: 10,
                width: "100%",
              }}
            >
              <label className="label-control items-center">
                Bệnh nhân <span style={{ color: "red" }}>*</span>{" "}
              </label>
              <Select
                name="patient"
                style={{ width: "100%" }}
                className="basic-single"
                classNamePrefix="select"
                options={queryPatient.isLoading ? [] : patientOptions}
                value={patientOptions?.find(
                  (item) => item.value === selectedPatient || ""
                )}
                onChange={(e) => setSelectedPatient(e.value)}
                onInputChange={_.debounce(function (e) {
                  setFirstName(e);
                }, 800)}
              />
            </div>

            {params?.id === "add" && (
              <Button
                onClick={() => setOpenModalQuick(true)}
                className="btn btn-primary margin-0 feather icon-user-plus ml-2"
                style={{ height: "38px" }}
              ></Button>
            )}
          </Col>
          <Col sm={6}>
            <div
              className="self-center"
              style={{
                paddingTop: 10,
                width: "100%",
                zIndex: 100,
              }}
            >
              <label className="label-control items-center">Loại phiếu</label>
              <Select
                name="category"
                style={{ width: "100%" }}
                className="basic-single"
                classNamePrefix="select"
                options={categorySchedule}
                value={categorySchedule?.find(
                  (item) => item.value === selectedCategory
                )}
                onChange={(e) => setSelectedCategory(e.value)}
                isSearchable
              />
            </div>
          </Col>
          {patientSection.map((item, index) => (
            <Col sm={item.span} key={index}>
              <Controller
                {...register(item.name)}
                control={control}
                render={({ field }) => (
                  <FormInput label={item.label} {...field} type={item.type} />
                )}
              />
            </Col>
          ))}
          <Col sm={6}>
            <div className="mt-10">
              <label>Giới tính</label>
              <div className="flex" style={{ marginLeft: "-10px" }}>
                <div className="form-check">
                  <FormInput
                    type="radio"
                    value="male"
                    checked={gender === "male"}
                    // {...field}
                    onChange={(e) =>
                      setGender(e?.target?.checked ? "male" : "female")
                    }
                  />

                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault1"
                  >
                    Nam
                  </label>
                </div>
                <div className="form-check" style={{ marginLeft: "50px" }}>
                  <FormInput
                    type="radio"
                    value="female"
                    onChange={(e) =>
                      setGender(!e?.target?.checked ? "male" : "female")
                    }
                    checked={gender === "female"}
                    // {...field}
                  />

                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault2"
                  >
                    Nữ
                  </label>
                </div>
              </div>
            </div>
          </Col>
          <Col>
            <Row className="">
              <Col sm={6} className="self-center">
                <label className="label-control items-center">
                  Tỉnh / Thành phố
                </label>
                <Select
                  className="basic-single"
                  placeholder="Chọn Tỉnh/Thành phố"
                  classNamePrefix="select"
                  options={cityList}
                  value={cityList.find((o) => o.value === selectedCity)}
                  onChange={(e) => handleChooseCity(e.value)}
                />
              </Col>

              <Col sm={6} className="self-center">
                <label className="label-control items-center">
                  Quận / Huyện / Thành phố
                </label>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  options={listProvince}
                  placeholder="Chọn Quận/Huyện/Thành phố"
                  value={
                    listProvince.find((o) => o.value === selectedProvince) ||
                    null
                  }
                  onChange={(e) => handleChooseProvince(e.value)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseLive}
        onClick={() => setCollapseLive(!collapseLive)}
        mt={30}
      >{`Sinh hiệu`}</SessionTitle>
      <Collapse in={collapseLive}>
        <Row>
          {vitalSections.map((item, index) => (
            <Col sm={item.span} key={index}>
              <Controller
                {...register(item?.name)}
                control={control}
                render={({ field }) => (
                  <FormInput
                    label={item.label}
                    {...field}
                    placeholderInput={`Nhập ${item?.label?.toLowerCase()} (${
                      item.suffixInput
                    })`}
                    type={item?.type}
                    suffixInput={` ${item?.suffixInput}`}
                    min={item?.min}
                  />
                )}
              />
            </Col>
          ))}
          <Col sm={6}>
            <Controller
              {...register("bmi")}
              control={control}
              render={({ field }) => (
                <FormInput
                  label={"Chỉ số BMI"}
                  {...field}
                  disabled
                  placeholderInput={`Chỉ số BMI (kg/m²)`}
                  type={"autonumber"}
                  suffix={` kg/m²`}
                  min={0}
                />
              )}
            />
          </Col>
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseClinical}
        onClick={() => setCollapseClinical(!collapseClinical)}
        mt={30}
      >{`Khám lâm sàng`}</SessionTitle>
      <Collapse in={collapseClinical}>
        <Row>
          {clinicalSections.map((item, index) => (
            <Col sm={item.span} key={index}>
              <Controller
                {...register(item.name)}
                control={control}
                render={({ field }) => (
                  <FormInput label={item.label} {...field} type={item.type} />
                )}
              />
            </Col>
          ))}
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseHistory}
        onClick={() => setCollapseHistory(!collapseHistory)}
        mt={30}
      >{`Tóm tắt tiền sử bệnh`}</SessionTitle>
      <Collapse in={collapseHistory}>
        <Row>
          <Col sm={12}>
            <Row className="flex" style={{ alignItems: "center" }}>
              <Col sm={1}>
                <Controller
                  {...register("checkFamilyHistory")}
                  control={control}
                  render={({ field, onChange }) => (
                    <input
                      style={{ width: "fit-content", marginLeft: "20px" }}
                      {...field}
                      onChange={(e) => setCheckFamilyHistory(e.target.checked)}
                      defaultChecked={field?.value}
                      type="checkbox"
                      className="form-control"
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <p className="cell-p">Tiền sử bệnh gia đình</p>
              </Col>
            </Row>
            <p>
              Có ai trong gia đình ông(bà) mắc 1 trong các bệnh: truyền nhiễm,
              tim mạch, đái tháo đường, lao, hen phế quản, ung thư, rối loạn tâm
              thần, bệnh khác.
            </p>
          </Col>
          {checkFamilyHistory && (
            <Col sm={12}>
              <Controller
                {...register("reasonFamilyHistory", {
                  required: checkFamilyHistory && !reasonFamilyHistory,
                })}
                control={control}
                render={({ field }) => (
                  <div>
                    <label
                      className="label-control mt-2"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Nếu có, đề nghị ghi tên cụ thể
                    </label>
                    <EditorCkClassic
                      html={reasonFamilyHistory || "<p></p>"}
                      onChange={(value) => setReasonFamilyHistory(value)}
                    />
                    {errors.reasonFamilyHistory && reasonFamilyHistory && (
                      <span className="text-c-red">
                        * Vui lòng nhập tóm tắt tiền sử bệnh{" "}
                      </span>
                    )}
                  </div>
                )}
              />
            </Col>
          )}
          <Col sm={12} className="mt-4">
            <Row className="flex" style={{ alignItems: "center" }}>
              <Col sm={1}>
                <Controller
                  {...register("checkSpecialDiseaseHistory")}
                  control={control}
                  render={({ field, onChange }) => (
                    <input
                      style={{ width: "fit-content", marginLeft: "20px" }}
                      {...field}
                      onChange={(e) =>
                        setCheckSpecialDiseaseHistory(e.target.checked)
                      }
                      defaultChecked={field?.value}
                      type="checkbox"
                      className="form-control"
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <p className="cell-p">Câu hỏi khác</p>
              </Col>
            </Row>
            <p>Ông(bà) có đang điều trị bệnh gì không?</p>
          </Col>
          {checkSpecialDiseaseHistory && (
            <Col sm={12}>
              <Controller
                {...register("reasonSpecialDiseaseHistory", {
                  required:
                    checkSpecialDiseaseHistory && !reasonSpecialDiseaseHistory,
                })}
                control={control}
                render={({ field }) => (
                  <div>
                    <label
                      className="label-control mt-2"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Nếu có xin hãy liệt kê các loại thuốc đang dùng và liều
                      lượng
                    </label>
                    <EditorCkClassic
                      html={reasonSpecialDiseaseHistory || "<p></p>"}
                      onChange={(value) =>
                        setReasonSpecialDiseaseHistory(value)
                      }
                    />
                    {errors.reasonSpecialDiseaseHistory &&
                      !reasonSpecialDiseaseHistory && (
                        <span className="text-c-red">
                          * Vui lòng nhập thông tin
                        </span>
                      )}
                  </div>
                )}
              />
            </Col>
          )}
          <Col sm={12} className="mt-4">
            <Row className="flex" style={{ alignItems: "center" }}>
              <Col sm={1}>
                <Controller
                  {...register("checkPregnancyHistory")}
                  control={control}
                  render={({ field, onChange }) => (
                    <input
                      style={{ width: "fit-content", marginLeft: "20px" }}
                      {...field}
                      onChange={(e) =>
                        setCheckPregnancyHistory(e.target.checked)
                      }
                      defaultChecked={field?.value}
                      type="checkbox"
                      className="form-control"
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <p className="cell-p">Tiền sử thai sản</p>
              </Col>
            </Row>
            <p> (Đối với phụ nữ)</p>
          </Col>
          {checkPregnancyHistory && (
            <Col sm={12}>
              <Controller
                {...register("reasonPregnancyHistory", {
                  required: checkPregnancyHistory && !reasonPregnancyHistory,
                })}
                control={control}
                render={({ field }) => (
                  <div>
                    <label
                      className="label-control mt-2"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Nếu có, đề nghị ghi tên cụ thể
                    </label>
                    <EditorCkClassic
                      html={reasonPregnancyHistory || "<p></p>"}
                      onChange={(value) => setReasonPregnancyHistory(value)}
                    />
                    {errors.reasonPregnancyHistory &&
                      !reasonPregnancyHistory && (
                        <span className="text-c-red">
                          * Vui lòng nhập thông tin tiền sử thai sản
                        </span>
                      )}
                  </div>
                )}
              />
            </Col>
          )}
          <Col sm={12} className="mt-4">
            <Row className="flex" style={{ alignItems: "center" }}>
              <Col sm={1}>
                <Controller
                  {...register("checkPersonalHistory")}
                  control={control}
                  render={({ field, onChange }) => (
                    <input
                      style={{ width: "fit-content", marginLeft: "20px" }}
                      {...field}
                      onChange={(e) =>
                        setCheckPersonalHistory(e.target.checked)
                      }
                      defaultChecked={field?.value}
                      type="checkbox"
                      className="form-control"
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <p className="cell-p">Tiền sử bản thân</p>
              </Col>
            </Row>
            <p>
              {" "}
              Ông(bà) mắc 1 trong các bệnh: truyền nhiễm, tim mạch, đái tháo
              đường, lao, hen phế quản, ung thư, rối loạn tâm thần, bệnh khác
            </p>
          </Col>
          {checkPersonalHistory && (
            <Col sm={12}>
              <Controller
                {...register("reasonPersonalHistory", {
                  required: checkPersonalHistory && !reasonPersonalHistory,
                })}
                control={control}
                render={({ field }) => (
                  <div>
                    <label
                      className="label-control mt-2"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      Nếu có, đề nghị ghi tên cụ thể
                    </label>
                    <EditorCkClassic
                      html={reasonPersonalHistory || "<p></p>"}
                      onChange={(value) => setReasonPersonalHistory(value)}
                    />
                    {errors.reasonPersonalHistory && !reasonPersonalHistory && (
                      <span className="text-c-red">
                        * Vui lòng nhập thông tin tiền sử bệnh bản thân
                      </span>
                    )}
                  </div>
                )}
              />
            </Col>
          )}
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseDetail}
        onClick={() => setCollapseDetail(!collapseDetail)}
        mt={30}
      >{`Tiền sử bệnh lý chi tiết`}</SessionTitle>
      <Collapse in={collapseDetail}>
        <Row>
          {detailDiases.map((item, index) => (
            <Col sm={12} key={index} className="mb-3">
              <Row className="flex" style={{ alignItems: "center" }}>
                <Col sm={1}>
                  <Controller
                    {...register(item.name.toString())}
                    control={control}
                    render={({ field }) => {
                      return (
                        <input
                          style={{ width: "fit-content", marginLeft: "20px" }}
                          {...field}
                          type="checkbox"
                          defaultChecked={field?.value}
                          className="form-control"
                        />
                      );
                    }}
                  />
                </Col>
                <Col sm={11}>
                  <p className="margin-0">{item.label}</p>
                </Col>
              </Row>
            </Col>
          ))}
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseWarranty}
        onClick={() => setCollapseWarranty(!collapseWarranty)}
        mt={30}
      >{`Thông tin bảo hiểm`}</SessionTitle>
      <Collapse in={collapseWarranty}>
        <Row>
          {insuranceSections.map((item, index) => (
            <Col sm={12} key={index} className="mb-3">
              <Row className="flex" style={{ alignItems: "center" }}>
                <Col sm={1}>
                  <Controller
                    {...register(item.name.toString())}
                    control={control}
                    render={({ field }) => (
                      <input
                        style={{ width: "fit-content", marginLeft: "20px" }}
                        {...field}
                        type="checkbox"
                        defaultChecked={field?.value}
                        className="form-control"
                        defaultValue={false}
                      />
                    )}
                  />
                </Col>
                <Col sm={11}>
                  <p className="margin-0">{item.label}</p>
                </Col>
              </Row>
            </Col>
          ))}
        </Row>
      </Collapse>
      <SessionTitle
        collapse
        active={collapseSum}
        onClick={() => setCollapseSum(!collapseSum)}
        mt={30}
      >{`Kết luận`}</SessionTitle>
      <Collapse in={collapseSum}>
        <Row>
          <Col sm={12}>
            <Controller
              {...register("reason")}
              control={control}
              render={({ field }) => (
                <div>
                  <label
                    className="label-control mt-2"
                    style={{ whiteSpace: "nowrap" }}
                  >
                    Chẩn đoán
                  </label>
                  <EditorCkClassic
                    html={reason || "<p></p>"}
                    onChange={(value) => setReason(value)}
                  />
                </div>
              )}
            />
          </Col>
          <Col sm={12}>
            <Controller
              {...register("result")}
              control={control}
              render={({ field }) => (
                <div>
                  <label
                    className="label-control mt-2"
                    style={{ whiteSpace: "nowrap" }}
                  >
                    Kết quả
                  </label>
                  <EditorCkClassic
                    html={result || "<p></p>"}
                    onChange={(value) => setResult(value)}
                  />
                </div>
              )}
            />
          </Col>
          <Col sm={12} className="mt-4">
            <Row className="flex" style={{ alignItems: "center" }}>
              <Col sm={1}>
                <Controller
                  {...register("reExaminate")}
                  control={control}
                  render={({ field, onChange }) => (
                    <input
                      style={{ width: "fit-content", marginLeft: "20px" }}
                      {...field}
                      onClick={() => setCheckReExaminate(!checkReExaminate)}
                      checked={checkReExaminate}
                      type="checkbox"
                      className="form-control"
                    />
                  )}
                />
              </Col>
              <Col sm={4}>
                <p className="cell-p">Quyết định tái khám</p>
              </Col>
            </Row>
          </Col>
          {checkReExaminate && (
            <Col sm={12}>
              <Controller
                {...register("dateExaminate", {
                  required: checkReExaminate,
                })}
                control={control}
                render={({ field }) => (
                  <FormInput
                    label="Ngày tái khám"
                    {...field}
                    required={checkReExaminate}
                    type="date"
                  />
                )}
              />
            </Col>
          )}
        </Row>
      </Collapse>
      {params && params?.id !== "add" && (
        <>
          <SessionTitle
            collapse
            active={collapseOrder}
            onClick={() => setCollapseOrder(!collapseOrder)}
            mt={30}
          >{`Phiếu bán hàng`}</SessionTitle>
          <Collapse in={collapseOrder}>
            <div>
              {medicalProfile?.transaction ? (
                <OrderInformation
                  id={
                    medicalProfile?.transaction &&
                    medicalProfile?.transaction[0]?.id
                  }
                />
              ) : (
                <></>
              )}
            </div>
          </Collapse>
        </>
      )}

      <CustomModal
        onChangeVisible={() => setShowModalResult(!showModalResult)}
        title={medicalResult ? "Kết quả" : "Thêm kết quả"}
        height="30vh"
        show={showModalResult}
      >
        <QuickForm
          data={medicalResult}
          onCancel={() => setShowModalResult(false)}
          medicalId={medicalProfile?.id}
        ></QuickForm>
      </CustomModal>
      <CustomModal
        onChangeVisible={() => setShowResults(!showResults)}
        title={"Kết quả"}
        height="30vh"
        show={showResults}
      >
        <div>
          <Button
            style={{ position: "absolute", right: 30, top: "-52px" }}
            onClick={() => {
              setShowModalResult(true);
              setShowResults(!showResults);
            }}
          >
            Thêm kết quả
          </Button>
          {medicalProfile?.results?.length ? (
            medicalProfile?.results?.map((item, index) => {
              return <ResultList data={item}></ResultList>;
            })
          ) : (
            <div>Không có kết quả</div>
          )}
        </div>
      </CustomModal>
      <CustomModal
        onChangeVisible={() => setOpenModalQuick(!openModalQuick)}
        title="Thêm bệnh nhân"
        height="40vh"
        show={openModalQuick}
      >
        <QuickFormPatient
          onCancel={() => setOpenModalQuick(false)}
        ></QuickFormPatient>
      </CustomModal>
      <ConfirmModal
        show={visibleConfirmSave}
        onChangeVisible={() => {
          setVisibleConfirmSave(!visibleConfirmSave);
        }}
        onConfirm={() => onSaveMedical()}
        title="Xác nhận"
        content={`Bạn có chắc chắn muốn ${
          typeAction === "add" ? "thêm" : "sửa"
        } thông tin bệnh án này?`}
        loading={mutationAddMedical.isLoading || mutationSaveMedical.isLoading}
      />
      {/* <ConfirmModal
        show={visibleConfirmDelete}
        onChangeVisible={() => {
          setVisibleConfirmDelete(!visibleConfirmDelete);
        }}
        onConfirm={() => handleDeleteMedical()}
        title="Xác nhận"
        content={`Bạn có chắc chắn muốn xóa thông tin bệnh án này?`}
        loading={mutationAddMedical.isLoading || mutationSaveMedical.isLoading}
      />*/}
      <HandleBtns />
    </form>
  );
};
