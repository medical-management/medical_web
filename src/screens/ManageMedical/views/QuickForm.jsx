/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import ConfirmModal from "../../../components/ConfirmModal";
import * as actions from "../actions";
import { APIS_SAVE_MEDICAL_RESULT } from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";
import { FormInput } from "../../../components/FormInputCustom";
import { useToasts } from "react-toast-notifications";
import Select from "react-select";
import SessionTitle from "../../../components/SessionTitle";

export default function QuickForm(props) {
  return (
    <React.Fragment>
      <div>
        <SubmitValidationForm data={props} />
      </div>
    </React.Fragment>
  );
}

const hematologyForm = [
  { type: "WBC", name: "resultWBC", normalValue: "4 -10", unit: "10^9/l" },
  { type: "RBC", name: "resultRBC", normalValue: "3.8 - 5.6", unit: "10^12/l" },
  { type: "Hp", name: "resultHP", normalValue: "12 - 18", unit: "g/l	" },
  { type: "Hct", name: "resultHCT", normalValue: "35 -52", unit: "%" },
  { type: "MCV", name: "resultMCV", normalValue: "80 - 97", unit: "Fl" },
  { type: "MCH", name: "resultMCH", normalValue: "26 - 32", unit: "g/dl" },
  {
    type: "MCHC",
    name: "resultMCHC",
    normalValue: "130 - 400",
    unit: "10^9/l	",
  },
  { type: "PLT", name: "resultPLT", normalValue: "11 - 15.7", unit: "%	" },
  { type: "MPV", name: "resultMPV", normalValue: "6.30 - 12", unit: "FI" },
  { type: "RDW", name: "resultRDW", normalValue: "11 - 15.7", unit: "%" },
  {
    type: "% Neutro",
    name: "resultNeutro",
    normalValue: "40 -74",
    unit: "%",
  },
  { type: "% Lym", name: "resultLym", normalValue: "19 - 48", unit: "%" },
  { type: "% Mono", name: "resultMono", normalValue: "3 - 9", unit: "%" },
];

const urineForm = [
  { type: "Bạch cầu (LEU)", name: "resultLEU", normalValue: "< 10/μL" },
  { type: "Nitrit (NIT)", name: "resultNIT", normalValue: "Âm Tính" },
  {
    type: "Urobilinogen (URO)",
    name: "resultURO",
    normalValue: "< 16,9μmol/L",
  },
  { type: "Protein (PRO)", name: "resultPRO", normalValue: "<0.1 g/L" },
  { type: "pH (PH)", name: "resultUrePH", normalValue: "4,8 - 7,4" },
  { type: "Hồng cầu (BLD)", name: "resultBLD", normalValue: "< 0,5/μL" },
  { type: "Tỉ trọng (SG)", name: "resultSG", normalValue: "1,015 - 1,025" },
  { type: "Thể cetonic", name: "resultcetonic", normalValue: "< 5 mmol/L" },
  { type: "Bilirubin (BIL)", name: "resultBIL", normalValue: "< 3,4 μmol/L" },
  { type: "Glucose (GLU)", name: "resultGLU", normalValue: "< 0,84 mol/L" },
  { type: "Dưỡng chấp", name: "resultChyme", normalValue: "" },
  { type: "Porphyrin", name: "resultPorphyrin", normalValue: "" },
  {
    type: "Protein Bence-Jones",
    name: "resultProteinBencejones",
    normalValue: "",
  },
];

const biochemicalForm = [
  {
    type: "Glucoza",
    name: "resultGlucoza",
    section: 1,
    normalValue: "80 - 110",
    unit: " mg/dl",
  },
  {
    type: "HbA1c",
    name: "resultHbA1c",
    section: 1,
    normalValue: "4,7 - 6,4",
    unit: " %",
  },
  {
    type: "URÊ",
    name: "resultUre",
    section: 2,
    normalValue: "20 - 40 ",
    unit: "mg/dl",
  },
  {
    type: "Creatine",
    name: "resultCritine",
    section: 2,
    normalValue: "1 - 2 ",
    unit: "mg/dl",
  },
  {
    type: "SGOT",
    name: "resultSGOT",
    section: 3,
    normalValue: "12 - 28",
    unit: " U/L",
  },
  {
    type: "SGPT",
    name: "resultSGPT",
    section: 3,
    normalValue: "12 - 35 ",
    unit: "U/L",
  },
  {
    type: "GGT",
    name: "resultGGT",
    section: 3,
    normalValue: "7 - 50 ",
    unit: "U/L",
  },

  { type: "HbsAg", name: "resultHbsAg", section: 4, normalValue: "Âm tính" },
  { type: "HbeAg", name: "resultHbeAg", section: 4, normalValue: "Âm tính" },
  {
    type: "Anit Hbs",
    name: "resultAnitHbs",
    section: 4,
    normalValue: "Âm tính",
  },
  {
    type: "Anti HCV",
    name: "resultAnitHCV",
    section: 4,
    normalValue: "Âm tính",
  },
  {
    type: "Cholesterol",
    name: "resultCholesterol",
    section: 5,
    normalValue: "130 - 180 ",
    unit: "mg/dl",
  },
  {
    type: "Triglyceric",
    name: "resultTriglyceric",
    section: 5,
    normalValue: "< 175",
    unit: " mg/dl",
  },
  { type: "ASO", name: "resultASO", section: 6, normalValue: "Âm tính" },
  { type: "AFP", name: "resultAFP", section: 7, normalValue: "Âm tính" },
  {
    type: "Acid Uric",
    name: "resultAcidUric",
    section: 7,
    normalValue: "120 - 420",
    unit: " μmol/L",
  },
  { type: "HP", name: "resultAnotherHP", section: 7, normalValue: "Âm tính" },
  {
    type: "Troponin",
    name: "resultTroponin",
    section: 7,
    normalValue: "Âm tính",
  },
  { type: "HIV", name: "resultHIV", section: 7, normalValue: "Âm tính" },
  { type: "TS", name: "resultTS", section: 7, normalValue: "2 - 5 phút" },
  { type: "TC", name: "resultTC", section: 7, normalValue: "8 - 15 phút" },
];

export const listResultType = [
  {
    value: "xray",
    label: "X-Quang",
  },
  {
    value: "hematology",
    label: "Xét nghiệm huyết học",
  },
  {
    value: "urine",
    label: "Xét nghiệm nước tiểu",
  },
  {
    value: "biochemical",
    label: "Xét nghiệm sinh hóa",
  },
  {
    value: "ECG",
    label: "Điện tâm đồ",
  },
];

let SubmitValidationForm = (props) => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const dispatch = useDispatch();

  const [payload, setPayload] = useState(null);

  const { addToast } = useToasts();

  const [gender, setGender] = useState("male");

  const [visibleConfirm, setVisibleConfirm] = useState(false);

  const mutationSaveMedicalResult = useMutation(
    "patient",
    APIS_SAVE_MEDICAL_RESULT
  );

  const [selectedResult, setSelectedResult] = useState("");

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [step, setStep] = useState(1);

  const submit = (values) => {
    setPayload({
      ...values,
      type: selectedResult,
      medicalId: props.data.medicalId,
    });
  };

  const handleSave = (status) => {
    mutationSaveMedicalResult.mutate({
      input: { ...payload },
    });
  };

  useEffect(() => {
    if (payload) setVisibleConfirm(true);
  }, [payload]);

  useEffect(() => {
    if (!visibleConfirm) setPayload(null);
  }, [visibleConfirm]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationSaveMedicalResult;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm kết quả thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        dispatch(actions.ADD_MEDICAL_RESULT_SUCCESS(data));
        setVisibleConfirm(false);
        props.data.onCancel();
      } else if (isError)
        addToast("Thêm kết quả thất bại, vui lòng thử lại", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationSaveMedicalResult.isLoading]);

  return (
    <>
      <form onSubmit={handleSubmit(submit)}>
        {step === 1 && (
          <div>
            <div
              className="self-center"
              style={{
                paddingTop: 10,
                width: "100%",
                zIndex: 100,
              }}
            >
              <label className="label-control items-center">
                Vui lòng chọn loại kết quả muốn thêm
              </label>
              <Select
                style={{ width: "100%" }}
                className="basic-single"
                classNamePrefix="select"
                options={listResultType}
                value={listResultType?.find(
                  (item) => item.value === selectedResult
                )}
                onChange={(e) => setSelectedResult(e.value)}
              />
            </div>
            <div className="flex mt-10 " style={{ justifyContent: "flex-end" }}>
              <Button
                className="margin-0"
                disabled={selectedResult === ""}
                onClick={() => setStep(2)}
              >
                Tiếp theo {">"}{" "}
              </Button>
            </div>
          </div>
        )}
        {step === 2 && (
          <>
            {selectedResult === "xray" && (
              <Row>
                <Col sm={12}>
                  <Controller
                    {...register("reqXQuang", { required: true })}
                    control={control}
                    render={({ field }) => (
                      <FormInput
                        required
                        label={"Yêu cầu"}
                        error={
                          errors?.resultXQuang ? "Vui lòng nhập Yêu cầu" : ""
                        }
                        {...field}
                      />
                    )}
                  />
                </Col>
                <Col sm={12}>
                  <Controller
                    {...register("resultXQuang", { required: true })}
                    control={control}
                    render={({ field }) => (
                      <FormInput
                        required
                        label={"Kết quả"}
                        error={
                          errors?.resultXQuang ? "Vui lòng nhập kết quả" : ""
                        }
                        {...field}
                      />
                    )}
                  />
                </Col>
              </Row>
            )}
            {selectedResult === "hematology" && (
              <div>
                <SessionTitle>Xét nghiệm huyết học</SessionTitle>
                <Row style={{ alignItems: "center" }}>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Loại XN
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={6}
                  >
                    Kết quả
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Giá trị <br /> bình thường
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Đơn vị
                  </Col>
                </Row>
                {hematologyForm.map((item, key) => {
                  return (
                    <Row style={{ alignItems: "center" }}>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                          fontWeight: "bold",
                        }}
                      >
                        {item.type}
                      </Col>
                      <Col
                        sm={6}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          alignItems: "center",
                          borderBottom: "1px solid #cecece",
                        }}
                      >
                        <Controller
                          {...register(item.name)}
                          control={control}
                          render={({ field }) => <FormInput {...field} />}
                        />
                      </Col>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                        }}
                      >
                        {item?.normalValue}
                      </Col>
                      <Col
                        sm={2}
                        style={{
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                        }}
                      >
                        {item?.unit}
                      </Col>
                    </Row>
                  );
                })}
              </div>
            )}

            {selectedResult === "urine" && (
              <div>
                <SessionTitle>Xét nghiệm nước tiểu</SessionTitle>
                <Row style={{ alignItems: "center" }}>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Loại XN
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={8}
                  >
                    Kết quả
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Giá trị <br /> bình thường
                  </Col>
                </Row>
                {urineForm.map((item, key) => {
                  return (
                    <Row style={{ alignItems: "center" }}>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                          fontWeight: "bold",
                        }}
                      >
                        {item.type}
                      </Col>
                      <Col
                        sm={8}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          alignItems: "center",
                          borderBottom: "1px solid #cecece",
                        }}
                      >
                        <Controller
                          {...register(item.name)}
                          control={control}
                          render={({ field }) => <FormInput {...field} />}
                        />
                      </Col>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                        }}
                      >
                        {item?.normalValue}
                      </Col>
                    </Row>
                  );
                })}
              </div>
            )}

            {selectedResult === "biochemical" && (
              <div>
                <SessionTitle>Xét nghiệm sinh hóa</SessionTitle>
                <Row style={{ alignItems: "center" }}>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Loại XN
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={6}
                  >
                    Kết quả
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Giá trị <br /> bình thường
                  </Col>
                  <Col
                    style={{ textAlign: "center", fontWeight: "bold" }}
                    sm={2}
                  >
                    Đơn vị
                  </Col>
                </Row>
                {biochemicalForm.map((item, key) => {
                  return (
                    <Row style={{ alignItems: "center" }}>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                          fontWeight: "bold",
                        }}
                      >
                        {item.type}
                      </Col>
                      <Col
                        sm={6}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          alignItems: "center",
                          borderBottom: "1px solid #cecece",
                        }}
                      >
                        <Controller
                          {...register(item.name)}
                          control={control}
                          render={({ field }) => <FormInput {...field} />}
                        />
                      </Col>
                      <Col
                        sm={2}
                        style={{
                          borderRight: "1px solid #cecece",
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                        }}
                      >
                        {item?.normalValue}
                      </Col>
                      <Col
                        sm={2}
                        style={{
                          height: "60px",
                          display: "flex",
                          borderBottom: "1px solid #cecece",
                          alignItems: "center",
                          placeContent: "center",
                        }}
                      >
                        {item?.unit}
                      </Col>
                    </Row>
                  );
                })}
              </div>
            )}

            <Card.Footer
              style={{
                backgroundColor: "white",
              }}
            >
              <div
                style={{
                  display: "flex",
                  gap: "10px",
                  justifyContent: "flex-end",
                }}
              >
                <Button className="margin-0" onClick={() => setStep(1)}>
                  {"<"} Trở lại
                </Button>
                <Button
                  className="margin-0 "
                  disabled={mutationSaveMedicalResult.isLoading}
                  variant="danger"
                  onClick={props?.data?.onCancel}
                >
                  Hủy
                </Button>

                {user?.decentralization[0]?.medical > 2 && (
                  <Button
                    type="submit"
                    className="margin-0"
                    disabled={mutationSaveMedicalResult.isLoading}
                  >
                    {FORM_CHANGE_INFO.processBtn}
                  </Button>
                )}
              </div>
            </Card.Footer>
          </>
        )}
      </form>
      <ConfirmModal
        show={visibleConfirm}
        onChangeVisible={() => {
          setVisibleConfirm(!visibleConfirm);
        }}
        onConfirm={() => handleSave()}
        title="Xác nhận"
        content={`Bạn có chắc chắn thêm kết quả này?`}
        loading={mutationSaveMedicalResult.isLoading}
      />
    </>
  );
};
