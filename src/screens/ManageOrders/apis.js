/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_ORDERS = async (input) =>
    await graphQLClient({
        queryString: ` query {
      find_all_sales_orders(req: { 
            take: ${input.take}, 
            skip: ${input.skip}, 
            ${input.status ? `status: "${input.status}"` : ""} 
            ${input.name ? `name: "${input.name}"` : ""} 
            ${input.date ? `date: "${input.date}"` : ""} 
            ${input.patientId ? `patientId: "${input.patientId}"` : ""} 
            ${input.itemId ? `itemId: "${input.itemId}"` : ""} 
            ${input.expenseName ? `expenseName: ${input.expenseName}` : ""} 
      }) {
      data {
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        patient{
          id
          firstname
          lastname
          dob
        }
        items{
          item {
            id
            name
          }
          id
          unit
          location
          note
          quantity
          isGrossProfit
          perGrossProfit
          perTax
          basePrice
          price
          taxAmount
          amount

        }
        expenses{
          createdat
          updatedat
          transaction {
            id
          }
          id
          name
          note
          perTax
          price
          taxAmount
          amount
        }
        recordType
        id
        type
        status
        name
        vendor
        location
        note
        paymentMenthod
        paymentNote
        paymentDate
        vendorName
        vendorPhone
        vendorAddress
        date
        totalTax
        totalNet
        totalGross
        totalAmount
      },
      take
      skip
      total
      nextPage
    }
  }`,
    });

export const APIS_FIND_ONE_ORDER = async (input) => {
    let resOderDetail = await graphQLClient({
        queryString: `query {
      find_one_sales_order(id: ${input.id}) {
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        patient{
          id
          firstname
          lastname
          dob
          phone
        }
        items{
          item {
            id
            name
            taxPrices
            salePrices
          }
          id
          unit
          inactive
          location
          note
          quantity
          isGrossProfit
          perGrossProfit
          perTax
          basePrice
          price
          taxAmount
          amount
        }
        expenses{
          createdat
          updatedat
          transaction {
            id
          }
          id
          name
          inactive
          note
          perTax
          price
          taxAmount
          amount
        }
        medical {
          id
        }
        recordType
        id
        status
        name
        vendor
        location
        note
        paymentMenthod
        paymentNote
        paymentDate
        vendorName
        vendorPhone
        vendorAddress
        date
        totalTax
        totalNet
        totalGross
        totalAmount
        }
    }`,
    });
    if (resOderDetail && resOderDetail.find_one_sales_order.items.length > 0) {
        const itemsOrder = [];
        for (const item of resOderDetail.find_one_sales_order.items) {
            const { find_fast_item } = await graphQLClient({
                queryString: ` query {
                          find_fast_item(req: { 
                            take: 1, 
                            skip: 0, 
                            ${`name: "${item.item.name}"`} 
                      }) {
                        id
                        name
                        shortName
                        salePrices,
                        taxPrices
                        available
                    }
                  }`,
            });
            item.item["shortName"] = item["shortName"] = find_fast_item.length > 0 ? find_fast_item[0].shortName : "";

            itemsOrder.push({
                ...item,
                salePrices: find_fast_item[0].salePrices,
                shortName: find_fast_item.length > 0 ? find_fast_item[0].shortName : 0,
                available: find_fast_item[0].available,
            });
        }

        resOderDetail.find_one_sales_order.items = await itemsOrder;
    }
    return await resOderDetail;
};

export const APIS_SAVE_SALE_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (key === "id" || key === "patient" || key === "medical" || key === "inactive")
            input.push(`${key}: ${data[key]}`);
        else if (
            key === "type" ||
            key === "status" ||
            key === "note" ||
            key === "paymentMenthod" ||
            key === "paymentNote"
        )
            input.push(`${key}: "${data[key]}"`);
        else if (key === "date" || key === "paymentDate") input.push(`${key}: "${moment(data[key]).utc().format()}"`);
    }
    return await graphQLClient({
        queryString: ` mutation {
      save_sales_order(input: { 
            recordType: "salesOrder",
            type: "salesOrder",
            ${input.join()}  
          }) {
            createdat
            updatedat
            updatedby {
              id
              firstname
              lastname
            }
            items{
              item {
                id
                name
                taxPrices
                salePrices
              }
              id
              unit
              location
              note
              quantity
              isGrossProfit
              perGrossProfit
              perTax
              basePrice
              price
              taxAmount
              amount
            }
            expenses{
              createdat
              updatedat
              transaction {
                id
              }
              id
              name
              note
              perTax
              price
              taxAmount
              amount
            }
         
            recordType
            id
            type
            status
            name
            vendor
            location
            note
            paymentMenthod
            paymentNote
            paymentDate
            vendorName
            vendorPhone
            vendorAddress
            date
            totalTax
            totalNet
            totalGross
            totalAmount
        }
    }`,
    });
};

export const APIS_CREATE_SALE_ORDER_FROM_MEDICAL = async (input) => {
    return await graphQLClient({
        queryString: ` mutation {
      save_sales_order(input: { 
            recordType: "salesOrder",
            ${input.id === "" ? "" : `id: ${input.id}`}
            patient: ${input.patient},
            ${input.medical === "" ? "" : `medical: ${input.medical}`}
            
            type: "${input.type}",
            status: "${input.status}",
            ${input.note ? `note: "${input.note}"` : ""}  
            ${input.date ? `date: "${input.date}"` : ""}  
            ${input.paymentMenthod ? `paymentMenthod: "${input.paymentMenthod}"` : ""}  
            ${input.paymentNote ? `paymentNote: "${input.paymentNote}"` : ""}  
            ${input.paymentDate ? `paymentDate: "${input.paymentDate}"` : ""}  
          }) {
           id
           
        }
    }`,
    });
};

export const APIS_FIND_ALL_MEDICAL = async ({ take, skip, name }) =>
    await graphQLClient({
        queryString: ` query {
      find_all_medical(req: { 
            take: ${take}, 
            skip: ${skip}, 
            ${name ? `name: "${name}"` : ""}           
      }) {
      data {
        patient {
          id
          firstname
          lastname
          dob
        }
        id
        name
        category
        firstname
        lastname
        reExaminate
        dateExaminate
      },
      take
      skip
      total
      nextPage
    }
  }`,
    });

export const APIS_SAVE_ITEM_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (
            key === "id" ||
            key === "itemId" ||
            key === "transactionId" ||
            key === "isGrossProfit" ||
            key === "perGrossProfit" ||
            key === "quantity" ||
            key === "basePrice" ||
            key === "perTax" ||
            key === "price" ||
            key === "amount" ||
            key === "inactive"
        )
            input.push(`${key}: ${data[key]}`);
        else if (key === "unit" || key === "location" || key === "note") input.push(`${key}: "${data[key]}"`);
        else if (key === "item") input.push(`itemId: ${data[key].id}`);
    }
    return await graphQLClient({
        queryString: ` mutation {
        save_item_transaction(input: {${input.join()}}) {
                id
          }
      }`,
    });
};

export const APIS_SAVE_SERVICES_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (
            key === "id" ||
            key === "transactionId" ||
            key === "quantity" ||
            key === "perTax" ||
            key === "price" ||
            key === "amount" ||
            key === "inactive"
        )
            input.push(`${key}: ${data[key]}`);
        else if (key === "name" || key === "note") input.push(`${key}: "${data[key]}"`);
    }
    return await graphQLClient({
        queryString: ` mutation {
      save_expenses_transaction(input: {${input.join()}}) {
              id
        }
    }`,
    });
};
