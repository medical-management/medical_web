import React from "react";
import { Button } from "react-bootstrap";
import moment from "moment";
import {
  Table,
  DataTableCell,
  TableBody,
  TableCell,
  TableHeader,
} from "@david.kucsai/react-pdf-table";
import { saveAs } from "file-saver";
import {
  PDFDownloadLink,
  Document,
  Page,
  Text,
  View,
  Image,
} from "@react-pdf/renderer";
import PdfDocument from "@react-pdf/renderer";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { StyleSheet, Font } from "@react-pdf/renderer";
import FontNPB from "../../../assets/fonts/Tinos-Bold.ttf";
import FontNPBI from "../../../assets/fonts/Tinos-BoldItalic.ttf";
import FontNPI from "../../../assets/fonts/Tinos-Italic.ttf";
import FontNPRegular from "../../../assets/fonts/Tinos-Regular.ttf";
import { convertCurrency } from "../../../utils/convertCurrencyToString";
import _ from "lodash";
import { paymentOptions } from "../../../constants/options";
import LogoBill from "../../../assets/images/logo-bill.png";
import Parser from "html-react-parser";

Font.register({
  family: "NguyenPhuong",
  fonts: [
    { src: FontNPRegular }, // font-style: normal, font-weight: normal
    { src: FontNPI, fontStyle: "italic" },
    {
      src: FontNPBI,
      fontStyle: "italic",
      fontWeight: 700,
    },
    { src: FontNPB, fontWeight: 700 },
  ],
});

// Reference font
const styles = StyleSheet.create({
  title: {
    fontFamily: "NguyenPhuong",
  },
  body: {
    paddingTop: "5px",
    paddingBottom: "5px",
    paddingHorizontal: "5px",
    fontFamily: "NguyenPhuong",
  },
  session: {
    borderBottom: "1px solid black",
    padding: 10,
  },
  inside: {
    border: "1px solid black",
  },
  image: {
    width: 70,
    height: "70px",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 10,
    bottom: 20,
    right: 20,
    textAlign: "center",
    color: "grey",
    fontFamily: "NguyenPhuong",
  },
  price: {
    textAlign: "right",
    padding: "2px 5px",
    fontSize: "10px",
  },
  header: {
    textAlign: "center",
    padding: "2px 5px",
    fontSize: "10px",
    fontWeight: 700,
  },
  item: {
    padding: "2px 5px",
    fontSize: "10px",
  },
});

export default function ButtonPrint({ view }) {
  const { order } = useSelector((state) => state["ORDERS"]);
  const [selectedPayment, setSelectedPayment] = React.useState("");
  const [selectedPatient, setSelectedPatient] = React.useState("");
  const [paymentNote, setPaymentNote] = React.useState("");
  const [paymentDate, setPaymentDate] = React.useState(
    moment().utc().format("YYYY-MM-DD")
  );
  const [listItems, setListItem] = React.useState([]);
  const [listServices, setListServices] = React.useState([]);

  useEffect(() => {
    if (order) {
      setSelectedPatient(order?.patient?.id);
      setListItem(
        order?.items?.map((item, index) => {
          return {
            index: index,
            id: item?.id,
            name: item?.item?.name,
            unit: item?.unit,
            note: item?.note,
            isGrossProfit: item?.isGrossProfit,
            perGrossProfit: item?.perGrossProfit,
            quantity: item?.quantity,
            price: item?.price,
            taxAmount: item?.taxAmount,
            taxPrices: item?.perTax,
            bin: item?.basePrice,
            total: item?.amount,
            itemInfo: item?.item,
          };
        }) || []
      );
      setListServices(
        order?.expenses?.map((item, index) => {
          return {
            index: index + order?.items?.length,
            id: item?.id,
            name: item?.name ?? item?.item?.name,
            unit: item?.unit,
            note: item?.note,
            price: item?.price,
            taxPrices: item?.perTax,
            total: item?.amount,
            isGrossProfit: item?.isGrossProfit || "",
            perGrossProfit: item?.perGrossProfit || "",
            quantity: item?.quantity,
            taxAmount: item?.taxAmount,
            bin: item?.basePrice,
            itemInfo: item?.item,
          };
        }) || []
      );
      setPaymentNote(order?.paymentNote);
      setSelectedPayment(order?.paymentMenthod);
    }
  }, [order]);

  const MyDoc = () => (
    <Document>
      <Page style={{ padding: 20 }}>
        <View>
          <View style={styles.inside}>
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                ...styles.session,
              }}
              fixed
            >
              <Image style={styles.image} src={LogoBill} />
              <View style={{ flex: 1, marginLeft: 10 }}>
                <Text style={{ fontSize: 14, ...styles.title }}>
                  PHÒNG KHÁM ĐA KHOA
                </Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 14,
                    ...styles.title,
                  }}
                >
                  NGUYÊN PHƯƠNG
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    ...styles.title,
                  }}
                >
                  Mã số thuế: 1201604765
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    ...styles.title,
                  }}
                >
                  Địa chỉ: 271B Khu 4, Thị trấn Cái Bè, Huyện Cái Bè, Tiền Giang
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    ...styles.title,
                  }}
                >
                  Điện thoại: <b>(0273) 3823468</b>
                </Text>
              </View>
            </View>
            <View style={{ display: "flex", padding: 10 }}>
              <Text
                style={{
                  fontWeight: "bold",
                  margin: "0px 0px 10px 0px",
                  textAlign: "center",
                  ...styles.title,
                }}
              >
                HÓA ĐƠN BÁN LẺ
              </Text>
              <View style={{ display: "flex" }}>
                <View style={{ display: "flex", flexDirection: "row" }}>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 2,
                      marginTop: 1,
                    }}
                  >
                    Mã hóa đơn:
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 7,
                      marginTop: 1,
                      fontWeight: 700,
                    }}
                  >
                    {order?.name}
                  </Text>
                </View>

                <View style={{ display: "flex", flexDirection: "row" }}>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 2,
                      marginTop: 1,
                    }}
                  >
                    Khách hàng:
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 7,
                      marginTop: 1,
                      fontWeight: 700,
                    }}
                  >
                    {order?.patient?.lastname} {order?.patient?.firstname}
                  </Text>
                </View>

                <View style={{ display: "flex", flexDirection: "row" }}>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 2,
                      marginTop: 1,
                    }}
                  >
                    Số điện thoại:
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 7,
                      marginTop: 1,
                      fontWeight: 700,
                    }}
                  >
                    {order?.patient?.phone}
                  </Text>
                </View>
                <View style={{ display: "flex", flexDirection: "row" }}>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 4,
                      marginTop: 1,
                    }}
                  >
                    Hình thức thanh toán:
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      fontSize: "10px",
                      flex: 5,
                      marginTop: 1,
                      fontWeight: 700,
                      marginLeft: 8,
                    }}
                  >
                    {paymentOptions?.find(
                      (item) => item.value === order?.paymentMenthod
                    )?.label || "Chưa thanh toán"}
                  </Text>
                  {order?.paymentMenthod && (
                    <Text
                      style={{
                        ...styles.title,
                        fontSize: "10px",
                        flex: 3,
                        marginTop: 1,
                      }}
                    >
                      Ngày thanh toán:
                    </Text>
                  )}
                  {order?.paymentMenthod && (
                    <Text
                      style={{
                        ...styles.title,
                        fontSize: "10px",
                        flex: 7,
                        marginTop: 1,
                        fontWeight: 700,
                      }}
                    >
                      Ngày {moment(order?.paymentDate).format("DD")}, tháng{" "}
                      {moment(order?.paymentDate).format("MM")}, năm{" "}
                      {moment(order?.paymentDate).format("YYYY")}
                    </Text>
                  )}
                </View>
              </View>
            </View>
            <View style={{ marginBottom: "-1px" }}>
              <Table>
                <TableHeader>
                  <TableCell
                    weighting={0.2}
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    STT
                  </TableCell>
                  <TableCell
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    Tên sản phẩm
                  </TableCell>
                  <TableCell
                    weighting={0.4}
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    ĐVT
                  </TableCell>
                  <TableCell
                    weighting={0.16}
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    SL
                  </TableCell>
                  <TableCell
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    Đơn giá
                  </TableCell>
                  <TableCell
                    weighting={0.22}
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    Thuế
                  </TableCell>
                  <TableCell
                    style={{
                      ...styles.title,
                      ...styles.header,
                      borderTop: "none",
                      borderBottom: "none",
                    }}
                  >
                    Thành tiền
                  </TableCell>
                </TableHeader>
              </Table>
            </View>
            <Table data={[...listItems, ...listServices]}>
              <TableHeader>
                <TableCell
                  weighting={0.2}
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (1)
                </TableCell>
                <TableCell
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (2)
                </TableCell>
                <TableCell
                  weighting={0.4}
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (3)
                </TableCell>
                <TableCell
                  weighting={0.16}
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (4)
                </TableCell>
                <TableCell
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (5)
                </TableCell>
                <TableCell
                  weighting={0.22}
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (6)
                </TableCell>
                <TableCell
                  style={{
                    ...styles.title,
                    ...styles.header,
                    borderTop: "none",
                    borderBottom: "none",
                  }}
                >
                  (7)
                </TableCell>
              </TableHeader>

              <TableBody>
                <DataTableCell
                  style={{
                    ...styles.title,
                    ...styles.item,
                    textAlign: "center",
                  }}
                  weighting={0.2}
                  getContent={(r) => `${r.index + 1}`}
                />
                <DataTableCell
                  style={{ ...styles.title, ...styles.item }}
                  getContent={(r) => `${(r?.name ?? r.itemInfo?.name) || ""}`}
                />
                <DataTableCell
                  style={{ ...styles.title, ...styles.item }}
                  weighting={0.4}
                  getContent={(r) => r?.unit || ""}
                />
                <DataTableCell
                  style={{
                    ...styles.title,
                    ...styles.item,
                    textAlign: "center",
                  }}
                  weighting={0.16}
                  getContent={(r) => r?.quantity || ""}
                />
                <DataTableCell
                  style={{ ...styles.title, ...styles.price }}
                  getContent={(r) =>
                    r?.price?.toLocaleString("it-IT", {
                      style: "currency",
                      currency: "VND",
                    }) || ""
                  }
                />
                <DataTableCell
                  style={{ ...styles.title, ...styles.price }}
                  weighting={0.22}
                  getContent={(r) => `${r?.taxPrices}%`}
                />
                <DataTableCell
                  style={{ ...styles.title, ...styles.price }}
                  getContent={(r) =>
                    r?.total?.toLocaleString("it-IT", {
                      style: "currency",
                      currency: "VND",
                    })
                  }
                />
              </TableBody>
            </Table>

            <View style={styles.session}>
              <View style={{ display: "flex", flexDirection: "row" }}>
                <Text
                  style={{
                    ...styles.title,
                    fontSize: "10px",
                    flex: 1,
                    paddingLeft: 320,
                  }}
                >
                  Thành tiền:
                </Text>
                <Text
                  style={{ ...styles.title, fontSize: "10px", fontWeight: 700 }}
                >
                  {(
                    parseInt(
                      _.sumBy(listServices, function (o) {
                        return o.total;
                      }).toString() || "0"
                    ) +
                    parseInt(
                      _.sumBy(listItems, function (o) {
                        return o.total;
                      }).toString() || "0"
                    )
                  )?.toLocaleString("it-IT", {
                    style: "currency",
                    currency: "VND",
                  })}
                </Text>
              </View>
            </View>
            <View style={{ display: "flex", ...styles.session }}>
              <Text style={{ ...styles.title, fontSize: "10px" }}>
                Số tiền viết bằng chữ:
              </Text>
              <Text
                style={{ ...styles.title, fontSize: "10px", fontWeight: 700 }}
              >
                {convertCurrency(Math.floor(order?.totalAmount).toString())}
              </Text>
            </View>
            <View style={styles.session}>
              <Text style={{ ...styles.title, fontSize: "10px" }}>
                Ghi chú: {order?.note && Parser(order?.note || "")}
              </Text>
            </View>
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                paddingBottom: 150,
                marginTop: 20,
              }}
            >
              <View style={{ display: "flex", flex: 1 }}>
                <Text
                  style={{
                    ...styles.title,
                    fontSize: "10px",
                    fontWeight: 700,
                    textAlign: "center",
                  }}
                >
                  Khách hàng
                </Text>
                <Text
                  style={{
                    ...styles.title,
                    fontSize: "9px",
                    fontStyle: "italic",
                    textAlign: "center",
                  }}
                >
                  Ký tên (Ghi rõ họ tên)
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    ...styles.title,
                    fontSize: "10px",
                    textAlign: "center",
                    fontWeight: 700,
                  }}
                >
                  Người xuất hóa đơn
                </Text>
                <Text
                  style={{
                    ...styles.title,
                    fontSize: "9px",
                    textAlign: "center",
                    fontStyle: "italic",
                  }}
                >
                  Ký tên (Ghi rõ họ tên)
                </Text>
              </View>
            </View>
          </View>
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) =>
              `${pageNumber} / ${totalPages}`
            }
            fixed
          />
        </View>
      </Page>
    </Document>
  );

  return (
    <>
      {/* {view && <MyDoc />} */}
      <PDFDownloadLink
        document={
          <>
            <MyDoc />
          </>
        }
        fileName={`HOA-DON-${order?.name}`}
      >
        {({ blob, url, loading, error }) =>
          order?.id && <Button className="mr-2 mt-2"> In </Button>
        }
      </PDFDownloadLink>
    </>
  );
}
