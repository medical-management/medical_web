import _ from "lodash";
import moment from "moment";
import React, { useEffect } from "react";
import { useQuery } from "react-query";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { convertCurrency } from "../../../utils/convertCurrencyToString";
import { APIS_FIND_ONE_ORDER } from "../apis";
import * as actions from "../actions";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import ButtonPrint from "./Pdf";
import { Button } from "react-bootstrap";
// Reference font
export default function ViewOrder({ data }) {
  const params = useParams();
  const queryOrder = useQuery(
    ["find_one_sales_order", params],
    () => params?.id !== "add" && APIS_FIND_ONE_ORDER(params)
  );
  const dispatch = useDispatch();

  const { order } = useSelector((state) => state["ORDERS"]);
  const [selectedPayment, setSelectedPayment] = React.useState("");
  const [selectedPatient, setSelectedPatient] = React.useState("");
  const [paymentNote, setPaymentNote] = React.useState("");
  const [paymentDate, setPaymentDate] = React.useState(
    moment().utc().format("YYYY-MM-DD")
  );
  const [listItems, setListItem] = React.useState([]);
  const [listServices, setListServices] = React.useState([]);
  useEffect(() => {
    if (!queryOrder.isLoading && !queryOrder.isError) {
      dispatch(
        actions.GET_INFO_ORDERS_SUCCESS(queryOrder?.data?.find_one_sales_order)
      );
    }
  }, [queryOrder.data, params]);
  useEffect(() => {
    if (order) {
      setSelectedPatient(order?.patient?.id);
      setListItem(
        order?.items?.map((item, index) => {
          return {
            index: index,
            id: item?.id,
            name: item?.item?.name,
            unit: item?.unit,
            note: item?.note,
            isGrossProfit: item?.isGrossProfit,
            perGrossProfit: item?.perGrossProfit,
            quantity: item?.quantity,
            price: item?.price,
            taxAmount: item?.taxAmount,
            taxPrices: item?.perTax,
            bin: item?.basePrice,
            total: item?.amount,
            itemInfo: item?.item,
          };
        }) || []
      );
      setListServices(
        order?.expenses?.map((item, index) => {
          return {
            index: index,
            id: item?.id,
            name: item?.name,
            note: item?.note,
            price: item?.price,
            taxPrices: item?.perTax,
            total: item?.amount,
          };
        }) || []
      );
      setPaymentNote(order?.paymentNote);
      setSelectedPayment(order?.paymentMenthod);
    }
  }, [order]);
  const columnsItem = React.useMemo(
    () => [
      {
        Header: "Tên sản phẩm",
        accessor: (row) => <p className="cell-p">{row?.itemInfo?.name}</p>,
      },
      {
        Header: "ĐVT",
        accessor: (row) => {
          return <p className="cell-p">{row?.unit}</p>;
        },
      },
      {
        Header: "Số lượng",
        accessor: (row) => {
          return <p className="cell-p">{row?.quantity}</p>;
        },
      },
      {
        Header: "Áp dụng LN",
        accessor: (row) => {
          return <p className="cell-p">{row.isGrossProfit ? "Có" : "Không"}</p>;
        },
      },

      {
        Header: "Lợi nhuận",
        accessor: (row) => {
          return <p className="cell-p">{row.perGrossProfit} %</p>;
        },
      },
      {
        Header: "Kho (VND)",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {row?.bin?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
      {
        Header: "Tồn kho",
        accessor: (row) => {
          return <p className="cell-p">{row?.available}</p>;
        },
      },
      {
        Header: "Đơn giá (VND)",
        accessor: (row) => (
          <p className="cell-p">
            {row?.price?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        ),
      },
      {
        Header: "Thuế (%)",
        accessor: (row) => <p className="cell-p">{row?.taxPrices} %</p>,
      },
      {
        Header: "Tổng tiền",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {Math.round(row?.total)?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
      {
        Header: "Ghi chú",
        accessor: (row) => (
          <input
            className="form-control"
            disabled
            style={{
              paddingLeft: "2px",
              paddingRight: "2px",
              minWidth: "150px",
            }}
            value={row?.note}
            onChange={(e) => {
              setListItem(
                listItems?.map((o) => {
                  if (o.index === row.index) {
                    return {
                      ...o,
                      note: e?.target?.value || "",
                    };
                  } else return o;
                })
              );
            }}
          />
        ),
      },
    ],
    [listItems]
  );

  const columnsService = React.useMemo(
    () => [
      {
        Header: "Tên dịch vụ",
        accessor: (row) => {
          return <p className="cell-p">{row?.name}</p>;
        },
      },
      {
        Header: "Ghi chú",
        accessor: (row) => {
          return <p className="cell-p">{row?.note}</p>;
        },
      },
      {
        Header: "Đơn giá (VND)",
        accessor: (row) => (
          <p className="cell-p">
            {row?.price?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        ),
      },

      {
        Header: "Thuế (%)",
        accessor: (row) => <p className="cell-p">{row?.taxPrices} %</p>,
      },
      {
        Header: "Tổng tiền",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {parseInt(row?.total || "0")?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
    ],
    [listServices]
  );

  const MyDoc = () => (
    <div color="black">
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{ flex: 1 }}>
          <p style={{ fontSize: 18 }}>PHÒNG KHÁM ĐA KHOA</p>
          <p
            style={{
              fontWeight: "bold",
              //   paddingLeft: "13px",
              fontSize: 18,
            }}
          >
            NGUYÊN PHƯƠNG
          </p>
        </div>
        <div style={{ flex: 0.9 }}>
          <p
            style={{
              flex: 1,
              fontSize: 14,
            }}
          >
            Địa chỉ: 271B Khu 4, Thị trấn Cái Bè, Huyện Cái Bè, Tiền Giang
          </p>
          <p
            style={{
              flex: 1,
              fontSize: 14,
              marginTop: -2,
            }}
          >
            MST: 1201604765
          </p>
          <p
            style={{
              flex: 1,
              fontSize: 14,
              marginTop: -2,
            }}
          >
            SĐT: <b>(0273) 3823468</b>
          </p>
        </div>
      </div>

      <p
        style={{
          fontWeight: "bold",
          margin: "20px 0px 10px 0px",
          textAlign: "center",
          fontSize: 18,
        }}
      >
        HÓA ĐƠN BÁN LẺ
      </p>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              marginTop: 5,
            }}
          >
            Mã hóa đơn:
          </p>
          <p
            style={{
              fontSize: "14px",
              flex: 5,
              marginTop: 5,
              fontWeight: 700,
            }}
          >
            {order?.name}
          </p>
        </div>

        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              marginTop: 5,
            }}
          >
            Khách hàng:
          </p>
          <p
            style={{
              fontSize: "14px",
              flex: 5,
              marginTop: 5,
              fontWeight: 700,
            }}
          >
            {order?.patient?.lastname} {order?.patient?.firstname}
          </p>
        </div>

        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              marginTop: 5,
            }}
          >
            Số điện thoại:
          </p>
          <p
            style={{
              fontSize: "14px",
              flex: 5,
              marginTop: 5,
              fontWeight: 700,
            }}
          >
            {order?.patient?.phone}
          </p>
        </div>
      </div>

      <p
        style={{
          marginTop: "20px",
          marginBottom: 10,
          fontSize: 16,
          marginBottom: "5px",
          fontWeight: 700,
        }}
      >
        Sản phẩm:
      </p>
      <CustomTable columns={columnsItem} data={listItems || []} />

      <div>
        <div style={{ display: "flex", marginTop: 10, flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Tổng tiền:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listItems, function (o) {
                return o.bin * o.quantity;
              }).toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Tổng thuế:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listItems, function (o) {
                return (o.bin * o.quantity * o.taxPrices) / 100;
              }).toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Thành tiền:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {(
              parseInt(
                _.sumBy(listItems, function (o) {
                  return (o.bin * o.quantity * o.taxPrices) / 100;
                }).toString() || "0"
              ) +
              parseInt(
                _.sumBy(listItems, function (o) {
                  return o.bin * o.quantity;
                })
                  .toFixed()
                  .toString() || "0"
              )
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
      </div>
      <p
        style={{
          marginTop: "20px",
          marginBottom: "10px",
          fontSize: 16,
          marginBottom: "5px",
          fontWeight: 700,
        }}
      >
        Dịch vụ:
      </p>
      <CustomTable columns={columnsService} data={listServices || []} />

      <div>
        <div style={{ display: "flex", marginTop: 10, flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Tổng tiền:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listServices, function (o) {
                return o.price;
              }).toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Tổng thuế:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listServices, function (o) {
                return (o.price * o.taxPrices) / 100;
              }).toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Thành tiền:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {(
              parseInt(
                _.sumBy(listServices, function (o) {
                  return (o.price * o.taxPrices) / 100;
                }).toString() || "0"
              ) +
              parseInt(
                _.sumBy(listServices, function (o) {
                  return o?.price;
                }).toString() || "0"
              )
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
      </div>
      <div
        style={{
          borderBottom: "1px solid #ddd",
          marginTop: 20,
        }}
      ></div>
      <div>
        <div style={{ display: "flex", marginTop: 10, flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Thành tiền sản phẩm:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listItems, function (o) {
                return o.total;
              })
                .toFixed()
                .toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Thành tiền dịch vụ:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {parseInt(
              _.sumBy(listServices, function (o) {
                return o.total;
              })
                .toFixed()
                .toString() || "0"
            )?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <p
            style={{
              fontSize: "14px",
              flex: 1,
              paddingLeft: 320,
            }}
          >
            Thành tiền:
          </p>
          <p style={{ fontSize: "14px", fontWeight: 700 }}>
            {(console.log(
              parseInt(
                _.sumBy(listServices, function (o) {
                  return o.total;
                }).toString() || "0"
              ),
              parseInt(
                _.sumBy(listItems, function (o) {
                  return o.total;
                }).toString() || "0"
              )
            ),
            parseInt(
              _.sumBy(listServices, function (o) {
                return o.total;
              }).toString() || "0"
            ) +
              parseInt(
                _.sumBy(listItems, function (o) {
                  return o.total;
                }).toString() || "0"
              ))?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
      </div>
      <div style={{ display: "flex" }}>
        <p style={{ fontSize: "14px" }}>Bằng chữ: </p>
        <p style={{ fontSize: "14px", fontWeight: 700, marginLeft: 8 }}>
          {convertCurrency(Math.floor(order?.totalAmount).toString())}
        </p>
      </div>
    </div>
  );

  const history = useHistory();

  return (
    <>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          marginBottom: 20,
        }}
      >
        <ButtonPrint></ButtonPrint>
        {order?.id && (
          <Button
            style={{ height: "43px", marginTop: 8 }}
            onClick={() => history.push(`/app/dashboard/orders/${order?.id}`)}
          >
            Chi tiết
          </Button>
        )}
      </div>
      <MyDoc />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 20,
        }}
      >
        <ButtonPrint></ButtonPrint>
        {order?.id && (
          <Button
            style={{ height: "43px", marginTop: 8 }}
            onClick={() => history.push(`/app/dashboard/orders/${order?.id}`)}
          >
            Chi tiết
          </Button>
        )}
      </div>
    </>
  );
}
