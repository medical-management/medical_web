/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row, Tab, Tabs } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import { useToasts } from "react-toast-notifications";

import EditorCkClassic from "../../../components/CK-Editor/CkClassicEditor";
import ConfirmModal from "../../../components/ConfirmModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import { paymentOptions, statusOrderOptions, statusOrderOptionsSelect } from "../../../constants/options";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import * as actions from "../actions";
import { APIS_FIND_ONE_ORDER, APIS_SAVE_SALE_ORDER, APIS_SAVE_SERVICES_ORDER } from "../apis";
import { APIS_FIND_FAST_ITEMS } from "../../ManageItems/apis";
import { APIS_FIND_FAST_PATIENT } from "../../ManagePatient/apis";
import { APIS_FIND_ALL_MEDICAL, APIS_SAVE_ITEM_ORDER } from "../apis";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import CustomModal from "../../../components/CustomModal";
import NumberFormat from "react-number-format";
import ButtonPrint from "./Pdf";
import { ROUTER_MANAGE_ORDERS } from "../router";

const OrderDetail = ({ id }) => {
    const params = useParams();
    const history = useHistory();
    const dispatch = useDispatch();
    const { addToast } = useToasts();
    const queryClient = useQueryClient();
    const { register, handleSubmit, control, setValue } = useForm();

    const { order, items, listPatient, medicals, orderItems, orderServices } = useSelector((state) => state["ORDERS"]);
    const { user } = useSelector((state) => state["AUTHENTICATION"]);

    const queryPatient = useQuery("find_fast_patient", () => APIS_FIND_FAST_PATIENT({ take: 10000, skip: 0 }), {
        enabled: listPatient && listPatient.length === 0,
    });
    const queryOrder = useQuery(
        ["find_one_sales_order", params],
        () => {
            if (history?.location?.pathname?.includes("/app/dashboard/medicals")) {
                return params?.id !== "add" && APIS_FIND_ONE_ORDER({ id });
            } else return params?.id !== "add" && APIS_FIND_ONE_ORDER(params);
        },
        {
            enabled: params && params.id !== "add",
        }
    );
    const queryMedicals = useQuery("medicals", () =>
        APIS_FIND_ALL_MEDICAL({
            take: 10000,
            skip: 0,
        })
    );
    const queryItem = useQuery("find_fast_item", () => APIS_FIND_FAST_ITEMS(), {
        enabled: items && items.length === 0,
    });

    const mutationSaveService = useMutation("save_expenses_transaction", APIS_SAVE_SERVICES_ORDER);
    const mutationSaveItem = useMutation("save_item_transaction", APIS_SAVE_ITEM_ORDER);
    const mutationSaveOrder = useMutation("save_sales_order", APIS_SAVE_SALE_ORDER);

    const [anotherData, setAnotherData] = useState({
        note: "",
        status: "pending",
        paymentMenthod: "",
        paymentNote: "",
        isCanPay: false,
    });

    const [selectedPatient, setSelectedPatient] = useState("");
    const [selectedMedical, setSelectedMedical] = useState("");

    const [isShowPaidModals, setIsShowPaidModals] = useState(false);
    const [key, setKey] = useState("item");
    const [payload, setPayload] = useState(null);

    const [confirmSave, setConfirmSave] = useState(false);

    const [disableField, setDisableField] = useState(false);
    const [listItem, setListItem] = useState([]);
    const [listService, setListService] = useState([]);

    useEffect(() => {
        if (queryItem.status === "success" && !queryItem.isLoading)
            dispatch(actions.GET_ALL_FAST_ITEM_SUCCESS(queryItem?.data?.find_fast_item));
    }, [queryItem.data]);

    useEffect(() => {
        if (!queryOrder.isLoading && queryOrder.data) {
            dispatch(actions.GET_INFO_ORDERS_SUCCESS(queryOrder?.data?.find_one_sales_order));
        }
    }, [queryOrder.data, params]);
    useEffect(() => {
        if (!queryMedicals.isLoading && !queryMedicals.isError)
            dispatch(actions.GET_ALL_MEDICALS_SUCCESS(queryMedicals?.data?.find_all_medical?.data));
    }, [queryMedicals.isLoading]);

    useEffect(() => {
        if (user?.decentralization[0]?.salesOrders === 0 || order?.status === "paid" || order?.status === "cancel") {
            setDisableField(true);
        } else setDisableField(false);
    }, [order, user]);

    useEffect(() => {
        if (params && params.id !== "add") {
            queryClient.prefetchQuery(["find_one_sales_order", params], () => APIS_FIND_ONE_ORDER(params));
            queryItem.refetch();
        } else dispatch(actions.CLEAR());
    }, [params.id]);

    useEffect(() => {
        let isCanPay = orderItems.length > 1;

        if (orderItems.length > 0)
            setListItem(
                orderItems
                    ?.filter((item) => !item.inactive)
                    .map((item, index) => {
                        if (isCanPay && item?.available < 0) isCanPay = false;
                        return {
                            index,
                            ...item,
                            item: { ...item.item, basePrice: item.salePrices },
                            name: item?.item?.name,
                            value: item?.item.id,
                            label: item?.item?.id && item.item.shortName ? item.item.shortName : "",
                        };
                    }) || []
            );
        if (orderServices.length > 0)
            setListService(
                orderServices
                    ?.filter((service) => !service.inactive)
                    .map((service, index) => ({
                        ...service,
                        index,
                    })) || []
            );
        setSelectedPatient(order?.patient?.id);
        setAnotherData({
            status: order?.status || "pending",
            note: order?.note,
            paymentMenthod: order?.paymentMenthod,
            paymentNote: order?.paymentNote,
            isCanPay,
        });
        // set form value
        Object.entries(order).forEach(([key, value]) => {
            setValue(key, value);
        });
    }, [orderItems, orderServices, order]);

    useEffect(() => {
        if (queryPatient.status === "success") {
            dispatch(actions.GET_ALL_PATIENT_INFO_SUCCESS(queryPatient?.data?.find_fast_patient));
        }
    }, [queryPatient.data]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationSaveOrder;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Cập nhật thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
                history.push(`${ROUTER_MANAGE_ORDERS}/${data?.save_sales_order?.id}`);
                queryOrder.refetch();
            }
            setConfirmSave(false);
        }
    }, [mutationSaveOrder.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationSaveItem;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Lưu thông tin sản phẩm thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
            } else if (isError) {
                addToast("Lưu thông tin sản phẩm thất bại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
            }
            queryOrder.refetch();
        }
    }, [mutationSaveItem.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationSaveService;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Lưu thông tin dịch vụ thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
            } else if (isError) {
                addToast("Thêm thông tin dịch vụ thất bại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
            }
            queryOrder.refetch();
        }
    }, [mutationSaveService.isLoading]);

    function handleBtnItem({ row, inactive }) {
        console.log({ row, inactive });
        const message = [];
        if (!row.item.id) message.push(" sản phẩm");
        if (!row.price || row.price === 0) message.push(" giá tiền");
        if (message.length > 0)
            return addToast(`Vui lòng không để trống${message.join()}`, {
                appearance: "error",
                autoDismiss: true,
            });
        mutationSaveItem.mutate({
            ...row,
            inactive,
            transactionId: id ? id : order.id,
        });
    }
    function handleBtnServices({ row, inactive }) {
        console.log({ row, inactive });
        const message = [];
        if (!row.name) message.push(" dịch vụ");
        if (!row.price || row.price === 0) message.push(" giá tiền");
        if (message.length > 0)
            return addToast(`Vui lòng không để trống${message.join()}`, {
                appearance: "error",
                autoDismiss: true,
            });
        mutationSaveService.mutate({
            ...row,
            inactive,
            transactionId: id ? id : order.id,
        });
    }

    const columnsItem = React.useMemo(
        () => [
            {
                Header: "Tên sản phẩm",
                isHeaderRequired: true,
                accessor: (row) => {
                    return (
                        <div
                            className="self-center"
                            style={{
                                paddingTop: 8,
                                minWidth: "200px",
                            }}
                        >
                            <Select
                                placeholder="Chọn sản phẩm"
                                name="patient"
                                style={{ width: "100%" }}
                                className="basic-single"
                                classNamePrefix="select"
                                options={items}
                                defaultValue={row}
                                onChange={(item) =>
                                    dispatch(actions.HANDLE_ITEMS_LINE({ row, item, isOverride: true }))
                                }
                                isSearchable
                                isDisabled={disableField}
                            />
                        </div>
                    );
                },
                getProps: () => ({
                    style: {
                        overflow: "unset",
                    },
                }),
            },
            {
                Header: "SL",
                isHeaderRequired: true,
                accessor: (row) => {
                    return (
                        <input
                            className="form-control mt-2"
                            disabled={!row?.name || disableField}
                            type="number"
                            style={{
                                paddingLeft: "2px",
                                paddingRight: "2px",
                                minWidth: "80px",
                                textAlign: "right",
                            }}
                            defaultValue={row?.quantity}
                            onBlur={(e) =>
                                dispatch(
                                    actions.HANDLE_ITEMS_LINE({
                                        row: { ...row, quantity: e.target.value || 0 },
                                        item: row?.item,
                                    })
                                )
                            }
                        />
                    );
                },
            },
            {
                Header: "Tồn kho",
                accessor: (row) => {
                    return <p className="cell-p text-right">{row?.available}</p>;
                },
            },
            {
                Header: "Tổng tiền",
                accessor: (row) => (
                    <p className="cell-p text-right">
                        {parseFloat(row?.amount?.toFixed())?.toLocaleString("it-IT", {
                            style: "currency",
                            currency: "VND",
                        })}
                    </p>
                ),
            },
            {
                Header: "Ghi chú",
                accessor: (row) => (
                    <input
                        className="form-control"
                        disabled={!row?.name || disableField}
                        style={{
                            paddingLeft: "2px",
                            paddingRight: "2px",
                            minWidth: "150px",
                        }}
                        defaultValue={row?.note}
                        onBlur={(e) =>
                            dispatch(
                                actions.HANDLE_ITEMS_LINE({
                                    row: { ...row, note: e.target.value },
                                    item: row?.item,
                                })
                            )
                        }
                    />
                ),
            },
            {
                Header: " ",
                accessor: (row) =>
                    !disableField && (
                        <div
                            style={{
                                display: "flex",
                                gap: 10,
                                justifyContent: "space-around",
                            }}
                        >
                            <Button
                                className="feather icon-check margin-0 btn-success"
                                onClick={() => handleBtnItem({ row, inactive: false })}
                            />
                            {row.id && (
                                <Button
                                    disabled={disableField}
                                    className="feather icon-minus margin-0 btn-danger"
                                    onClick={() => handleBtnItem({ row, inactive: true })}
                                />
                            )}
                        </div>
                    ),
            },
        ],
        [listItem, items]
    );
    const columnsService = React.useMemo(
        () => [
            {
                Header: "Tên dịch vụ",
                isHeaderRequired: true,
                accessor: (row) => {
                    return (
                        <input
                            disabled={disableField}
                            style={{
                                paddingLeft: "2px",
                                paddingRight: "2px",
                                minWidth: "150px",
                            }}
                            placeholder="Nhập tên dịch vụ"
                            className="form-control"
                            defaultValue={row?.name}
                            onBlur={(e) => dispatch(actions.HANDLE_SEVICES_LINE({ ...row, name: e.target.value }))}
                        />
                    );
                },
            },
            {
                Header: "Ghi chú",
                accessor: (row) => {
                    return (
                        <input
                            disabled={disableField}
                            style={{
                                paddingLeft: "2px",
                                paddingRight: "2px",
                                minWidth: "200px",
                            }}
                            className="form-control"
                            placeholder="Nhập ghi chú"
                            defaultValue={row?.note}
                            onBlur={(e) => dispatch(actions.HANDLE_SEVICES_LINE({ ...row, note: e.target.value }))}
                        />
                    );
                },
            },
            {
                Header: "Giá tiền",
                accessor: (row) => (
                    <input
                        className="form-control mt-2"
                        type="number"
                        style={{
                            paddingLeft: "2px",
                            paddingRight: "2px",
                            minWidth: "80px",
                            textAlign: "right",
                        }}
                        defaultValue={row?.price}
                        onBlur={(e) => dispatch(actions.HANDLE_SEVICES_LINE({ ...row, price: e.target.value }))}
                    />
                ),
            },
            {
                Header: "Tổng tiền",
                accessor: (row) => {
                    return (
                        <p className="cell-p text-right">
                            {parseInt(row?.amount?.toFixed() || 0)?.toLocaleString("it-IT", {
                                style: "currency",
                                currency: "VND",
                            })}
                        </p>
                    );
                },
            },
            {
                Header: " ",
                accessor: (row) =>
                    !disableField && (
                        <div
                            style={{
                                display: "flex",
                                gap: 10,
                                justifyContent: "space-around",
                            }}
                        >
                            <Button
                                className="feather icon-check margin-0 btn-success"
                                onClick={() => handleBtnServices({ row, inactive: false })}
                            />
                            {row.id && (
                                <Button
                                    disabled={disableField}
                                    className="feather icon-minus margin-0 btn-danger"
                                    onClick={() => handleBtnServices({ row, inactive: true })}
                                />
                            )}
                        </div>
                    ),
            },
        ],
        [listService, orderServices, order]
    );
    function HeaderOrderDetail() {
        return (
            <Card.Header
                className="header-card"
                style={{
                    position: "relative",
                    marginLeft: "-30px",
                    justifyContent: "space-between",
                }}
            >
                <div className="flex" style={{ gap: 10 }}>
                    <p
                        className="feather icon-chevron-left icon"
                        onClick={() => history.push(`${ROUTER_MANAGE_ORDERS}`)}
                    />
                    <Card.Title as="h4" className="title">
                        {"THÔNG TIN PHIẾU BÁN HÀNG"} {order?.name ? ` - ${order?.name}` : ""}
                    </Card.Title>
                </div>
                <BtnGroup />
            </Card.Header>
        );
    }
    function BodyOrderDetail() {
        return (
            <>
                <SessionTitle>THÔNG TIN CHÍNH</SessionTitle>
                <Row>
                    {params?.id !== "add" && (
                        <>
                            <Col sm={6}>
                                <label className="label-control items-center">Ngày cập nhật</label>
                                <input className="form-control" defaultValue={order?.updatedat} disabled />
                            </Col>
                            <Col sm={6}>
                                <label className="label-control items-center">Người cập nhật</label>
                                <input
                                    className="form-control"
                                    defaultValue={order?.updatedby?.toUpperCase()}
                                    disabled
                                />
                            </Col>
                        </>
                    )}

                    <Col sm={6} className="flex" style={{ alignItems: "flex-end", alignSelf: "center" }}>
                        <div
                            className="self-center"
                            style={{
                                paddingTop: 10,
                                width: "100%",
                            }}
                        >
                            <label className="label-control items-center">
                                Bệnh nhân <span style={{ color: "red" }}>*</span>{" "}
                            </label>
                            <Select
                                name="patient"
                                style={{ width: "100%" }}
                                className="basic-single"
                                classNamePrefix="select"
                                options={listPatient}
                                defaultValue={listPatient?.find((item) => item.value === selectedPatient || "")}
                                onChange={(e) => setSelectedPatient(e.value)}
                                isDisabled={disableField}
                            />
                        </div>
                    </Col>
                    <Col sm={6} className="flex" style={{ alignItems: "flex-end", alignSelf: "center" }}>
                        <div
                            className="self-center"
                            style={{
                                paddingTop: 10,
                                width: "100%",
                            }}
                        >
                            <label className="label-control items-center">Bệnh án</label>
                            <Select
                                name="patient"
                                style={{ width: "100%" }}
                                className="basic-single"
                                classNamePrefix="select"
                                options={queryMedicals.isLoading ? [] : medicals}
                                defaultValue={medicals?.find((item) => item.value === selectedMedical || "")}
                                onChange={(e) => setSelectedMedical(e.value)}
                                isDisabled={disableField}
                            />
                        </div>
                    </Col>

                    <Col sm={6} style={{ alignSelf: "center" }}>
                        <label className="label-control items-center">Trạng thái</label>
                        <Select
                            className="basic-single"
                            placeholder="Chọn trạng thái"
                            classNamePrefix="select"
                            isDisabled={disableField}
                            options={
                                anotherData.status !== "paid"
                                    ? statusOrderOptions.filter((o) => o.value !== "paid")
                                    : statusOrderOptionsSelect
                            }
                            defaultValue={statusOrderOptionsSelect.find((o) => o.value === anotherData.status)}
                            onChange={(e) => setAnotherData({ ...anotherData, status: e.value })}
                        />
                    </Col>
                    <Col sm={6}>
                        <Controller
                            {...register("date")}
                            control={control}
                            render={({ field }) => (
                                <FormInput
                                    disabled={disableField}
                                    label={"Ngày"}
                                    type="date"
                                    {...field}
                                    defaultValue={moment()?.format("YYYY-MM-DD")}
                                />
                            )}
                        />
                    </Col>
                    <Col sm={12} style={{ zIndex: 0 }}>
                        <label className="label-control mt-2" style={{ whiteSpace: "nowrap" }}>
                            Ghi chú
                        </label>
                        <EditorCkClassic
                            html={anotherData.note || ""}
                            onChange={() => {}}
                            onBlur={(value) => setAnotherData({ ...anotherData, note: value })}
                            disabled={disableField}
                        />
                    </Col>
                </Row>
            </>
        );
    }
    function PaidOrderDetail() {
        return (
            <>
                <SessionTitle>THÔNG TIN THANH TOÁN</SessionTitle>
                <Row>
                    <Col sm={6}>
                        <div
                            className="self-center"
                            style={{
                                paddingTop: 10,
                                width: "100%",
                                zIndex: 100,
                            }}
                        >
                            <label className="label-control items-center">Phương thức thanh toán</label>
                            <Select
                                style={{ width: "100%" }}
                                className="basic-single"
                                classNamePrefix="select"
                                options={paymentOptions}
                                defaultValue={paymentOptions?.find((item) => item.value === anotherData.paymentMenthod)}
                                isDisabled={disableField}
                            />
                        </div>
                    </Col>
                    <Col sm={6}>
                        <Controller
                            {...register("paymentDate")}
                            control={control}
                            render={({ field }) => (
                                <FormInput label={"Ngày thanh toán"} disabled={disableField} type="date" {...field} />
                            )}
                        />
                    </Col>
                    <Col sm={12} style={{ zIndex: 0 }}>
                        <label className="label-control mt-2" style={{ whiteSpace: "nowrap" }}>
                            Ghi chú
                        </label>
                        <EditorCkClassic
                            html={anotherData.paymentNote || ""}
                            onChange={() => {}}
                            onBlur={(value) => setAnotherData({ ...anotherData, paymentNote: value })}
                            disabled={disableField}
                        />
                    </Col>
                </Row>
            </>
        );
    }
    function SublistOrderDetail() {
        return (
            <div className="custom-tab">
                <Tabs
                    id="controlled-tab-example"
                    activeKey={key}
                    onSelect={(k) => setKey(k)}
                    style={{
                        marginTop: 20,
                        marginLeft: "0px",
                        borderBottom: "1px solid #aaa ",
                    }}
                >
                    <Tab eventKey="item" title="Sản phẩm">
                        <SessionTitle>Danh sách sản phẩm</SessionTitle>
                        <div className="flex-height-table">
                            <CustomTable columns={columnsItem} data={listItem || []} />
                        </div>
                    </Tab>
                    <Tab eventKey="service" title="Dịch vụ" style={{ padding: 0 }}>
                        <SessionTitle>Danh sách dịch vụ</SessionTitle>
                        <div className="flex-height-table">
                            <CustomTable columns={columnsService} data={listService || []} />
                        </div>
                    </Tab>
                </Tabs>
            </div>
        );
    }
    function SummaryOrderDetail() {
        return (
            <Row style={{ fontSize: "16", marginTop: "-150px" }}>
                <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
                    SẢN PHẨM:
                </Col>
                <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
                    {parseInt(_.sumBy(listItem, (o) => o.amount) || 0)?.toLocaleString("it-IT", {
                        style: "currency",
                        currency: "VND",
                    })}
                </Col>
                <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
                    DỊCH VỤ:
                </Col>
                <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
                    {parseInt(_.sumBy(listService, (o) => o.amount) || 0)?.toLocaleString("it-IT", {
                        style: "currency",
                        currency: "VND",
                    })}
                </Col>
                <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
                    TỔNG CỘNG:
                </Col>
                <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
                    {(
                        parseInt(_.sumBy(listItem, (o) => o.amount) || 0) +
                        parseInt(_.sumBy(listService, (o) => o.amount) || 0)
                    )?.toLocaleString("it-IT", {
                        style: "currency",
                        currency: "VND",
                    })}
                </Col>
            </Row>
        );
    }
    function BtnGroup() {
        return (
            <div className="d-flex justify-content-end">
                {(anotherData.status === "approved" || anotherData.status === "paid") && <ButtonPrint />}
                {order?.status === "pending" && (
                    <Button
                        type="submit"
                        variant="info"
                        onClick={() => {
                            setPayload({
                                ...order,
                                patient: order.patient.id,
                                status: "approved",
                            });
                            setAnotherData({ ...anotherData, status: "approved" });
                            setConfirmSave(true);
                        }}
                        className="mr-2 mt-2 float-right"
                        disabled={mutationSaveOrder.isLoading}
                    >
                        Duyệt
                    </Button>
                )}
                {!disableField && anotherData.isCanPay && order?.status === "approved" && (
                    <Button
                        variant="danger"
                        onClick={() => setIsShowPaidModals(true)}
                        className="mr-2 mt-2 float-right"
                        disabled={mutationSaveOrder.isLoading}
                    >
                        Thanh toán
                    </Button>
                )}
                {!disableField && (
                    <Button
                        type="submit"
                        className="mr-2 mt-2 float-right"
                        variant="success"
                        disabled={mutationSaveOrder.isLoading}
                    >
                        Lưu
                    </Button>
                )}
            </div>
        );
    }
    function Modals() {
        const [dataModal, setDataModal] = useState({
            paymentMenthod: "",
            paymentNote: "",
            paymentDate: moment().format(),
        });
        return (
            <>
                <ConfirmModal
                    content={`Bạn chắc chắn lưu thông tin phiếu bán hàng ?`}
                    onChangeVisible={() => setConfirmSave(false)}
                    show={confirmSave}
                    onConfirm={() => mutationSaveOrder.mutate(payload)}
                    loading={mutationSaveOrder.isLoading}
                />

                <CustomModal
                    show={isShowPaidModals}
                    onChangeVisible={() => setIsShowPaidModals(!isShowPaidModals)}
                    title="THANH TOÁN"
                >
                    <div
                        className="self-center"
                        style={{
                            paddingTop: 8,
                            minWidth: "200px",
                        }}
                    >
                        <SessionTitle>THÔNG TIN THANH TOÁN</SessionTitle>
                        <Row>
                            <Col sm={6}>
                                <div
                                    className="self-center"
                                    style={{
                                        paddingTop: 10,
                                        width: "100%",
                                        zIndex: 100,
                                    }}
                                >
                                    <label className="label-control items-center">
                                        Phương thức thanh toán{" "}
                                        <span style={{ color: "red", marginLeft: "6px" }}>*</span>
                                    </label>
                                    <Select
                                        style={{ width: "100%" }}
                                        className="basic-single"
                                        classNamePrefix="select"
                                        options={paymentOptions}
                                        defaultValue={paymentOptions?.find(
                                            (item) => item.value === dataModal.paymentMenthod
                                        )}
                                        onChange={(e) => setDataModal({ ...dataModal, paymentMenthod: e.value })}
                                    />
                                    {!dataModal.paymentMenthod && (
                                        <label style={{ color: "red" }} className="label-control items-center">
                                            Vui lòng chọn phương thức thanh toán
                                            <span style={{ color: "red", marginLeft: "6px" }}>*</span>
                                        </label>
                                    )}
                                </div>
                            </Col>
                            <Col sm={6}>
                                <Controller
                                    {...register("paymentDate")}
                                    control={control}
                                    render={({ field }) => (
                                        <FormInput
                                            label={"Ngày thanh toán"}
                                            type="date"
                                            defaultValue={moment().format("YYYY-MM-DD")}
                                            onChange={(e) =>
                                                setDataModal({
                                                    ...dataModal,
                                                    paymentDate: moment(e.target.value).format(),
                                                })
                                            }
                                        />
                                    )}
                                />
                            </Col>
                            <Col sm={12} style={{ zIndex: 0 }}>
                                <label className="label-control mt-2" style={{ whiteSpace: "nowrap" }}>
                                    Ghi chú
                                </label>
                                <EditorCkClassic
                                    html={dataModal.paymentNote || ""}
                                    onChange={() => {}}
                                    onBlur={(value) => setDataModal({ ...dataModal, paymentNote: value })}
                                    disabled={disableField}
                                />
                            </Col>
                        </Row>
                    </div>
                    <div
                        style={{
                            placeContent: "end",
                            marginTop: 10,
                            gap: 10,
                            display: "flex",
                        }}
                    >
                        <Button className="btn btn-danger margin-0" onClick={() => setIsShowPaidModals(false)}>
                            Hủy
                        </Button>
                        <Button
                            className="margin-0"
                            onClick={() => {
                                if (!dataModal.paymentMenthod)
                                    return addToast("Vui lòng chọn phương thức thanh toán!", {
                                        appearance: "warning",
                                        autoDismiss: true,
                                    });
                                mutationSaveOrder.mutate({
                                    ...order,
                                    ...dataModal,
                                    status: "paid",
                                    patient: selectedPatient,
                                    medical: order?.medical?.id || "",
                                });
                                setIsShowPaidModals(false);
                            }}
                        >
                            Lưu
                        </Button>
                    </div>
                </CustomModal>
            </>
        );
    }

    return (
        <React.Fragment>
            <Breadcrumb />
            <Card style={{ padding: "30px 30px 140px 30px" }}>
                <form
                    onSubmit={handleSubmit((values) => {
                        if (selectedPatient) {
                            setPayload({
                                ...order,
                                ...values,
                                ...anotherData,
                                patient: selectedPatient,
                                medical: order?.medical?.id || "",
                            });
                            setConfirmSave(true);
                        } else
                            addToast("Vui lòng chọn bệnh nhân!", {
                                appearance: "error",
                                autoDismiss: true,
                            });
                    })}
                >
                    <HeaderOrderDetail />
                    <BodyOrderDetail />
                    {anotherData.status === "paid" && <PaidOrderDetail />}
                    {params?.id !== "add" && (
                        <>
                            <SublistOrderDetail />
                            <SummaryOrderDetail />
                        </>
                    )}
                </form>
            </Card>
        </React.Fragment>
    );
};

export default OrderDetail;
