/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import { createAction } from "redux-actions";

export const GET_ALL_ORDERS = createAction("SALES_ORDER/GET_ALL_ORDERS");
export const GET_ALL_ORDERS_SUCCESS = createAction("SALES_ORDER/GET_ALL_ORDERS_SUCCESS");
export const GET_ALL_ORDERS_FAILED = createAction("SALES_ORDER/GET_ALL_ORDERS_FAILED");

export const GET_INFO_ORDERS = createAction("SALES_ORDER/GET_INFO_ORDERS");
export const GET_INFO_ORDERS_SUCCESS = createAction("SALES_ORDER/GET_INFO_ORDERS_SUCCESS");
export const GET_INFO_ORDERS_FAILED = createAction("SALES_ORDER/GET_INFO_ORDERS_FAILED");

export const GET_ALL_FAST_ITEM = createAction("SALES_ORDER/GET_ALL_FAST_ITEM");
export const GET_ALL_FAST_ITEM_SUCCESS = createAction("SALES_ORDER/GET_ALL_FAST_ITEM_SUCCESS");
export const GET_ALL_FAST_ITEM_FAILED = createAction("SALES_ORDER/GET_ALL_FAST_ITEM_FAILED");
export const GET_ALL_PATIENT_INFO = createAction("SALES_ORDER/GET_ALL_PATIENT_INFO");
export const GET_ALL_PATIENT_INFO_SUCCESS = createAction("SALES_ORDER/GET_ALL_PATIENT_INFO_SUCCESS");
export const GET_ALL_PATIENT_INFO_FAILED = createAction("SALES_ORDER/GET_ALL_PATIENT_INFO_FAILED");

export const GET_ALL_MEDICALS = createAction("SALES_ORDER/GET_ALL_MEDICALS");
export const GET_ALL_MEDICALS_SUCCESS = createAction("SALES_ORDER/GET_ALL_MEDICALS_SUCCESS");
export const GET_ALL_MEDICALS_FAILED = createAction("SALES_ORDER/GET_ALL_MEDICALS_FAILED");

export const CHANGE_INFO_ORDER = createAction("SALES_ORDER/CHANGE_INFO_ORDER");
export const CHANGE_INFO_ORDER_SUCCESS = createAction("SALES_ORDER/CHANGE_INFO_ORDER_SUCCESS");
export const CHANGE_INFO_ORDER_FAILED = createAction("SALES_ORDER/CHANGE_INFO_ORDER_FAILED");

export const CLEAR = createAction("SALES_ORDER/CLEAR_ORDERS");

export const HANDLE_ITEMS_LINE = createAction("SALES_ORDER/HANDLE_ITEMS_LINE");
export const HANDLE_SEVICES_LINE = createAction("SALES_ORDER/HANDLE_SERVICES_LINE");
