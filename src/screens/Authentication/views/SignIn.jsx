/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Field, reduxForm } from "redux-form";
import { useMutation } from "react-query";
import { useDispatch } from "react-redux";

import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import back4 from "../../../assets/images/bg-images/bg4.jpg";
import { FORM_SIGN_IN } from "./Lang";
import { name } from "../reducer";
import { APIS_LOGIN } from "../apis";
import { set, clear } from "../../../services/controls";
import { useHistory } from "react-router-dom";
import { BASE_URL } from "../../../config/constant";
import * as actions from "../actions";
import { useToasts } from "react-toast-notifications";
import { ROUTER_MANAGE_SCHEDULE } from "../../ManageSchedulers/router";

export default function SignIn() {
  return (
    <React.Fragment>
      <Breadcrumb />
      <div className="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch">
        <div className="row align-items-center w-100 align-items-stretch bg-white">
          <div
            className="d-none d-lg-flex col-lg-7 aut-bg-img align-items-center d-flex justify-content-center"
            style={{
              backgroundImage: `url(${back4})`,
              backgroundSize: "cover",
              backgroundAttachment: "fixed",
              backgroundPosition: "center",
            }}
          >
            <div className="col-md-8">
              <h1 className="text-white mb-5">Đăng Nhập</h1>
              <p className="text-white">
                Chào mừng đến với hệ thống Y tế số của Phòng khám Đa khoa Nguyên
                Phương
              </p>
            </div>
          </div>

          <div className="col-lg-5 align-items-stret h-100 align-items-center d-flex justify-content-center">
            <div className=" auth-content text-center">
              <SignUpForm />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

const SubmitValidationForm = (props) => {
  const { error, handleSubmit, submitting } = props;
  const { addToast } = useToasts();

  const history = useHistory();
  const dispatch = useDispatch();
  const mutationLogin = useMutation("input", APIS_LOGIN);

  const submit = (values) => {
    clear();
    mutationLogin.mutate({
      input: values,
    });
  };

  useEffect(() => {
    const { isLoading, data, isError } = mutationLogin;

    if (!isLoading) {
      if (!isError) {
        if (!!data) {
          set("accesstoken", data.login_user.accesstoken);
          history.push(ROUTER_MANAGE_SCHEDULE);
        }
      } else {
        addToast("Sai thông tin đăng nhập, vui lòng thử lại!", {
          appearance: "error",
          autoDismiss: true,
        });
      }
    }
  }, [mutationLogin.isLoading]);

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card>
        <Card.Header>
          <Card.Title as="h3">{FORM_SIGN_IN.title}</Card.Title>
        </Card.Header>
        <Card.Body>
          <Field
            name="username"
            type="text"
            component={renderField}
            required={true}
            label={FORM_SIGN_IN.username}
          />
          <Field
            name="password"
            type="password"
            component={renderField}
            required={false}
            label={FORM_SIGN_IN.password}
          />
          {error && <strong>Sai thông tin xác thực, vui lòng thử lại!</strong>}
        </Card.Body>
        <Card.Footer>
          <Button
            type="submit"
            className="mr-1 float-right"
            disabled={submitting}
          >
            {FORM_SIGN_IN.signinBtn}
          </Button>
        </Card.Footer>
      </Card>
    </form>
  );
};

const validate = (values) => {
  const errors = {};
  // console.log({ values });
  if (!values.username)
    errors.username = `${FORM_SIGN_IN.username} không được trống`;
  if (!values.password)
    errors.password = `${FORM_SIGN_IN.password} không được trống`;
  return errors;
};

const renderField = ({
  input,
  label,
  type,
  meta: { asyncValidating, touched, error },
}) => (
  <Row className="my-3">
    <Col sm={4} style={{ alignSelf: "center" }}>
      <label className="label-control" style={{ marginBottom: "0px" }}>
        {label}:
      </label>
    </Col>
    <Col sm={8} className={asyncValidating ? "async-validating" : ""}>
      <input
        {...input}
        placeholder={label}
        type={type}
        className="form-control"
      />
      {touched && error && <span className="text-c-red">* {error} </span>}
    </Col>
  </Row>
);

const SignUpForm = reduxForm({
  form: name, // a unique identifier for this form
  validate,
  asyncChangeFields: ["username"],
})(SubmitValidationForm);
