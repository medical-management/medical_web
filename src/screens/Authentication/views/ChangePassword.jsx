/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Field, reduxForm } from "redux-form";
import { useMutation } from "react-query";

import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import back4 from "../../../assets/images/bg-images/bg4.jpg";
import { name } from "../reducer";
import { APIS_CHANGE_PASS } from "../apis";
import { useHistory } from "react-router-dom";
import { BASE_URL } from "../../../config/constant";
import { FORM_CHANGE_PASS } from "./Lang";
import { useToasts } from "react-toast-notifications";

export default function ChangePassword() {
  return (
    <React.Fragment>
      <Breadcrumb />
      <div className="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch">
        <div className="row align-items-center w-100 align-items-stretch bg-white">
          <div
            className="d-none d-lg-flex col-lg-7 aut-bg-img align-items-center d-flex justify-content-center"
            style={{
              backgroundImage: `url(${back4})`,
              backgroundSize: "cover",
              backgroundAttachment: "fixed",
              backgroundPosition: "center",
            }}
          >
            <div className="col-md-8">
              <h1 className="text-white mb-5">Đổi mật khẩu</h1>
              <p className="text-white">
                Chào mừng đến với hệ thống Y tế số của Phòng khám Đa khoa Nguyên
                Phương
              </p>
            </div>
          </div>

          <div className="col-lg-5 align-items-stret h-100 align-items-center d-flex justify-content-center">
            <div className=" auth-content text-center">
              <ChangePasswordForm />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

const SubmitValidationForm = (props) => {
  const { error, handleSubmit, pristine, reset, submitting } = props;

  const { addToast } = useToasts();

  const history = useHistory();

  const mutationChangePassword = useMutation("input", APIS_CHANGE_PASS);

  const submit = (values) => {
    mutationChangePassword.mutate({
      input: values,
      output: ["id", "accesstoken", "refreshtoken"],
    });
  };

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangePassword;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Đổi mật khẩu thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push(BASE_URL);
      }
    }
  }, [mutationChangePassword.isLoading]);

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card>
        <Card.Header>
          <Card.Title as="h3">{FORM_CHANGE_PASS.title}</Card.Title>
        </Card.Header>
        <Card.Body>
          <Field
            name="oldPassword"
            type="password"
            component={renderField}
            required={false}
            label={FORM_CHANGE_PASS.oldPassword}
          />
          <Field
            name="newPassword"
            type="password"
            component={renderField}
            required={false}
            label={FORM_CHANGE_PASS.newPassword}
          />
          {error && <strong>{error}</strong>}
        </Card.Body>
        <Card.Footer>
          <Button
            type="submit"
            className="mr-1 float-right"
            disabled={submitting}
          >
            {FORM_CHANGE_PASS.processBtn}
          </Button>
        </Card.Footer>
      </Card>
    </form>
  );
};

const validate = (values) => {
  const errors = {};
  // console.log({ values });
  if (!values.oldPassword)
    errors.oldPassword = `${FORM_CHANGE_PASS.oldPassword} không được trống!`;
  if (!values.newPassword)
    errors.newPassword = `${FORM_CHANGE_PASS.newPassword} không được trống!`;
  if (values.oldPassword === values.newPassword)
    errors.samePass = `Mật khẩu không được trùng nhau!`;
  return errors;
};

const renderField = ({
  input,
  label,
  type,
  meta: { asyncValidating, touched, error },
}) => (
  <Row className="my-3">
    <Col sm={3}>
      <label className="label-control">{label}</label>
    </Col>
    <Col sm={9} className={asyncValidating ? "async-validating" : ""}>
      <input
        {...input}
        placeholder={`Vui lòng nhập ${label}`}
        type={type}
        className="form-control"
      />
      {touched && error && <span className="text-c-red">* {error} </span>}
    </Col>
  </Row>
);

const ChangePasswordForm = reduxForm({
  form: name,
  validate,
  asyncChangeFields: ["username"],
})(SubmitValidationForm);
