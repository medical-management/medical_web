/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Field, reduxForm } from "redux-form";
import { useMutation } from "react-query";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";

import back4 from "../../../assets/images/bg-images/bg4.jpg";
import { FORM_SIGN_UP } from "./Lang";
import { name } from "../reducer";
import { APIS_LOGIN } from "../apis";
export default function SignUp() {
    return (
        <React.Fragment>
            <Breadcrumb />
            <div className="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch">
                <div className="row align-items-center w-100 align-items-stretch bg-white">
                    <div
                        className="d-none d-lg-flex col-lg-8 aut-bg-img align-items-center d-flex justify-content-center"
                        style={{
                            backgroundImage: `url(${back4})`,
                            backgroundSize: "cover",
                            backgroundAttachment: "fixed",
                            backgroundPosition: "center",
                        }}
                    >
                        <div className="col-md-8">
                            <h1 className="text-white mb-5">Sign up Datta Able</h1>
                            <p className="text-white">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever.
                            </p>
                        </div>
                    </div>

                    <div className="col-lg-4 align-items-stret h-100 align-items-center d-flex justify-content-center">
                        <div className=" auth-content text-center">
                            <SignUpForm />
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

const SubmitValidationForm = (props) => {
    const { error, handleSubmit, pristine, reset, submitting } = props;
    const submit = (values) => {
        mutationTodo.mutate({
            input: values,
            output: ["id", "username", "accesstoken", "refreshtoken", "firstname", "lastname"],
        });
    };
    const mutationTodo = useMutation("input", APIS_LOGIN);
    console.log(mutationTodo.isLoading);
    console.log(mutationTodo.error);
    console.log(mutationTodo.data);
    return (
        <form onSubmit={handleSubmit(submit)}>
            <Card>
                <Card.Header>
                    <div className="mb-4">
                        <i className="feather icon-user-plus auth-icon" />
                    </div>
                    <Card.Title as="h3">{FORM_SIGN_UP.title}</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Field
                        name="username"
                        type="text"
                        component={renderField}
                        required={true}
                        label={FORM_SIGN_UP.username}
                    />
                    <Field
                        name="password"
                        type="password"
                        component={renderField}
                        required={false}
                        label={FORM_SIGN_UP.password}
                    />
                    {error && <strong>{error}</strong>}
                </Card.Body>
                <Card.Footer>
                    <Button
                        type="button"
                        variant="danger"
                        className="float-right"
                        disabled={pristine || submitting}
                        onClick={reset}
                    >
                        Xóa giá trị
                    </Button>
                    <Button type="submit" className="mr-1 float-right" disabled={submitting}>
                        Đăng nhập
                    </Button>
                </Card.Footer>
            </Card>
        </form>
    );
};
const validate = (values) => {
    const errors = {};
    // console.log({ values });
    if (!values.username) errors.username = `${FORM_SIGN_UP.username} không được trống`;
    if (!values.password) errors.password = `${FORM_SIGN_UP.password} không được trống`;
    return errors;
};

const renderField = ({ input, label, type, meta: { asyncValidating, touched, error } }) => (
    <Row className="my-3">
        <Col sm={3}>
            <label className="label-control">{label}</label>
        </Col>
        <Col sm={9} className={asyncValidating ? "async-validating" : ""}>
            <input {...input} placeholder={label} type={type} className="form-control" />
            {touched && error && <span className="text-c-red">* {error} </span>}
        </Col>
    </Row>
);
const SignUpForm = reduxForm({
    form: name, // a unique identifier for this form
    validate,
    asyncChangeFields: ["username"],
})(SubmitValidationForm);
