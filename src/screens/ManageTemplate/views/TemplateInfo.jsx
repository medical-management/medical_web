/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row, Tab, Tabs } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import { useToasts } from "react-toast-notifications";
import EditorCkClassic from "../../../components/CK-Editor/CkClassicEditor";
import ConfirmModal from "../../../components/ConfirmModal";
import CustomModal from "../../../components/CustomModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import {
  paymentOptions,
  statusOrderOptions,
  statusOrderOptionsSelect,
} from "../../../constants/options";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import { APIS_INTEGRATE_ITEM } from "../../Integration/apis";
import { APIS_FIND_FAST_ITEMS } from "../../ManageItems/apis";
import { APIS_FIND_FAST_PATIENT } from "../../ManagePatient/apis";
import * as actions from "../actions";
import {
  APIS_FIND_ALL_MEDICAL,
  APIS_FIND_ONE_TEMPLATE,
  APIS_SAVE_ITEM_ORDER,
  APIS_SAVE_SERVICES_ORDER,
  APIS_SAVE_TEMPLATE,
} from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";

export default function OrderInformation() {
  return (
    <React.Fragment>
      <Breadcrumb />
      <Card style={{ padding: 30 }}>
        <SubmitValidationForm />
      </Card>
    </React.Fragment>
  );
}

let SubmitValidationForm = (props) => {
  const params = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const [description, setDescrition] = useState("");
  const [note, setNote] = useState("");

  const { template, items, listPatient, medicals } = useSelector(
    (state) => state["TEMPLATES"]
  );
  const {
    register,
    handleSubmit,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const [status, setStatus] = useState(null);
  const [selectedPatient, setSelectedPatient] = useState("");
  const [selectedMedical, setSelectedMedical] = useState("");

  const [lastSavedIndex, setLastSavedIndex] = useState(0);
  const [select, setSelect] = useState("");
  const [name, setName] = useState("");

  const [showSelectItem, setShowSelectItem] = useState(false);
  const [selectingItem, setSelectingItem] = useState(null);
  const [selectingRow, setSelectingRow] = useState(null);

  const [key, setKey] = useState("item");

  const [firstName, setFirstName] = useState("");

  const queryPatient = useQuery("listpatients", () =>
    APIS_FIND_FAST_PATIENT({ firstname: firstName, take: 10, skip: 0 })
  );

  const queryOrder = useQuery(
    ["find_one_sales_order", params],
    () => params?.id !== "add" && APIS_FIND_ONE_TEMPLATE(params)
  );

  const queryMedicals = useQuery("medicals", () =>
    APIS_FIND_ALL_MEDICAL({
      take: 30,
      skip: 0,
      name: medicalName,
    })
  );

  const queryItem = useQuery("itemsList", () =>
    APIS_FIND_FAST_ITEMS({ name: name, take: 10, skip: 0 })
  );

  const mutationSaveServiceOrder = useMutation(
    "serviceOrder",
    APIS_SAVE_SERVICES_ORDER
  );

  useEffect(() => {
    if (selectingRow) setShowSelectItem(true);
  }, [selectingRow]);

  const [medicalName, setMedicalName] = useState("");

  const [payload, setPayload] = useState(null);

  const mutationChangeProfile = useMutation("input", APIS_SAVE_TEMPLATE);
  const mutationAddProfile = useMutation("input", APIS_SAVE_TEMPLATE);
  const mutationSaveItemOrder = useMutation("itemOrder", APIS_SAVE_ITEM_ORDER);

  const mutationIntegration = useMutation("integrate", APIS_INTEGRATE_ITEM);

  const [itemToSave, setItemToSave] = useState(null);
  const [serviceToSave, setServiceToSave] = useState(null);
  const [confirmSaveService, setConfirmSaveService] = useState(false);

  const [checkedSearch, setCheckedSearch] = useState(false);

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [confirmSave, setConfirmSave] = useState(false);
  const [confirmSaveItem, setConfirmSaveItem] = useState(false);
  const queryClient = useQueryClient();
  const { addToast } = useToasts();

  const [selectedPayment, setSelectedPayment] = useState("");
  const [paymentNote, setPaymentNote] = useState("");

  const [disableField, setDisableField] = useState(false);

  useEffect(() => {
    if (user?.decentralization[0]?.medical === 0) {
      setDisableField(true);
    }
  }, [user]);

  let baseItem = {
    id: null,
    index: 0,
    name: "",
    unit: "",
    note: "",
    isGrossProfit: false,
    perGrossProfit: 0,
    quantity: 0,
    price: "",
    amount: "",
    bin: "",
    total: 0,
    item: {
      id: "",
      name: "",
    },
  };

  const [listItems, setListItem] = useState([
    {
      index: 0,
      id: 0,
      name: "",
      unit: "",
      note: "",
      isGrossProfit: false,
      perGrossProfit: 0,
      quantity: 0,
      price: "",
      amount: "",
      bin: "",
      total: 0,
    },
  ]);

  let baseService = {
    id: null,
    index: 0,
    name: "",
    note: "",
    quantity: "",
    price: "",
    taxPrices: "",
    total: "",
  };

  const [listServices, setListServices] = useState([
    {
      id: 0,
      index: 0,
      name: "",
      note: "",
      quantity: "",
      price: "",
      taxPrices: "",
      total: "",
    },
  ]);

  useEffect(() => {
    if (params.id !== "add") {
      if (history?.location?.search !== "?integration=true")
        queryClient.prefetchQuery(["find_one_sales_order", params], () =>
          APIS_FIND_ONE_TEMPLATE(params)
        );
    } else dispatch(actions.GET_INFO_TEMPLATE_SUCCESS(null));
  }, [params.id]);

  useEffect(() => {
    if (queryItem.status === "success" && !queryItem.isLoading)
      dispatch(actions.GET_ALL_FAST_ITEM_SUCCESS(queryItem?.data));
  }, [queryItem.data]);

  useEffect(() => {
    if (!queryOrder.isLoading && !queryOrder.isError) {
      dispatch(
        actions.GET_INFO_TEMPLATE_SUCCESS(
          queryOrder?.data?.find_one_sales_order
        )
      );
      if (queryOrder.data.find_one_sales_order) {
        setDescrition(
          queryOrder?.data?.find_one_sales_order?.description || ""
        );
        setNote(queryOrder?.data?.find_one_sales_order?.note || "");
        setSelect(queryOrder?.data?.find_one_sales_order?.category || "");
      }
    }
  }, [queryOrder.data, params]);

  useEffect(() => {
    queryClient.prefetchQuery("medicals", queryMedicals);
  }, [medicalName]);

  useEffect(() => {
    if (!queryMedicals.isLoading && !queryMedicals.isError)
      dispatch(actions.GET_ALL_MEDICALS_SUCCESS(queryMedicals?.data));
  }, [queryMedicals.isLoading]);

  useEffect(() => {
    queryClient.fetchQuery("itemsList", queryItem);
  }, [name]);

  useEffect(() => {
    if (params.id !== "add" && template) {
      setNote(template?.note);
      setSelectedPatient(template?.patient?.id);
      setListItem(
        template?.items?.map((item, index) => {
          return {
            index: index,
            id: item?.id,
            name: item?.item?.name,
            unit: item?.unit,
            note: item?.note,
            isGrossProfit: item?.isGrossProfit,
            perGrossProfit: item?.perGrossProfit,
            quantity: item?.quantity,
            price: item?.price,
            taxAmount: item?.taxAmount,
            taxPrices: item?.perTax,
            bin: item?.price,
            total: item?.amount,
            itemInfo: item?.item,
          };
        }) || []
      );
      setListServices(
        template?.expenses?.map((item, index) => {
          return {
            index: index,
            id: item?.id,
            name: item?.name,
            note: item?.note,
            price: item?.price,
            taxPrices: item?.perTax,
            total: item?.amount,
          };
        }) || []
      );
      // set form value
      Object.entries(template).forEach(([key, value]) => {
        setValue(key, value);
      });
      setStatus(template?.status);
      setPaymentNote(template?.paymentNote);
      setSelectedPayment(template?.paymentMenthod);
    }
  }, [template]);

  const submit = (values) => {
    setPayload({
      info: {
        ...template,
        ...values,
        id: params?.id === "add" ? "" : template?.id,
        show: checkedSearch,
        totalTax:
          parseInt(
            _.sumBy(listItems, function (o) {
              return o.taxPrices * o.bin;
            }) || 0
          ) +
          parseInt(
            _.sumBy(listServices, function (o) {
              return o.taxPrices * o.bin;
            }) || 0
          ),
        totalAmount:
          parseInt(
            _.sumBy(listItems, function (o) {
              return o.total;
            }) || 0
          ) +
          parseInt(
            _.sumBy(listServices, function (o) {
              return o.total;
            }) || 0
          ),
      },
      items: listItems?.map((item) => ({
        id: item?.id || "",
        name: item?.name,
        unit: item?.unit,
        location: item?.location,
        note: item?.note,
        itemId: item?.value,
        isGrossProfit: item?.isGrossProfit,
        perGrossProfit: item?.perGrossProfit,
        quantity: item?.quantity,
        perTax: item?.taxPrices,
        basePrice: item?.bin,
        price: item?.price,
        amount: item?.total,
      })),
      services: listServices?.map((item) => ({
        id: item?.id || "",
        name: item?.name,
        note: item?.note,
        perTax: item?.taxPrices,
        price: item?.price,
        amount: item?.total,
      })),
    });
  };

  const handleSave = () => {
    if (params.id !== "add") {
      mutationChangeProfile.mutate(payload);
    } else {
      mutationAddProfile.mutate({
        input: {
          ...payload,
          id: "",
        },
      });
    }
  };

  useEffect(() => {
    queryClient.fetchQuery("listpatients", queryPatient);
  }, [firstName]);

  useEffect(() => {
    if (queryPatient.status === "success") {
      dispatch(actions.GET_ALL_PATIENT_INFO_SUCCESS(queryPatient?.data));
    }
  }, [queryPatient.data]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        setConfirmSave(false);
        dispatch(actions.CHANGE_INFO_TEMPLATE_SUCCESS(data));
      }
    }
  }, [mutationChangeProfile.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationAddProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push("/app/dashboard/items");
      }
    }
  }, [mutationAddProfile.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationIntegration;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });

        dispatch(actions.CHANGE_INFO_TEMPLATE_SUCCESS(data));
      }
    }
  }, [mutationIntegration.isLoading]);

  useEffect(() => {
    if (!confirmSave) {
      setPayload(null);
    }
  }, [confirmSave]);

  useEffect(() => {
    if (payload) setConfirmSave(true);
  }, [payload]);

  const handleSaveItemOrder = (item) => {
    setLastSavedIndex(item.index);
    delete item.index;
    mutationSaveItemOrder.mutate(item);
  };

  useEffect(() => {
    if (itemToSave !== null) setConfirmSaveItem(true);
    else setConfirmSave(false);
  }, [itemToSave]);

  useEffect(() => {
    if (!confirmSaveItem) setItemToSave(null);
  }, [confirmSaveItem]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationSaveItemOrder;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm thông tin sản phẩm thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        setConfirmSaveItem(false);
        setListItem(
          listItems?.map((item) => {
            if (item.index === lastSavedIndex) {
              return {
                ...item,
                id: lastSavedIndex,
              };
            }
            return item;
          })
        );
      } else if (isError) {
        addToast("Thêm thông tin sản phẩm thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
      }
    }
  }, [mutationSaveItemOrder.isLoading]);

  const handleSaveServicesOrder = (item) => {
    setLastSavedIndex(item.index);
    delete item.index;
    mutationSaveServiceOrder.mutate(item);
  };

  useEffect(() => {
    if (serviceToSave !== null) setConfirmSaveService(true);
    else setConfirmSaveService(false);
  }, [serviceToSave]);

  useEffect(() => {
    if (!confirmSaveService) setServiceToSave(null);
  }, [confirmSaveService]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationSaveServiceOrder;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Thêm thông tin dịch vụ thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        setConfirmSaveService(false);
        setListServices(
          listServices?.map((item) => {
            if (item.index === lastSavedIndex) {
              return {
                ...item,
                id: lastSavedIndex,
              };
            }
            return item;
          })
        );
      } else if (isError) {
        addToast("Thêm thông tin dịch vụ thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
      }
    }
  }, [mutationSaveServiceOrder.isLoading]);

  const columnsItem = React.useMemo(
    () => [
      {
        Header: "Tên sản phẩm",
        accessor: (row) => (
          <div
            className="self-center"
            style={{
              paddingTop: 8,
              minWidth: "200px",
            }}
          >
            <Select
              placeholder="Chọn sản phẩm"
              name="patient"
              style={{ width: "100%" }}
              className="basic-single"
              classNamePrefix="select"
              options={items}
              defaultValue={{
                value: row?.itemInfo?.id,
                label: row?.itemInfo?.name,
                unit: row?.itemInfo?.unit,
                price: parseInt(row?.itemInfo?.salePrices),
                bin: parseInt(row?.itemInfo?.salePrices),
                taxPrices: parseInt(row?.itemInfo?.taxPrices),
              }}
              onChange={(e) => {
                setListItem(
                  listItems?.map((o) => {
                    if (o?.index === row?.index) {
                      return {
                        ...o,
                        ...e,
                        name: e?.label,
                        isGrossProfit: false,
                        perGrossProfit: 0,
                        total: e?.price * (1 + parseInt(e?.taxPrices || "0")),
                        quantity: 1,
                      };
                    } else return { ...o };
                  })
                );
              }}
              onInputChange={_.debounce(function (e) {
                setName(e);
              }, 800)}
            />
          </div>
        ),
      },
      {
        Header: "ĐVT",
        accessor: (row) => {
          return (
            <input
              disabled={!row?.name || disableField}
              className="form-control mt-2"
              value={row?.unit}
              onChange={(e) => {
                setListItem(
                  listItems?.map((o) => {
                    if (o.index === row.index) {
                      return {
                        ...o,
                        unit: e?.target?.value || "",
                      };
                    } else return o;
                  })
                );
              }}
              style={{
                paddingLeft: "2px",
                paddingRight: "2px",
                minWidth: "80px",
              }}
            />
          );
        },
      },
      {
        Header: "SL",
        accessor: (row) => (
          <input
            className="form-control mt-2"
            disabled={!row?.name || disableField}
            type="number"
            style={{
              paddingLeft: "2px",
              paddingRight: "2px",
              minWidth: "80px",
            }}
            value={row?.quantity < 0 ? 0 : row?.quantity}
            onChange={(e) => {
              setListItem(
                listItems?.map((o) => {
                  if (o.index === row.index) {
                    return {
                      ...o,
                      quantity:
                        parseInt(e.target.value) < 0
                          ? 0
                          : parseInt(e.target.value),
                      total:
                        parseInt(
                          parseInt(e.target.value) < 0
                            ? "0"
                            : e.target.value || "0"
                        ) *
                        row?.price *
                        (1 + row.taxPrices / 100),
                    };
                  } else return o;
                })
              );
            }}
          />
        ),
      },
      {
        Header: "Áp dụng LN",
        accessor: (row) => {
          return (
            <input
              type="checkbox"
              className="form-control"
              disabled={!row?.name || disableField}
              defaultChecked={row?.isGrossProfit}
              onChange={(e) => {
                setListItem(
                  listItems?.map((o) => {
                    if (o.index === row.index) {
                      return {
                        ...o,
                        isGrossProfit: !row?.isGrossProfit,
                        total:
                          parseInt(row?.quantity || "0") *
                          parseInt(row?.bin) *
                          (1 + row.taxPrices / 100),
                        price: row.bin,
                        perGrossProfit: 0,
                      };
                    } else return o;
                  })
                );
              }}
            />
          );
        },
      },
      {
        Header: "Lợi nhuận (%)",
        accessor: (row) => {
          return (
            <input
              type="number"
              min="0"
              className="form-control mt-2"
              style={{ paddingLeft: "2px", paddingRight: "2px" }}
              disabled={!row?.isGrossProfit}
              value={row?.perGrossProfit < 0 ? 0 : row?.perGrossProfit}
              onChange={(e) => {
                setListItem(
                  listItems?.map((o) => {
                    if (o.index === row?.index) {
                      return {
                        ...o,
                        perGrossProfit:
                          parseInt(e.target.value) < 0
                            ? 0
                            : parseInt(e.target.value),
                        total:
                          parseInt(row?.quantity || "0") *
                          parseInt(
                            row?.bin *
                              (1 + parseInt(e.target.value || "0") / 100)
                          ) *
                          (1 + row?.taxPrices / 100),
                        price:
                          row?.bin *
                          (1 + parseInt(e.target.value || "0") / 100),
                      };
                    } else return o;
                  })
                );
              }}
            />
          );
        },
      },
      {
        Header: "Kho (VND)",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {row?.bin?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
      {
        Header: "Đơn giá (VND)",
        accessor: (row) => (
          <p className="cell-p">
            {row?.price?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        ),
      },
      {
        Header: "Thuế (%)",
        accessor: (row) => <p className="cell-p">{row?.taxPrices}</p>,
      },
      {
        Header: "Tổng tiền",
        accessor: (row) => (
          <p className="cell-p">
            {row?.total?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        ),
      },
      {
        Header: "Ghi chú",
        accessor: (row) => (
          <input
            className="form-control"
            disabled={!row?.name || disableField}
            style={{
              paddingLeft: "2px",
              paddingRight: "2px",
              minWidth: "150px",
            }}
            value={row?.note}
            onChange={(e) => {
              setListItem(
                listItems?.map((o) => {
                  if (o.index === row.index) {
                    return {
                      ...o,
                      note: e?.target?.value || "",
                    };
                  } else return o;
                })
              );
            }}
          />
        ),
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization[0]?.salesOrders > 2 && (
              <div>
                {listItems?.length > 1 && !row?.id && (
                  <Button
                    disabled={disableField}
                    style={{
                      height: 43,
                      marginLeft: "10px",
                    }}
                    className="feather icon-minus margin-0 btn-danger"
                    onClick={() => {
                      setListItem(
                        listItems?.filter((o) => o.index !== row.index)
                      );
                    }}
                  ></Button>
                )}
              </div>
            )
          );
        },
      },
    ],
    [listItems, items]
  );
  const columnsService = React.useMemo(
    () => [
      {
        Header: "Tên dịch vụ",
        accessor: (row) => {
          return (
            <input
              style={{
                paddingLeft: "2px",
                paddingRight: "2px",
                minWidth: "150px",
              }}
              placeholder="Nhập tên dịch vụ"
              className="form-control"
              value={row?.name}
              onChange={(e) =>
                setListServices(
                  listServices.map((o) => {
                    if (o.index === row?.index) {
                      return {
                        ...o,
                        name: e.target.value,
                      };
                    } else return o;
                  })
                )
              }
            ></input>
          );
        },
      },
      {
        Header: "Ghi chú",
        accessor: (row) => {
          return (
            <input
              disable={!row?.name}
              style={{
                paddingLeft: "2px",
                paddingRight: "2px",
                minWidth: "200px",
              }}
              className="form-control"
              value={row?.note}
              placeholder="Nhập ghi chú"
              onChange={(e) =>
                setListServices(
                  listServices.map((o) => {
                    if (o?.index === row?.index) {
                      return {
                        ...o,
                        note: e.target.value,
                      };
                    } else return o;
                  })
                )
              }
            ></input>
          );
        },
      },
      {
        Header: "Đơn giá",
        accessor: (row) => {
          return (
            <input
              disable={!row?.name}
              style={{
                paddingLeft: "2px",
                paddingRight: "2px",
                minWidth: "100px",
              }}
              className="form-control"
              type="number"
              value={parseInt(row?.price) < 0 ? 0 : parseInt(row?.price)}
              onChange={(e) =>
                setListServices(
                  listServices.map((o) => {
                    if (o.index === row.index) {
                      return {
                        ...o,
                        price:
                          parseInt(e.target.value) < 0
                            ? 0
                            : parseInt(e.target.value),
                        total:
                          parseInt(e.target.value) *
                          (1 + parseInt(row?.taxPrices || "0") / 100),
                      };
                    } else return o;
                  })
                )
              }
            ></input>
          );
        },
      },
      {
        Header: "Thuế",
        accessor: (row) => {
          return (
            <input
              disable={!row?.name}
              style={{
                paddingLeft: "2px",
                paddingRight: "2px",
                minWidth: "100px",
              }}
              className="form-control"
              value={
                parseInt(row?.taxPrices) < 0 ? 0 : parseInt(row?.taxPrices)
              }
              type="number"
              onChange={(e) =>
                setListServices(
                  listServices.map((o) => {
                    if (o.index === row.index) {
                      return {
                        ...o,
                        taxPrices:
                          parseInt(e.target.value) < 0
                            ? 0
                            : parseInt(e.target.value),
                        total:
                          row?.price *
                          (1 + parseInt(e.target.value || "0") / 100),
                      };
                    } else return o;
                  })
                )
              }
            ></input>
          );
        },
      },
      {
        Header: "Tổng tiền",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {parseInt(row?.total || "0")?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization[0]?.salesOrders > 2 && (
              <div>
                {listServices?.length > 1 && !row?.id && (
                  <Button
                    disabled={disableField}
                    style={{
                      height: 43,
                      marginLeft: "10px",
                    }}
                    className="feather icon-minus margin-0 btn-danger"
                    onClick={() => {
                      setListServices(
                        listServices?.filter((o) => o.index !== row.index)
                      );
                    }}
                  ></Button>
                )}
              </div>
            )
          );
        },
      },
    ],
    [listServices, key]
  );

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card.Header
        className="header-card"
        style={{
          position: "relative",
          marginLeft: "-30px",
        }}
      >
        <p
          className="feather icon-chevron-left icon"
          onClick={() => history.goBack()}
        />
        <Card.Title as="h4" className="title">
          {"THÔNG TIN HỒ SƠ MẪU"} {template?.name ? ` - ${template?.name}` : ""}
        </Card.Title>
      </Card.Header>
      <SessionTitle>THÔNG TIN CHÍNH</SessionTitle>
      <Row>
        <Col sm={6}>
          <Controller
            {...register("name", { required: true })}
            control={control}
            render={({ field }) => (
              <FormInput
                required
                disabled={disableField}
                label={"Tên mẫu"}
                {...field}
                error={errors?.name ? "Vui lòng nhập tên hồ sơ" : ""}
              />
            )}
          />
        </Col>
        <Col sm={6}>
          <Controller
            {...register("globalsearch")}
            control={control}
            render={({ field }) => (
              <FormInput
                disabled={disableField}
                label={"Viết tắt"}
                {...field}
              />
            )}
          />
        </Col>

        <Col
          sm={6}
          style={{
            alignSelf: "center",
            display: "flex",
            placeItems: "center",
            paddingTop: 6,
            paddingBottom: 6,
          }}
        >
          <p className="cell-p margin-0">Cho phép tìm kiếm</p>

          <Controller
            {...register("show")}
            control={control}
            render={({ field, onChange }) => (
              <input
                style={{ width: "fit-content", marginLeft: "20px" }}
                {...field}
                onChange={(e) => setCheckedSearch(e.target.checked)}
                defaultChecked={field?.value}
                type="checkbox"
                className="form-control"
              />
            )}
          />
        </Col>
        {params?.id !== "add" && (
          <Col sm={6}>
            <Controller
              control={control}
              render={({ field }) => (
                <FormInput
                  {...field}
                  label="Người sửa"
                  value={template?.updatedby}
                  disabled
                />
              )}
            />
          </Col>
        )}
        {params?.id !== "add" && (
          <Col sm={6}>
            <Controller
              control={control}
              render={({ field }) => (
                <FormInput
                  {...field}
                  label="Ngày tạo"
                  value={template?.createdat}
                  disabled
                />
              )}
            />
          </Col>
        )}
        {params?.id !== "add" && (
          <Col sm={6}>
            <Controller
              control={control}
              render={({ field }) => (
                <FormInput
                  {...field}
                  label="Ngày cập nhật"
                  value={template?.updatedat}
                  disabled
                />
              )}
            />
          </Col>
        )}
        <Col sm={12} style={{ zIndex: 0 }}>
          <label
            className="label-control mt-2"
            style={{ whiteSpace: "nowrap" }}
          >
            Ghi chú
          </label>
          {disableField ? (
            <FormInput disabled={disableField}></FormInput>
          ) : (
            <EditorCkClassic
              html={note || ""}
              onChange={(value) => setNote(value)}
            />
          )}
        </Col>
      </Row>
      <div className="custom-tab">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          style={{
            marginTop: 20,
            marginLeft: "0px",
            borderBottom: "1px solid #aaa ",
          }}
        >
          <Tab eventKey="item" title="Sản phẩm">
            <SessionTitle>Danh sách sản phẩm</SessionTitle>
            <div className="flex-height-table">
              <CustomTable columns={columnsItem} data={listItems || []} />
              <Button
                style={{
                  height: 43,
                  marginTop: "8px",
                  position: "absolute",
                  bottom: 220,
                  right: 0,
                }}
                className="margin-0 btn"
                disabled={disableField}
                onClick={() =>
                  setListItem([
                    ...listItems,
                    {
                      ...baseItem,
                      index:
                        listItems?.length === 0
                          ? 0
                          : parseInt(
                              listItems
                                .sort(
                                  (a, b) =>
                                    parseInt(a?.index) - parseInt(b?.index)
                                )
                                .reverse()[0]?.index + 1
                            ),
                    },
                  ])
                }
              >
                THÊM SẢN PHẨM
              </Button>
            </div>
          </Tab>
          <Tab eventKey="service" title="Dịch vụ" style={{ padding: 0 }}>
            <SessionTitle>Danh sách dịch vụ</SessionTitle>
            <div className="flex-height-table">
              <CustomTable columns={columnsService} data={listServices || []} />
              <Button
                style={{
                  height: 43,
                  marginTop: "8px",
                  position: "absolute",
                  bottom: 220,
                  right: 0,
                }}
                className="margin-0 btn"
                disabled={disableField}
                onClick={() =>
                  setListServices([
                    ...listServices,
                    {
                      ...baseService,
                      index:
                        listServices?.length === 0
                          ? 0
                          : parseInt(
                              listServices
                                .sort(
                                  (a, b) =>
                                    parseInt(a?.index) - parseInt(b?.index)
                                )
                                .reverse()[0]?.index + 1
                            ),
                    },
                  ])
                }
              >
                THÊM DỊCH VỤ
              </Button>
            </div>
          </Tab>
        </Tabs>
      </div>
      <Row style={{ fontSize: "16", marginTop: "-150px" }}>
        <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
          SẢN PHẨM:
        </Col>
        <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
          {parseInt(
            _.sumBy(listItems, function (o) {
              return o.total;
            }) || 0
          )?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND",
          })}
        </Col>
        <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
          DỊCH VỤ:
        </Col>
        <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
          {parseInt(
            _.sumBy(listServices, function (o) {
              return o.total;
            }) || 0
          )?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND",
          })}
        </Col>
        <Col sm={9} style={{ textAlign: "end", fontWeight: "bold" }}>
          TỔNG CỘNG:
        </Col>
        <Col style={{ textAlign: "end", maxWidth: 200 }} sm={3}>
          {(
            parseInt(
              _.sumBy(listItems, function (o) {
                return o.total;
              }) || 0
            ) +
            parseInt(
              _.sumBy(listServices, function (o) {
                return o.total;
              }) || 0
            )
          )?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND",
          })}
        </Col>
      </Row>
      <div
        className="flex"
        style={{ gap: 10, marginTop: 80, placeContent: "end" }}
      >
        {params?.id !== "add" &&
          user?.decentralization[0]?.salesOrders > 2 &&
          template?.status === "approved" && (
            <Button
              onClick={() => setStatus("paid")}
              type="submit"
              className="mr-2 mt-2 float-right"
              disabled={mutationChangeProfile.isLoading}
            >
              Thanh toán
            </Button>
          )}
        {params?.id !== "add" &&
          user?.decentralization[0]?.salesOrders > 2 &&
          template?.status === "pending" && (
            <Button
              type="submit"
              onClick={() => setStatus("approved")}
              className="mr-2 mt-2 float-right"
              disabled={mutationChangeProfile.isLoading}
            >
              Duyệt
            </Button>
          )}
        {((params?.id === "add" &&
          user?.decentralization[0]?.salesOrders > 1) ||
          (params?.id !== "add" &&
            user?.decentralization[0]?.salesOrders > 2)) && (
          <Button
            type="submit"
            className="mr-0 mt-2 float-right"
            disabled={mutationChangeProfile.isLoading}
          >
            {FORM_CHANGE_INFO.processBtn}
          </Button>
        )}
      </div>

      <ConfirmModal
        content={`Bạn có muốn ${
          params?.id === "add" ? "THÊM MỚI" : "CẬP NHẬT"
        } hồ sơ mẫu này?`}
        onChangeVisible={() => setConfirmSave(false)}
        show={confirmSave}
        onConfirm={() => handleSave()}
        loading={mutationChangeProfile.isLoading}
      ></ConfirmModal>
      <ConfirmModal
        content={`Bạn có muốn thêm hồ sơ mẫu này?`}
        onChangeVisible={() => setConfirmSaveItem(false)}
        show={confirmSaveItem}
        onConfirm={() => handleSaveItemOrder(itemToSave)}
        loading={mutationSaveItemOrder.isLoading}
      ></ConfirmModal>
      <ConfirmModal
        content={`Bạn có muốn thêm dịch vụ này?`}
        onChangeVisible={() => setConfirmSaveService(false)}
        show={confirmSaveService}
        onConfirm={() => handleSaveServicesOrder(serviceToSave)}
        loading={mutationSaveServiceOrder.isLoading}
      ></ConfirmModal>
      <CustomModal
        show={showSelectItem}
        onChangeVisible={() => setShowSelectItem(!showSelectItem)}
        title="Chọn sản phẩm"
      >
        <div
          className="self-center"
          style={{
            paddingTop: 8,
            minWidth: "200px",
          }}
        >
          <Select
            name="patient"
            style={{ width: "100%" }}
            className="basic-single"
            classNamePrefix="select"
            options={queryItem.isLoading ? [] : items}
            value={items?.find((o) => o.label === selectingItem?.name || "")}
            onChange={(e) => setSelectingItem(e)}
            onInputChange={_.debounce(function (e) {
              setName(e);
            }, 800)}
          />
        </div>
        <div
          style={{
            placeContent: "end",
            marginTop: 10,
            gap: 10,
            display: "flex",
          }}
        >
          <Button
            className="btn btn-danger margin-0"
            onClick={() => {
              setShowSelectItem(false);
              setSelectingItem(null);
            }}
          >
            Hủy
          </Button>
          <Button
            className="margin-0"
            onClick={() => {
              setListItem(
                listItems?.map((o) => {
                  if (o?.index === selectingRow?.index) {
                    return {
                      ...o,
                      ...selectingRow,
                      ...selectingItem,
                      name: selectingItem?.label,
                      isGrossProfit: false,
                      perGrossProfit: 0,
                      total:
                        selectingItem?.price *
                        (1 + parseInt(selectingItem?.taxPrices || "0")),
                      quantity: 1,
                    };
                  } else return { ...o };
                })
              );
              setShowSelectItem(false);
            }}
          >
            Lưu
          </Button>
        </div>
      </CustomModal>
    </form>
  );
};
