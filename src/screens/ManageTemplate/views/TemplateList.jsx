import React, { useEffect, useState } from "react";
import { Button, Card, Col, InputGroup, Row } from "react-bootstrap";
import { useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import CustomPagination from "../../../components/Pagination";
import { statusOrderOptions } from "../../../constants/options";
import CustomTable from "../../../views/tables/react-table/CustomTable";
// import moment from "moment";
import * as actions from "../actions";
import { APIS_FIND_ALL_TEMPLATES } from "../apis";
import moment from "moment";

function OrderList() {
  const queryClient = useQueryClient();
  const queryItem = useQuery("items", () => APIS_FIND_ALL_TEMPLATES(query));
  const params = useParams();
  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);
  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [query, setQuery] = useState({ take: pageSizes, skip: pages });

  const { orders, page, pageSize, totalPage, isNextPage } = useSelector(
    (state) => state["TEMPLATES"]
  );
  const dispatch = useDispatch();

  const history = useHistory();

  const [orderList, setOrdersList] = useState([]);

  const [show, setShow] = useState(false);

  // useEffect(() => {
  //   if (
  //     user?.decentralization == null ||
  //     (user?.decentralization?.length && user?.decentralization[0]?.items === 0)
  //   ) {
  //     alert("Bạn không có quyền thao tác");
  //     history.push("/profile");
  //   }
  // }, [user]);

  useEffect(() => {
    if (queryItem.status === "success" && !queryItem.isLoading)
      dispatch(actions.GET_ALL_TEMPLATES_SUCCESS(queryItem?.data));
    else if (queryItem.status === "error" && !queryItem.isLoading) {
      alert(queryItem?.error?.response?.errors[0]?.message);
      history.push("/profile");
    }
  }, [queryItem.data]);

  useEffect(() => {
    setOrdersList(orders);
  }, [orders, params]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setQuery({ take: pageSizes, skip: 0, name: e?.target[1]?.value || "" });
  };

  useEffect(() => {
    setQuery({ ...query, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQuery({ ...query, skip: pages });
  }, [pages]);

  useEffect(() => {
    queryClient.fetchQuery("items", queryItem);
  }, [query]);

  const handleAdvanceSearch = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      skip: 0,
      salePrices: e.target[0].value,
      purchasePrices: e.target[1].value,
    });
  };

  const onChangeEndDate = (e) => {
    setQuery({
      ...query,
      skip: 0,
      enddate: e,
      // dob: moment(e.target[4].value).utc().format(),
      // gender: gender,
    });
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "Tên giao dịch",
        accessor: (row) => {
          return (
            <p
              className="cursor-pointer cell-p"
              style={{ fontWeight: "bold" }}
              onClick={() => history.push(`/app/dashboard/orders/${row?.id}`)}
            >
              {row?.name}
            </p>
          );
        },
      },
      {
        Header: "Trạng thái",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {
                statusOrderOptions.find((item) => item?.value === row?.status)
                  ?.label
              }
            </p>
          );
        },
      },
      {
        Header: "Ngày giao dịch",
        accessor: (row) => {
          return (
            <p className="cell-p">{moment(row?.date).format("DD/MM/YYYY")}</p>
          );
        },
      },
      {
        Header: "Ngày cập nhật",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {moment(row?.updatedat).format("DD/MM/YYYY")}
            </p>
          );
        },
      },
      {
        Header: "Bệnh nhân",
        accessor: (row) => {
          return (
            <p
              className="cursor-pointer cell-p"
              onClick={() =>
                history.push(`/app/dashboard/patients/${row?.patient?.id}`)
              }
            >
              {row?.patient?.lastname} {row?.patient?.firstname}
            </p>
          );
        },
      },
      {
        Header: "Tổng tiền",
        accessor: (row) => {
          return (
            <p className="cell-p">
              {Math.round(row?.totalAmount)?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          );
        },
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization[0]?.items > 2 && (
              <Button
                className="margin-0"
                onClick={() => history.push(`/app/dashboard/orders/${row.id}`)}
              >
                <p className="feather icon-edit cell-p"></p>
              </Button>
            )
          );
        },
      },
    ],
    [user?.decentralization]
  );

  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                gap: 20,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Card.Title
                as="h5"
                style={{ whiteSpace: "nowrap", textAlign: "left" }}
              >
                Danh sách hồ sơ mẫu
              </Card.Title>
              <form
                style={{ width: "100%" }}
                onSubmit={(e) => {
                  e.preventDefault();
                  setQuery({
                    ...query,
                    take: pageSizes,
                    skip: 0,
                    name: e?.target[0]?.value || "",
                  });
                }}
              >
                <InputGroup>
                  <input
                    placeholder={`Tìm phiếu bằng Tên`}
                    className="form-control"
                    style={{ flex: 1 }}
                  />
                  <InputGroup.Append>
                    <Button
                      type="submit"
                      className="feather icon-search"
                    ></Button>
                  </InputGroup.Append>
                </InputGroup>
              </form>
              {user?.decentralization?.length &&
              user?.decentralization[0]?.items > 1 ? (
                <Button
                  style={{ margin: 0, whiteSpace: "nowrap" }}
                  x
                  onClick={() => history.push("/app/dashboard/templates/add")}
                >
                  Thêm phiếu
                </Button>
              ) : (
                ""
              )}
            </Card.Header>
            <Card.Body>
              <form onSubmit={handleSubmit}>
                <Row className="mb-3">
                  <Col className="d-flex align-items-center">
                    Hiển thị
                    <select
                      className="form-control w-auto mx-2"
                      value={pageSizes}
                      onChange={(e) => {
                        setPageSizes(Number(e.target.value));
                      }}
                    >
                      {[5, 10, 20, 50, 100].map((pgsize) => (
                        <option key={pgsize} value={pgsize}>
                          {pgsize}
                        </option>
                      ))}
                    </select>
                    {/* <p
                      className="feather icon-filter margin-0 cursor-pointer"
                      style={{
                        color: !show ? "black" : "#04a9f5",
                        fontWeight: "bold",
                        fontSize: 16,
                      }}
                      onClick={() => setShow(!show)}
                    ></p> */}
                  </Col>
                </Row>
                <Button className="hidden" type="submit"></Button>
              </form>
              {/* {show && (
                <div>
                  <div>
                    <form onSubmit={handleAdvanceSearch}>
                      <div className="flex" style={{ flexWrap: "nowrap" }}>
                        <div
                          className="self-center"
                          style={{
                            paddingBottom: 10,
                            width: "220px",
                            zIndex: 100,
                          }}
                        >
                          <label className="label-control items-center">
                            Phân loại
                          </label>
                          <Select
                            style={{ width: "220px" }}
                            className="basic-single"
                            classNamePrefix="select"
                            options={categoryOptions}
                            onChange={(e) =>
                              setQuery({ ...query, skip: 0, category: e.value })
                            }
                          />
                        </div>
                      </div>

                      <Button type="submit" className="hidden"></Button>
                    </form>
                  </div>
                </div>
              )} */}
              <div className="">
                <CustomTable columns={columns} data={orderList || []} />
              </div>
              <CustomPagination
                pageIndex={page}
                totalPage={totalPage}
                gotoPage={(e) => setPages(e)}
                canPreviousPage={page > 0}
                canNextPage={page < totalPage - 1}
              ></CustomPagination>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
}

export default OrderList;
