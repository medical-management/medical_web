/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import { gql } from "graphql-request";
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_TEMPLATES = async (input) =>
  await graphQLClient({
    queryString: ` query {
      find_all_medical_form(req: { 
            take: ${input.take}, 
            skip: ${input.skip}, 
            ${input.name ? `name: "${input.name}"` : ""} 
            ${input.date ? `date: "${input.date}"` : ""} 
      }) {
      data {
        inactive
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        id
        name
        shortName
        note
        show
        totalTax
        totalNet
        totalGross
        totalAmount
      }
      take
      skip
      total
      nextPage
    }
  }`,
  });

export const APIS_FIND_ONE_TEMPLATE = async (input) =>
  await graphQLClient({
    queryString: `query {
      find_one_medical_form(id: ${input.id}) {
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        patient{
          id
          firstname
          lastname
          dob
        }
        items{
          item {
            id
            name
            taxPrices
            salePrices
          }
          id
          unit
          location
          note
          quantity
          isGrossProfit
          perGrossProfit
          perTax
          basePrice
          price
          taxAmount
          amount
        }
        expenses{
          createdat
          updatedat
          transaction {
            id
          }
          id
          name
          note
          perTax
          price
          taxAmount
          amount
        }
        medical {
          id
        }
        recordType
        id
        type
        status
        name
        vendor
        location
        note
        paymentMenthod
        paymentNote
        paymentDate
        vendorName
        vendorPhone
        vendorAddress
        date
        totalTax
        totalNet
        totalGross
        totalAmount
        }
    }`,
  });

export const APIS_SAVE_TEMPLATE = async (input) => {
  const { info, items, services } = input?.input;

  let itemData = [];
  let serviceData = [];

  for (const item of items) {
    let data = [];
    for (const key in item) {
      if (
        key === "value" ||
        key === "index" ||
        item[key] === undefined ||
        item[key] === ""
      )
        continue;
      else if (
        [
          "id",
          "itemId",
          "isGrossProfit",
          "perGrossProfit",
          "price",
          "basePrice",
          "quantity",
          "perTax",
          "amount",
        ].includes(key)
      )
        data.push(` ${key}: ${item[key]} `);
      else data.push(` ${key}: "${item[key]}" `);
    }
    itemData.push(`{${data.join()}}`);
  }

  for (const item of services) {
    let ser = [];
    for (const key in item) {
      if (
        key === "value" ||
        key === "index" ||
        item[key] === undefined ||
        item[key] === ""
      )
        continue;
      else if (["id", "price", "perTax", "amount"].includes(key))
        ser.push(` ${key}: ${item[key]} `);
      else ser.push(` ${key}: "${item[key]}" `);
    }
    serviceData.push(`{${ser.join()}}`);
  }

  return await graphQLClient({
    queryString: ` mutation {
      save_medical_form(input: { 
            recordType: "medical_form",
            ${info.id === "" ? "" : `id: ${info.id}`}
            ${info.note ? `note: "${info.note}"` : ""}  
            ${info.show ? `show: ${info.show}` : ""} 
            ${info.name ? `name: "${info.name}"` : ""} 
            ${info.totalAmount ? `totalAmount: ${info.totalAmount}` : ""} 
            ${info.totalTax ? `totalTax: ${info.totalTax}` : ""} 
            ${info.totalGross ? `totalGross: ${info.totalGross}` : ""} 
            ${info.totalNet ? `totalNet: ${info.totalNet}` : ""} 
            items: [${itemData}]
            expenses: [${serviceData}]         
        
          }) {
              id
        }
    }`,
  });
};

export const APIS_FIND_ALL_MEDICAL = async ({ take, skip, name }) =>
  await graphQLClient({
    queryString: ` query {
      find_all_medical(req: { 
            take: ${take}, 
            skip: ${skip}, 
            ${name ? `name: "${name}"` : ""}           
      }) {
      data {
        patient {
          id
          firstname
          lastname
          dob
        }
        id
        name
        category
        firstname
        lastname
        reExaminate
        dateExaminate
      },
      take
      skip
      total
      nextPage
    }
  }`,
  });

export const APIS_SAVE_ITEM_ORDER = async (input) =>
  await graphQLClient({
    queryString: ` mutation {
      save_item_transaction(input: { 
            itemId: ${input.itemId},
            
            transactionId: ${input.transactionId},
            unit:" ${input.unit || " "}",
            location: "${input.location || " "}",
            note: "${input.note || " "} ",
            isGrossProfit: ${input.isGrossProfit || false},
            perGrossProfit: ${input.perGrossProfit || 0},
            quantity: ${input.quantity || 0},
            perTax: ${input.perTax || 0},
            basePrice: ${input.basePrice || 0},
            amount: ${input.amount || 0},
            price: ${input.price || 0},            
          }) {
              id
        }
    }`,
  });

export const APIS_SAVE_SERVICES_ORDER = async (input) =>
  await graphQLClient({
    queryString: ` mutation {
      save_expenses_transaction(input: { 
            transactionId: ${input.transactionId},
            note: "${input.note || " "} ",
            name: "${input.name || " "} ",
            perTax: ${input.perTax || 0},
            amount: ${input.amount || 0},
            price: ${input.price || 0},            
          }) {
              id
        }
    }`,
  });
