/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import graphQLClient from "../../services/graphql";

export const APIS_CALC_HEADER_DASHBOARD = async () => {
    return await graphQLClient({
        queryString: `
     query {
        calculate_header_dashboard {
            qtyPeriorExpireItem
            qtyExpireItem
            qtyPendingScheduledDay
            qtyPendingScheduledWeek
            qtyPendingTransaction
            qtyPaidTransactionDay
            profictDay
            profictMonth
         }
     }
     `,
    });
};

export const APIS_CALC_REVENUE_DASHBOARD = async (req) => {
    const response = await graphQLClient({
        queryString: `
     query {
        chart_profict(req:{
            type: "${req.type}",
            startDate: "${moment(req.startDate).utc().format()}",
            endDate: "${moment(req.endDate).add(1, "days").utc().format()}"
          }){
            key,
            value
          }
     }
     `,
    });
    return response;
};

export const APIS_CALC_BEST_SELLER_DASHBOARD = async (req) => {
    const response = await graphQLClient({
        queryString: `
     query {
        best_seller_dashboard(req:{
            startDate: "${moment(req.startDate).utc().format()}",
            endDate: "${moment(req.endDate).add(1, "days").utc().format()}"
          }){
            itemId
            item
            amount
          }
     }
     `,
    });
    return response;
};

export const APIS_FIND_ALL_DATA_WAREHOUSE = async (pagination) => {
    const request = [];
    for (const key in pagination)
        if (pagination[key] && (key === "take" || key === "skip")) request.push(` ${key}: ${pagination[key]}`);

    const response = await graphQLClient({
        queryString: `
     query {
        find_all_data_warehouse(req:{${request.join()}}){
            data{
                id
                name
                shortName
                bin
                startDate
                endDate
                salePrices
                purchasePrices
                available
              },
              take
              skip
              total
          }
     }
     `,
    });
    return response;
};
