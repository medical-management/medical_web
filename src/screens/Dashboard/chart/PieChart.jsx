import React, { useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const PieChart = ({ data }) => {
    useEffect(() => {
        let chart = am4core.create("am-pie-2", am4charts.PieChart);

        chart.data =
            data && data.length > 0
                ? data
                      .filter((i) => i.amount > 0)
                      .map((o) => ({ key: o.item, value: o.amount }))
                : [];

        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "key";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        chart.legend = new am4charts.Legend();
    }, [data]);

    return <div id="am-pie-2" style={{ width: "100%", height: "500px" }} />;
};

export default PieChart;
