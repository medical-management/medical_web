import moment from "moment";
import React, { useState, useEffect } from "react";
import Chart from "react-apexcharts";

export default function AreaChart({ data, name, startDate, endDate }) {
    const [dataChart, setDataChart] = useState({ name: "", data: [] });
    const [categories, setCategories] = useState([]);
    useEffect(() => {
        const listDays = [];
        const converDataChart = [];
        let i = 0;
        while (moment(startDate).add(i, "days").unix() <= moment(endDate).unix()) {
            const dayConvert = moment(startDate).add(i, "days").format("DD/MM");
            listDays.push(dayConvert);
            if (data) {
                const foundDataChart = data.find((o) => o.key.slice(0, 5) == dayConvert);
                converDataChart.push(foundDataChart ? foundDataChart.value : 0);
            }
            i++;
        }
        setCategories(listDays);
        setDataChart({ name, data: converDataChart });
    }, [data]);

    return (
        <Chart
            height={350}
            type="area"
            options={{
                dataLabels: {
                    enabled: false,
                },
                stroke: {
                    curve: "smooth",
                },
                colors: ["#FF5370", "#FFB64D"],
                xaxis: {
                    type: "categories",
                    categories,
                },
                yaxis: {
                    labels: {
                        formatter: (value) =>
                            value?.toLocaleString("it-IT", {
                                style: "currency",
                                currency: "VND",
                            }),
                    },
                },
                tooltip: {
                    y: {
                        title: { formatter: () => "Doanh thu bán hàng:" },
                        formatter: (value) =>
                            value?.toLocaleString("it-IT", {
                                style: "currency",
                                currency: "VND",
                            }),
                    },
                },
            }}
            series={[dataChart]}
        />
    );
}
