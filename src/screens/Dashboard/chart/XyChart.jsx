import React, { useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import moment from "moment";

am4core.useTheme(am4themes_animated);

const XyChart = ({ data, name, startDate, endDate }) => {
    useEffect(() => {
        let chart = am4core.create("am-xy-1", am4charts.XYChart);
        let dataChart = [];
        if (name == "month")
            dataChart = [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((month) => {
                let foundData;
                if (data)
                    foundData = data.find(
                        (item) => item.key.slice(0, 2) == month
                    );
                return {
                    key: month,
                    value: foundData ? foundData.value : 0,
                };
            });
        else {
            const listYear = [];
            let i = 0;

            while (
                moment(startDate).add(i, "years").unix() <=
                moment(endDate).unix()
            ) {
                const key = moment(startDate).add(i, "years").format("YYYY");
                const foundDataChart = data
                    ? data.find((o) => o.key == key)
                    : null;
                listYear.push({
                    key,
                    value: foundDataChart ? foundDataChart.value : 0,
                });
                i++;
            }

            dataChart = listYear;
        }
        chart.data = dataChart;

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "key";
        categoryAxis.title.text = `Doanh thu bán hàng theo tháng của năm ${moment().format(
            "YYYY"
        )}`;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Việt Nam Đồng";

        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "key";
        series.name = "Doanh thu bán hàng";
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = true;

        chart.cursor = new am4charts.XYCursor();
    }, [data]);

    return <div id="am-xy-1" style={{ width: "100%", height: "350px" }} />;
};

export default XyChart;
