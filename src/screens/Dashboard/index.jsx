import React, { useState, useEffect } from "react";
import { Row, Col, Card, Table } from "react-bootstrap";
import { ToggleButtonGroup, ToggleButton, ButtonToolbar } from "react-bootstrap";
import { useQuery, useQueryClient } from "react-query";
import moment from "moment";
import { chartOption } from "./constants";

import PieChart from "./chart/PieChart";
import XyChart from "./chart/XyChart.jsx";
import AreaChart from "./chart/AreaChart";
import { APIS_CALC_HEADER_DASHBOARD, APIS_CALC_REVENUE_DASHBOARD, APIS_CALC_BEST_SELLER_DASHBOARD } from "./apis";

import WarehouseTable from "./warehouseTable";

const Header = ({ data }) => {
    return (
        <>
            <Col md={4} xl={3}>
                <Card>
                    <Card.Body>
                        <h6 className="mb-4">Sản phẩm (cận hạn/hết hạn)</h6>
                        <h3 className="f-w-300 d-flex justify-content-center m-b-0">
                            <span className="float-right text-c-purple">
                                {data?.qtyPeriorExpireItem}/{data?.qtyExpireItem}
                            </span>
                        </h3>
                    </Card.Body>
                </Card>
            </Col>
            <Col md={4} xl={3}>
                <Card>
                    <Card.Body>
                        <h6 className="mb-4">Phiếu khám bệnh (ngày/tuần)</h6>
                        <h3 className="f-w-300 d-flex justify-content-center m-b-0">
                            <span className="float-right text-c-purple">
                                {data?.qtyPendingScheduledDay}/{data?.qtyPendingScheduledWeek}
                            </span>
                        </h3>
                    </Card.Body>
                </Card>
            </Col>
            <Col md={4} xl={3}>
                <Card>
                    <Card.Body>
                        <h6 className="mb-4">Phiếu giao dịch đang chờ duyệt</h6>
                        <h3 className="f-w-300 d-flex justify-content-center m-b-0">
                            <span className="float-right text-c-purple">{data?.qtyPendingTransaction}</span>
                        </h3>
                    </Card.Body>
                </Card>
            </Col>
            <Col md={4} xl={3}>
                <Card>
                    <Card.Body>
                        <h6 className="mb-4">Phiếu bán hàng đã thanh toán</h6>
                        <h3 className="f-w-300 d-flex justify-content-center m-b-0">
                            <span className="float-right text-c-purple">{data?.qtyPaidTransactionDay}</span>
                        </h3>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
};
const Revenue = ({ dataHeader, dataRevenue, prefetchQuery }) => {
    return (
        <>
            <Col xl={9} md={9} xs={8}>
                <Card>
                    <Card.Header>
                        <Row>
                            <Col className="align-self-center" sm={4}>
                                <Card.Title as="h5">DOANH THU BÁN HÀNG</Card.Title>
                            </Col>
                            <Col
                                sm={4}
                                className="self-center"
                                style={{
                                    minWidth: "220px",
                                    marginBottom: "10px",
                                }}
                            >
                                <label className="label-control items-center">Từ ngày</label>
                                <input
                                    required
                                    className="form-control"
                                    type="date"
                                    disabled={dataRevenue.type == "month"}
                                    defaultValue={dataRevenue.startDate}
                                    style={{
                                        paddingLeft: "10px",
                                        flex: 12,
                                        height: "38px",
                                    }}
                                    onBlur={(e) =>
                                        prefetchQuery({
                                            ...dataRevenue,
                                            startDate: e.target.value,
                                        })
                                    }
                                />
                            </Col>
                            <Col
                                sm={4}
                                className="self-center"
                                style={{
                                    minWidth: "220px",
                                    marginBottom: "10px",
                                }}
                            >
                                <label className="label-control items-center">Đến ngày</label>
                                <input
                                    required
                                    className="form-control"
                                    type="date"
                                    disabled={dataRevenue.type == "month"}
                                    defaultValue={dataRevenue.endDate}
                                    style={{
                                        paddingLeft: "10px",
                                        flex: 12,
                                        height: "38px",
                                    }}
                                    onBlur={(e) =>
                                        prefetchQuery({
                                            ...dataRevenue,
                                            endDate: e.target.value,
                                        })
                                    }
                                />
                            </Col>
                        </Row>
                    </Card.Header>
                    <Card.Body>
                        <ButtonToolbar>
                            <ToggleButtonGroup variant="radio" name="options" value={dataRevenue?.type}>
                                {chartOption.map((o) => (
                                    <ToggleButton
                                        variant="outline-primary"
                                        value={o.value}
                                        size="sm"
                                        key={o.value}
                                        onClick={() =>
                                            prefetchQuery({
                                                ...dataRevenue,
                                                startDate:
                                                    o.value === "day"
                                                        ? moment().startOf("month").format("YYYY-MM-DD")
                                                        : dataRevenue.startDate,
                                                endDate:
                                                    o.value === "day"
                                                        ? moment().format("YYYY-MM-DD")
                                                        : dataRevenue.endDate,
                                                type: o.value,
                                            })
                                        }
                                    >
                                        {o.lable}
                                    </ToggleButton>
                                ))}
                            </ToggleButtonGroup>
                        </ButtonToolbar>
                        {dataRevenue.type === "day" ? (
                            <AreaChart
                                data={dataRevenue?.data}
                                name={dataRevenue?.type}
                                startDate={dataRevenue?.startDate}
                                endDate={dataRevenue?.endDate}
                            />
                        ) : (
                            <XyChart
                                data={dataRevenue?.data}
                                name={dataRevenue?.type}
                                startDate={dataRevenue?.startDate}
                                endDate={dataRevenue?.endDate}
                            />
                        )}
                    </Card.Body>
                </Card>
            </Col>
            <Col xl={3} md={3} xs={4}>
                <Card className="theme-bg2">
                    <Card.Body>
                        <div className="row align-items-center justify-content-center">
                            <div className="col-auto"></div>
                            <div className="col">
                                <h3 className="text-white f-w-300">
                                    {dataHeader?.profictDay?.toLocaleString("it-IT", {
                                        style: "currency",
                                        currency: "VND",
                                    })}
                                </h3>
                                <h5 className="text-white">DOANH THU NGÀY</h5>
                            </div>
                        </div>
                    </Card.Body>
                </Card>
                <Card className="theme-bg visitor">
                    <Card.Body>
                        <div className="row align-items-center justify-content-center">
                            <div className="col-auto"></div>
                            <div className="col">
                                <h3 className="text-white f-w-300">
                                    {dataHeader?.profictMonth?.toLocaleString("it-IT", {
                                        style: "currency",
                                        currency: "VND",
                                    })}
                                </h3>
                                <h5 className="text-white">DOANH THU THÁNG</h5>
                            </div>
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
};
const MonitorWarehouse = ({ dataBestSeller, prefetchQuery }) => {
    return (
        <>
            <Col md={12}>
                <Card>
                    <Card.Header>
                        <Row>
                            <Col className="align-self-center" sm={4}>
                                <Card.Title as="h5">MẶT HÀNG CHIẾN LƯỢC</Card.Title>
                            </Col>
                            <Col
                                sm={4}
                                className="self-center"
                                style={{
                                    minWidth: "220px",
                                    marginBottom: "10px",
                                }}
                            >
                                <label className="label-control items-center">Từ ngày</label>
                                <input
                                    required
                                    className="form-control"
                                    type="date"
                                    defaultValue={dataBestSeller.startDate}
                                    onChange={(e) =>
                                        prefetchQuery({
                                            ...dataBestSeller,
                                            startDate: e.target.value,
                                        })
                                    }
                                    style={{
                                        paddingLeft: "10px",
                                        flex: 12,
                                        height: "38px",
                                    }}
                                />
                            </Col>
                            <Col
                                sm={4}
                                className="self-center"
                                style={{
                                    minWidth: "220px",
                                    marginBottom: "10px",
                                }}
                            >
                                <label className="label-control items-center">Đến ngày</label>
                                <input
                                    required
                                    className="form-control"
                                    type="date"
                                    defaultValue={dataBestSeller.endDate}
                                    onChange={(e) =>
                                        prefetchQuery({
                                            ...dataBestSeller,
                                            endDate: e.target.value,
                                        })
                                    }
                                    style={{
                                        paddingLeft: "10px",
                                        flex: 12,
                                        height: "38px",
                                    }}
                                />
                            </Col>
                        </Row>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col>
                                <PieChart data={dataBestSeller.data} />
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
                <WarehouseTable />
            </Col>
        </>
    );
};

export default function DashEcommerce() {
    const queryClient = useQueryClient();
    const prefetchQuery = async ({ query, apis }) => await queryClient.prefetchQuery(query, () => apis);
    // initial state header
    const [dataHeader, setDataHeader] = useState(null);
    const queryHeader = useQuery(["calculate_header_dashboard"], () => APIS_CALC_HEADER_DASHBOARD(), {
        enabled: !dataHeader,
    });
    // initial state revenue
    const [dataRevenue, setRevenue] = useState({
        type: "day",
        startDate: moment().startOf("month").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        data: null,
    });
    const queryRevenue = useQuery(["chart_profict", dataRevenue], () => APIS_CALC_REVENUE_DASHBOARD(dataRevenue), {
        enabled: !dataRevenue.data,
    });
    // initial state monitor item
    const [dataBestSeller, setBestSeller] = useState({
        startDate: moment().startOf("month").format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        data: null,
    });
    const queryBestSeller = useQuery(
        ["best_seller_dashboard", dataBestSeller],
        () => APIS_CALC_BEST_SELLER_DASHBOARD(dataBestSeller),
        { enabled: !dataBestSeller.data }
    );

    useEffect(() => {
        const { isLoading, data, isSuccess } = queryHeader;
        if (!isLoading && isSuccess) {
            setDataHeader(data.calculate_header_dashboard);
        }
    }, [queryHeader.isLoading, queryHeader.isSuccess]);

    useEffect(() => {
        const { isLoading, data, isSuccess } = queryRevenue;
        if (!isLoading && isSuccess) {
            setRevenue({ ...dataRevenue, data: data.chart_profict });
        }
    }, [queryRevenue.isLoading, queryRevenue.isSuccess]);

    useEffect(() => {
        const { isLoading, data, isSuccess } = queryBestSeller;
        if (!isLoading && isSuccess) {
            setBestSeller({
                ...dataBestSeller,
                data: data.best_seller_dashboard,
            });
        }
    }, [queryBestSeller.isLoading, queryBestSeller.isSuccess]);

    const handlePrefetchRevenue = async (value) => {
        let startDate = value.startDate;
        let endDate = value.endDate;
        switch (value.type) {
            case "month":
                startDate = moment().startOf("year").utc().format();
                endDate = moment().endOf("year").utc().format();
                break;
            case "year":
                startDate = moment(value.startDate).startOf("year").utc().format();
                endDate = moment(value.endDate).endOf("year").utc().format();
                break;
            default:
                break;
        }
        setRevenue({ type: value.type, startDate, endDate });
        await prefetchQuery({
            query: ["chart_profict", { type: value.type, startDate, endDate }],
            apis: APIS_CALC_REVENUE_DASHBOARD({
                type: value.type,
                startDate,
                endDate,
            }),
        });
    };
    const handleBestSeller = async (value) => {
        setBestSeller(value);
        await prefetchQuery({
            query: ["best_seller_dashboard", value],
            apis: APIS_CALC_BEST_SELLER_DASHBOARD(value),
        });
    };
    return (
        <React.Fragment>
            <Row>
                <Header query={queryHeader} data={dataHeader} />
                <Revenue dataHeader={dataHeader} dataRevenue={dataRevenue} prefetchQuery={handlePrefetchRevenue} />
                <MonitorWarehouse dataBestSeller={dataBestSeller} prefetchQuery={handleBestSeller} />
            </Row>
        </React.Fragment>
    );
}

// export default DashEcommerce;
