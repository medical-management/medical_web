/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Manager User
 */
import { lazy } from "react";

export const ROUTER_DASHBOAR_REPORT_ADMIN = "/app/dashboard/report-admin";

const routers = [
  {
    exact: true,
    path: ROUTER_DASHBOAR_REPORT_ADMIN,
    component: lazy(() => import("./index")),
  },
];
export default routers;
