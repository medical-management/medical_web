import React, { useEffect, useState } from "react";
import { Row, Col, Card, Pagination } from "react-bootstrap";
import BTable from "react-bootstrap/Table";
import { useTable, usePagination } from "react-table";
import { useQuery, useQueryClient } from "react-query";

import { APIS_FIND_ALL_DATA_WAREHOUSE } from "./apis";
import moment from "moment";

function Table({ columns, data, prefetchQuery, pagination }) {
    const [paginationTable, setPaginationTable] = useState({
        take: 0,
        skip: 0,
        totalPage: 0,
        canPreviousPage: false,
        canNextPage: false,
        nextPage: false,
        previousPage: false,
    });
    const { getTableProps, getTableBodyProps, headerGroups, prepareRow, page } = useTable(
        {
            columns,
            data,
            manualPagination: true,
        },
        usePagination
    );

    function handleChangeLimit(e) {
        const skip = e.target.value && Number(e.target.value) > 1 ? Number(e.target.value) - 1 : 0;
        prefetchQuery({ ...pagination, skip });
    }

    function handleChangePage(skip) {
        prefetchQuery({ ...pagination, skip });
    }
    useEffect(() => {
        const { take, skip, total } = pagination;
        setPaginationTable({
            take,
            skip,
            totalPage: Math.floor(total / take) > 0 ? Math.floor(total / take) : 1,
            canPreviousPage: skip > 0,
            canNextPage: Math.floor(total / take) > skip + 1,
            nextPage: skip + 1,
            previousPage: skip - 1,
        });
    }, [pagination]);
    return (
        <>
            <Row className="mb-3">
                <Col className="d-flex align-items-center mt-4" md={6}>
                    SL
                    <select
                        className="form-control w-auto mx-2"
                        value={pagination.take}
                        onChange={(e) =>
                            prefetchQuery({
                                ...pagination,
                                skip: 0,
                                take: Number(e.target.value),
                            })
                        }
                    >
                        {[5, 10, 20, 30, 40, 50].map((o) => (
                            <option key={o} value={o}>
                                {o}
                            </option>
                        ))}
                    </select>
                </Col>

                <Col xs={12} style={{ overflow: "scroll", height: "50vh" }}>
                    <BTable striped bordered hover responsive {...getTableProps()}>
                        <thead>
                            {headerGroups.map((headerGroup) => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map((column) => (
                                        <th {...column.getHeaderProps()} style={{ textAlign: "center" }}>
                                            <b style={{ color: "#000" }}>{column.render("Header")}</b>
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        <tbody {...getTableBodyProps()}>
                            {page.map((row) => {
                                prepareRow(row);
                                return (
                                    <tr {...row.getRowProps()}>
                                        {row.cells.map((cell) => (
                                            <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                        ))}
                                    </tr>
                                );
                            })}
                        </tbody>
                    </BTable>
                </Col>
            </Row>

            <Row className="justify-content-between mt-3">
                <Col sm={12} md={6}>
                    <span className="d-flex align-items-center">
                        Trang {paginationTable.skip + 1} of {paginationTable.totalPage} | Đến Trang:
                        <input
                            type="number"
                            className="form-control ml-2"
                            defaultValue={paginationTable.skip + 1}
                            onChange={handleChangeLimit}
                            style={{ width: "100px" }}
                        />
                    </span>
                </Col>
                <Col sm={12} md={6}>
                    <Pagination className="justify-content-end">
                        <Pagination.First
                            onClick={() => handleChangePage(0)}
                            disabled={!paginationTable.canPreviousPage}
                        />
                        <Pagination.Prev
                            onClick={() => handleChangePage(paginationTable.previousPage)}
                            disabled={!paginationTable.canPreviousPage}
                        />
                        <Pagination.Next
                            onClick={() => handleChangePage(paginationTable.nextPage)}
                            disabled={!paginationTable.canNextPage}
                        />
                        <Pagination.Last
                            onClick={() => handleChangePage(paginationTable.totalPage - 1)}
                            disabled={!paginationTable.canNextPage}
                        />
                    </Pagination>
                </Col>
            </Row>
        </>
    );
}

function App() {
    const [pagination, setPagination] = useState({
        take: 5,
        skip: 0,
        total: 0,
    });
    const [data, setData] = useState(null);
    const queryClient = useQueryClient();
    const queryDataWarehouse = useQuery(
        ["find_all_data_warehouse", pagination],
        () => APIS_FIND_ALL_DATA_WAREHOUSE(pagination),
        { enabled: !data }
    );

    const columns = React.useMemo(() => [
        {
            Header: "Thông tin chính",
            columns: [
                {
                    Header: "Tên mặt hàng",
                    accessor: (row) => (
                        <span className="d-inline-block text-truncate" style={{ maxWidth: "150px" }}>
                            {row.name}
                        </span>
                    ),
                },
                {
                    Header: "Tên viết tắt",
                    accessor: (row) => <span className="cursor-pointer cell-p">{row.shortName}</span>,
                },
                {
                    Header: "Kho",
                    accessor: "bin",
                },
                {
                    Header: "Hạn dùng",
                    accessor: (row) => (
                        <span className="cursor-pointer cell-p">{moment(row.endDate).format("DD/MM/YYYY")}</span>
                    ),
                },
            ],
        },
        {
            Header: "Thông tin chi tiết",
            columns: [
                {
                    Header: "Giá bán",
                    accessor: (row) => (
                        <p className="cell-p text-right">
                            {row?.salePrices?.toLocaleString("it-IT", {
                                style: "currency",
                                currency: "VND",
                            })}
                        </p>
                    ),
                },
                {
                    Header: "Giá nhập",
                    accessor: (row) => (
                        <p className="cell-p text-right">
                            {row?.purchasePrices?.toLocaleString("it-IT", {
                                style: "currency",
                                currency: "VND",
                            })}
                        </p>
                    ),
                },
                {
                    Header: "Tồn kho",
                    accessor: (row) => (
                        <p className="text-right" style={row?.available < 0 ? { color: "#ff0000" } : {}}>
                            {row?.available < 0 ? `(${row?.available * -1})` : row?.available}
                        </p>
                    ),
                },
            ],
        },
    ]);

    const prefetchQuery = async (value) => {
        setPagination(value);
        await queryClient.prefetchQuery(["find_all_data_warehouse", value], () => APIS_FIND_ALL_DATA_WAREHOUSE(value));
    };

    useEffect(() => {
        const { isLoading, data, isSuccess, isError, error } = queryDataWarehouse;
        if (!isLoading && isSuccess) {
            setData(data.find_all_data_warehouse.data);
            setPagination({
                take: data.find_all_data_warehouse.take,
                skip: data.find_all_data_warehouse.skip,
                total: data.find_all_data_warehouse.total,
            });
        }
    }, [queryDataWarehouse.isLoading, queryDataWarehouse.isSuccess]);

    return (
        <Card>
            <Card.Header>
                <h5>TỒN KHO</h5>
            </Card.Header>
            <Card.Body>
                <Table
                    columns={columns}
                    data={data ? data : []}
                    prefetchQuery={prefetchQuery}
                    pagination={pagination}
                />
            </Card.Body>
        </Card>
    );
}

export default App;
