import React, { useEffect, useState } from "react";
import { Button, Card, Col, InputGroup, Row } from "react-bootstrap";
import { useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import CustomPagination from "../../../components/Pagination";
import {
  categoryOptionsFilter,
  dateSorting,
  categoryOptions,
} from "../../../constants/options";
import CustomTable from "../../../views/tables/react-table/CustomTable";
// import moment from "moment";
import * as actions from "../actions";
import { APIS_FIND_ALL_ITEMS } from "../apis";
import moment from "moment";
import CustomDrawer from "../../../components/CustomDrawer";

function ItemList() {
  const queryClient = useQueryClient();
  const queryItem = useQuery("items", () => APIS_FIND_ALL_ITEMS(query));
  const params = useParams();
  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);
  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const [query, setQuery] = useState({ take: pageSizes, skip: pages });

  const { items, page, pageSize, totalPage, isNextPage } = useSelector(
    (state) => state["ITEMS"]
  );
  const dispatch = useDispatch();

  const history = useHistory();

  const [itemList, setItemList] = useState([]);

  const [show, setShow] = useState(false);

  // useEffect(() => {
  //   if (
  //     user?.decentralization == null ||
  //     (user?.decentralization?.length && user?.decentralization[0]?.items === 0)
  //   ) {
  //     alert("Bạn không có quyền thao tác");
  //     history.push("/profile");
  //   }
  // }, [user]);

  useEffect(() => {
    if (queryItem.status === "success" && !queryItem.isLoading)
      dispatch(actions.GET_ALL_ITEMS_SUCCESS(queryItem?.data));
    else if (queryItem.status === "error" && !queryItem.isLoading) {
      alert(queryItem?.error?.response?.errors[0]?.message);
      history.push("/profile");
    }
  }, [queryItem.data]);

  useEffect(() => {
    setItemList(items);
  }, [items, params]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setQuery({ take: pageSizes, skip: 0, name: e?.target[1]?.value || "" });
  };

  useEffect(() => {
    setQuery({ ...query, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQuery({ ...query, skip: pages });
  }, [pages]);

  useEffect(() => {
    queryClient.fetchQuery("items", queryItem);
  }, [query]);

  const handleAdvanceSearch = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      skip: 0,
      salePrices: e.target[0].value,
      purchasePrices: e.target[1].value,
    });
    setShow(false);
  };

  const onChangeEndDate = (e) => {
    setQuery({
      ...query,
      skip: 0,
      enddate: e,
      // dob: moment(e.target[4].value).utc().format(),
      // gender: gender,
    });
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "Thông tin sản phẩm",
        columns: [
          {
            Header: "Tên sản phẩm",
            accessor: (row) => {
              return (
                <p
                  className="cursor-pointer cell-p"
                  onClick={() => history.push(`/app/dashboard/items/${row.id}`)}
                >
                  {row?.name}
                </p>
              );
            },
          },
          {
            Header: "Tên viết tắt",
            accessor: (row) => {
              return (
                <p
                  className="cursor-pointer cell-p"
                  onClick={() => history.push(`/app/dashboard/items/${row.id}`)}
                >
                  {row?.shortName}
                </p>
              );
            },
          },
          {
            Header: "Phân loại",
            accessor: (row) => (
              <p className="cell-p">
                {categoryOptions?.find((item) => item?.value === row?.category)
                  ?.label || ""}
              </p>
            ),
          },
        ],
      },
      {
        Header: "Thông tin giá",
        columns: [
          {
            Header: "Giá bán",
            accessor: (row) => (
              <p className="cell-p">
                {row?.salePrices?.toLocaleString("it-IT", {
                  style: "currency",
                  currency: "VND",
                })}
              </p>
            ),
          },
          {
            Header: "Giá nhập",
            accessor: (row) => (
              <p className="cell-p">
                {row?.purchasePrices?.toLocaleString("it-IT", {
                  style: "currency",
                  currency: "VND",
                })}
              </p>
            ),
          },
        ],
      },
      {
        Header: "Thông tin lưu kho",
        columns: [
          {
            Header: "Hạn sử dụng",
            accessor: "endDate",
          },

          {
            Header: "SKU",
            accessor: "sku",
          },
          {
            Header: "UPC",
            accessor: "upc",
          },
          {
            Header: "Số SERI",
            accessor: "seri",
          },

          {
            Header: "Kho",
            accessor: "bin",
          },
        ],
      },

      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            user?.decentralization[0]?.items > 2 && (
              <Button
                className="margin-0"
                onClick={() => history.push(`/app/dashboard/items/${row.id}`)}
              >
                <p className="feather icon-edit cell-p"></p>
              </Button>
            )
          );
        },
      },
    ],
    [user?.decentralization]
  );
  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                gap: 20,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Card.Title
                as="h5"
                style={{ whiteSpace: "nowrap", textAlign: "left" }}
              >
                Danh sách sản phẩm
              </Card.Title>
              <form
                style={{ width: "100%" }}
                onSubmit={(e) => {
                  e.preventDefault();
                  setQuery({
                    ...query,
                    take: pageSizes,
                    skip: 0,
                    name: e?.target[0]?.value || "",
                  });
                }}
              >
                <InputGroup>
                  <input
                    placeholder={`Tìm sản phẩm bằng Tên`}
                    className="form-control"
                    style={{ flex: 1 }}
                  />
                  <InputGroup.Append>
                    <Button
                      type="submit"
                      className="feather icon-search"
                    ></Button>
                  </InputGroup.Append>
                </InputGroup>
              </form>
              {user?.decentralization?.length &&
              user?.decentralization[0]?.items > 1 ? (
                <Button
                  style={{ margin: 0, whiteSpace: "nowrap" }}
                  x
                  onClick={() => history.push("/app/dashboard/items/add")}
                >
                  Thêm sản phẩm
                </Button>
              ) : (
                ""
              )}
            </Card.Header>
            <Card.Body>
              <form onSubmit={handleSubmit}>
                <Row className="mb-3">
                  <Col className="d-flex align-items-center">
                    Hiển thị
                    <select
                      className="form-control w-auto mx-2"
                      value={pageSizes}
                      onChange={(e) => {
                        setPageSizes(Number(e.target.value));
                      }}
                    >
                      {[5, 10, 20, 50, 100].map((pgsize) => (
                        <option key={pgsize} value={pgsize}>
                          {pgsize}
                        </option>
                      ))}
                    </select>
                    <p
                      className="feather icon-filter margin-0 cursor-pointer"
                      style={{
                        color: !show ? "black" : "#04a9f5",
                        fontWeight: "bold",
                        fontSize: 16,
                      }}
                      onClick={() => setShow(!show)}
                    ></p>
                  </Col>
                </Row>
                <Button className="hidden" type="submit"></Button>
              </form>

              <div className="">
                <CustomTable columns={columns} data={itemList || []} />
              </div>
              <CustomPagination
                pageIndex={page}
                totalPage={totalPage}
                gotoPage={(e) => setPages(e)}
                canPreviousPage={page > 0}
                canNextPage={page < totalPage - 1}
              ></CustomPagination>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {show && (
        <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
          <div>
            <p
              style={{
                width: "100%",
                fontSize: 18,
                fontWeight: 700,
                borderBottom: "1px solid #ddd",
                paddingBottom: 10,
              }}
            >
              Lọc sản phẩm
            </p>
            <form onSubmit={handleAdvanceSearch}>
              <div className="flex" style={{ flexDirection: "column" }}>
                <div
                  className="self-center"
                  style={{
                    paddingLeft: "5px",
                    width: "100%",
                    marginBottom: "10px",
                  }}
                >
                  <label className="label-control items-center">Giá nhập</label>
                  <input
                    placeholder={`Nhập giá nhập`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",
                      flex: 12,
                      height: "38px",
                    }}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    paddingLeft: "5px",
                    // minWidth: "220px",
                    width: "100%",
                    marginBottom: "10px",
                  }}
                >
                  <label className="label-control items-center">Giá bán</label>
                  <input
                    placeholder={`Nhập giá bán`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",
                      flex: 12,
                      height: "38px",
                    }}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    paddingBottom: 10,
                    marginLeft: 10,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Phân loại
                  </label>
                  <Select
                    style={{ width: "100%" }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={categoryOptionsFilter}
                    value={
                      categoryOptionsFilter?.find(
                        (item) => item.value === query?.category
                      ) ||
                      categoryOptionsFilter?.find(
                        (item) => item.label === "Tất cả"
                      )
                    }
                    onChange={(e) =>
                      setQuery({
                        ...query,
                        skip: 0,
                        category: e.value,
                      })
                    }
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    paddingBottom: 10,
                    marginLeft: 10,
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Hạn sử dụng
                  </label>
                  <Select
                    style={{ width: "100%" }}
                    className="basic-single"
                    classNamePrefix="select"
                    options={dateSorting}
                    value={
                      dateSorting?.find(
                        (item) => item.value === query?.endDate
                      ) || dateSorting?.find((item) => item.label === "Tất cả")
                    }
                    onChange={(e) =>
                      setQuery({
                        ...query,
                        skip: 0,
                        endDate: e.value,
                      })
                    }
                  />
                </div>
              </div>

              <Button
                type="submit"
                style={{ float: "right", margin: 0, marginRight: "-5px" }}
              >
                Áp dụng
              </Button>
            </form>
          </div>
        </CustomDrawer>
      )}
    </React.Fragment>
  );
}

export default ItemList;
