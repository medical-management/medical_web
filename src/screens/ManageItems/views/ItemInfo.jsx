/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import { useToasts } from "react-toast-notifications";
import EditorCkClassic from "../../../components/CK-Editor/CkClassicEditor";
import ConfirmModal from "../../../components/ConfirmModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import { categoryOptions } from "../../../constants/options";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { APIS_INTEGRATE_ITEM } from "../../Integration/apis";
import * as actions from "../actions";
import { APIS_FIND_ONE_ITEM, APIS_FIND_ONE_ITEM_INTEGRATION, APIS_SAVE_ITEM } from "../apis";
import { name } from "../reducer";
import { FORM_CHANGE_INFO } from "./Lang";
import Parser from "html-react-parser";

export default function ChangeProfile() {
    return (
        <React.Fragment>
            <Breadcrumb />
            <Card style={{ padding: 30 }}>
                <SubmitValidationForm />
            </Card>
        </React.Fragment>
    );
}

let SubmitValidationForm = (props) => {
    const params = useParams();
    const history = useHistory();
    const dispatch = useDispatch();
    const [description, setDescrition] = useState("");
    const [note, setNote] = useState("");
    const { item } = useSelector((state) => state[name]);
    const {
        register,
        handleSubmit,
        control,
        setValue,
        getValues,
        formState: { errors },
    } = useForm();

    const [select, setSelect] = useState("");
    const queryItem = useQuery(["find_one_item", params], () => params?.id !== "add" && APIS_FIND_ONE_ITEM(params));
    const [disableField, setDisableField] = useState(false);

    const queryItemIntegrate = useQuery(
        ["find_one_item_integration", params],
        () =>
            params.id !== "add" &&
            history?.location?.search === "?integration=true" &&
            APIS_FIND_ONE_ITEM_INTEGRATION(params)
    );
    const [payload, setPayload] = useState(null);

    const mutationChangeProfile = useMutation("input", APIS_SAVE_ITEM);
    const mutationAddProfile = useMutation("input", APIS_SAVE_ITEM);
    const mutationIntegration = useMutation("integrate", APIS_INTEGRATE_ITEM);

    const { user } = useSelector((state) => state["AUTHENTICATION"]);

    const [confirm, setConfirm] = useState(false);
    const [confirmSave, setConfirmSave] = useState(false);
    const queryClient = useQueryClient();
    const { addToast } = useToasts();
    useEffect(() => {
        if (user && user?.decentralization?.length && user?.decentralization[0].items < 2) {
            setDisableField(true);
        }
    }, [user]);
    useEffect(() => {
        if (params.id !== "add") {
            if (history?.location?.search !== "?integration=true")
                queryClient.prefetchQuery(["find_one_item", params], () => APIS_FIND_ONE_ITEM(params));
            else
                queryClient.prefetchQuery(["find_one_item_integration", params], () =>
                    APIS_FIND_ONE_ITEM_INTEGRATION(params)
                );
        } else dispatch(actions.GET_INFO_ITEM_SUCCESS(null));
    }, [params.id]);

    useEffect(() => {
        if (!queryItem.isLoading && !queryItem.isError) {
            dispatch(actions.GET_INFO_ITEM_SUCCESS(queryItem?.data?.find_one_item));
            if (queryItem.data.find_one_item) {
                setDescrition(queryItem?.data?.find_one_item?.description || "");
                setNote(queryItem?.data?.find_one_item?.note || "");
                setSelect(queryItem?.data?.find_one_item?.category || "");
            }
        }
    }, [queryItem.data, params]);

    useEffect(() => {
        if (!queryItemIntegrate.isLoading && !queryItemIntegrate.isError) {
            dispatch(actions.GET_INFO_ITEM_SUCCESS(queryItemIntegrate?.data?.find_one_item_integration));
        }
    }, [queryItemIntegrate.isLoading, params]);

    useEffect(() => {
        if (params.id !== "add" && item) {
            setNote(item?.note);
            setDescrition(item?.description);
            // set form value
            Object.entries(item).forEach(([key, value]) => {
                setValue(key, value);
            });
        }
    }, [item]);

    const submit = (values) => {
        setPayload({
            ...values,
            recordType: "item",
            taxPrices: parseFloat((values.taxPrices || "0").toString().replace(/\./g, "")),
            purchasePrices: parseFloat((values.purchasePrices || "0").toString().replace(/\./g, "")),
            salePrices: parseFloat((values.salePrices || "0").toString().replace(/\./g, "")),
            startDate: moment(values.startDate).utc().format(),
            endDate: moment(values.endDate).utc().format(),
            description,
            note,
            close: false,
            category: select || " ",
        });
    };

    const handleSave = () => {
        if (history?.location?.search !== "?integration=true") {
            if (params.id !== "add") {
                mutationChangeProfile.mutate({
                    input: payload,
                });
            } else {
                mutationAddProfile.mutate({
                    input: {
                        ...payload,
                        id: "",
                    },
                });
            }
        } else if (history?.location?.search === "?integration=true") {
            mutationIntegration.mutate({
                data: [payload],
                isverify: false,
                inactive: false,
                fileName: `items-${moment().valueOf()}`,
            });
        }
    };

    useEffect(() => {
        const { isLoading, data, isError } = mutationChangeProfile;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Cập nhật thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });

                dispatch(actions.CHANGE_INFO_ITEM_SUCCESS(data));
                setDescrition(data.save_item.description || "");
                setNote(data.save_item.note || "");
                setConfirmSave(false);
                setConfirm(false);
            }
        }
    }, [mutationChangeProfile.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationAddProfile;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Thêm thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
                history.push("/app/dashboard/items");
            }
        }
    }, [mutationAddProfile.isLoading]);

    useEffect(() => {
        const { isLoading, data, isError } = mutationIntegration;

        if (!isLoading) {
            if (!isError && !!data) {
                addToast("Cập nhật thông tin thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });

                dispatch(actions.CHANGE_INFO_ITEM_SUCCESS(data));
                setDescrition(data.save_item.description || "");
                setNote(data.save_item.note || "");
                setConfirm(false);
            }
        }
    }, [mutationIntegration.isLoading]);

    useEffect(() => {
        if (!confirmSave) setPayload(null);
    }, [confirmSave]);

    useEffect(() => {
        if (payload) setConfirmSave(true);
    }, [payload]);

    const onChangeStatus = () => {
        const payloadChange = {
            ...item,
            close: !item.close,
        };
        mutationChangeProfile.mutate({
            input: {
                ...payloadChange,
            },
        });
    };

    return (
        <form onSubmit={handleSubmit(submit)}>
            <Card.Header className="header-card" style={{ position: "relative", marginLeft: "-30px" }}>
                <p className="feather icon-chevron-left icon" onClick={() => history.push("/app/dashboard/items")} />
                <Card.Title as="h4" className="title">
                    {params?.id === "add" ? "Thêm sản phẩm" : FORM_CHANGE_INFO.title}
                </Card.Title>
                {history?.location?.search !== "?integration=true" && (
                    <div className="label-control custom-switch" style={{ position: "absolute", right: "25px" }}>
                        <input
                            type="checkbox"
                            className="custom-control-input cursor-pointer"
                            id="customSwitch1"
                            checked={!item.close}
                            style={{ cursor: "pointer" }}
                            onClick={() => setConfirm(true)}
                            disabled={disableField}
                        />
                        <label className="custom-control-label" htmlFor="customSwitch1">
                            {item.close === false ? "Đang được bán" : "Ngừng bán"}
                        </label>
                    </div>
                )}
            </Card.Header>
            <SessionTitle>{`Thông tin chi tiết ${item.id ? `- Mã SP: ${item.id}` : "" || ""}`}</SessionTitle>
            {/* {params.id !== "add" && <Field name="id" component={renderField} label={"Mã sản phẩm"} disabled />} */}
            <Row>
                <Col sm={6}>
                    <Controller
                        {...register("name", { required: true })}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                label="Tên sản phẩm"
                                disabled={disableField}
                                {...field}
                                required
                                error={errors?.name ? "Vui lòng nhập tên sản phẩm" : ""}
                            />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("shortName")}
                        control={control}
                        render={({ field }) => <FormInput disabled={disableField} label="Tên hiển thị" {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("sku")}
                        control={control}
                        render={({ field }) => <FormInput disabled={disableField} label="SKU" {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("upc")}
                        control={control}
                        render={({ field }) => <FormInput disabled={disableField} label="UPC" {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("vendor")}
                        control={control}
                        render={({ field }) => (
                            <FormInput disabled={disableField} label={FORM_CHANGE_INFO.vendor} {...field} />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <div
                        className="self-center"
                        style={{
                            paddingTop: 10,
                            width: "100%",
                            zIndex: 100,
                        }}
                    >
                        <label className="label-control items-center">{FORM_CHANGE_INFO.category}</label>
                        {disableField ? (
                            <FormInput
                                label={"Bệnh nhân"}
                                disabled
                                full
                                value={categoryOptions?.find((item) => item.value === select)?.label}
                            />
                        ) : (
                            <Select
                                style={{ width: "100%" }}
                                className="basic-single"
                                classNamePrefix="select"
                                options={categoryOptions}
                                value={categoryOptions?.find((item) => item.value === select)}
                                onChange={(e) => setSelect(e.value)}
                            />
                        )}
                    </div>
                </Col>
                {params?.id !== "add" && (
                    <Col sm={6}>
                        <Controller
                            {...register("updatedat")}
                            control={control}
                            render={({ field }) => <FormInput label={"Ngày chỉnh sửa phiếu"} {...field} disabled />}
                        />
                    </Col>
                )}
                {params?.id !== "add" && (
                    <Col sm={6}>
                        <Controller
                            {...register("updatedby")}
                            control={control}
                            render={({ field }) => <FormInput label={"Người chỉnh sửa phiếu"} {...field} disabled />}
                        />
                    </Col>
                )}

                <Col sm={12} style={{ zIndex: 0 }}>
                    <label className="label-control mt-2" style={{ whiteSpace: "nowrap" }}>
                        Chống chỉ định
                    </label>
                    {disableField ? (
                        <div
                            style={{
                                borderRadius: "4px",
                                border: "1px solid #ced4da",
                                minHeight: "50px",
                            }}
                        >
                            {Parser(item?.note || " ")}
                        </div>
                    ) : (
                        <EditorCkClassic html={item?.note || ""} onChange={(value) => setNote(value)} />
                    )}
                </Col>
                <Col sm={12} style={{ zIndex: 0 }}>
                    <label className="label-control mt-2" style={{ whiteSpace: "nowrap" }}>
                        Mô tả
                    </label>
                    {disableField ? (
                        <div
                            style={{
                                borderRadius: "4px",
                                border: "1px solid #ced4da",
                                minHeight: "50px",
                            }}
                        >
                            {Parser(item?.description || " ")}
                        </div>
                    ) : (
                        <EditorCkClassic html={item?.description || ""} onChange={(value) => setDescrition(value)} />
                    )}
                </Col>
            </Row>
            <SessionTitle mt={30}>Thông tin bán hàng</SessionTitle>
            <Row>
                <Col sm={6}>
                    <Controller
                        {...register("startDate")}
                        control={control}
                        render={({ field }) => <FormInput label={FORM_CHANGE_INFO.startDate} type="date" {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("endDate")}
                        control={control}
                        render={({ field }) => <FormInput label={FORM_CHANGE_INFO.endDate} type="date" {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("seri")}
                        control={control}
                        render={({ field }) => <FormInput label={FORM_CHANGE_INFO.seri} {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("bin")}
                        control={control}
                        render={({ field }) => <FormInput label={FORM_CHANGE_INFO.bin} {...field} />}
                    />
                </Col>
                <Col sm={4}>
                    <Controller
                        {...register("purchasePrices")}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                suffixInput={"VND"}
                                type="autonumber"
                                label={FORM_CHANGE_INFO.purchasePrices}
                                {...field}
                                onBlur={(e) => {
                                    const salesPrices =
                                        (1 + parseFloat(getValues("perProfit") / 100)) * parseFloat(e.target.value);
                                    setValue("salePrices", salesPrices.toFixed(0));
                                }}
                            />
                        )}
                    />
                </Col>
                <Col sm={4}>
                    <Controller
                        {...register("perProfit")}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                suffixInput={"%"}
                                type="autonumber"
                                label={FORM_CHANGE_INFO.perProfit}
                                {...field}
                                onBlur={(e) => {
                                    const salesPrices =
                                        (1 + parseFloat(e.target.value) / 100) *
                                        parseFloat(getValues("purchasePrices"));
                                    setValue("salePrices", salesPrices.toFixed(0));
                                }}
                            />
                        )}
                    />
                </Col>
                <Col sm={4}>
                    <Controller
                        {...register("salePrices")}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                suffixInput={"VND"}
                                type="autonumber"
                                label={FORM_CHANGE_INFO.salePrices}
                                {...field}
                            />
                        )}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("tax")}
                        control={control}
                        render={({ field }) => <FormInput label={FORM_CHANGE_INFO.tax} {...field} />}
                    />
                </Col>
                <Col sm={6}>
                    <Controller
                        {...register("taxPrices")}
                        control={control}
                        render={({ field }) => (
                            <FormInput
                                suffixInput={"%"}
                                type="autonumber"
                                label={FORM_CHANGE_INFO.taxPrices}
                                {...field}
                            />
                        )}
                    />
                </Col>
            </Row>

            {((params?.id === "add" && user?.decentralization[0]?.items > 1) ||
                (params?.id !== "add" && user?.decentralization[0]?.items > 2)) && (
                <Button type="submit" className="mr-0 mt-2 float-right" disabled={mutationChangeProfile.isLoading}>
                    {FORM_CHANGE_INFO.processBtn}
                </Button>
            )}

            <ConfirmModal
                content={`Bạn có muốn ${!item?.close ? "ngừng kinh doanh" : "mở lại kinh doanh"} sản phẩm này?`}
                onChangeVisible={() => setConfirm(false)}
                show={confirm}
                onConfirm={() => onChangeStatus()}
                loading={mutationChangeProfile.isLoading}
            ></ConfirmModal>

            <ConfirmModal
                content={`Bạn có muốn ${params?.id === "add" ? "THÊM MỚI" : "CẬP NHẬT"} sản phẩm này?`}
                onChangeVisible={() => setConfirmSave(false)}
                show={confirmSave}
                onConfirm={() => handleSave()}
                loading={mutationChangeProfile.isLoading}
            ></ConfirmModal>
        </form>
    );
};
