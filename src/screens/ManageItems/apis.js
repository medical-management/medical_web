/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_ITEMS = async (input) =>
    await graphQLClient({
        queryString: ` query {
          find_all_item(req: { 
            take: ${input.take}, 
            skip: ${input.skip}, 
            ${input.name ? `name: "${input.name}"` : ""} 

            ${input.category ? `category: "${input.category}"` : ""} 
            ${input.endDate ? `endDate: "${input.endDate}"` : ""} 

      }) {
      data {
        inactive
        createdat
        updatedat
        recordType
        id
        name
        shortName
        sku
        upc
        vendor
        category
        note
        description
        startDate
        endDate
        seri
        bin
        salePrices
        purchasePrices
        tax
        taxPrices
      },
      take
      skip
      total
      nextPage
    }
  }`,
    });

export const APIS_FIND_FAST_ITEMS = async (input) =>
    await graphQLClient({
        queryString: ` query {
          find_fast_item(req: { 
            take: 10000, 
            skip: 0, 
            ${input && input.name ? `name: "${input.name}"` : ""} 
      }) {
        id
        name
        shortName
        sku
        upc
        vendor
        note
        startDate
        endDate
        seri
        bin
        salePrices
        purchasePrices
        tax
        taxPrices
        available
        
    }
  }`,
    });

export const APIS_FIND_ONE_ITEM = async (input) =>
    await graphQLClient({
        queryString: `query {
      find_one_item(id: ${input.id}) {
        inactive
        createdat
        updatedat
        recordType
        updatedby{
          id
          firstname
          lastname
        }
        id
        name
        shortName
        sku
        upc
        vendor
        category
        note
        description
        startDate
        endDate
        seri
        bin
        salePrices
        purchasePrices
        tax
        taxPrices
        close
        }
    }`,
    });

export const APIS_FIND_ONE_ITEM_INTEGRATION = async (input) =>
    await graphQLClient({
        queryString: `query {
      find_one_item_integration(id: ${input.id}) {
        inactive
        recordType
        id
        name
        shortName
        sku
        upc
        vendor
        category
        note
        description
        startDate
        endDate
        seri
        bin
        salePrices
        purchasePrices
        tax
        taxPrices
        close
        }
    }`,
    });

export const APIS_SAVE_ITEM = async ({ input }) =>
    await graphQLClient({
        queryString: ` mutation {
      save_item(input: { 
            recordType: "${input.recordType}",
            ${input.id === "" ? "" : `id: ${input.id},`}
            name: "${input.name}",
            shortName: "${input.shortName || " "}",
            sku:  "${input.sku || " "}",
            upc: "${input.upc || " "}",
            vendor: "${input.vendor || " "}",
            category:  "${input.category || " "}",
            note:  "${input.note || " "}",
            description:  "${input.description.toString() || " "}",
            startDate:  "${input.startDate}",
            endDate:  "${input.endDate}",
            seri:  "${input.seri || " "}",
            bin:  "${input.bin || " "}",
            purchasePrices:  ${input.purchasePrices || parseFloat(0)},
            salePrices:  ${input.salePrices || parseFloat(0)},
            taxPrices:  ${input.taxPrices || parseFloat(0)},
            close:  ${input.close},
            tax:  "${input.tax || " "}"}) {
              createdat
              updatedat
              recordType
              id
              name
              shortName
              sku
              upc
              vendor
              category
              note
              description
              startDate
              endDate
              seri
              bin
              salePrices
              purchasePrices
              tax
              taxPrices
              close
        }
    }`,
    });
