/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import { gql } from "graphql-request";
import graphQLClient from "../../services/graphql";

export const APIS_CHANGE_INFO = async ({ input }) =>
    await graphQLClient({
        queryString: `
    mutation {
        update_user_info(input: { 
            recordType: "${input.recordType}",
            id:  "${input.id}",
            firstname: "${input.firstname}",
            lastname:  "${input.lastname}",
            email:  "${input.email}",
            phone:  "${input.phone}",
            gender:  "${input.gender}",
            address:  "${input.address}",
            city:  "${input.city}",
            province:  "${input.province}",
            nation:  "${input.nation}",
            cardID:  "${input.cardID}",
            note:  "${input.note}"}) {
            id
        }
    }
`,
    });
export const APIS_FIND_ONE_USER_INFOR = async () =>
    await graphQLClient({
        queryString: ` query {
        find_one_user_info {
            id
            firstname
            lastname
            email
            phone
            gender
            address
            city
            province
            nation
            cardID
            note
            status
            dob
            role
            username
            question
            answer
        }
    }`,
    });
