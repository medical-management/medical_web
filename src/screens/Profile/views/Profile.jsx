/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect, useState } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Field, reduxForm } from "redux-form";
import { useMutation } from "react-query";
import { useToasts } from "react-toast-notifications";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { FORM_CHANGE_INFO } from "./Lang";
import { name } from "../../Authentication/reducer";
import { APIS_CHANGE_INFO } from "../apis";
import { BASE_URL } from "../../../config/constant";
import { connect } from "react-redux";
import moment from "moment";
import { useHistory, useParams } from "react-router-dom";
import SessionTitle from "../../../components/SessionTitle";
import Select from "react-select";
import { questionList } from "../../../constants/questionList";
import { APIS_SAVE_QUESTION } from "../../ManageUser/apis";
import { renderField } from "../../../components/FormInput";
import { useSelector } from "react-redux";
export default function ChangeProfile() {
  return (
    <React.Fragment>
      <Breadcrumb />
      <div>
        <SubmitValidationForm />
      </div>
    </React.Fragment>
  );
}

let SubmitValidationForm = (props) => {
  const params = useParams();

  const { error, handleSubmit, pristine, reset, submitting } = props;
  const { user } = useSelector((state) => state["AUTHENTICATION"]);
  const history = useHistory();
  const mutationChangeProfile = useMutation("input", APIS_CHANGE_INFO);
  const mutationQuestion = useMutation("input", APIS_SAVE_QUESTION);

  const [selectedQuestion, setSelectedQuestion] = useState("");
  const [answer, setAnswer] = useState("");
  const { addToast } = useToasts();

  const submit = (values) => {
    mutationChangeProfile.mutate({
      input: {
        ...values,
        recordType: "user",
        dob: moment(values?.dob).utc().format(),
      },
    });
  };

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        global.location.reload();
      }
    }
  }, [mutationChangeProfile.isLoading]);

  const statusList = [
    {
      label: "Thử việc",
      value: "fresher",
    },
    { label: "Chính thức", value: "official" },
  ];

  useEffect(() => {
    const { isLoading, data, isError } = mutationQuestion;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin câu hỏi bảo mật thành công!", {
          appearance: "success",
          autoDismiss: true,
        });

        // global.location.reload();
      }
    }
  }, [mutationQuestion.isLoading]);

  useEffect(() => {
    setSelectedQuestion(user?.question);
    setAnswer(user?.answer);
  }, [user]);

  const handleSaveQuestion = async () => {
    await mutationQuestion.mutate({
      input: {
        question: selectedQuestion,
        answer: answer,
      },
    });
  };

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card>
        <Card.Header className="header-card">
          <p
            className="feather icon-chevron-left icon"
            onClick={() => history.goBack()}
          />
          <Card.Title as="h4" className="title">
            {FORM_CHANGE_INFO.title}
          </Card.Title>
        </Card.Header>
        <Card.Body>
          <SessionTitle title="Thông tin hành chính">
            Thông tin hành chính
          </SessionTitle>
          <Row>
            <Col sm={6}>
              <Field
                name="lastname"
                component={renderField}
                required={true}
                label={FORM_CHANGE_INFO.lastName}
              />
            </Col>
            <Col sm={6}>
              <Field
                name="firstname"
                component={renderField}
                required={true}
                label={FORM_CHANGE_INFO.firstName}
              />
            </Col>
            <Col sm={6} className="self-center">
              <label className="label-control items-center">Trạng thái</label>
              <Select
                className="basic-single"
                placeholder="Chọn trạng thái"
                classNamePrefix="select"
                options={statusList}
                value={statusList.find((o) => o.value === user?.status)}
                isDisabled
              />
            </Col>
            <Col sm={6}>
              <Field
                name="cardId"
                component={renderField}
                required={false}
                label={"Số CMND/CCCD"}
              />
            </Col>
            {/* <Col sm={6}>
              <Field
                name="role"
                component={renderField}
                required={false}
                label={"Chức vụ"}
                value="Nhân viên"
              />
            </Col> */}
          </Row>
          <SessionTitle title="Thông tin liên lạc">
            Thông tin liên lạc
          </SessionTitle>
          <Row>
            <Col sm={6}>
              <Field
                name="email"
                component={renderField}
                required={false}
                label={FORM_CHANGE_INFO.email}
              />
            </Col>
            <Col sm={6}>
              <Field
                name="phone"
                component={renderField}
                required={false}
                prefix={"(+84)"}
                label={"Số điện thoại"}
              />
            </Col>
            <Col sm={6}>
              <Field
                name="dob"
                component={renderField}
                required={true}
                label={FORM_CHANGE_INFO.dob}
                type="date"
              />
            </Col>
            <Col sm={6}>
              <div>
                <label
                  style={{
                    alignSelf: "center",
                    marginBottom: 0,
                    marginTop: "9px",
                  }}
                >
                  {FORM_CHANGE_INFO.gender}{" "}
                  <span style={{ color: "red" }}>*</span>
                </label>
                <div>
                  <label>
                    <Field
                      name="gender"
                      component={renderField}
                      type="radio"
                      value="male"
                    />
                    Nam
                  </label>
                  <label style={{ marginLeft: "50px" }}>
                    <Field
                      name="gender"
                      component={renderField}
                      type="radio"
                      value="female"
                    />
                    Nữ
                  </label>
                </div>
                <Field
                  name="genderError"
                  component={renderField}
                  show="hidden"
                ></Field>
              </div>
            </Col>

            <Col sm={6}>
              <Field
                name="province"
                component={renderField}
                required={false}
                label={FORM_CHANGE_INFO.province}
              />
            </Col>
            <Col sm={6}>
              <Field
                name="city"
                component={renderField}
                required={false}
                label={FORM_CHANGE_INFO.city}
              />
            </Col>
            <Col sm={12}>
              <Field
                name="address"
                component={renderField}
                required={false}
                label={FORM_CHANGE_INFO.address}
              />
            </Col>
          </Row>
          <div>
            <SessionTitle title={"Thông tin đăng nhập"}>
              Thông tin đăng nhập
            </SessionTitle>
            <Field
              name="username"
              component={renderField}
              required={true}
              label={"Tên đăng nhập"}
            />
            <SessionTitle title={"Thông tin đăng nhập"}>
              Thông tin xác thực
            </SessionTitle>
            {params?.id !== "add" && (
              <div>
                <Row>
                  <Col sm={12} className="self-center">
                    <label className="label-control items-center">
                      Câu hỏi bảo mật
                    </label>
                  </Col>
                  <Col sm={12}>
                    <Select
                      className="basic-single"
                      // classNamePrefix="select"
                      placeholder="Chọn câu hỏi bảo mật"
                      options={questionList}
                      value={
                        questionList.find(
                          (o) => o.value === selectedQuestion
                        ) || ""
                      }
                      onChange={(e) => setSelectedQuestion(e.value)}
                    />
                  </Col>
                </Row>
                <div style={{ marginTop: "20px" }}>
                  <Row>
                    <Col sm={12} className="self-center">
                      <label className="label-control items-center">
                        Câu trả lời
                      </label>
                    </Col>
                    <Col sm={10}>
                      <input
                        placeholder={`Nhập câu trả lời`}
                        className="form-control"
                        style={{ paddingLeft: "10px" }}
                        value={answer}
                        onChange={(e) => setAnswer(e.target.value || "")}
                        disabled={selectedQuestion === ""}
                      />
                    </Col>
                    <Col
                      sm={2}
                      style={{
                        textAlign: "right",
                      }}
                    >
                      <Button
                        style={{
                          backgroundColor: "transparent",
                          color: "#09a9f5",
                          marginRight: 0,
                        }}
                        onClick={() => handleSaveQuestion()}
                      >
                        Lưu
                      </Button>
                    </Col>
                  </Row>
                </div>
              </div>
            )}
          </div>
          {error && <strong>{error}</strong>}
        </Card.Body>
        <Card.Footer>
          <Button
            type="submit"
            className="mr-1 float-right"
            disabled={submitting}
          >
            {FORM_CHANGE_INFO.processBtn}
          </Button>
        </Card.Footer>
      </Card>
    </form>
  );
};

const validate = (values) => {
  const errors = {};
  if (!values.oldPassword)
    errors.oldPassword = `${FORM_CHANGE_INFO.oldPassword} không được trống`;
  if (!values.newPassword)
    errors.newPassword = `${FORM_CHANGE_INFO.newPassword} không được trống`;
  return errors;
};

SubmitValidationForm = reduxForm({
  form: name,
  validate,
  // asyncChangeFields: ["username",'firstname'],
})(SubmitValidationForm);

SubmitValidationForm = connect((state) => {
  return {
    initialValues: state[name].user,
  };
})(SubmitValidationForm);
