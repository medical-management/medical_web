/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */

export const FORM_CHANGE_INFO = {
    title: "Thông tin cá nhân",
    email: "Email",
    firstName: "Tên",
    lastName: "Họ và tên lót",
    phone: "Số điện thoại",
    gender: "Giới tính",
    address: "Địa chỉ",
    city: "Thành phố",
    province: "Tỉnh",
    nation: "Quốc tịch",
    dob: 'Ngày sinh',
    processBtn: 'Lưu',
    cancelBtn: 'Hủy'
};
