/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
export const FORM_SIGN_UP = {
    title: "Mẫu đăng ký",
    username: "Tài Khoản",
    password: "Mật Khẩu",
    signupBtn: "Đăng Ký",
    clearBtn: "Xóa",
};
