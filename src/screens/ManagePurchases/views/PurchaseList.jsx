import React, { useEffect, useState } from "react";
import { Button, Card, Col, InputGroup, Row } from "react-bootstrap";
import { useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import CustomPagination from "../../../components/Pagination";
import { statusOrderOptions } from "../../../constants/options";
import CustomTable from "../../../views/tables/react-table/CustomTable";
// import moment from "moment";
import * as actions from "../actions";
import { APIS_FIND_ALL_PURCHASE, APIS_SAVE_PURCHASE_ORDER } from "../apis";
import moment from "moment";
import { useMutation } from "react-query";
import { useToasts } from "react-toast-notifications";
import { ROUTER_MANAGE_PURCHASE } from "../router";
import CustomDrawer from "../../../components/CustomDrawer";

function PurchaseList() {
    const queryClient = useQueryClient();
    const params = useParams();

    const { user } = useSelector((state) => state["AUTHENTICATION"]);
    const { orders, page, totalPage } = useSelector((state) => state["PURCHASES"]);

    const queryOrder = useQuery("items", () => APIS_FIND_ALL_PURCHASE(query));
    const mutationSavePurchaseOrder = useMutation("save_purchase_order", APIS_SAVE_PURCHASE_ORDER);
    const dispatch = useDispatch();
    const history = useHistory();
    const { addToast } = useToasts();

    const [pageSizes, setPageSizes] = useState(10);
    const [pages, setPages] = useState(0);
    const [query, setQuery] = useState({ take: pageSizes, skip: pages });
    const [purchaseList, setPurchaseList] = useState([]);
    const [show, setShow] = useState(false);

    useEffect(() => {
        if (queryOrder.data && !queryOrder.isLoading)
            dispatch(actions.GET_ALL_ORDERS_SUCCESS(queryOrder?.data?.find_all_purchase_orders));
        else if (queryOrder.status === "error" && !queryOrder.isLoading) {
            alert(queryOrder?.error?.response?.errors[0]?.message);
            history.push("/profile");
        }
    }, [queryOrder.data]);

    useEffect(() => {
        setPurchaseList(orders);
    }, [orders, params]);

    useEffect(() => {
        setQuery({ ...query, take: pageSizes, skip: 0 });
    }, [pageSizes]);

    useEffect(() => {
        setQuery({ ...query, skip: pages });
    }, [pages]);

    useEffect(() => {
        queryClient.fetchQuery("items", queryOrder);
    }, [query]);

    const handleAdvanceSearch = (e) => {
        e.preventDefault();
        setQuery({
            ...query,
            skip: 0,
            salePrices: e.target[0].value,
            purchasePrices: e.target[1].value,
        });
    };

    useEffect(() => {
        const { isLoading, data, isError } = mutationSavePurchaseOrder;

        if (!isLoading) {
            if (!isError && !!data) {
                setShow(false);
                addToast("Thêm thông tin phiếu mua hàng thành công!", {
                    appearance: "success",
                    autoDismiss: true,
                });
                history.push(`${ROUTER_MANAGE_PURCHASE}/${data.save_purchase_order.id}`);
            } else if (isError)
                addToast("Thêm thông tin phiếu mua hàng thất bại!", {
                    appearance: "error",
                    autoDismiss: true,
                });
        }
    }, [mutationSavePurchaseOrder.isLoading]);

    const handleSubmit = (e) => {
        e.preventDefault();
        setQuery({ take: pageSizes, skip: 0, name: e?.target[1]?.value || "" });
    };

    const columns = React.useMemo(
        () => [
            {
                Header: "Tên giao dịch",
                accessor: (row) => (
                    <p
                        className="cursor-pointer cell-p"
                        style={{ fontWeight: "bold" }}
                        onClick={() => history.push(`/app/dashboard/purchases/${row?.id}`)}
                    >
                        {row?.name}
                    </p>
                ),
            },
            {
                Header: "Nhà cung cấp",
                accessor: (row) => <p className="cell-p">{row.vendorName}</p>,
            },
            {
                Header: "Trạng thái",
                accessor: (row) => (
                    <p className="cell-p">{statusOrderOptions.find((item) => item?.value === row?.status)?.label}</p>
                ),
            },
            {
                Header: "Ngày giao dịch",
                accessor: (row) => <p className="cell-p">{moment(row?.date).format("DD/MM/YYYY")}</p>,
            },

            {
                Header: "Tổng tiền",
                accessor: (row) => (
                    <p className="cell-p text-right">
                        {parseFloat(row?.totalAmount.toFixed(0))?.toLocaleString("it-IT", {
                            style: "currency",
                            currency: "VND",
                        })}
                    </p>
                ),
            },
            {
                Header: "Thao tác",
                accessor: (row) =>
                    user?.decentralization[0]?.purchaseOrders > 2 && (
                        <Button className="margin-0" onClick={() => history.push(`/app/dashboard/purchases/${row.id}`)}>
                            <p className="feather icon-edit cell-p"></p>
                        </Button>
                    ),
            },
        ],
        [purchaseList]
    );

    return (
        <React.Fragment>
            <Row>
                <Col>
                    <Card>
                        <Card.Header
                            style={{
                                alignItems: "center",
                                gap: 20,
                                display: "flex",
                                justifyContent: "space-between",
                            }}
                        >
                            <Card.Title as="h5" style={{ whiteSpace: "nowrap", textAlign: "left" }}>
                                Danh sách Phiếu bán hàng
                            </Card.Title>
                            <form
                                style={{ width: "100%" }}
                                onSubmit={(e) => {
                                    e.preventDefault();
                                    setQuery({
                                        ...query,
                                        take: pageSizes,
                                        skip: 0,
                                        name: e?.target[0]?.value || "",
                                    });
                                }}
                            >
                                <InputGroup>
                                    <input
                                        placeholder={`Tìm phiếu bằng Tên`}
                                        className="form-control"
                                        style={{ flex: 1 }}
                                    />
                                    <InputGroup.Append>
                                        <Button type="submit" className="feather icon-search"></Button>
                                    </InputGroup.Append>
                                </InputGroup>
                            </form>
                            {user?.decentralization?.length && user?.decentralization[0]?.items > 1 ? (
                                <Button
                                    style={{ margin: 0, whiteSpace: "nowrap" }}
                                    onClick={() => history.push("/app/dashboard/purchases/add")}
                                >
                                    Thêm phiếu
                                </Button>
                            ) : (
                                ""
                            )}
                        </Card.Header>
                        <Card.Body>
                            <form onSubmit={handleSubmit}>
                                <Row className="mb-3">
                                    <Col className="d-flex align-items-center">
                                        Hiển thị
                                        <select
                                            className="form-control w-auto mx-2"
                                            value={pageSizes}
                                            onChange={(e) => {
                                                setPageSizes(Number(e.target.value));
                                            }}
                                        >
                                            {[5, 10, 20, 50, 100].map((pgsize) => (
                                                <option key={pgsize} value={pgsize}>
                                                    {pgsize}
                                                </option>
                                            ))}
                                        </select>
                                        <p
                                            className="feather icon-filter margin-0 cursor-pointer"
                                            style={{
                                                color: !show ? "black" : "#04a9f5",
                                                fontWeight: "bold",
                                                fontSize: 16,
                                            }}
                                            onClick={() => setShow(!show)}
                                        ></p>
                                    </Col>
                                </Row>
                                <Button className="hidden" type="submit"></Button>
                            </form>

                            <div className="">
                                <CustomTable columns={columns} data={purchaseList || []} />
                            </div>
                            <CustomPagination
                                pageIndex={page}
                                totalPage={totalPage}
                                gotoPage={(e) => setPages(e)}
                                canPreviousPage={page > 0}
                                canNextPage={page < totalPage - 1}
                            ></CustomPagination>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            {show && (
                <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
                    <div>
                        <p
                            style={{
                                width: "100%",
                                fontSize: 18,
                                fontWeight: 700,
                                borderBottom: "1px solid #ddd",
                                paddingBottom: 10,
                            }}
                        >
                            Lọc phiếu mua hàng
                        </p>
                        <form onSubmit={handleAdvanceSearch}>
                            <div className="flex" style={{ flexDirection: "column" }}>
                                <div
                                    className="self-center"
                                    style={{
                                        paddingBottom: 10,
                                        marginLeft: 10,
                                        width: "100%",
                                    }}
                                >
                                    <label className="label-control items-center">Trạng thái</label>
                                    <Select
                                        style={{ width: "100%" }}
                                        className="basic-single"
                                        classNamePrefix="select"
                                        options={statusOrderOptions}
                                        value={
                                            statusOrderOptions?.find((item) => item.value === query?.status) ||
                                            statusOrderOptions?.find((item) => item.label === "Tất cả")
                                        }
                                        onChange={(e) =>
                                            setQuery({
                                                ...query,
                                                skip: 0,
                                                status: e.value,
                                            })
                                        }
                                    />
                                </div>
                            </div>

                            <Button type="submit" style={{ float: "right", margin: 0, marginRight: "-5px" }}>
                                Áp dụng
                            </Button>
                        </form>
                    </div>
                </CustomDrawer>
            )}
        </React.Fragment>
    );
}

export default PurchaseList;
