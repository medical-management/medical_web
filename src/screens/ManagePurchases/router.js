/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Manager User
 */
import { lazy } from "react";

export const ROUTER_MANAGE_PURCHASE = "/app/dashboard/purchases";

const routers = [
  {
    exact: true,
    path: ROUTER_MANAGE_PURCHASE,
    component: lazy(() => import("./views/PurchaseList")),
  },
  {
    path: `${ROUTER_MANAGE_PURCHASE}/:id`,
    component: lazy(() => import("./views/PurchaseInfo")),
  },
];
export default routers;
