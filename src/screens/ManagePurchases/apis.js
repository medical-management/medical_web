/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import moment from "moment";
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_PURCHASE = async (input) =>
    await graphQLClient({
        queryString: ` query {
      find_all_purchase_orders(req: { 
            take: ${input.take}, 
            skip: ${input.skip}, 
            ${input.status ? `status: "${input.status}"` : ""} 
            ${input.name ? `name: "${input.name}"` : ""} 
            ${input.date ? `date: "${input.date}"` : ""} 
            ${input.patientId ? `patientId: "${input.patientId}"` : ""} 
            ${input.itemId ? `itemId: "${input.itemId}"` : ""} 
            ${input.expenseName ? `expenseName: ${input.expenseName}` : ""} 
      }) {
      data {
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        patient{
          id
          firstname
          lastname
          dob
        }
        items{
          item {
            id
            name
          }
          id
          unit
          location
          note
          quantity
          isGrossProfit
          perGrossProfit
          perTax
          basePrice
          price
          taxAmount
          amount
        }
        expenses{
          inactive
          createdat
          updatedat
          transaction {
            id
          }
          id
          name
          note
          perTax
          price
          taxAmount
          amount
        }
        recordType
        id
        type
        status
        name
        vendor
        location
        note
        paymentMenthod
        paymentNote
        paymentDate
        vendorName
        vendorPhone
        vendorAddress
        date
        totalTax
        totalNet
        totalGross
        totalAmount
      },
      take
      skip
      total
      nextPage
    }
  }`,
    });

export const APIS_FIND_ONE_ORDER = async (input) => {
    let resOderDetail = await graphQLClient({
        queryString: `query {
      find_one_purchase_order(id: ${input.id}) {
        createdat
        updatedat
        updatedby {
          id
          firstname
          lastname
        }
        patient{
          id
          firstname
          lastname
          dob
        }
        items{
          item {
            id
            name
            shortName
            taxPrices
            salePrices
          }
          id
          unit
          inactive
          location
          note
          quantity
          isGrossProfit
          perGrossProfit
          perTax
          basePrice
          price
          taxAmount
          amount
        }
        expenses{
          inactive
          createdat
          updatedat
          transaction {
            id
          }
          id
          name
          note
          perTax
          price
          taxAmount
          amount
        }
        recordType
        id
        status
        name
        vendor
        location
        note
        paymentMenthod
        paymentNote
        paymentDate
        vendorName
        vendorPhone
        vendorAddress
        date
        totalTax
        totalNet
        totalGross
        totalAmount
        }
    }`,
    });
    if (resOderDetail && resOderDetail.find_one_purchase_order.items.length > 0) {
        const itemsOrder = [];
        for (const item of resOderDetail.find_one_purchase_order.items) {
            const { find_fast_item } = await graphQLClient({
                queryString: ` query {
                        find_fast_item(req: { 
                          take: 1, 
                          skip: 0, 
                          ${`id: "${item.item.id}"`} 
                    }) {
                      id
                      name
                      shortName
                      salePrices
                      purchasePrices
                      taxPrices
                      available
                  }
                }`,
            });
            item.item["shortName"] = find_fast_item.length > 0 ? find_fast_item[0].shortName : "";
            itemsOrder.push({
                ...item,

                salePrices: find_fast_item.length > 0 ? find_fast_item[0].salePrices : 0,
                taxPrices: find_fast_item.length > 0 ? find_fast_item[0].taxPrices : 0,
                available: find_fast_item.length > 0 ? find_fast_item[0].available : 0,
            });
        }

        resOderDetail.find_one_purchase_order.items = await itemsOrder;
    }
    return await resOderDetail;
};

export const APIS_SAVE_PURCHASE_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (key === "id" || key === "patient" || key === "medical" || key === "inactive")
            input.push(`${key}: ${data[key]}`);
        else if (
            key === "type" ||
            key === "status" ||
            key === "note" ||
            key === "paymentMenthod" ||
            key === "paymentNote" ||
            key === "vendorName" ||
            key === "vendorPhone" ||
            key === "vendorAddress"
        )
            input.push(`${key}: "${data[key]}"`);
        else if (key === "date" || key === "paymentDate") input.push(`${key}: "${moment(data[key]).utc().format()}"`);
    }
    return await graphQLClient({
        queryString: ` mutation {
      save_purchase_order(input: { 
            recordType: "purchaseOrders",
            type: "purchaseOrder"
           ${input.join()}
          }) {
            createdat
            updatedat
            updatedby {
              id
              firstname
              lastname
            }
            patient{
              id
              firstname
              lastname
              dob
            }
            items{
              item {
                id
                name
                taxPrices
                salePrices
              }
              id
              unit
              location
              note
              quantity
              isGrossProfit
              perGrossProfit
              perTax
              basePrice
              price
              taxAmount
              amount
            }
            expenses{
              inactive
              createdat
              updatedat
              transaction {
                id
              }
              id
              name
              note
              perTax
              price
              taxAmount
              amount
            }
            recordType
            id
            type
            status
            name
            vendor
            location
            note
            paymentMenthod
            paymentNote
            paymentDate
            vendorName
            vendorPhone
            vendorAddress
            date
            totalTax
            totalNet
            totalGross
            totalAmount
        }
    }`,
    });
};

export const APIS_SAVE_ITEM_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (
            key === "id" ||
            key === "itemId" ||
            key === "transactionId" ||
            key === "isGrossProfit" ||
            key === "perGrossProfit" ||
            key === "quantity" ||
            key === "perTax" ||
            key === "basePrice" ||
            key === "price" ||
            key === "amount" ||
            key === "inactive"
        )
            input.push(`${key}: ${data[key]}`);
        else if (key === "unit" || key === "location" || key === "note") input.push(`${key}: "${data[key]}"`);
        else if (key === "item") input.push(`itemId: ${data[key].id}`);
        else if (key === "taxPrices") input.push(`perTax: ${data[key]}`);
    }
    return await graphQLClient({
        queryString: ` mutation {
          save_item_transaction(input: {${input.join()}}) {
                  id
            }
        }`,
    });
};

export const APIS_SAVE_SERVICES_ORDER = async (data) => {
    const input = [];
    for (const key in data) {
        if (!data[key]) continue;
        if (
            key === "id" ||
            key === "transactionId" ||
            key === "quantity" ||
            key === "perTax" ||
            key === "price" ||
            key === "amount" ||
            key === "inactive"
        )
            input.push(`${key}: ${data[key]}`);
        else if (key === "name" || key === "note") input.push(`${key}: "${data[key]}"`);
    }
    return await graphQLClient({
        queryString: ` mutation {
        save_expenses_transaction(input: {${input.join()}}) {
                id
          }
      }`,
    });
};
