/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Manager User
 */
import { lazy } from "react";

export const ROUTER_MANAGE_USER = "/app/admin/users";

const routers = [
    {
        exact: true,
        path: ROUTER_MANAGE_USER,
        component: lazy(() => import("./views/UserList")),
    },
    {
        path: `${ROUTER_MANAGE_USER}/:id`,
        component: lazy(() => import("./views/UserProfile")),
    }
];
export default routers;
