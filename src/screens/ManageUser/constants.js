export const roleOption = {
    admin: "Quản trị viên",
    employee: "Nhân viên",
};

export const genderOption = {
    male: "Nam",
    female: "Nữ",
};
