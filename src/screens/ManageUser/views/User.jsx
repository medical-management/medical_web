/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Field, reduxForm } from "redux-form";
import { useMutation } from "react-query";

import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import back4 from "../../../assets/images/bg-images/bg4.jpg";
import { FORM_CHANGE_INFO } from "./Lang";
import { name } from "../reducer";
import { APIS_CHANGE_INFO } from "../apis";
import { useHistory } from "react-router-dom";
import { BASE_URL } from "../../../config/constant";
import { useToasts } from "react-toast-notifications";

export default function ChangeProfile() {
  return (
    <React.Fragment>
      <Breadcrumb />
      <div>
        <ChangeProfileForm />
      </div>
    </React.Fragment>
  );
}

const SubmitValidationForm = (props) => {
  const { error, handleSubmit, pristine, reset, submitting } = props;

  const history = useHistory();
  const { addToast } = useToasts();

  const mutationChangeProfile = useMutation("input", APIS_CHANGE_INFO);

  const submit = (values) => {
    mutationChangeProfile.mutate({
      input: values,
      output: ["id", "accesstoken", "refreshtoken"],
    });
  };

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Đổi mật khẩu thành công!", {
          appearance: "success",
          autoDismiss: true,
        });

        history.push(BASE_URL);
      }
    }
  }, [mutationChangeProfile.isLoading]);

  return (
    <form onSubmit={handleSubmit(submit)}>
      <Card>
        <Card.Header>
          <Card.Title as="h4">{FORM_CHANGE_INFO.title}</Card.Title>
        </Card.Header>
        <Card.Body>
          <Field
            name="firstName"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.firstName}
          />
          <Field
            name="lastName"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.lastName}
          />
          <Row>
            <Col sm={3}>
              <label>{FORM_CHANGE_INFO.gender}</label>
            </Col>
            <Col sm={9} style={{ marginTop: "-35px" }}>
              <div>
                <label>
                  <Field
                    name="gender"
                    component={renderField}
                    type="radio"
                    value="male"
                  />
                  Nam
                </label>
                <label style={{ marginLeft: "40px" }}>
                  <Field
                    name="gender"
                    component={renderField}
                    type="radio"
                    value="female"
                  />
                  Nữ
                </label>
              </div>
            </Col>
          </Row>
          <Field
            name="dob"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.dob}
            type="date"
          />
          <Field
            name="nation"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.nation}
          />
          <Field
            name="address"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.address}
          />
          <Field
            name="city"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.city}
          />
          <Field
            name="province"
            component={renderField}
            required={false}
            label={FORM_CHANGE_INFO.province}
          />
          {error && <strong>{error}</strong>}
        </Card.Body>
        <Card.Footer>
          <Button
            type="submit"
            className="mr-1 float-right"
            disabled={submitting}
          >
            {FORM_CHANGE_INFO.processBtn}
          </Button>
        </Card.Footer>
      </Card>
    </form>
  );
};

const validate = (values) => {
  const errors = {};
  // console.log({ values });
  if (!values.oldPassword)
    errors.oldPassword = `${FORM_CHANGE_INFO.oldPassword} không được trống`;
  if (!values.newPassword)
    errors.newPassword = `${FORM_CHANGE_INFO.newPassword} không được trống`;
  return errors;
};

const renderField = ({
  input,
  label,
  type,
  meta: { asyncValidating, touched, error },
}) => (
  <Row className="my-3">
    <Col sm={3}>
      <label className="label-control">{label}</label>
    </Col>
    <Col sm={9} className={asyncValidating ? "async-validating" : ""}>
      <input
        {...input}
        placeholder={`Vui lòng nhập ${label}`}
        type={type}
        className="form-control"
      />
      {touched && error && <span className="text-c-red">* {error} </span>}
    </Col>
  </Row>
);

const ChangeProfileForm = reduxForm({
  form: name, // a unique identifier for this form
  validate,
  asyncChangeFields: ["username"],
})(SubmitValidationForm);
