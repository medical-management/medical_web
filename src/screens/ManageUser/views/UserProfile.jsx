/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import _ from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import ConfirmModal from "../../../components/ConfirmModal";
import { FormInput } from "../../../components/FormInputCustom";
import SessionTitle from "../../../components/SessionTitle";
import Address from "../../../constants/address.json";
import { permissionsList } from "../../../constants/permission";
import Breadcrumb from "../../../layouts/AdminLayout/Breadcrumb";
import { APIS_INTEGRATE_EMPLOYEE } from "../../Integration/apis";
import * as actions from "../actions";
import {
  APIS_FIND_ONE_USER,
  APIS_FIND_ONE_USER_INTEGRATION,
  APIS_RESET_PASSWORD,
  APIS_SAVE_EMPLOYEE,
} from "../apis";
import { FORM_CHANGE_INFO } from "./Lang";
import { useToasts } from "react-toast-notifications";

export default function ChangeProfile() {
  const history = useHistory();
  const params = useParams();
  return (
    <React.Fragment>
      <Breadcrumb />
      <Card style={{ padding: 30 }}>
        <Card.Header className="header-card" style={{ marginLeft: "-30px" }}>
          <p
            className="feather icon-chevron-left icon"
            onClick={() =>
              history.push(
                history?.location?.search === "?integration=true"
                  ? "/app/integrate/employee"
                  : "/app/admin/users"
              )
            }
          />
          <Card.Title as="h4" className="title">
            {params?.id === "add" ? "Thêm nhân viên" : FORM_CHANGE_INFO.title}
          </Card.Title>
        </Card.Header>
        <SubmitValidationForm />
      </Card>
    </React.Fragment>
  );
}

let SubmitValidationForm = (props) => {
  const params = useParams();

  const history = useHistory();

  const dispatch = useDispatch();

  const { employee, isLoading } = useSelector((state) => state["USERS"]);

  const [gender, setGender] = useState("male");

  const [disableField, setDisableField] = useState(false);

  const {
    register,
    handleSubmit,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  const queryEmployee = useQuery(
    ["user", params],
    () => params.id !== "add" && APIS_FIND_ONE_USER(params)
  );

  const queryEmployeeIntegration = useQuery(
    ["userIntegration", params],
    () =>
      params.id !== "add" &&
      history?.location?.search === "?integration=true" &&
      APIS_FIND_ONE_USER_INTEGRATION(params)
  );

  const queryClient = useQueryClient();
  const mutationChangeProfile = useMutation(
    "change_profile",
    APIS_SAVE_EMPLOYEE
  );
  const mutationDeleteProfile = useMutation("delete", APIS_SAVE_EMPLOYEE);
  const mutationResetPassword = useMutation("reset", APIS_RESET_PASSWORD);

  const mutationAddProfile = useMutation("input", APIS_SAVE_EMPLOYEE);
  const mutationIntegration = useMutation("inte", APIS_INTEGRATE_EMPLOYEE);

  const [status, setStatus] = useState(null);

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const cityList = _.sortedUniq(Address.address.map((item) => item.city)).map(
    (item) => {
      return {
        value: item,
        label: item,
      };
    }
  );
  const statusList = [
    {
      label: "Thử việc",
      value: "fresher",
    },
    { label: "Chính thức", value: "official" },
  ];
  const [selected, setSelected] = useState(null);
  const [isVisible, setIsVisible] = useState(false);
  const [isVisibleReset, setIsVisibleReset] = useState(false);
  const [isVisibleConfirm, setIsVisibleConfirm] = useState(false);

  const [payload, setPayload] = useState(null);

  const [selectedCity, setSelectedCity] = useState("");
  const [selectedProvince, setSelectedProvince] = useState("");
  const [selectedWard, setSelectedWard] = useState("");
  const [note, setNote] = useState("");

  const [listProvince, setListProvince] = useState([]);
  const [listWard, setListWard] = useState([]);
  const { addToast } = useToasts();

  const [permission, setPermission] = useState({
    adjustments: 0,
    employees: 0,
    items: 0,
    medical: 0,
    patients: 0,
    purchaseOrders: 0,
    reports: 0,
    salesOrders: 0,
    schedules: 0,
  });

  useEffect(() => {
    if (params.id !== "add") {
      if (history?.location?.search === "?integration=true")
        queryClient.prefetchQuery(queryEmployeeIntegration);
      else queryClient.prefetchQuery(queryEmployee);
    }

    if (params.id === "add") dispatch(actions.GET_EMPLOYEE_INFO_SUCCESS(null));
  }, [params.id]);

  useEffect(() => {
    if (queryEmployee?.isSuccess && queryEmployee.data?.find_one_employee?.id) {
      dispatch(
        actions.GET_EMPLOYEE_INFO_SUCCESS(queryEmployee.data.find_one_employee)
      );
      setStatus(queryEmployee?.data?.find_one_employee?.status);
      setPermission(
        queryEmployee?.data?.find_one_employee?.decentralization?.length
          ? queryEmployee?.data?.find_one_employee?.decentralization[0]
          : permission
      );
    }
  }, [queryEmployee?.isLoading]);

  useEffect(() => {
    if (
      queryEmployeeIntegration?.isSuccess &&
      !queryEmployeeIntegration.isLoading &&
      history?.location?.search === "?integration=true"
    ) {
      dispatch(
        actions.GET_EMPLOYEE_INFO_SUCCESS(
          queryEmployeeIntegration.data.find_one_employee_integration
        )
      );
      setStatus(
        queryEmployeeIntegration?.data?.find_one_employee_integration?.status
      );
    }
  }, [queryEmployeeIntegration.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError, status } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        setIsVisibleConfirm(false);
        // history.push("/app/admin/users");
      } else if (isError || status === "error") {
        addToast("Cập nhật thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
        setIsVisibleConfirm(false);
      }
    }
  }, [mutationChangeProfile.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationResetPassword;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Reset mật khẩu thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        // history.push("/app/admin/users");
        setIsVisibleReset(false);
      } else if (isError && !!data) {
        setIsVisibleReset(false);
        addToast("Reset mật khẩu thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
      }
    }
  }, [mutationResetPassword.isLoading]);

  useEffect(() => {
    const { isLoading, isError, isSuccess, status, error } = mutationAddProfile;

    if (!isLoading) {
      if (!isError && isSuccess) {
        addToast("Thêm thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push("/app/admin/users");
      } else if (error?.length || status === "error") {
        addToast("Thêm thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
        setIsVisibleConfirm(false);
      }
    }
  }, [mutationAddProfile.isLoading]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationIntegration;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        // history.push("app/integrate/employee");
      } else if (status === "error")
        addToast("Cập nhật thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationIntegration.isLoading]);

  useEffect(() => {
    if (
      params.id !== "add" &&
      employee &&
      employee.decentralization?.length > 0
    ) {
      setSelectedCity(employee?.city);
      setSelectedProvince(employee?.province);
      setNote(employee?.note);
      setStatus(employee?.status);
      setGender(employee?.gender);
      // set form value
      Object.entries(employee).forEach(([key, value]) => {
        setValue(key, value);
      });
    }
  }, [employee, params, history.location.search]);

  useEffect(() => {
    if (
      user &&
      user?.decentralization?.length &&
      user?.decentralization[0].employees < 2
    ) {
      setDisableField(true);
    }
  }, [user]);

  const submit = (values) => {
    if (history?.location?.search !== "?integration=true") {
      if (params.id !== "add") {
        setPayload({
          ...values,
          recordType: "user",
          dob: moment(values.dob).utc().format(),
          province: selectedProvince,
          city: selectedCity,
          ward: selectedWard,
          note: note,
          status: status,
          gender: gender,
          ...permission,
        });
      } else {
        setPayload({
          ...values,
          recordType: "employee",
          id: "",
          dob: moment(values.dob).utc().format(),
          province: selectedProvince,
          password: values.username,
          city: selectedCity,
          ward: selectedWard,
          gender: gender,
          status: status,
          note: note,
          ...permission,
        });
      }
    } else {
      setPayload({
        ...values,
        recordType: "employee",
        dob: moment(values.dob).utc().format(),
        province: selectedProvince,
        city: selectedCity,
        gender: gender,
        ward: selectedWard,
        note: note,
        status: status,
      });
    }
  };
  const handleSave = (values) => {
    // console.log(values);
    if (history?.location?.search !== "?integration=true") {
      if (params.id !== "add") {
        mutationChangeProfile.mutate({
          input: payload,
        });
      } else {
        mutationAddProfile.mutate({
          input: payload,
        });
      }
    } else {
      mutationIntegration.mutate({
        data: [payload],
        isverify: false,
        inactive: false,
        fileName: `employee-${moment().valueOf()}`,
      });
    }
  };

  const handleResetPassword = async () => {
    await mutationResetPassword.mutate({
      input: {
        ...employee,
        password: employee.username,
      },
    });
  };

  const handleChangePermission = (selected, key) => {
    setPermission({ ...permission, [key]: selected });
  };

  const handleChooseCity = (value) => {
    setSelectedCity(value);
    setSelectedWard(null);
    setListWard([]);
    setListProvince([]);
    setSelectedProvince(null);
    if (value !== null) {
      setSelectedCity(value);
      setListProvince(
        _.uniqBy(
          Address.address.map((item) => {
            if (item.city === value) {
              return {
                label: item.province,
                value: item.province,
              };
            }
          }),
          function (e) {
            if (e) return e.label;
          }
        ).filter((n) => n)
      );
    } else setSelectedCity("");
  };

  const handleChooseProvince = (value) => {
    setSelectedWard(null);
    setListWard([]);
    if (value !== null) {
      setSelectedProvince(value);
      setListWard(
        _.uniqBy(
          Address.address.map((item) => {
            if (item.province === value && item.city === selectedCity) {
              return {
                label: item.ward,
                value: item.ward,
              };
            }
          }),
          function (e) {
            if (e) return e.label;
          }
        ).filter((n) => n)
      );
    } else setSelectedProvince("");
  };

  useEffect(() => {
    handleChooseCity(selectedCity);
  }, [selectedCity]);

  useEffect(() => {
    handleChooseProvince(selectedProvince);
  }, [selectedProvince]);

  useEffect(() => {
    setSelectedWard(employee?.ward);
  }, [selectedWard]);

  useEffect(() => {
    if (selected !== null) {
      setIsVisible(true);
    }
  }, [selected]);

  useEffect(() => {
    const { isLoading, data, isError } = mutationDeleteProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        setIsVisible(false);
        addToast("Xóa thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        history.push("/app/admin/users");
      }
    }
  }, [mutationDeleteProfile.isLoading]);

  useEffect(() => {
    if (!isVisibleConfirm) setPayload(null);
  }, [isVisibleConfirm]);

  useEffect(() => {
    if (payload) setIsVisibleConfirm(true);
  }, [payload]);

  const onDelete = () => {
    mutationDeleteProfile.mutate({
      input: {
        ...selected,
        recordType: "employee",
        inactive: true,
        ...permission,
      },
    });
  };

  return (
    //  <form onSubmit={handleSubmit(submit)}>
    <form onSubmit={handleSubmit(submit)}>
      <SessionTitle>Thông tin hành chính</SessionTitle>
      <Row>
        <Col sm={6}>
          <Controller
            {...register("lastname", { required: true })}
            control={control}
            render={({ field }) => (
              <FormInput
                label="Họ và Tên lót"
                {...field}
                disabled={disableField}
                required
                error={errors?.lastname ? "Vui lòng nhập Họ và tên lót" : ""}
              />
            )}
          />
        </Col>
        <Col sm={6}>
          <Controller
            {...register("firstname", { required: true })}
            control={control}
            render={({ field }) => (
              <FormInput
                required
                disabled={disableField}
                label="Tên"
                {...field}
                error={errors?.firstname ? "Vui lòng nhập Tên" : ""}
              />
            )}
          />
        </Col>
        <Col sm={6} className="self-center">
          <label className="label-control items-center">Trạng thái</label>
          {disableField ? (
            <input
              className="form-control"
              value={statusList.find((o) => o.value === status)?.label || null}
              disabled
            />
          ) : (
            <Select
              className="basic-single"
              classNamePrefix="select"
              options={statusList}
              placeholder="Chọn trạng thái"
              value={statusList.find((o) => o.value === status) || null}
              onChange={(e) => setStatus(e.value)}
            />
          )}
        </Col>
        <Col sm={6}>
          <Controller
            {...register("cardID")}
            control={control}
            render={({ field }) => (
              <FormInput
                label="Số CMND/CCCD"
                {...field}
                disabled={disableField}
              />
            )}
          />
        </Col>
      </Row>
      <SessionTitle mt="20">Thông tin liên lạc</SessionTitle>
      <Row>
        <Col sm={6}>
          <Controller
            {...register("email", { required: true })}
            control={control}
            render={({ field }) => (
              <FormInput
                label="Email"
                required
                disabled={disableField}
                {...field}
                error={errors?.email ? "Vui lòng nhập Email" : ""}
              />
            )}
          />
        </Col>
        <Col sm={6}>
          <Controller
            {...register("phone")}
            control={control}
            render={({ field }) => (
              <FormInput
                label="Số điện thoại"
                {...field}
                disabled={disableField}
              />
            )}
          />
        </Col>
        <Col sm={6}>
          <Controller
            {...register("dob", { required: true })}
            control={control}
            render={({ field }) => (
              <FormInput
                required
                type="date"
                error={errors?.dob ? "Vui lòng nhập ngày sinh" : ""}
                disabled={disableField}
                label="Ngày sinh"
                {...field}
              />
            )}
          />
        </Col>
        <Col>
          <div className="mt-10">
            <label htmlhtmlFor="">Giới tính</label>
            <div className="flex" style={{ marginLeft: "-10px" }}>
              <div className="form-check">
                <FormInput
                  disabled={disableField}
                  type="radio"
                  value="male"
                  checked={gender === "male"}
                  // {...field}
                  onChange={(e) =>
                    setGender(e?.target?.checked ? "male" : "female")
                  }
                />

                <label className="form-check-label" htmlFor="flexRadioDefault1">
                  Nam
                </label>
              </div>
              <div className="form-check" style={{ marginLeft: "50px" }}>
                <FormInput
                  disabled={disableField}
                  type="radio"
                  value="female"
                  onChange={(e) =>
                    setGender(!e?.target?.checked ? "male" : "female")
                  }
                  checked={gender === "female"}
                  // {...field}
                />

                <label className="form-check-label" htmlFor="flexRadioDefault2">
                  Nữ
                </label>
              </div>
            </div>
          </div>
        </Col>

        <Col sm={6} className="self-center">
          <label className="label-control items-center">Tỉnh / Thành phố</label>
          {disableField ? (
            <FormInput
              value={cityList.find((o) => o.value === selectedCity)}
              disabled
            />
          ) : (
            <Select
              className="basic-single"
              placeholder="Chọn Tỉnh/Thành phố"
              classNamePrefix="select"
              options={cityList}
              value={cityList.find((o) => o.value === selectedCity)}
              onChange={(e) => handleChooseCity(e.value)}
            />
          )}
        </Col>

        <Col sm={6} className="self-center">
          <label className="label-control items-center">
            Quận / Huyện / Thành phố
          </label>
          {disableField ? (
            <FormInput
              value={
                listProvince.find((o) => o.value === selectedProvince) || null
              }
              disabled
            />
          ) : (
            <Select
              className="basic-single"
              classNamePrefix="select"
              options={listProvince}
              placeholder="Chọn Quận/Huyện/Thành phố"
              value={
                listProvince.find((o) => o.value === selectedProvince) || null
              }
              onChange={(e) => handleChooseProvince(e.value)}
            />
          )}
        </Col>
        <Col sm={12}>
          <Controller
            {...register("address")}
            control={control}
            render={({ field }) => (
              <FormInput disabled={disableField} label="Địa chỉ" {...field} />
            )}
          />
        </Col>
      </Row>

      <SessionTitle mt="20">Thông tin đăng nhập</SessionTitle>
      <Row>
        <Col sm={12}>
          <Controller
            {...register("username", { required: true, minLength: 6 })}
            control={control}
            render={({ field }) => (
              <FormInput
                label="Tên đăng nhập"
                {...field}
                disabled={disableField}
                required
                error={
                  errors?.username?.type === "required"
                    ? "Vui lòng nhập tên đăng nhập"
                    : errors?.username?.type === "minLength"
                    ? "Tên đăng nhập phải có ít nhất 6 ký tự"
                    : ""
                }
              />
            )}
          />
        </Col>
      </Row>
      {user?.decentralization[0].employees >= 3 &&
        history?.location?.search !== "?integration=true" && (
          <div>
            <SessionTitle title="Phân quyền" mt="30"></SessionTitle>
            <Row className="mt-20">
              <Col sm={6} className="self-center">
                <label className="label-control items-center">
                  Quản lý bệnh nhân
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.patients]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.patients]}
                    onChange={(e) =>
                      handleChangePermission(e.value, "patients")
                    }
                  />
                )}
              </Col>

              <Col sm={6} className="self-center">
                <label className="label-control items-center">
                  Quản lý bệnh án
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.medical]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.medical]}
                    onChange={(e) => handleChangePermission(e.value, "medical")}
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý lịch khám bệnh
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.schedules]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.schedules]}
                    onChange={(e) =>
                      handleChangePermission(e.value, "schedules")
                    }
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý các mặt hàng
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.items]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.items]}
                    onChange={(e) => handleChangePermission(e.value, "items")}
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý nhân viên
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.employees]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.employees]}
                    onChange={(e) =>
                      handleChangePermission(e.value, "employees")
                    }
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý phiếu bán hàng
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.salesOrders]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.salesOrders]}
                    onChange={(e) =>
                      handleChangePermission(e.value, "salesOrders")
                    }
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý mua hàng
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.purchaseOrders]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.purchaseOrders]}
                    onChange={(e) =>
                      handleChangePermission(e.value, "purchaseOrders")
                    }
                  />
                )}
              </Col>

              <Col sm={6} className="self-center mt-10">
                <label className="label-control items-center">
                  Quản lý kiểm hàng
                </label>
                {disableField ? (
                  <FormInput
                    value={permissionsList[permission?.reports]}
                    disabled
                  />
                ) : (
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={permissionsList}
                    value={permissionsList[permission?.reports]}
                    onChange={(e) => handleChangePermission(e.value, "reports")}
                  />
                )}
              </Col>
            </Row>
          </div>
        )}
      <div style={{ marginTop: 20 }}>
        {((params?.id === "add" && user?.decentralization[0]?.employees > 1) ||
          (params?.id !== "add" &&
            user?.decentralization[0]?.employees > 2)) && (
          <Button
            type="submit"
            // onClick={() => setIsVisibleConfirm(true)}
            className="mr-0 float-right"
          >
            {FORM_CHANGE_INFO.processBtn}
          </Button>
        )}
        {user?.decentralization[0].employees >= 4 &&
          params?.id !== "add" &&
          history?.location?.search !== "?integration=true" &&
          params.id !== "add" && (
            <Button
              className="mr-2 float-right btn btn-danger"
              disabled={isLoading}
              onClick={() => {
                setSelected(employee);
              }}
            >
              Xóa
            </Button>
          )}
        {user?.decentralization[0].employees >= 3 &&
          params?.id !== "add" &&
          history?.location?.search !== "?integration=true" &&
          params.id !== "add" && (
            <Button
              className="mr-2 float-right"
              onClick={() => setIsVisibleReset(true)}
              style={{
                backgroundColor: "transparent",
                color: "#09a9f5",
                marginRight: 0,
              }}
            >
              Đặt lại mật khẩu
            </Button>
          )}
      </div>

      <ConfirmModal
        show={isVisible}
        onChangeVisible={() => {
          setIsVisible(!isVisible);
          setSelected(null);
        }}
        onConfirm={() => onDelete()}
        title="Xóa nhân viên"
        content={`Bạn có chắc chắn muốn xóa ${employee?.lastname} ${employee?.firstname}?`}
        loading={mutationDeleteProfile.isLoading}
      />
      <ConfirmModal
        show={isVisibleReset}
        onChangeVisible={() => {
          setIsVisibleReset(!isVisibleReset);
        }}
        onConfirm={() => handleResetPassword()}
        title="Đặt lại mật khẩu"
        content={`Bạn có chắc chắn muốn đặt lại mật khẩu cho ${employee?.lastname} ${employee?.firstname}?`}
        loading={mutationResetPassword.isLoading}
      />
      <ConfirmModal
        show={isVisibleConfirm}
        onChangeVisible={() => {
          setIsVisibleConfirm(!isVisibleConfirm);
        }}
        onConfirm={() => handleSave()}
        title={`${
          params?.id === "add" ? "Thêm nhân viên" : "Cập nhật thông tin"
        }`}
        content={`Bạn có chắc chắn muốn ${
          params?.id === "add" ? "THÊM" : "SỬA THÔNG TIN"
        } nhân viên này?`}
        loading={
          mutationChangeProfile.isLoading || mutationAddProfile.isLoading
        }
      />
    </form>
  );
};
