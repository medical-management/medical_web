import React, { useEffect, useState } from "react";
import { Button, Card, Col, InputGroup, Row } from "react-bootstrap";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Select from "react-select";
import ConfirmModal from "../../../components/ConfirmModal";
import CustomPagination from "../../../components/Pagination";
import { detectGender } from "../../../utils/formatter";
import CustomTable from "../../../views/tables/react-table/CustomTable";
import * as actions from "../actions";
import { APIS_FIND_ALL_USER, APIS_SAVE_EMPLOYEE } from "../apis";
import { useToasts } from "react-toast-notifications";
import CustomDrawer from "../../../components/CustomDrawer";

function UserList() {
  const queryClient = useQueryClient();
  const { addToast } = useToasts();

  const history = useHistory();

  const dispatch = useDispatch();

  const { employees, page, pageSize, totalPage, isNextPage, isLoading } =
    useSelector((state) => state["USERS"]);

  const { user } = useSelector((state) => state["AUTHENTICATION"]);

  const mutationChangeProfile = useMutation("input", APIS_SAVE_EMPLOYEE);
  const mutationDeleteProfile = useMutation("input", APIS_SAVE_EMPLOYEE);

  const [isVisible, setIsVisible] = useState(false);

  const [selected, setSelected] = useState(null);
  const [show, setShow] = useState(false);

  useEffect(() => {
    const { isLoading, data, isError } = mutationChangeProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        addToast("Cập nhật thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        // history.push("/app/admin/users");
      } else if (isError && !!data)
        addToast("Cập nhật thông tin thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
    }
  }, [mutationChangeProfile.isLoading]);

  const [userList, setUserList] = useState([]);

  const [pageSizes, setPageSizes] = useState(10);
  const [pages, setPages] = useState(0);

  const [query, setQuery] = useState({
    take: pageSizes,
    skip: pages,
    inactive: false,
  });

  const queryUser = useQuery("users", () => APIS_FIND_ALL_USER(query));

  useEffect(() => {
    if (queryUser.status === "success")
      dispatch(actions.GET_ALL_EMPLOYEE_INFO_SUCCESS(queryUser?.data));
  }, [queryUser.data]);

  useEffect(() => {
    setUserList(employees);
  }, [employees, history]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setQuery({
      ...query,
      take: pageSizes,
      skip: 0,
      firstname: e?.target[1]?.value || "",
    });
  };

  useEffect(() => {
    setQuery({ ...query, take: pageSizes, skip: 0 });
  }, [pageSizes]);

  useEffect(() => {
    setQuery({ ...query, skip: pages });
  }, [pages]);

  useEffect(() => {
    queryClient.fetchQuery("users", queryUser);
  }, [query]);

  useEffect(() => {
    if (!isVisible) setSelected(null);
  }, [isVisible]);

  useEffect(() => {
    if (selected !== null) {
      setIsVisible(true);
    }
  }, [selected]);

  // useEffect(()=>{
  //   if(!isVisible) setSelected(null)
  // },[isVisible])

  useEffect(() => {
    const { isLoading, data, isError } = mutationDeleteProfile;

    if (!isLoading) {
      if (!isError && !!data) {
        setIsVisible(false);
        addToast("Xóa thông tin thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        queryClient.fetchQuery("users", queryUser);
      }
    }
  }, [mutationDeleteProfile.isLoading]);

  const onDelete = () => {
    mutationDeleteProfile.mutate({
      input: {
        ...selected,
        recordType: "employee",
        inactive: true,
        adjustments: 0,
        employees: 0,
        items: 0,
        medical: 0,
        patients: 0,
        purchaseOrders: 0,
        reports: 0,
        salesOrders: 0,
        schedules: 0,
      },
    });
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "Họ và Tên",
        accessor: (row) => {
          return (
            <p
              style={{ cursor: "pointer" }}
              className="cell-p"
              onClick={() => history.push(`/app/admin/users/${row.id}`)}
            >
              {row?.lastname} {row?.firstname}
            </p>
          );
        },
      },
      {
        Header: "Giới tính",
        accessor: (row) => {
          return (
            <p
              className="cell-p"
              onClick={() => history.push(`/app/admin/users/${row.id}`)}
            >
              {detectGender(row?.gender)}
            </p>
          );
        },
      },
      {
        Header: "Số điện thoại",
        accessor: "phone",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Địa chỉ",
        accessor: "address",
      },
      {
        Header: "Thao tác",
        accessor: (row) => {
          return (
            <div className="flex">
              {user?.decentralization?.length &&
                user?.decentralization[0]?.employees > 2 && (
                  <Button
                    onClick={() => {
                      setSelected(row);
                    }}
                    className="btn btn-danger"
                  >
                    <p className="feather icon-trash-2 cell-p"></p>
                  </Button>
                )}
              {user?.decentralization?.length &&
                user?.decentralization[0]?.employees > 3 && (
                  <Button
                    onClick={() => history.push(`/app/admin/users/${row.id}`)}
                  >
                    <p className="feather icon-edit cell-p"></p>
                  </Button>
                )}
            </div>
          );
        },
      },
    ],
    [user?.decentralization]
  );
  const handleAdvanceSearch = (e) => {
    e.preventDefault();

    setQuery({
      ...query,
      skip: 0,
      firstname: e.target[1].value,
      lastname: e.target[0].value,
      email: e.target[2].value,
      phone: e.target[3].value,
      // dob: moment(e.target[4].value).utc().format(),
      // gender: gender,
    });

    setShow(false);
  };

  const onChangeGender = (e) => {
    setQuery({
      ...query,
      skip: 0,
      gender: e,
    });
  };

  const onChangeDob = (e) => {
    setQuery({
      ...query,
      skip: 0,
      dob: e,
    });
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <Card.Header
              style={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                gap: 20,
              }}
            >
              <Card.Title
                as="h5"
                style={{ whiteSpace: "nowrap", textAlign: "left" }}
              >
                Danh sách nhân viên{" "}
              </Card.Title>
              <form
                style={{ width: "100%" }}
                onSubmit={(e) => {
                  e.preventDefault();
                  setQuery({
                    ...query,
                    take: pageSizes,
                    skip: 0,
                    firstname: e?.target[0]?.value || "",
                  });
                }}
              >
                <InputGroup>
                  <input
                    placeholder={`Tìm nhân viên bằng Tên`}
                    className="form-control"
                    style={{ flex: 1 }}
                  />
                  <InputGroup.Append>
                    <Button
                      type="submit"
                      className="feather icon-search"
                    ></Button>
                  </InputGroup.Append>
                </InputGroup>
              </form>
              {user?.decentralization?.length &&
                user?.decentralization[0]?.employees > 1 && (
                  <Button
                    style={{ margin: 0, whiteSpace: "nowrap" }}
                    onClick={() => history.push("/app/admin/users/add")}
                  >
                    Thêm nhân viên
                  </Button>
                )}
            </Card.Header>
            <Card.Body>
              <form onSubmit={handleSubmit}>
                <Row className="mb-3">
                  <Col className="d-flex align-items-center">
                    Hiển thị
                    <select
                      className="form-control w-auto mx-2"
                      value={pageSizes}
                      onChange={(e) => {
                        setPageSizes(Number(e.target.value));
                      }}
                    >
                      {[5, 10, 20, 50, 100].map((pgsize) => (
                        <option key={pgsize} value={pgsize}>
                          {pgsize}
                        </option>
                      ))}
                    </select>
                    <p
                      className="feather icon-filter margin-0 cursor-pointer"
                      style={{
                        color: !show ? "black" : "#04a9f5",
                        fontWeight: "bold",
                        fontSize: 16,
                      }}
                      onClick={() => setShow(!show)}
                    ></p>
                  </Col>
                </Row>
                <Button className="hidden" type="submit"></Button>
              </form>
              <CustomTable columns={columns} data={userList || []} />
              <CustomPagination
                pageIndex={page}
                totalPage={totalPage}
                gotoPage={(e) => setPages(e)}
                canPreviousPage={page > 0}
                canNextPage={page < totalPage - 1}
              ></CustomPagination>
            </Card.Body>
          </Card>
        </Col>
        <ConfirmModal
          show={isVisible}
          onChangeVisible={() => {
            setIsVisible(!isVisible);
          }}
          onConfirm={() => onDelete()}
          title="Xóa nhân viên"
          content={`Bạn có chắc chắn muốn xóa ${selected?.lastname} ${selected?.firstname}?`}
          loading={mutationDeleteProfile.isLoading}
        />
      </Row>
      {show && (
        <CustomDrawer show={show} handleShow={(e) => setShow(e)}>
          <div>
            <p
              style={{
                width: "100%",
                fontSize: 18,
                fontWeight: 700,
                borderBottom: "1px solid #ddd",
                paddingBottom: 10,
              }}
            >
              Lọc nhân viên
            </p>
            <form
              style={{
                borderBottom: "1px solid #EAEAEA",
                paddingBottom: "10px",
                marginBottom: "10px",
              }}
              onSubmit={handleAdvanceSearch}
            >
              <div
                className="flex"
                style={{ flexDirection: "column", gap: 10 }}
              >
                <div style={{ width: "100%" }} className="self-center">
                  <label className="label-control items-center">Họ</label>
                  <input
                    placeholder={`Nhập Họ`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",
                      width: "100%",
                      height: "38px",
                    }}
                    defaultValue={query?.lastname}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">Tên</label>
                  <input
                    placeholder={`Nhập Tên`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",

                      height: "38px",
                    }}
                    defaultValue={query?.firstname}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">Email</label>
                  <input
                    placeholder={`Nhập Email`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",
                      flex: 12,
                      height: "38px",
                    }}
                    defaultValue={query?.email}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Số điện thoại
                  </label>
                  <input
                    placeholder={`Nhập số điện thoại`}
                    className="form-control"
                    style={{
                      paddingLeft: "10px",
                      flex: 12,
                      height: "38px",
                    }}
                    defaultValue={query?.phone}
                  />
                </div>
                <div
                  className="self-center"
                  style={{
                    width: "100%",
                  }}
                >
                  <label className="label-control items-center">
                    Ngày sinh
                  </label>
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    options={[
                      { label: "Tất cả", value: "" },
                      { label: "Tăng dần", value: "ASC" },
                      { label: "Giảm dần", value: "DESC" },
                    ]}
                    onChange={(e) => onChangeDob(e.value)}
                    value={[
                      { label: "Tất cả", value: "" },
                      { label: "Tăng dần", value: "ASC" },
                      { label: "Giảm dần", value: "DESC" },
                    ].find((item) => item.value === query?.dob)}
                  />
                </div>
                {/* <div className="self-center" style={{paddingRight:'10px', paddingLeft:'5px', width:'120px'}}>
                    <label className="label-control items-center">
                      Giới tính
                    </label>
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      options={[
                        { label: "Nam", value: "male" },
                        { label: "Nữ", value: "female" },
                      ]}
                      onChange={(e)=>onChangeGender(e.value)}
                    />
                  </div> */}
              </div>
              <Button
                type="submit"
                style={{
                  float: "right",
                  margin: 0,
                  marginRight: "-2px",
                  marginTop: "10px",
                }}
              >
                Áp dụng
              </Button>
            </form>
          </div>
        </CustomDrawer>
      )}
    </React.Fragment>
  );
}

export default UserList;
