/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import { gql } from "graphql-request";
import graphQLClient from "../../services/graphql";

export const APIS_FIND_ALL_USER = async (input) => {
    return await graphQLClient({
        queryString: `
    query {
      find_all_user(req: { 
      take: ${input?.take}, 
      skip: ${input?.skip}, 
      ${input?.firstname ? `firstname: "${input?.firstname}"` : ""} 
      ${input?.lastname ? `lastname: "${input?.lastname}"` : ""} 
      ${input?.dob ? `dob: "${input?.dob}"` : ""} 
      ${input?.gender ? `gender: "${input?.gender}"` : ""} 
      ${input?.email ? `email: "${input?.email}"` : ""} 
      ${input?.phone ? `phone: "${input?.phone}"` : ""} 
    }) {
        data {
          id
          firstname
          lastname
          email
          phone
          gender
          address
          city
          province
          nation
          cardID
          note
          username
          password
          status
          dob
          role
        }
        take
        skip
        total
        nextPage
      }
    }
  `,
    });
};
export const APIS_FIND_ONE_USER = async (input) =>
    await graphQLClient({
        queryString: `
  query {
    find_one_employee(id: "${input?.id}") {
        id
        firstname
        lastname
        email
        phone
        gender
        address
        city
        province
        nation
        cardID
        note
        username
        password
        status
        dob
        role
        ${
            !input?.isIntegration
                ? `decentralization {
          employees
          patients
          medical
          schedules
          items
          salesOrders
          purchaseOrders
          adjustments
          reports
        }`
                : ""
        }
        
      }
  }
`,
    });

export const APIS_FIND_ONE_USER_INTEGRATION = async (input) =>
    await graphQLClient({
        queryString: `
  query {
    find_one_employee_integration(id: "${input?.id}") {
        id
        firstname
        lastname
        email
        phone
        gender
        address
        city
        province
        nation
        cardID
        note
        username
        password
        status
        dob
        role
      }
  }
`,
    });

export const APIS_CHANGE_INFO = async ({ input }) =>
    await graphQLClient({
        queryString: `mutation {
      update_user_info(input: { 
          recordType: "${input?.recordType || ""}",
          id:  "${input?.id}",
          firstname: "${input?.firstname || ""}",
          lastname:  "${input?.lastname || ""}",
          dob:  "${input?.dob || ""}",
          email:  "${input?.email || ""}",
          phone:  "${input?.phone || ""}",
          gender:  "${input?.gender || ""}",
          address:  "${input?.address || ""}",
          city:  "${input?.city || ""}",
          province:  "${input?.province || ""}",
          nation:  "${input?.nation || ""}",
          cardID:  "${input?.cardID || ""}",
          note:  "${input?.note || ""}"}) {
          id
      }
  }`,
    });

export const APIS_SAVE_EMPLOYEE = async ({ input }) =>
    await graphQLClient({
        queryString: ` 
        mutation {
      save_employee(input: { 
            recordType: "${input?.recordType || ""}",
            ${input?.id === "" ? "" : `id: "${input?.id}",`}
            ${!input?.status ? "" : `status: "${input?.status}",`}
            firstname: "${input?.firstname || ""}",
            inactive: ${input?.inactive || false},
            lastname:  "${input?.lastname || ""}",
            username: "${input?.username || ""}",
            password: "${input?.password || ""}",
            email:  "${input?.email || ""}",
            phone:  "${input?.phone || ""}",
            gender:  "${input?.gender || ""}",
            dob:  "${input?.dob || ""}",
            address:  "${input?.address || ""}",
            city:  "${input?.city || ""}",
            province:  "${input?.province || ""}",
            nation:  "${input?.nation || ""}",
            cardID:  "${input?.cardID || ""}",
            note:  "${input?.note || ""}",
            employees: ${input?.employees},
            patients: ${input?.patients},
            medical: ${input?.medical},
            schedules: ${input?.schedules},
            items: ${input?.items},
            salesOrders: ${input?.salesOrders},
            purchaseOrders: ${input?.purchaseOrders},
            adjustments: ${input?.adjustments},
            reports: ${input?.reports},
 }) {
            id
            inactive
        }
    }`,
    });

export const APIS_RESET_PASSWORD = async ({ input }) =>
    await graphQLClient({
        queryString: ` 
        mutation {
          reset_password_employee(input: { 
            recordType: "${input?.recordType || ""}",
            id: "${input?.id}",
            username: "${input?.username || ""}",
            password: "${input?.password || ""}",
            email:  "${input?.email || ""}",
            phone:  "${input?.phone || ""}",
            gender:  "${input?.gender || ""}",
          }) {
            id
        }
    }`,
    });

export const APIS_SAVE_QUESTION = async ({ input }) =>
    await graphQLClient({
        queryString: `mutation {
    save_security_questions(input: { 
        question: "${input?.question || ""}",
        answer:  "${input?.answer || ""}"
        }) {
        id
    }
}`,
    });

export const APIS_FIND_ALL_EMPLOYEE = async ({ take, skip, firstname, lastname, email }) => {
    return await graphQLClient({
        queryString: `
  query {
    find_all_user(req: { take: ${take}, skip: ${skip}, 
      ${firstname ? `firstname: "${firstname}",` : ""} 
      ${lastname ? `lastname: "${lastname}",` : ""} 
      ${email ? `email: "${email}"` : ""}  }) {
          data {
              id
              firstname
              lastname
              gender
              address
              cardID
              email
              phone
              role
              note
              dob
              username
              password
          }
          take
          skip
          nextPage
          total
      }
  }
  `,
    });
};

export const APIS_DELETE_EMPLOYEE = async ({ input }) =>
    await graphQLClient({
        queryString: ` 
        mutation {
      save_employee(input: { 
            recordType: "${input?.recordType || ""}",
            ${input?.id === "" ? "" : `id: "${input?.id}",`}
            firstname: "${input?.firstname || ""}",
            inactive: ${input?.inactive || false},
            lastname:  "${input?.lastname || ""}",
            username: "${input?.username || ""}",
            password: "${input?.password || ""}",
            email:  "${input?.email || ""}",
            phone:  "${input?.phone || ""}",
            gender:  "${input?.gender || ""}",
            dob:  "${input?.dob || ""}",
 }) {
            id
            inactive
        }
    }`,
    });
