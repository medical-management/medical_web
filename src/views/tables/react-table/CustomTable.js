import React from "react";
import BTable from "react-bootstrap/Table";
import { useTable, usePagination, useGlobalFilter } from "react-table";

function CustomTable({ columns, data, pageSize }) {
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page, // Instead of using 'rows', we'll use pag
    } = useTable(
        {
            columns,
            data,
            manualPagination: true,
            initialState: {},
        },
        useGlobalFilter,
        usePagination
    );

    // Render the UI for your table
    return (
        <>
            <BTable striped bordered hover responsive {...getTableProps()} className="custom-table">
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => (
                                <th {...column.getHeaderProps()} style={{ textAlign: "center" }}>
                                    <b style={{ color: "#444" }}>{column.render("Header")}</b>
                                    {column.isHeaderRequired && <span style={{ color: "red" }}> (*)</span>}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <tr
                                style={{
                                    backgroundColor: row?.original?.customBackground || "transparent",
                                    color: row?.original?.customColor || "#888",
                                }}
                                {...row.getRowProps()}
                            >
                                {row.cells.map((cell) => {
                                    return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </BTable>
        </>
    );
}

export default CustomTable;
