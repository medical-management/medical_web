/**
 * @author Thanh Tho
 * @company 10minutes Ltc
 * @Link (reacr query): https://react-query.tanstack.com/graphql
 *          Author          Branch          Version            Comment
 *          Thanhtho        master          1.0.0              Initital project
 */
import React, { useEffect } from "react";
import jwtDecode from "jwt-decode";
import { useQuery, useQueryClient } from "react-query";
import { useSelector, useDispatch } from "react-redux";

import Loader from "../components/Loader/Loader";
import { APIS_FIND_ONE_USER_INFOR } from "../screens/Profile/apis";
import { name } from "../screens/Authentication/reducer.js";
import * as actions from "../screens/Authentication/actions.js";
import cookie from "js-cookie";
import { useHistory } from "react-router-dom";

const verifyToken = (serviceToken) => {
    if (!!serviceToken === false) {
        return false;
    }
    const decoded = jwtDecode(serviceToken);
    return decoded.exp > Date.now() / 1000;
};

export const JWTProvider = ({ children }) => {
    const dispatch = useDispatch();
    const queryClient = useQueryClient();
    const history = useHistory();
    // const mutationLogin = useMutation("input", APIS_LOGIN);
    const queryUser = useQuery("find_one_user_infor_auth", APIS_FIND_ONE_USER_INFOR);

    // const { isLoading, data, error } = mutationLogin;
    useEffect(() => {
        const checkToken = async () => {
            const accesstoken = cookie.get("accesstoken");
            const refreshtoken = cookie.get("refreshtoken");
            console.log({ accesstoken, refreshtoken });
            if (!accesstoken && !refreshtoken) {
                dispatch(actions.LOGOUT);
                cookie.remove("accesstoken");
                cookie.remove("refreshtoken");
            }
        };
        checkToken();
    }, []);

    useEffect(() => {
        if (!queryUser.isLoading) {
            if (!queryUser.isError) dispatch(actions.LOGIN_SUCCESS(queryUser.data.find_one_user_info));
            else {
                dispatch(actions.LOGIN_FAILED(queryUser.error));
                history.push("/auth/signin");
            }
        }
    }, [queryUser.isLoading, queryUser.data, queryUser.isError]);

    // if (!state.isInitialised) return <Loader />;
    return <div>{children}</div>;
};

export default JWTProvider;
