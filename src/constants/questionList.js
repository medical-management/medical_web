export const questionList = [
  { value: "placeOfBirth", label: "Nơi sinh của bạn ở đâu?" },
  { value: "juniorSchoolName", label: "Trường tiểu học của bạn tên gì?" },
  { value: "secondarySchoolName", label: "Trường trung học của bạn tên gì?" },
  { value: "universitySchoolName", label: "Trường đại học/cao đẳng/ trung cấp của bạn tên gì?" },
  { value: "childhoodFriendName", label: "Bạn thời thơi ấu của bạn tên gì" },
  { value: "parentsLocation", label: "Cha/mẹ bạn sinh sống ở đâu?" },
  { value: "parentsCareer", label: "Cha/mẹ bạn làm nghề gì?" },
  { value: "dateOfFirstJob", label: "Ngày bắt đầu làm việc của bạn là ngày nào?" },
];
