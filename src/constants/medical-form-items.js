export const patientSection = [
  {
    label: "Họ",
    name: "lastname",
    span: 6,
    type: "text",
  },
  {
    label: "Tên",
    name: "firstname",
    span: 6,
    type: "text",
  },
  {
    label: "CMND/CCCD",
    name: "cardID",
    span: 6,
    type: "text",
  },
  {
    label: "Ngày cấp",
    name: "identityDate",
    span: 6,
    type: "date",
  },
  {
    label: "Nơi cấp",
    name: "identityLocation",
    span: 12,
    type: "text",
  },
  {
    label: "Email",
    name: "email",
    span: 6,
    type: "text",
  },
  {
    label: "Điện thoại",
    name: "phone",
    span: 6,
    type: "text",
  },
  {
    label: "Ngày sinh",
    name: "dob",
    span: 6,
    type: "date",
  },
];

export const vitalSections = [
  {
    label: "Huyết áp tâm thu",
    name: "bloodpressure1",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "mmHg",
  },
  {
    label: "Huyết áp tâm trương",
    name: "bloodpressure2",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "mmHg",
  },

  {
    label: "Nhiệt độ cơ thể",
    name: "temperature",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "°C",
  },
  {
    label: "Mạch",
    name: "heartbeat",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "nhịp",
  },
  {
    label: "Mắt trái",
    name: "leftEye",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "/10",
  },
  {
    label: "Mắt phải",
    name: "rigthtEye",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "/10",
  },
  {
    label: "Chiều cao",
    name: "height",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "cm",
  },
  {
    label: "Cân nặng",
    name: "weight",
    span: 6,
    type: "autonumber",
    min: 0,
    suffixInput: "kg",
  },
];

export const clinicalSections = [
  {
    label: "Hô hấp",
    name: "respire",
    span: 6,
    type: "text",
  },
  {
    label: "Tiêu hóa",
    name: "digestive",
    span: 6,
    type: "text",
  },
  {
    label: "Tuần hoàn",
    name: "circulatorySystem",
    span: 6,
    type: "text",
  },
  {
    label: "Nội tiết",
    name: "endocrineSystem",
    span: 6,
    type: "text",
  },
  {
    label: "Sinh dục, tiết niệu",
    name: "genitourinarySystem",
    span: 6,
    type: "text",
  },
  {
    label: "Cơ xương khớp",
    name: "osteoarthritis",
    span: 6,
    type: "text",
  },
  {
    label: "Thần kinh",
    name: "nerveSystem",
    span: 6,
    type: "text",
  },
  {
    label: "Tâm thần",
    name: "mental",
    span: 6,
    type: "text",
  },
  {
    label: "Ngoại khoa",
    name: "surgery",
    span: 6,
    type: "text",
  },
  {
    label: "Sản phụ khoa",
    name: "obstetric",
    span: 6,
    type: "text",
  },
  {
    label: "Hàm trên",
    name: "upperTeeth",
    span: 6,
    type: "text",
  },
  {
    label: "Hàm dưới",
    name: "lowerTeeth",
    span: 6,
    type: "text",
  },
  {
    label: "Da liễu",
    name: "dermatology",
    span: 6,
    type: "text",
  },
];
export const detailDiases = [
  {
    label: "Có bệnh hay bị thương trong vòng 5 năm qua",
    name: "detail1",
  },
  {
    label: "Có bệnh thần kinh hay bị thương ở đầu",
    name: "detail2",
  },
  {
    label: "Bệnh mắt hoặc giảm thị lực (trừ trường hợp đeo kinh thuốc)",
    name: "detail3",
  },
  {
    label: "Bệnh ở tai, giảm sức nghe hoặc thăng bằng",
    name: "detail4",
  },
  {
    label: "Bệnh ở tim, hoặc nhồi máu cơ tim, các bệnh tim mạch khác",
    name: "detail5",
  },
  {
    label:
      "Phẩu thuật can thiệp tim - mạch (Thay van, bắc cầu nối, tạo hình mạch, máy tạo nhịp, đặt slent mạch, ghép tim)",
    name: "detail6",
  },
  {
    label: "Tăng huyết áp",
    name: "detail7",
  },
  {
    label: "Khó thở",
    name: "detail8",
  },
  {
    label: "Bệnh phổi, hen, khi phết hũng, viêm phế quản mạn tính",
    name: "detail9",
  },
  {
    label: "Bệnh thận, lọc máu",
    name: "detail10",
  },
  {
    label: "Đái tháo đường hoặc kiểm soát tăng đường huyết",
    name: "detail11",
  },
  {
    label: "Bệnh tâm thần",
    name: "detail12",
  },
  {
    label: "Mất ý thức, rối loạn ý thức",
    name: "detail13",
  },
  {
    label: "Ngất, chống mặt",
    name: "detail14",
  },
  {
    label: "Bệnh tiêu hóa",
    name: "detail15",
  },
  {
    label: "Rối loạn giấc ngủ, ngừng thở khi ngủ, ngủ rũ ban ngày, ngáy to",
    name: "detail16",
  },
  {
    label: "Tai biến mạch máu não hoặc liệt",
    name: "detail17",
  },
  {
    label: "Bệnh hoặc tổn thương cột sống",
    name: "detail18",
  },
  {
    label: "Sử dụng rượu thường xuyên, liên tục",
    name: "detail19",
  },
  {
    label: "Sử dụng ma túy và chất gây nghiện",
    name: "detail20",
  },
  // {
  //   label: "Rối loạn giấc ngủ, ngừng thở khi ngủ, ngủ rủ ban ngày, ngáy to",
  //   name: "detail21",
  // },
];

export const insuranceSections = [
  {
    name: "insurance1",
    label: "Đã từng hoặc đang nghiện thuốc lá, rượu, bia không? Mức độ?",
  },
  {
    name: "insurance2",
    label: "Đã bao giờ dùng thuốc gây nghiện chưa (ma túy) chưa?",
  },
  {
    name: "insurance3",
    label:
      "Có dị tật hoặc bệnh bẩm sinh từ nhỏ không? (Bệnh tim bẩm sinh,... )",
  },
  {
    name: "insurance4",
    label:
      "Đã từng nằm viện điều trị chưa? Khi nào? Lý do? Đợt nằm viện dài nhất là bao lâu? Điều trị nội khoa hay ngoại khoa?",
  },
  {
    name: "insurance5",
    label:
      "Có kiểm huyết áp hằng năm không? Chỉ số huyết áp lúc cao nhất là bao nhiêu?",
  },
  {
    name: "insurance6",
    label: "Có mắc bệnh nghề nghiệp không? (Bụi phổi, nhiễm độc hóa chất,...)",
  },
  {
    name: "insurance7",
    label:
      "Đã từng xét nghiệm về bệnh da liễu chưa? (Bệnh lậu, giang mai, AIDS,...)",
  },
  {
    name: "insurance8",
    label: "Có mắc bệnh truyền nhiễm không? (Viêm, gan, lau phổi,...)",
  },
  {
    name: "insurance9",
    label: "Có thấy nổi hạch bất thường, u bứu ở đâu không?",
  },
  {
    name: "insurance10",
    label: "Đã bao giờ bị tai nạn? Hậu quả? Di chứng? Giám định thương tật?",
  },
  {
    name: "insurance11",
    label: "Đã từng đau tức ngực, đau thắt ngực không? Khi nào? Mức độ?",
  },
  {
    name: "insurance12",
    label: "Đã từng bị ngất hoặc khó thở không? Khi nào? Mức độ?",
  },
  {
    name: "insurance13",
    label:
      "Đã từng bị xuất huyết bất thường không? (Xuất huyết dưới da, xuất huyết tiêu hóa, chảy máu cam, ...)",
  },
  {
    name: "insurance14",
    label: "Đã bao giờ bị truyền máu, bán hoặc cho máu không?",
  },
  { name: "insurance15", label: "Có mắc bệnh di truyền nào không?" },
  {
    name: "insurance16",
    label:
      "Có ai trong gia đình chết hay mắc bệnh: Lao, viêm gan, ung thư, tâm thần, các bệnh di truyền hay liên quan đến AIDS,... không?",
  },
];
