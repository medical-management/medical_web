export const categoryOptionsFilter = [
    {
        label: "Tất cả",
        value: "",
    },
    {
        label: "Viên nén",
        value: "capsule",
    },
    {
        label: "Bột",
        value: "flour",
    },
    {
        label: "Nước",
        value: "liquid",
    },
    {
        label: "Thực phẩm chức năng",
        value: "functional",
    },
    {
        label: "Mỹ phẩm",
        value: "cosmetic",
    },
    {
        label: "Dụng cụ y tế",
        value: "equipment",
    },
    {
        label: "Đông y",
        value: "east",
    },
];
export const categoryOptions = [
    {
        label: "Viên nén",
        value: "capsule",
    },
    {
        label: "Bột",
        value: "flour",
    },
    {
        label: "Nước",
        value: "liquid",
    },
    {
        label: "Thực phẩm chức năng",
        value: "functional",
    },
    {
        label: "Mỹ phẩm",
        value: "cosmetic",
    },
    {
        label: "Dụng cụ y tế",
        value: "equipment",
    },
    {
        label: "Đông y",
        value: "east",
    },
];
export const statusSchedule = [
    {
        label: "Mới",
        value: "new",
    },
    {
        label: "Đã khám",
        value: "complete",
    },
    {
        label: "Hủy khám",
        value: "cancel",
    },
];

export const typeSchedule = [
    {
        label: "Khám bệnh không có đặt lịch",
        value: "not",
    },
    {
        label: "Khám bệnh có đặt lịch",
        value: "yes",
    },
];

export const categorySchedule = [
    {
        label: "Phiếu khám bệnh thông thường",
        value: "normal",
    },
    {
        label: "Đăng ký giấy khám sức khỏe đi làm",
        value: "work",
    },
    {
        label: "Đăng ký giấy khám sức khỏe thi bằng lái xe",
        value: "license",
    },
    {
        label: "Đăng ký giấy khám sức khỏe bảo hiểm",
        value: "warranty",
    },
    {
        label: "Cấp cứu",
        value: "emergency",
    },
    {
        label: "Khác",
        value: "other",
    },
];

export const shiftSchedule = [
    {
        label: "Ca sáng",
        value: "morning",
    },
    {
        label: "Ca chiều",
        value: "afternoon",
    },
];

export const dateSorting = [
    {
        label: "Tất cả",
        value: "",
    },
    {
        label: "Tăng dần",
        value: "ASC",
    },
    {
        label: "Giảm dần",
        value: "DESC",
    },
];
export const paymentOptions = [
    {
        label: "Tiền mặt",
        value: "cash",
    },
    {
        label: "Ngân hàng",
        value: "bank",
    },
    {
        label: "Momo",
        value: "momo",
    },
    {
        label: "Khác",
        value: "other",
    },
];

export const statusOrderOptions = [
    {
        label: "Đang chờ duyệt",
        value: "pending",
    },
    {
        label: "Đã thanh toán",
        value: "paid",
    },
    {
        label: "Đã duyệt",
        value: "approved",
    },
    // {
    //   label: "Nháp",
    //   value: "draft",
    // },
];

export const statusOrderOptionsSelect = [
    {
        label: "Đang chờ duyệt",
        value: "pending",
    },
    {
        label: "Đã duyệt",
        value: "approved",
    },
    {
        label: "Đã thanh toán",
        value: "paid",
    },
];
