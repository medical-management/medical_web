const SessionTitle = ({ children, mt, collapse, active, onClick }) => {
  return (
    <div
      onClick={() => onClick && onClick()}
      style={{
        borderBottom: "1px solid #ced4da",
        marginBottom: "15px",
        marginTop: `${mt || 20}px`,
        display: "flex",
        justifyContent: collapse && "space-between",
        cursor: collapse && "pointer",
      }}
    >
      <p
        style={{
          fontSize: "18px",
          fontWeight: "500",
          marginBottom: "5px",
          color: "#777",
        }}
      >
        {children}
      </p>
      {!active && collapse && (
        <span
          className="feather icon-chevron-right"
          style={{ fontWeight: "bold", fontSize: "18" }}
        ></span>
      )}
      {active && collapse && (
        <span
          className="feather icon-chevron-down"
          style={{ fontWeight: "bold", fontSize: "18" }}
        ></span>
      )}
    </div>
  );
};

export default SessionTitle;
