import { Row, Col, Pagination } from "react-bootstrap";

const CustomPagination = ({
  canPreviousPage,
  canNextPage,
  gotoPage,
  pageIndex = 0,
  totalPage,
}) => (
  <Row className="justify-content-between mt-3">
    <Col sm={12} md={6}>
      <span className="d-flex align-items-center">
        Trang
        <strong style={{ marginLeft: "5px" }}>
          {pageIndex + 1} / {totalPage}
        </strong>
        | Đi tới trang:
        <input
          type="number"
          className="form-control ml-2"
          defaultValue={(pageIndex + 1).toString()}
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          style={{ width: "100px" }}
        />
      </span>
    </Col>
    <Col sm={12} md={6}>
      <Pagination className="justify-content-end">
        <Pagination.First
          onClick={() => gotoPage(0)}
          disabled={!canPreviousPage}
        />
        <Pagination.Prev
          onClick={() => gotoPage(pageIndex-1)}
          disabled={!canPreviousPage}
        />
        <Pagination.Next onClick={() => gotoPage(pageIndex+1)} disabled={!canNextPage} />
        <Pagination.Last
          onClick={() => gotoPage(totalPage - 1)}
          disabled={!canNextPage}
        />
      </Pagination>
    </Col>
  </Row>
);

export default CustomPagination;
