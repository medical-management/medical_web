import React from "react";
import "./style.css";

const SpinnerBackDrop = () => {
    return (
        <React.Fragment>
            <div id="backdrop">
                <div class="text-center loading">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};
export default SpinnerBackDrop;
