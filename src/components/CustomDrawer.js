import React, { useRef } from "react";
import useOnClickOutside from "../hooks/useOnClickOutside";

const CustomDrawer = ({ children, handleShow, show }) => {
  const ref = useRef();
  const handleClickOutside = () => {
    // Your custom logic here
    handleShow(!show);
  };

  useOnClickOutside(ref, handleClickOutside);

  return (
    <React.Fragment>
      <div
        style={{
          width: "100vw",
          height: "100vh",
          backgroundColor: "rgba(100,100,100,0.4)",
          zIndex: 10,
          position: "fixed",
          right: 0,
          top: 0,
        }}
      >
        <div
          ref={ref}
          style={{
            width: 300,
            height: "100vh",
            overflowY: "auto",
            position: "fixed",
            right: 0,
            zIndex: 10,
            top: 0,
            backgroundColor: "white",
            padding: 20,
          }}
          className=""
        >
          {children}
        </div>
      </div>
    </React.Fragment>
  );
};

export default CustomDrawer;
