import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";

const AllCkEditor = (props) => {
  return <CKEditor isReadOnly {...props} />;
};

export default AllCkEditor;
