import React from "react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import AllCkEditor from "./AllCkEditor";

const CkClassicEditor = (props) => {
    return (
        <React.Fragment>
            <AllCkEditor
                {...props}
                editor={ClassicEditor}
                data={props.html}
                onReady={(editor) => {
                    if (props.editor === "document") {
                        const toolbarContainer = document.querySelector(".document-editor__toolbar");
                        toolbarContainer.appendChild(editor.ui.view.toolbar.element);
                        window.editor = editor;
                    }
                    // console.log( 'Editor is ready to use!', editor );
                }}
                onChange={(event, editor) => props.onChange(editor.getData())}
                onBlur={(event, editor) => {
                    if (props.onBlur) props.onBlur(editor.getData());
                }}
                onFocus={(editor) => {
                    // console.log( 'Focus.', editor );
                }}
                disabled={props.disabled}
            />
        </React.Fragment>
    );
};

export default CkClassicEditor;
