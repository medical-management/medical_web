import { Modal, Button, Spinner } from "react-bootstrap";

const ConfirmModal = ({
  show,
  onChangeVisible,
  onConfirm,
  title,
  content,
  loading,
}) => (
  <Modal centered show={show} onHide={onChangeVisible}>
    <Modal.Header closeButton>
      <Modal.Title as="h5">{title || "Xác nhận thao tác"}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <p>{content || "Bạn có chắc chắn muốn thực hiện thao tác này?"}</p>
    </Modal.Body>
    <Modal.Footer>
      <Button
        variant="secondary"
        onClick={onChangeVisible}
        disabled={loading}
        className="flex"
        style={{ placeItems: "center" }}
      >
        {loading && (
          <Spinner
            as="span"
            className='mr-2'
            animation="border"
            size="sm"
            role="status"
            aria-hidden="true"
          />
        )}
        Hủy
      </Button>
      <Button
        variant="primary"
        onClick={() => onConfirm()}
        className="flex"
        disabled={loading}
        style={{ placeItems: "center" }}
      >
        {loading && (
          <Spinner
            as="span"
            animation="border"
            className='mr-2'
            size="sm"
            role="status"
            aria-hidden="true"
          />
        )}
        Xác nhận
      </Button>
    </Modal.Footer>
  </Modal>
);

export default ConfirmModal;
