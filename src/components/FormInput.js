import NumberFormat from "react-number-format";

export const renderField = ({
  input,
  label,
  type,
  prefix,
  suffix,
  required,
  show,
  defaultValue,
  className,
  decimalSeparator = ",",
  thousandSeparator = ".",
  meta: { asyncValidating, touched, error },
  disabled = false,
}) => (
  <div className={`${type === "radio" || show === "hidden" ? "" : "my-2"}`}>
    <label className="label-control" style={{ whiteSpace: "nowrap" }}>
      {label} {required && <span style={{ color: "red" }}>*</span>}
    </label>
    {show !== "hidden" && (
      <div style={{ display: "flex", alignItems: "center" }}>
        {prefix && (
          <p
            className="label-control"
            style={{
              marginRight: "6px",
              marginBottom: "4px",
              flex: 1,
              whiteSpace: "nowrap",
            }}
          >
            {prefix}
          </p>
        )}
        {type !== "autonumber" && (
          <input
            {...input}
            placeholder={`Nhập ${label ? label.toLowerCase() : ""}`}
            type={type}
            className={`form-control ${className}`}
            disabled={disabled}
            style={{ paddingLeft: "10px", flex: 12 }}
            defaultValue={defaultValue}
          />
        )}
        {type === "autonumber" && (
          <NumberFormat
            {...input}
            style={{ paddingLeft: "10px", flex: 12 }}
            placeholder={`Nhập ${label ? label.toLowerCase() : ""}`}
            className="form-control"
            decimalSeparator={decimalSeparator}
            thousandSeparator={thousandSeparator}
          />
        )}
        {suffix && (
          <p
            className="label-control"
            style={{ marginLeft: "6px", marginBottom: "4px", flex: 2 }}
          >
            {suffix}
          </p>
        )}
      </div>
    )}
    {touched && error && <span className="text-c-red">* {error} </span>}
  </div>
);
