import { Modal, Button } from "react-bootstrap";

const CustomModal = ({
  show,
  onChangeVisible,
  title,
  footer,
  children,
  height,
}) => (
  <Modal
    centered
    show={show}
    onHide={onChangeVisible}
    size="xl"
    style={{ height: `${height} !important` }}
  >
    <Modal.Header closeButton>
      <Modal.Title as="h5">{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>{children}</Modal.Body>
    {footer && (
      <Modal.Footer>
        {footer}
        <Button
          variant="secondary"
          onClick={onChangeVisible}
          className="flex"
          style={{ placeItems: "center" }}
        >
          Đóng
        </Button>
      </Modal.Footer>
    )}
  </Modal>
);

export default CustomModal;
