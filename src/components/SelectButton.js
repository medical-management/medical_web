import { SplitButton, Dropdown, Spinner } from "react-bootstrap";

const SelectButton = ({ selected, onChange, onClick, loading }) => (
  <SplitButton
    title={selected}
    variant="primary"
    id={`dropdown-split-variants-`}
    // key={}
    onChange={(e) => onChange(e)}
    onClick={(e) => onClick(e)}
    className="mr-2 mb-2 text-capitalize"
    disabled={loading}
  >
    {loading && (
      <Spinner
        as="span"
        animation="border"
        className="mr-2"
        size="sm"
        role="status"
        aria-hidden="true"
      />
    )}
    <Dropdown.Item eventKey="1">Duyệt</Dropdown.Item>
    <Dropdown.Item eventKey="2">Xóa</Dropdown.Item>
  </SplitButton>
);

export default SelectButton;
