const Avatar = ({ src, w = 50, h = 50, color = "#acf4fc" }) => (
  <div
    style={{
      backgroundColor: color,
      width: w,
      height: h,
      borderRadius: "9999px",
      justifyContent: "center",
      display: "flex",
      alignItems: "center",
      fontSize: "25px",
      color: "white",
      fontWeight: "bolder",
    }}
  >
    {src?.toString()?.charAt(0)}
  </div>
);

export default Avatar;
