import React from "react";
import NumberFormat from "react-number-format";

export const FormInput = React.forwardRef((props, ref = null) => (
  <div
    className={`${
      props?.type === "radio" || props?.show === "hidden" ? "" : "my-2"
    }`}
    style={{ width: props?.full && "100%" }}
  >
    {props?.label && (
      <label className="label-control" style={{ whiteSpace: "nowrap" }}>
        {props?.label}
        {props?.required && (
          <span style={{ color: "red", marginLeft: "6px" }}>*</span>
        )}
      </label>
    )}
    {props?.show !== "hidden" && (
      <div style={{ display: "flex", alignItems: "center" }}>
        {props?.prefixInput && (
          <p
            className="label-control"
            style={{
              marginRight: "6px",
              marginBottom: "4px",
              flex: 1,
              whiteSpace: "nowrap",
            }}
          >
            {props?.prefixInput}
          </p>
        )}
        {props?.type !== "autonumber" && (
          <input
            ref={ref}
            {...props}
            placeholder={`Nhập ${
              props?.label ? props?.label.toLowerCase() : ""
            }`}
            min={props?.minVal}
            max={props?.maxVal}
            type={props?.type}
            defaultChecked={props?.defaultChecked}
            checked={props?.checked}
            className={`form-control ${props?.className}`}
            disabled={props?.disabled}
            style={{ paddingLeft: "10px", flex: 12 }}
            defaultValue={props?.defaultValue}
            onChange={(e) => props?.onChange && props?.onChange(e)}
            required={false}
            onBlur={props?.onBlurInput}
            // onClick={() => props?.onClick()}
          />
        )}
        {props?.type === "autonumber" && (
          <NumberFormat
            {...props}
            style={{ paddingLeft: "10px", flex: 12 }}
            placeholder={
              props?.placeholderInput
                ? props?.placeholderInput
                : `Nhập ${props?.label ? props?.label.toLowerCase() : ""}`
            }
            className="form-control"
            decimalSeparator={props?.decimalSeparator}
            thousandSeparator={props?.thousandSeparator}
          />
        )}
        {props?.suffixInput && (
          <p
            className="label-control"
            style={{ marginLeft: "6px", marginBottom: "4px", flex: 2 }}
          >
            {props?.suffixInput}
          </p>
        )}
      </div>
    )}
    {props?.onBlur && props?.error && (
      <span className="text-c-red">* {props?.error} </span>
    )}
  </div>
));
